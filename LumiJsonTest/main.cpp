#include <pybind11/pybind11.h>
#include <string>

#include "../src/Lumimask.h"

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

bool passed(std::string lumimaskfile, int run, int lumi){
  Lumimask* lumimask = new Lumimask(lumimaskfile);
  bool pass = lumimask->filter(run,lumi);
  return pass;
}

namespace py = pybind11;

PYBIND11_MODULE(LumiJsonTest, m) {
    m.doc() = R"pbdoc(
        Pybind11 binding the Lumimask c++
    )pbdoc";

    m.def("passed", &passed, R"pbdoc(
        Check run,lumi passing lumijson.
    )pbdoc");

#ifdef VERSION_INFO
    m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
    m.attr("__version__") = "dev";
#endif
}
