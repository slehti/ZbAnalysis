Instructions for running this analysis

ROOT is found at
dx7-hip-02 > echo $ROOTSYS/
/data/root-6.18.00/

git clone ssh://git@gitlab.cern.ch:7999/slehti/ZbAnalysis.git
cd ZbAnalysis
make


You need to have multicrab dirs as input data. These are the latest multicrabs
on dx7-hip-02.hip.helsinki.fi:
/data/multicrab_ZbNanoAODAnalysis_v10214_Run2016BCDEFGH_20191125T1505/
/data/multicrab_ZbNanoAODAnalysis_v10214_Run2017BCDEF_20191125T1449/
/data/multicrab_ZbNanoAODAnalysis_v10214_Run2018ABCD_20191125T1421/

Running lasts about 37h+

Howto run:
analyse.py <multicrab dir>

nohup ./analyse.py /jme/multicrab_ZbAnalysis_v1255p1_Run2023BCD_20230816T0849/ >& run2023.txt &




Run 3 jobs for muons, 3 for electrons (e.g. in separate terminals)
analyse.py /data/multicrab_ZbNanoAODAnalysis_v10214_Run2016BCDEFGH_20191125T1505/
analyse.py /data/multicrab_ZbNanoAODAnalysis_v10214_Run2017BCDEF_20191125T1449/
analyse.py /data/multicrab_ZbNanoAODAnalysis_v10214_Run2018ABCD_20191125T1421/
EDIT analyse.py to CHANGE LINE
L21/L22 LEPTONFLAVOR =
by hand from 11 to 13 or vice versa, after which send the analysis running again
analyse.py /data/multicrab_ZbNanoAODAnalysis_v10214_Run2016BCDEFGH_20191125T1505/
analyse.py /data/multicrab_ZbNanoAODAnalysis_v10214_Run2017BCDEF_20191125T1449/
analyse.py /data/multicrab_ZbNanoAODAnalysis_v10214_Run2018ABCD_20191125T1421/

This will produce 6 pseudomulticrab dirs in the local directory.

Plotting
Howto run:
plot.py <pseudomulticrab dirs>
How to plot all data
plot.py <pseudo-2016mu> <pseudo-2017mu> <pseudo-2018mu> <pseudo-2016ele> <pseudo-2017ele> <pseudo-2018ele>
Running lasts about 2h10min

How to plot muons only
plot.py	<pseudo-2016mu>	<pseudo-2017mu>	<pseudo-2018mu>

Howto plot 2018 electrons only
plot.py <pseudo-2018ele>

Example for plotting 2018 muons
plot.py multicrab_ZbNanoAODAnalysis_v10214_Run2018ABCD_20191125T1421_processed20200114T1931_Muon

The script plot.py will produce a directory figures/ containing plots in different formats.
How to produce the ROOT file for Mikko:
cd figures/
../merge_jme.py jet_*.root
The output is jme_bplusZ_merged_vX.root, which you can rename according to version and used data. Naming examples
jme_bplusZ_Run2_v25.root (contains 2016,17,18 e and mu)
jme_bplusZ_2018ABCD_Muons_v22.root (contains only 2018 mu)


If you want to produce the root file without ttbar background, in file plot.py uncomment line L1812
#    blacklist = ["TTJets"]                                                                                                                                                                    



Howto debug:
To make the analysis run faster, change the number of events in analyse.py line
L24/L25
maxEvents = -1
#maxEvents = 10000

and limit the number of datasets read from line L455 by removing the comment
#    whitelist = ["DY1JetsToLL_M_50","DoubleMuon_Run201\dC"]                                                                                                                                This will run only DY1Jets and DoubleMuon201XC, 201X being any year 2016/(2017/2018


Where to write new histograms into the root files: file plot.py between lines L1598 - L1723
Example histogram name: h_Zpt_RMPF_alpha30_eta_00_13


Hi Sami,

The followings shows how to get pileup_2018.txt:

- Extract corresponding pileup json file from [1] : pileup_latest.txt
- Convert json pileup format to ASCII using JSONtoASCII.py [2] : pileup_2018.txt
- Get mu using functions in parsePileUpJSON2.h [3] with corresponding pileup json
Cheers,
Minsuk

[1] https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions18/13TeV/PileUp/
[cms-service-dqm.web.cern.ch]
[2] https://github.com/tankit/OffsetTreeMaker/blob/master/JSONtoASCII.py [github.com]
[3] https://github.com/cihar29/OffsetTreeMaker/blob/master/plugins/parsePileUpJSON2.h [github.com]

 
