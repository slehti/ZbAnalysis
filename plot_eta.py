#!/usr/bin/env python3
#
# Plot low pt eta to see potential spikes
# 15.8.2024/S.Lehti
#

import sys
import os
import re
import subprocess
import array
from optparse import OptionParser

import ROOT

ROOT.gROOT.SetBatch(True)
ROOT.gErrorIgnoreLevel = ROOT.kWarning

#endings = [".C",".png",".pdf"]
endings = [".png"]


def usage():
    print()
    print("### Usage:  ",os.path.basename(sys.argv[0]),"<multicrab histograms>")
    print()

from plot_v3 import Dataset,getDatasetsMany,read,mergeExtDatasets,normalizeToLumi,mergeDatasets,reorderDatasets,setStyles

def main(opts, args):

    if len(args) == 0:
        usage()
        sys.exit()

    multicrabdirs = args
    for multicrabdir in multicrabdirs:
        if not os.path.exists(multicrabdir) or not os.path.isdir(multicrabdir):
            usage()
            sys.exit()
        else:
            print(multicrabdir)

    whitelist = opts.includeTasks
    blacklist = opts.excludeTasks

    print("Getting datasets")
    datasets = getDatasetsMany(multicrabdirs,whitelist=whitelist,blacklist=blacklist)
    print("Datasets received")

#    for d in datasets:
#        print d.name
#    print "-------------"
#    datasets = removeDatasets("SingleMuon_Run2017C_31Mar2018_v1_299368_302029",datasets)


    datasets = read(datasets)
    print("datasets read",len(datasets))
    datasets = mergeExtDatasets(datasets)
    print("Merged ext datasets",len(datasets))
    datasets = normalizeToLumi(datasets)
    print("datasets normalized to lumi",len(datasets))
    #writeCounters(datasets)
    #print("wrote counters")
    datasets = mergeDatasets("Data","_Run20\d\d\S_",datasets)
    print("Merged data datasets")
    datasets = mergeDatasets("MC","^(?!Data).*",datasets)
    print("Merged MC datasets")


    datasets = reorderDatasets(datasets)
    print("datasets reordered")
    
    for d in datasets:
        print(d.name,d.isData())
        

    ROOT.gROOT.LoadMacro("tdrstyle_mod14.C+")
    ROOT.setTDRStyle()

    ROOT.extraText = "Simulation";
    
    setStyles(datasets)

    histogramNames = ["analysis/h_jetseta_pt20","analysis/h_jetseta_pt30","analysis/h_jetseta_pt40"]
    histogramColors = [ROOT.kBlack,ROOT.kBlue,ROOT.kOrange]
    print("HistogramNames",len(histogramNames))

    print("Entering plotting")
    canvas = ROOT.TCanvas("canvas","",500,500)
    canvas.cd()
    frame = d.histo[histogramNames[0]].Clone("frame")
    frame.GetXaxis().SetTitle("#eta(PUPPI jet)")
    frame.GetYaxis().SetTitle("Number of entries")
    frame.Draw()
    for i,hname in enumerate(histogramNames):
        print("Plotting histo",hname)
        d.histo[hname].SetLineColor(histogramColors[i])
        d.histo[hname].Draw("same")

    output = "jeteta"
    for ending in endings:
        canvas.Print(output+ending)
        print("Output:",output+ending)
    print("Workdir:",os.getcwd())

if __name__ == "__main__":
    parser = OptionParser(usage="Usage: %prog [options]")
    parser.add_option("-i", "--includeTasks", dest="includeTasks", default="None", type="string",
                      help="Only perform action for this dataset(s) [default: \"\"]")

    parser.add_option("-e", "--excludeTasks", dest="excludeTasks", default="None", type="string",
                      help="Exclude this dataset(s) from action [default: \"\"]")

    (opts, args) = parser.parse_args()

    opts.includeTasks = opts.includeTasks.split(',')
    opts.excludeTasks = opts.excludeTasks.split(',')

    if 'None' in opts.includeTasks:
        opts.includeTasks = []
    if 'None' in opts.excludeTasks:
        opts.excludeTasks = []

    print(opts.includeTasks,opts.excludeTasks)

    main(opts, args)
