#!/usr/bin/env python
#
# Plot histograms
# 27.11.2018/S.Lehti
#

import sys
import os
import re
import subprocess
import array

import ROOT

ROOT.gROOT.SetBatch(True)
ROOT.gErrorIgnoreLevel = ROOT.kWarning

#endings = [".C",".png",".pdf"]
endings = [".png"]

def usage():
    print
    print("### Usage:  ",os.path.basename(sys.argv[0]),"<multicrab histograms>")
    print

class Style:
    def __init__(self,markerStyle=1,markerSize=1,color=1,label="",drawOptions="SAME"):
        self.markerStyle = markerStyle
        self.markerSize  = markerSize
        self.color       = color
        self.label       = label
        self.drawOptions = drawOptions
        #print "check Style:",self.markerStyle,self.markerSize,self.color,self.drawOptions
    def getMarkerStyle(self):
        return self.markerStyle
    def getMarkerSize(self):
        return self.markerSize
    def getColor(self):
        return self.color
    def getLabel(self):
        return self.label
    def getDrawOptions(self):
        return self.drawOptions

outputdir = "figures"
    
histogramStyles = {}
histogramStyles["Data"]            = Style(21,1,1,"Data","EPSAME")
histogramStyles["MC"] = Style(20,1,2,"Simulation","HISTSAME")

####histogramStyles["DYJetsToLL_M_50"] = Style(20,1,1,"DY+jets (ht bins)","HISTSAME")
histogramStyles["DYJetsToLL_M_50"] = Style(20,1,1,"DY+jets(Pythia)","HISTSAME")
histogramStyles["DYJetsToLL"] = Style(20,1,2,"DY+jets(Pythia)","HISTSAME")
histogramStyles["DYNJetsToLL_M_50"] = Style(22,1,1,"DY+jets","HISTSAME")
histogramStyles["DYJetsToLL_M_50_Herwig"] = Style(22,1,1,"DY+jets(Herwig)","HISTSAME")
histogramStyles["DYBJetsToLL_M_50"] = Style(22,1,1,"DY+bjets","HISTSAME")
histogramStyles["TTJets"] = Style(1,1,2,"t\bar{t}","HISTSAME")
histogramStyles["SingleTop"] = Style(1,1,3,"top","HISTSAME")
histogramStyles["WJetsToLNu"] = Style(1,1,4,"W+j","HISTSAME")


labels = {}
labels["R_pT"] = "p_{T} balance"
labels["R_MPF"] = "MPF"
labels["alpha"] = "p_{T}^{Jet2}/p_{T}^{Z}"

xsecs = {}
xsecs["TTJets"] = 6.639e+02 # https://twiki.cern.ch/twiki/bin/view/CMS/HowToGenXSecAnalyzer#Running_the_GenXSecAnalyzer_on_a
xsecs["ST_tW_top_5f_inclusiveDecays"]            = 35.85 # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["ST_tW_antitop_5f_inclusiveDecays"]        = 35.85 # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["ST_t_channel_top_4f_inclusiveDecays"]     = 136.02# https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["ST_t_channel_antitop_4f_inclusiveDecays"] = 80.95 # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["ST_s_channel_4f_InclusiveDecays"]         = 11.36 # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["DYJetsToLL_M_50_HT_70to100"]    = 209.592
xsecs["DYJetsToLL_M_50_HT_100to200"]   = 181.302 # McM times NNLO/LO ratio of inclusive sample
xsecs["DYJetsToLL_M_50_HT_200to400"]   = 50.4177 # McM times NNLO/LO ratio of inclusive sample
xsecs["DYJetsToLL_M_50_HT_400to600"]   = 6.98314
xsecs["DYJetsToLL_M_50_HT_600to800"]   = 1.6841
xsecs["DYJetsToLL_M_50_HT_800to1200"]  = 0.775392
xsecs["DYJetsToLL_M_50_HT_1200to2500"] = 0.18622
xsecs["DYJetsToLL_M_50_HT_2500toInf"]  = 0.004384
xsecs["DY1JetsToLL_M_10to50"]    = 730.3 # DAS XSDB
xsecs["DY2JetsToLL_M_10to50"]    = 387.4 # DAS XSDB
xsecs["DY3JetsToLL_M_10to50"]    = 95.02 # DAS XSDB
xsecs["DY4JetsToLL_M_10to50"]    = 36.71 # DAS XSDB
xsecs["DY1JetsToLL_M_50"]    = 877.8 # DAS XSDB
xsecs["DY2JetsToLL_M_50"]    = 304.4 # DAS XSDB
xsecs["DY3JetsToLL_M_50"]    = 111.5 # DAS XSDB
xsecs["DY4JetsToLL_M_50"]    = 44.03 # DAS XSDB
xsecs["DYJetsToLL_M_50_herwig"] = 1424.0 # DAS XSDB
xsecs["DYBJetsToLL_M_50_ZPT_100to200"] = 3.088 # DAS XSDB
xsecs["DYBJetsToLL_M_50_ZPT_200toInf"] = 0.3159 # DAS XSDB
xsecs["WJetsToLNu_HT_70To100"]    = 1.353e+03*1.2138
xsecs["WJetsToLNu_HT_100To200"]   = 1.293e+03*1.2138
xsecs["WJetsToLNu_HT_200To400"]   = 3.86e+02*1.2138
xsecs["WJetsToLNu_HT_400To600"]   = 47.9*1.2138
xsecs["WJetsToLNu_HT_600To800"]   = 12.8*1.2138
xsecs["WJetsToLNu_HT_800To1200"]  = 5.26*1.2138
xsecs["WJetsToLNu_HT_1200To2500"] = 1.33*1.2138
xsecs["WJetsToLNu_HT_2500ToInf"]  = 3.089e-02*1.2138

root_re = re.compile("(?P<rootfile>([^/]*histograms.root))")

class Dataset:
    def __init__(self,name,multicrabdir=""):
        self.name = os.path.basename(name)
        self.multicrabdir = multicrabdir
        self.histofiles = []
        self.histo = {}
        self.lumi = 0
        if self.name in histogramStyles.keys():
            self.style = histogramStyles[name]
        else:
            self.style = Style()
        
    def readSkim(self):
        eventFiles = execute("ls %s"%(os.path.join(self.multicrabdir,self.name,"results","events*.root")))

        self.isdata = False
        data_re = re.compile("_Run201")
        match = data_re.search(self.name)
        if match:
            self.isdata = True

        self.skimCounters = None
        for ef in eventFiles:
            fIN = ROOT.TFile.Open(ef)
            if self.skimCounters == None:
                self.skimCounters = fIN.Get(os.path.join("configInfo","skimCounter")).Clone("skimCounter")
                self.skimCounters.SetDirectory(0)
            else:
                self.skimCounters.Add(fIN.Get(os.path.join("configInfo","skimCounter")).Clone("skimCounter"))
            fIN.Close()
        self.counters = self.skimCounters
        self.uwcounters = self.skimCounters
        return True

    def read(self):
        #print "Reading",self.name
        # read cfgInfo
        histofile = os.path.join(self.multicrabdir,self.name,"results","histograms.root")
        if not os.path.exists(histofile):
            return False
        self.histofiles.append(histofile)
        fIN = ROOT.TFile.Open(self.histofiles[0])
        self.counters = fIN.Get(os.path.join("configInfo","weighted counters")).Clone("weighted counters")
        self.counters.SetDirectory(0)
        self.uwcounters = fIN.Get(os.path.join("configInfo","unweighted counters")).Clone("unweighted counters")
        self.uwcounters.SetDirectory(0)
        #print "check counters size",self.name,self.counters.GetEntries(),self.uwcounters.GetEntries()
        h_isdata   = fIN.Get(os.path.join("configInfo","isdata")).Clone("isdata")
        self.isdata = h_isdata.GetBinContent(1)
        #print "check read",self.name,self.isdata
        if self.isdata:
            h_lumi = fIN.Get(os.path.join("configInfo","lumi")).Clone("lumi")
            self.lumi = h_lumi.GetBinContent(1)
        else:
            self.jetlabels = fIN.Get(os.path.join("configInfo","jet labels, unweighted")).Clone("jet labels")
            self.jetlabels.SetDirectory(0)

        rpaths = []
        rpaths.append("analysis")
#        if not self.isdata:
#            rpaths.append("analysis/PSWeight0")
        hdir = fIN.cd(rpaths[0])
        gDir = ROOT.gFile.GetDirectory(rpaths[0])
        keys = gDir.GetListOfKeys()
        nkeys = gDir.GetNkeys()
        for i in range(nkeys):
            keyname = keys.At(i).GetName()
            #print "check glu",keyname
            obj = fIN.Get(os.path.join(rpaths[0],keyname)).Clone(keyname)
            if obj.IsFolder():
                rpaths.append(os.path.join(rpaths[0],keyname))

        # read histograms
        histonames = []
        for rpath in rpaths:
            #print rpath
            hdir = fIN.cd(rpath)
            gDir = ROOT.gFile.GetDirectory(rpath)
            keys = gDir.GetListOfKeys()
            nkeys = gDir.GetNkeys()
            for i in range(nkeys):
                keyname = keys.At(i).GetName()
                obj = fIN.Get(os.path.join(rpath,keyname)).Clone(keyname)
                #if obj.IsFolder():
                #    continue
                #print "check file dir",obj.IsFolder()

                #obj.SetDirectory(0)
                if isinstance(obj, ROOT.TH1F):
                    histonames.append(os.path.join(rpath,keyname))
                if isinstance(obj, ROOT.TH2F):
                    histonames.append(os.path.join(rpath,keyname))

        #print "Reading histograms:"
        for histoname in histonames:
            #print "   ",histoname
            self.histo[histoname] = fIN.Get(histoname).Clone(histoname)
            self.histo[histoname].Reset()
            self.histo[histoname].SetDirectory(0)
            #           self.histo[histoname].SetMarkerSize(self.style.getMarkerSize())
            #           self.histo[histoname].SetMarkerStyle(self.style.getMarkerStyle())
            #           self.histo[histoname].SetFillColor(self.style.getColor())
        fIN.Close()

        for fname in self.histofiles:
            fIN = ROOT.TFile.Open(fname)
            #fIN.ls()
            for histoname in histonames:
                #print "check",histoname,self.histo[histoname].GetEntries()
                hIN = fIN.Get(histoname).Clone("histoname")
                self.histo[histoname].Add(hIN)
            fIN.Close()

        return True

    def append(self,dset,norm=1):
        #print "check norm",norm
        if len(self.histo.keys()) > 0:
            for keyname in self.histo.keys():
                #print "check histo keynames",keyname
                self.histo[keyname].Add(dset.histo[keyname])
        else:
            self.histo = dset.histo
            
#        for h in dset.histofiles:
#            self.histofiles.append(h)
        self.isdata = dset.isData()
        if hasattr(self, 'counters'):
            self.counters.Add(dset.counters)
        else:
            self.counters = dset.counters
        if not self.isData():
            #print "check append",hasattr(self, 'jetlabels')
            if hasattr(self, 'jetlabels'):
                #print "check append adding2",dset.jetlabels.GetBinContent(1)
                self.jetlabels.Add(dset.jetlabels)
                #print "check append adding3",self.jetlabels.GetBinContent(1)
            else:
                #print "check append adding1",dset.jetlabels.GetBinContent(1)
                if hasattr(dset, 'jetlabels'):
                    self.jetlabels = dset.jetlabels

        self.lumi += dset.lumi

    def histogram(self,histoname):
        fIN0 = ROOT.TFile.Open(self.histofiles[0])
        h = fIN0.Get(os.path.join("analysis",histoname)).Clone(histoname)
        h.Reset()
        h.SetDirectory(0)
        fIN0.Close()
        for fname in self.histofiles:
            fIN = ROOT.TFile.Open(fname)
            hIN = fIN.Get(os.path.join("analysis",histoname))
            h.Add(hIN)
            fIN.Close()
        h.SetMarkerSize(self.style.getMarkerSize())
        h.SetMarkerStyle(self.style.getMarkerStyle())
        h.SetFillColor(self.style.getColor())
        return h

    def exists(self,histoname):
        return histoname in self.histo

    def isData(self):
        return self.isdata #.GetBinContent(1)

    def getLumi(self):
        return self.lumi

    def getEvents(self):
        return self.counters.GetBinContent(self.counters.GetXaxis().FindBin("Skim: All events"))

#    def getSize(self):
#        return self.histo[self.histo.keys()[0]].Integral()

    def normalizeToLumi(self,lumi):
        if self.isData():
            return
        dsetname = self.name
        if not self.isdata and "Run201" in self.name:
            dsetname = dsetname[:-8]
        #print "check normalizeToLumi",self.name,dsetname
        xsec = xsecs[dsetname]
        nev = self.getEvents()
        norm = lumi*xsec/nev
        #print "check norm",lumi,xsec,nev,norm
        for keyname in self.histo.keys():
            #print "check histo",keyname,self.histo[keyname].GetMaximum()
            self.histo[keyname].Scale(norm)
            #print "check histo after",self.histo[keyname].GetMaximum()
        self.counters.Scale(norm)
        self.jetlabels.Scale(norm)

        
def execute(cmd):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
    (s_in, s_out) = (p.stdin, p.stdout)

    f = s_out
    ret=[]
    for line in f:
        line = str(line,'utf-8')
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

def getDatasetsMany(multicrabdirs,whitelist=[],blacklist=[]):
    datasets = []
    for m in multicrabdirs:
        ds = getDatasets(m,whitelist,blacklist)
        datasets.extend(ds)
    return datasets

def getDatasets(multicrabdir,whitelist=[],blacklist=[]):
    datasets = []
    cands = execute("ls %s"%multicrabdir)

    if len(whitelist) > 0:
        #print "check whitelist 1 ",whitelist,blacklist
        datasets_whitelist = []
        for d in cands:
            for wl in whitelist:
                wl_re = re.compile(wl)
                match = wl_re.search(d)
                if match:
                    datasets_whitelist.append(d)
                    break
        #print "check whitelist",datasets_whitelist
        cands = datasets_whitelist

    if len(blacklist) > 0:
        #print "check blacklist 1 ",whitelist,blacklist
        datasets_blacklist = []
        for d in cands:
            found = False
            for bl in blacklist:
                bl_re = re.compile(bl)
                match = bl_re.search(d)
                if match:
                    found = True
                    break
            if not found:
                datasets_blacklist.append(d)
        cands = datasets_blacklist

    for c in cands:
        resultdir = os.path.join(multicrabdir,c,"results")
        if os.path.exists(resultdir):
            datasets.append(Dataset(c,multicrabdir)) # os.path.join(multicrabdir,c)))

    return datasets
                                                                                                                                                                                                                                                                                                                                                                                        

def mergeExtDatasets(datasets):
    for d in datasets:
        #hptname = "h_alpha_RpT"
        #print "check histos mergeExtDatasets",d.name,hptname,d.histo[hptname].GetEntries()
        #print "check mergeExtDatasets in",d.name
        mergeDsets = []
        name = d.name
        if "ext" in name:
            ext_re = re.compile("(?P<name>\S+)_ext")
            match = ext_re.search(name)
            if match:
                name = match.group("name")
                d.name = name
        for dd in datasets:
            if name in dd.name:
                mergeDsets.append(dd)
        if len(mergeDsets) > 1:
            datasets = mergeDatasets(name,name,datasets) 
    """
    for d in datasets:
        print "check mergeExtDatasets in",d.name
        mergeDsets = []
        if "ext" in d.name:
            continue
        name_re = re.compile(d.name)
        for dd in datasets:
            match = name_re.search(dd.name)
            if match:
                mergeDsets.append(dd)
        if len(mergeDsets) > 1:
            datasets = mergeDatasets(d.name,d.name,datasets)
    for d in datasets:
        print "check mergeExtDatasets out",d.name
        if "ext" in d.name:
            ext_re = re.compile("(?P<name>\S+)_ext")
            match = ext_re.search(d.name)
            if match:
#                d.name = match.group("name")
                print "check mergeExtDatasetsmatch",d.name,match.group("name")
    """
    #for d in datasets:
    #    print "check mergeExtDatasets out",d.name
    return datasets

def mergeDatasets(datasetName,datasetRe,datasets,lumi=0):
    #print "check mergeDatasets",datasetName,datasetRe,len(datasets)
    subset = []
    rest   = []
    dset_re = re.compile(datasetRe)
    isdata = False
    for d in datasets:
        match = dset_re.search(d.name)
        if match:
            if d.isData():
                isdata = True
            subset.append(d)
        else:
            rest.append(d)
    if len(subset) == 0:
        print("Warning, dataset",datasetName,"is empty!")
    else:
#        if isdata or lumi > 0:
        print("Merging datasets")
        for d in subset:
            print("    ",d.name)
        print("     as\033[94m",datasetName,'\033[0m')
    #print "check subset"
    #for s in subset:
    #    print s.name
    #print "check rest"
    #for s in rest:
    #    print s.name
    if len(subset) > 0:
        mergedDset = Dataset(datasetName)
#        print "check jets all",mergedDset.jetlabels.GetBincontent(1)
        for d in subset:
            #if not d.isData():
            #    print "check jets all",d.name,d.jetlabels.GetBinContent(1)
            newd = d
            if not newd.isData() and lumi > 0:
                newd.normalizeToLumi(lumi)
            #print "appending",newd.name
            mergedDset.append(newd)
            #if not d.isData():
            #    print "append done",newd.name,mergedDset.jetlabels.GetBinContent(1)
        #if not mergedDset.isData():
        #    print "check jets all",mergedDset.jetlabels.GetBinContent(1)
        rest.append(mergedDset)
    return rest

def removeDatasets(datasetRe,datasets):
    subset = []
    dset_re = re.compile(datasetRe)
    for d in datasets:
        match = dset_re.search(d.name)
        if not match:
            subset.append(d)
    return subset

def read(datasets):
    dsets = []
    for d in datasets:
        if d.read() or d.readSkim():
            dsets.append(d)
    return dsets

def getLumi(datasets):
    lumi = 0
    for d in datasets:
        if d.isData():
            lumi+=d.getLumi()
    print("Luminosity",lumi)
    return lumi

def normalizeToLumi(datasets):
    lumi = getLumi(datasets)
    for d in datasets:
        d.normalizeToLumi(lumi)
    return datasets

def getSize(d):
    return d.histo[d.histo.keys()[0]].Integral()

def orderBySize(datasets):
    ordered = []
    data = []
    mc = []
    for d in datasets:
        #print d.name
        if d.isData():
            data.append(d)
        else:
            mc.append(d)
            
    mc.sort(key=getSize)

    histonames = mc[0].histo.keys()
    for h in histonames:
        for i in range(len(mc)):
            for j in range(0,i):
                mc[i].histo[h].Add(mc[j].histo[h])

    ordered.extend(reversed(mc))
    ordered.extend(data)
#    for d in ordered:
#        print "ordered",d.name
#    sys.exit()
    return ordered


def plotHisto(datasets,histogramName,xtitle="x-axis",ytitle="y-axis",ratioPlot=False,logy=False,plot=True):
    #print "check plot",histogramName
    canvasx = 500
    canvasy = 500
    labelsize = 20
    titlesize = 25
    if ratioPlot:
        canvasy = 700
    hname = os.path.basename(histogramName)
    canvas = ROOT.TCanvas(hname,"",canvasx,canvasy)
    canvas.cd()

    if ratioPlot:
        mainpadfraction = 0.7
        mainpad = ROOT.TPad("main","",0,1-mainpadfraction,1,1)
        ratiopad = ROOT.TPad("ratio","",0,0,1,1-mainpadfraction)
        mainpad.Draw()
        ratiopad.Draw()
        mainpad.cd()
        bottomMargin = mainpad.GetBottomMargin();
        mainpad.SetBottomMargin(0);
        
    #h_data = None
    #h_mc   = []
    h_frame = None
    lumi = round(0.001*getLumi(datasets),1)
    for d in datasets:
        #print "check keys",d.histo.keys(),len(d.histo.keys())
        h = d.histo[histogramName]
        #if d.isData():
        #    h_data = h
        #else:
        #    h_mc.append(h)
        if h_frame == None:
            h_frame = h
        else:
            if h.GetMaximum() > h_frame.GetMaximum():
                h_frame = h

    xmin = h_frame.GetXaxis().GetBinLowEdge(1)
    xmax = h_frame.GetXaxis().GetBinUpEdge(h_frame.GetXaxis().GetNbins())
    ymin = 1
    ymax = h_frame.GetMaximum()*1.2
    ytitleoffset = 1.15
    xtitleoffset = 0.8
    if logy:
        ymax = h_frame.GetMaximum()*2
        ROOT.gPad.SetLogy()
        ytitleoffset = 2.0
        xtitleoffset = 3.0
    #print "check frame",xmin,xmax,ymin,ymax
    frameMain = ROOT.TH1D(hname,";%s;%s"%(xtitle,ytitle),25,xmin,xmax)
    frameMain.SetMaximum(ymax)
    frameMain.SetMinimum(ymin)
    frameMain.Draw()
    """
    frame = ROOT.TH2F("frame","",2,xmin,xmax,2,ymin,ymax)
    frame.Reset()
    frame.SetStats(0)
    frame.GetYaxis().SetLabelSize(labelsize)
    frame.GetYaxis().SetTitleOffset(ytitleoffset)
    frame.GetYaxis().SetTitleSize(titlesize)
    frame.GetYaxis().SetTitle(ytitle)
    frame.GetXaxis().SetLabelSize(labelsize)
    frame.GetXaxis().SetTitleSize(titlesize)
    frame.GetXaxis().SetTitleOffset(xtitleoffset)
    frame.GetXaxis().SetTitle(xtitle)
    frame.Draw()
    """
    datasets = orderBySize(datasets)
    h = {}
    for d in datasets:
        h[d.name] = d.histo[histogramName].Clone("d.name")
        h[d.name].SetMarkerSize(d.style.getMarkerSize())
        h[d.name].SetMarkerStyle(d.style.getMarkerStyle())
        h[d.name].SetFillColor(d.style.getColor())
                                            
        #print "check histogram",d.name,h[d.name].GetMaximum()
        h[d.name].Draw(histogramStyles[d.name].getDrawOptions())

    ROOT.gPad.RedrawAxis()


    cms = ROOT.TLatex(0.17,0.96,"CMS preliminary")
    cms.SetNDC()
    cms.SetTextSize(0.03)
    cms.Draw()

    lumitext = ROOT.TLatex(0.55,0.96,"%s fb^{-1}"%lumi)
    lumitext.SetNDC()
    lumitext.SetTextSize(0.03)
    lumitext.Draw()

    cmenergy = ROOT.TLatex(0.75,0.96,"#sqrt{s} = 13 TeV")
    cmenergy.SetNDC()
    cmenergy.SetTextSize(0.03)
    cmenergy.Draw()

    if ratioPlot:
        ratiopad.cd()
        #ratiopad.SetFillColor(5)
        ratiopad.SetTopMargin(0)
        ratiopad.SetBottomMargin(bottomMargin*2)
        ratioRange = 1
        rframe = ROOT.TH2F("ratioframe","",2,xmin,xmax,2,-ratioRange,ratioRange)
        rframe.Reset()
        rframe.SetStats(0)
        rframe.GetYaxis().SetLabelSize(labelsize)
        rframe.GetYaxis().SetTitleOffset(frameMain.GetYaxis().GetTitleOffset())
        rframe.GetYaxis().SetTitleSize(titlesize)
        rframe.GetYaxis().SetTitle("Data/MC")
        rframe.GetXaxis().SetLabelSize(labelsize)
        rframe.GetXaxis().SetTitleSize(titlesize)
        rframe.GetXaxis().SetTitleOffset(xtitleoffset)
        rframe.GetXaxis().SetTitle(xtitle)
        rframe.Draw()
        zeroline = ROOT.TLine(0,0,2.0,0)
        #zeroline.SetNDC()
        zeroline.SetLineStyle(2)
        zeroline.Draw()

    if not os.path.exists(outputdir):
        os.mkdir(outputdir)
    for ending in endings:
        canvas.Print(os.path.join(outputdir,hname+ending))

#def plotHisto(datasets,histogramName,hptname,xtitle="x-axis",ytitle="y-axis",ratioPlot=False,logy=False):

def plotPTZvsNev(datasets,histogramName,hptname,xtitle="x-axis",ytitle="y-axis",ratioPlot=False,logy=False):

    histonames = []
    histonames.append(hptname)
    histonames.append(hptname+"_a100")
#    histonames.append(hptname+"_a80")
#    histonames.append(hptname+"_a60")
#    histonames.append(hptname+"_a50")
    histonames.append(hptname+"_a40")
    histonames.append(hptname+"_a30")
    histonames.append(hptname+"_a20")
    histonames.append(hptname+"_a15")
    histonames.append(hptname+"_a10")
    histolegends = []
    histolegends.append("No #alpha cut")
    histolegends.append("#alpha_{max} < 1.00")
#    histolegends.append("#alpha_{max} < 0.80")
#    histolegends.append("#alpha_{max} < 0.60")
#    histolegends.append("#alpha_{max} < 0.50")
    histolegends.append("#alpha_{max} < 0.40")
    histolegends.append("#alpha_{max} < 0.30")
    histolegends.append("#alpha_{max} < 0.20")
    histolegends.append("#alpha_{max} < 0.15")
    histolegends.append("#alpha_{max} < 0.10")
        
    histos = []
    datasetData = None
    for d in datasets:
        if d.isData():
            datasetData = d
            break
    if datasetData == None:
        print("plotPTZvsNev: no data dataset found")
        sys.exit()

    xmin = 30 
    xmax = 2000 
    ymin = 1
    factor = 1.2
    if logy:
        factor = 20
    ymax = datasetData.histo[hptname].GetMaximum()*factor
                            
    frameMain = ROOT.TH1D(histogramName,";%s;%s"%(xtitle,ytitle),25,xmin,xmax)
    frameMain.SetMaximum(ymax)
    frameMain.SetMinimum(ymin)
    frameMain.Draw()
    #print "check ymin ymax",ymin,ymax

    ROOT.extraText = "Preliminary"
    lumi = round(0.001*getLumi(datasets),1)
    ROOT.lumi_13TeV = "%s fb^{-1}"%lumi
    canvas = ROOT.tdrCanvas(histogramName,frameMain,4)
    canvas.SetLogx()
    if logy:
        canvas.SetLogy()
    canvas.cd(1)
    ROOT.gPad.SetLogx()
    ROOT.gPad.Update()

    entries = 0
    j = 0
    for i,hname in enumerate(histonames):
        histo = datasetData.histo[hname]
        if i == 0:
            entries = histo.GetEntries()
            print("Histogram",hname,", entries",histo.GetEntries())
        histo.SetLineWidth(2)
        j = j + i
        if j > 10:
            j = 12
        histo.SetLineColor(1)
        histo.SetFillColor(ROOT.kBlue-10+j)
        histo.Draw("same")
        histos.append(histo)


    frameMain.GetXaxis().SetMoreLogLabels()
    frameMain.GetXaxis().SetNoExponent()
    ROOT.gPad.RedrawAxis()

    #cms = ROOT.TLatex(xmin+0.125*(xmax-xmin),1.01*ymax,"CMS preliminary")
    cms = ROOT.TLatex(0.17,0.95,"CMS preliminary")
    cms.SetNDC()
    cms.SetTextSize(20)
    cms.Draw()

    lumitext = ROOT.TLatex(0.55,0.95,"%s fb^{-1}"%lumi)
    lumitext.SetNDC()
    lumitext.SetTextSize(20)
    lumitext.Draw()
    
    cmenergy = ROOT.TLatex(0.75,0.95,"#sqrt{s} = 13 TeV")
    cmenergy.SetNDC()
    cmenergy.SetTextSize(20)
    cmenergy.Draw()

    nev = ROOT.TLatex(0.25,0.95,"%s events"%(entries))
    nev.SetNDC()
    nev.SetTextSize(20)
    nev.Draw()

    legend = ROOT.TLegend(0.7,0.6,0.93,0.9)

    legend.SetLineColor(0)
    legend.SetShadowColor(0)
    legend.SetTextSize(0.04)
    for i,h in enumerate(histos):
        legend.AddEntry(h,histolegends[i],"f")
    legend.Draw()

    if not os.path.exists(outputdir):
        os.mkdir(outputdir)
    for ending in endings:
        canvas.Print(os.path.join(outputdir,histogramName+ending))


PT_BALANCE_DATA = 0
PT_BALANCE_MC   = 0
MPF_DATA        = 0
MPF_MC          = 0
def plotJES(datasets,histogramName,hname,xtitle="x-axis",ytitle="y-axis",ratioPlot=False,logy=False,plot=True):

    global PT_BALANCE_DATA
    global PT_BALANCE_MC
    global MPF_DATA
    global MPF_MC

    print("check plotJES",histogramName,hname)
    hptname = ""
    if "_RpT" in hname:
        hptname  = hname
    if "_RMPF" in hname:
        hmpfname = hname
    else:
        hmpfname = hptname.replace("_RpT","_RMPF")

    btag = ""
    btag_re = re.compile("btag(?P<algo>\S+)(?P<wp>(loose)|(medium)|(tight))")
    btag_match = btag_re.search(hname)
    wp = ""
    if btag_match:
        btag = btag_match.group(0)
        wp = btag_match.group("wp")
        wp = wp[3:]
        wp = wp.replace("_","<Z_{pT}<")
        label_algo = btag_match.group("algo")
    #h_alpha_RpT_btagCSVV2loose_Zpt100_140

    gtag_re = re.compile("(?P<algo>gluontag)")
    gtag_match = gtag_re.search(hname)
    if gtag_match:
        btag = "gluontag"
        label_algo = gtag_match.group("algo")

    qtag_re = re.compile("(?P<algo>quarktag)")
    qtag_match = qtag_re.search(hname)
    if qtag_match:
        btag = "quarktag"
        label_algo = qtag_match.group("algo")
        
    zpt_re = re.compile("(?P<ptbin>Zpt\d+_\S+?)($|_)")
    zpt_match = zpt_re.search(hname)
    zpt = ""
    if zpt_match:
        zpt = zpt_match.group("ptbin")
        zpt = zpt[3:]
        zpt = zpt.replace("_","<Z_{pT}<")
    if "_RpT_" in hname:
        histogramName = histogramName+hptname[hptname.find("_RpT_")+4:]
#        hptname       = hptname+"_"+btag
#        hmpfname      = hmpfname+"_"+btag
    mpf_re = re.compile("_(?P<value>RMPF\S*)(?P<value2>_alpha\S+)")
    match = mpf_re.search(hname)
    if match:
        histogramName = histogramName+match.group("value")+match.group("value2")
    print("check histogramName ---- ",histogramName)
    canvasx = 500
    canvasy = 500
    labelsize = 20
    titlesize = 25
    if ratioPlot:
        canvasy = 700

#    canvas = tdrCanvas(histogramName,frame)
#    canvas = ROOT.TCanvas(histogramName,"",canvasx,canvasy)
#    canvas.cd()
    """
    if ratioPlot:
        mainpadfraction = 0.7
        mainpad = ROOT.TPad("main","",0,1-mainpadfraction,1,1)
        ratiopad = ROOT.TPad("ratio","",0,0,1,1-mainpadfraction)
        mainpad.Draw()
        ratiopad.Draw()
        mainpad.cd()
        bottomMargin = mainpad.GetBottomMargin();
        mainpad.SetBottomMargin(0);
    """    
    #h_data = None
    #h_mc   = []
    h_frame = None
    lumi = round(0.001*getLumi(datasets),1)

    #print "check hptname",hptname
    for d in datasets:
        #print "dataset",d.name,len(d.histo.keys())
        #for k in d.histo.keys():
        #    print k
        print("dataset",d.name,hname)
        if not hname in d.histo.keys():
            print("check not hptname in d.histo.keys")
            continue
        #print "check keys",d.histo.keys()
        #print "check histos",d.name,hptname,d.histo[hptname].GetEntries()
        h = d.histo[hname]
        #if d.isData():
        #    h_data = h
        #else:
        #    h_mc.append(h)
        if h_frame == None:
            h_frame = h
        else:
            if h.GetMaximum() > h_frame.GetMaximum():
                h_frame = h
                    
        xmin = h_frame.GetXaxis().GetBinLowEdge(1)
        xmax = h_frame.GetXaxis().GetBinUpEdge(h_frame.GetXaxis().GetNbins())
        ymin = 0.6001
        #ymin = 0.4001
        ymax = 1.1
        ytitleoffset = 2
        xtitleoffset = 3
        if logy:
            ymax = h_frame.GetMaximum()*2
            ROOT.gPad.SetLogy()
            ytitleoffset = 2.0
            xtitleoffset = 3.0

    """
    frame = ROOT.TH2F("frame","",2,xmin,xmax,2,ymin,ymax)
    frame.Reset()
    frame.SetStats(0)
#    frame.GetYaxis().SetLabelSize(labelsize)
#    frame.GetYaxis().SetTitleOffset(ytitleoffset)
#    frame.GetYaxis().SetTitleSize(titlesize)
    frame.GetYaxis().SetTitle(ytitle)
#    frame.GetXaxis().SetLabelSize(labelsize)
#    frame.GetXaxis().SetTitleSize(titlesize)
#    frame.GetXaxis().SetTitleOffset(xtitleoffset)
    frame.GetXaxis().SetTitle(xtitle)
    frame.Draw()
    """
    #print "check frame 1"
    frameName = hptname.replace("/","_")
    frameMain = ROOT.TH1D(frameName,";%s;%s"%(xtitle,ytitle),25,xmin,xmax)
    frameMain.SetMaximum(ymax)
    frameMain.SetMinimum(ymin)
    #print "check ymin ymax",ymin,ymax

    frameRatio = ROOT.TH1D(frameName+"_ratio",";%s;Data/MC"%(xtitle),25,xmin,xmax);
    frameRatio.SetMaximum(1.1)
    frameRatio.SetMinimum(0.9)
    #print "check frame 2"
    ROOT.extraText = "Preliminary"
    ROOT.lumi_13TeV = "%s fb^{-1}"%lumi
    canvas = ROOT.tdrDiCanvas(histogramName,frameMain,frameRatio,4)
    canvas.cd(1)
    ROOT.gPad.Update()
    
    datasets = orderBySize(datasets)
    hpt = {}
    hpt_b = {}
    hpt_c = {}
    hpt_g = {}
    hpt_uds = {}
    hmpf = {}
    hmpf_b = {}
    hmpf_c = {}
    hmpf_g = {}
    hmpf_uds = {}
    hmZ = {}
    hmZgen = {}
    for d in datasets:
        if not hname in d.histo.keys():
            continue
        #print "dataset",d.name,d.isData()
        if d.isData():
            #print "check",d.name,hptname,d.histo[hptname].GetEntries()
            if not "data" in hpt.keys():
                if hptname in d.histo:
                    hpt["data"] = d.histo[hptname].Clone("data")
                if hmpfname in d.histo:
                    hmpf["data"] = d.histo[hmpfname].Clone("data")
                if "h_Zpt_mZ" in d.histo:
                    hmZ["data"] = d.histo["h_Zpt_mZ"].Clone("data")
            else:
                if hptname in d.histo:
                    hpt["data"].Add(d.histo[hptname])
                if hmpfname in d.histo:
                    hmpf["data"].Add(d.histo[hmpfname])
                if "h_Zpt_mZ" in d.histo:
                    hmZ["data"].Add(d.histo["h_Zpt_mZ"])
        else:
            # all mc added together
            if d.exists("h_Zpt_mZ"):
                hmZ[d.name] = d.histo["h_Zpt_mZ"].Clone(d.name)
            if d.exists("h_Zpt_mZgen"):
                hmZgen[d.name] = d.histo["h_Zpt_mZgen"].Clone(d.name)
            if d.exists(hptname):
                hpt[d.name] = d.histo[hptname].Clone(d.name)
                #print "check hpt entries",hpt[d.name].GetEntries()
            if d.exists(hmpfname):
                hmpf[d.name] = d.histo[hmpfname].Clone(d.name)
                #print "check hmpfname",hmpfname,d.name
                #print "check hmpf entries",hmpf[d.name].GetEntries()
            #print "check _genb ---------------",hmpfname+"_genb",d.exists(hmpfname+"_genb")
            if d.exists(hptname+"_genb"):
                hpt_b[d.name] = d.histo[hptname+"_genb"].Clone(d.name)
                #print "check hpt_b entries",hpt_b[d.name].GetEntries()
            if d.exists(hmpfname+"_genb"):
                hmpf_b[d.name] = d.histo[hmpfname+"_genb"].Clone(d.name)
            if d.exists(hptname+"_genc"):
                hpt_c[d.name] = d.histo[hptname+"_genc"].Clone(d.name)
                #print "check hpt_c entries",hpt_c[d.name].GetEntries()
            if d.exists(hmpfname+"_genc"):
                hmpf_c[d.name] = d.histo[hmpfname+"_genc"].Clone(d.name)
            if d.exists(hptname+"_geng"):
                hpt_g[d.name] = d.histo[hptname+"_geng"].Clone(d.name)
            if d.exists(hmpfname+"_geng"):
                hmpf_g[d.name] = d.histo[hmpfname+"_geng"].Clone(d.name)
            if d.exists(hptname+"_genuds"):
                hpt_uds[d.name] = d.histo[hptname+"_genuds"].Clone(d.name)
                #print "check hpt_uds entries",hpt_uds[d.name].GetEntries()
            if d.exists(hmpfname+"_genuds"):
                hmpf_uds[d.name] = d.histo[hmpfname+"_genuds"].Clone(d.name)
            #print "check hpt sum",hpt_b[d.name].GetEntries()+hpt_c[d.name].GetEntries()+hpt_g[d.name].GetEntries()+hpt_uds[d.name].GetEntries()

            ####hmpf_b[d.name] = d.histo[hmpfname+"_genb"].Clone(d.name)
    #print "check X 1"
    #for ibin in range(1,hpt["data"].GetNbinsX()+1):
    #    print "check hpt['data']",hpt["data"].GetXaxis().GetBinUpEdge(ibin),hpt["data"].GetBinContent(ibin,15)
#    print "data entries",hpt["data"].GetEntries()
#    print "check hpt_tag entries",d.name,hptname,hpt_tag["DYJetsToLL_M_50"].GetEntries()
    x = []
    xerr = []
    ydata = []
    ydataerr = []
    ymc = {}
    ymcerr = {}
    ymc_stat = {}
    ymc_staterr = {}
    ymc_b_stat = {}
    ymc_b_staterr = {}
    ymc_c_stat = {}
    ymc_c_staterr = {}
    ymc_g_stat = {}
    ymc_g_staterr = {}
    ymc_uds_stat = {}
    ymc_uds_staterr = {}
    for mc in hpt.keys():
        if not mc == "data":
            ymc[mc] = []
            ymcerr[mc] = []
            ymc_stat[mc] = []
            ymc_staterr[mc] = []
            ymc_b_stat[mc] = []
            ymc_b_staterr[mc] = []
            ymc_c_stat[mc] = []
            ymc_c_staterr[mc] = []
            ymc_g_stat[mc] = [] 
            ymc_g_staterr[mc] = []
            ymc_uds_stat[mc] = [] 
            ymc_uds_staterr[mc] = []

    h_data = ROOT.TH1F("pt","",10,xmin,xmax)

    ydata_mpf = []
    ydata_mpferr = []
    ymc_mpf = {}
    ymc_mpferr = {}
    ymc_mpf_stat = {}
    ymc_mpf_staterr = {}
    ymc_mpf_b_stat = {}
    ymc_mpf_b_staterr = {}
    ymc_mpf_c_stat = {}
    ymc_mpf_c_staterr = {}
    ymc_mpf_g_stat = {}
    ymc_mpf_g_staterr = {}
    ymc_mpf_uds_stat = {}
    ymc_mpf_uds_staterr = {}
    for mc in hmpf.keys():
        if not mc == "data":
            ymc_mpf[mc] = []
            ymc_mpferr[mc] = []
            ymc_mpf_stat[mc] = []
            ymc_mpf_staterr[mc] = []
            ymc_mpf_b_stat[mc] = []
            ymc_mpf_b_staterr[mc] = []
            ymc_mpf_c_stat[mc] = []
            ymc_mpf_c_staterr[mc] = []
            ymc_mpf_g_stat[mc] = []
            ymc_mpf_g_staterr[mc] = []
            ymc_mpf_uds_stat[mc] = []
            ymc_mpf_uds_staterr[mc] = []
    h_data_mpf = ROOT.TH1F("mpf","",10,xmin,xmax)
    
    """
    ydata.append(1.0)
    ydataerr.append(1.0)
    ymc.append(1.0)
    ymcerr.append(1.0)
    x.append(0)
    xerr.append(0)
    """
    #print "check hpt keys",hpt.keys()
    nbinsx = 0
    if "MC" in hpt:
        nbinsx = hpt["MC"].GetNbinsX()+1
    if nbinsx == 0 and "MC" in hmpf:
        nbinsx = hmpf["MC"].GetNbinsX()+1
    #print "check hpt",hpt
    #print "check hmpf",hmpf
    for ibin in range(1,nbinsx):
        if "MC" in hpt:
            bc = hpt["MC"].GetXaxis().GetBinCenter(ibin)
        else:
            bc = hmpf["MC"].GetXaxis().GetBinCenter(ibin)

        #h_data.Fill(bc,1.0)
        #x.append(bc)
        #xerr.append(0.0)

        firstBin = 1
        if "h_Zpt_RpT_" in hname or "h_Zpt_RMPF" in hname:
            firstBin = ibin
        if "data" in hpt.keys():
            ydataslice = hpt["data"].ProjectionY("",firstBin,ibin)

        #print "check alphagraph",ibin,bc,ydataslice.GetEntries(),ydataslice.GetMean()
        #for j iCommonPlots: fixed bug breaking QCDNormalizationn range(1,61):
        #    print "   xbin:",ibin,j,hpt["data"].GetBinContent(ibin,j),ydataslice.GetBinContent(j)
        #print "check1",ydataslice
        #if ydataslice.GetMean() > 0:
            ydata.append(ydataslice.GetMean())
            ydataerr.append(ydataslice.GetMeanError())
        #print "check ymc.keys()",ymc.keys()
        for mc in ymc.keys():
            ymcslice = hpt[mc].ProjectionY("",firstBin,ibin)

#            print "check alphagraph mc",ibin,bc,ymcslice[mc].GetEntries(),ymcslice[mc].GetMean()
#        for j in range(1,61):
#            print "   xbin:",ibin,j,hpt["mc"].GetBinContent(ibin,j),ymcslice.GetBinContent(j),ymcslice.GetBinCenter(j)
            ymc[mc].append(ymcslice.GetMean())
            ymcerr[mc].append(ymcslice.GetMeanError())

            all = hpt[mc].Integral(firstBin,ibin,1,hpt[mc].GetNbinsY())
            if all < 0:
                all = 0
#            b = hpt_b[mc].Integral(firstBin,ibin,1,hpt[mc].GetNbinsY())
#            if b < 0:
#                b = 0
            #print "check all",all
            ymc_stat[mc].append(all)
            ymc_staterr[mc].append(math.sqrt(all))

            if mc in hpt_b:
                b = hpt_b[mc].Integral(firstBin,ibin,1,hpt[mc].GetNbinsY())
                if b < 0:
                    b = 0
                ymc_b_stat[mc].append(b)
                ymc_b_staterr[mc].append(math.sqrt(b))
            if mc in hpt_c:
                c = hpt_c[mc].Integral(firstBin,ibin,1,hpt[mc].GetNbinsY())
                if c < 0:
                    c = 0
                ymc_c_stat[mc].append(c)
                ymc_c_staterr[mc].append(math.sqrt(c))
            if mc in hpt_g:
                g = hpt_g[mc].Integral(firstBin,ibin,1,hpt[mc].GetNbinsY())
                if g < 0:
                    g = 0
                ymc_g_stat[mc].append(g)
                ymc_g_staterr[mc].append(math.sqrt(g))
            if mc in hpt_uds:
                uds = hpt_uds[mc].Integral(firstBin,ibin,1,hpt[mc].GetNbinsY())
                if uds < 0:
                    uds = 0
                ymc_uds_stat[mc].append(uds)
                ymc_uds_staterr[mc].append(math.sqrt(uds))

            #print "check Integral",mc,ibin,hpt[mc].Integral(1,ibin,1,hpt[mc].GetNbinsY()),hpt_tag[mc].Integral(1,ibin,1,hpt[mc].GetNbinsY())
#            for i in range(1,ibin+1):
#                hpt[mc].GetXbin
        
        x.append(bc)
        xerr.append(0.0)

        if "data" in hmpf.keys():
            ydataslice_mpf = hmpf["data"].ProjectionY("",firstBin,ibin)
            ydata_mpf.append(ydataslice_mpf.GetMean())
            ydata_mpferr.append(ydataslice_mpf.GetMeanError())
        #print "check ymc_mpf.keys()",ymc_mpf.keys()
        for mc in ymc_mpf.keys():
            ymcslice_mpf = hmpf[mc].ProjectionY(" ",firstBin,ibin)
            ymc_mpf[mc].append(ymcslice_mpf.GetMean())
            ymc_mpferr[mc].append(ymcslice_mpf.GetMeanError())

            all = hmpf[mc].Integral(firstBin,ibin,1,hmpf[mc].GetNbinsY())
            if all < 0:
                all = 0
            ymc_mpf_stat[mc].append(all)
            ymc_mpf_staterr[mc].append(math.sqrt(all))

            if mc in hmpf_b:
                b = hmpf_b[mc].Integral(firstBin,ibin,1,hmpf_b[mc].GetNbinsY())
                if b < 0:
                    b = 0
                ymc_mpf_b_stat[mc].append(b)
                ymc_mpf_b_staterr[mc].append(math.sqrt(b))

            if mc in hmpf_c:
                c = hmpf_c[mc].Integral(firstBin,ibin,1,hmpf_c[mc].GetNbinsY())
                if c < 0:
                    c = 0
                ymc_mpf_c_stat[mc].append(c)
                ymc_mpf_c_staterr[mc].append(math.sqrt(c))

            if mc in hmpf_uds:
                uds = hmpf_uds[mc].Integral(firstBin,ibin,1,hmpf_uds[mc].GetNbinsY())
                if uds < 0:
                    uds = 0
                ymc_mpf_uds_stat[mc].append(uds)
                ymc_mpf_uds_staterr[mc].append(math.sqrt(uds))

            if mc in hmpf_g:
                g = hmpf_g[mc].Integral(firstBin,ibin,1,hmpf_g[mc].GetNbinsY())
                if g < 0:
                    g = 0
                ymc_mpf_g_stat[mc].append(g)
                ymc_mpf_g_staterr[mc].append(math.sqrt(g))

#        ydata.append(1.0)
#        ydataerr.append(1.0)
#        ymc.append(1.0)
#        ymcerr.append(1.0)
#    print "check ymc_stat",ymc_stat
#    print "check ymc_b_stat",ymc_b_stat
#    sys.exit()
    #print "check X 2"
    graph_bpurity = {}
    graph_cpurity = {}
    graph_gpurity = {}
    graph_udspurity = {}
    graph_stat   = {}
    graph_mpf_purity = {}
    graph_mpf_stat   = {}
    graphs_out = {}
    for mc in ymc.keys():
        #print "check x",x,ymc_stat[mc],ymc_b_stat[mc]
        graph_all = ROOT.TGraphErrors(len(x),array.array('d',x),array.array('d',ymc_stat[mc]),array.array('d',xerr),array.array('d',ymc_staterr[mc]))
        if len(ymc_b_stat[mc]) > 0:
            graph_b = ROOT.TGraphErrors(len(x),array.array('d',x),array.array('d',ymc_b_stat[mc]),array.array('d',xerr),array.array('d',ymc_b_staterr[mc]))
            graph_bpurity[mc] = getRatio(graph_b,graph_all)
            graph_bpurity[mc].SetTitle("b purity")
            graph_bpurity[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
            graph_bpurity[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
        if len(ymc_c_stat[mc]) > 0:
            #print "check xc",x,ymc_stat[mc],ymc_c_stat[mc]
            graph_c = ROOT.TGraphErrors(len(x),array.array('d',x),array.array('d',ymc_c_stat[mc]),array.array('d',xerr),array.array('d',ymc_c_staterr[mc]))
            graph_cpurity[mc] = getRatio(graph_c,graph_all)
            graph_cpurity[mc].SetTitle("c purity")
            graph_cpurity[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
            graph_cpurity[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
        if len(ymc_g_stat[mc]) > 0:
            #print "check xg",x,ymc_stat[mc],ymc_g_stat[mc]
            graph_g = ROOT.TGraphErrors(len(x),array.array('d',x),array.array('d',ymc_g_stat[mc]),array.array('d',xerr),array.array('d',ymc_g_staterr[mc]))
            graph_gpurity[mc] = getRatio(graph_g,graph_all)
            graph_gpurity[mc].SetTitle("gluon purity")
            graph_gpurity[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
            graph_gpurity[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
        if len(ymc_uds_stat[mc]) > 0:
            #print "check xuds",x,ymc_stat[mc],ymc_uds_stat[mc]
            graph_uds = ROOT.TGraphErrors(len(x),array.array('d',x),array.array('d',ymc_uds_stat[mc]),array.array('d',xerr),array.array('d',ymc_uds_staterr[mc]))
            graph_udspurity[mc] = getRatio(graph_uds,graph_all)
            graph_udspurity[mc].SetTitle("uds purity")
            graph_udspurity[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
            graph_udspurity[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
        
        graph_stat[mc] = graph_all
        graph_stat[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())

        graph_mpf_all = ROOT.TGraphErrors(len(x),array.array('d',x),array.array('d',ymc_mpf_stat[mc]),array.array('d',xerr),array.array('d',ymc_mpf_staterr[mc]))
        if len(ymc_mpf_b_stat[mc]) > 0:
            graph_mpf_b = ROOT.TGraphErrors(len(x),array.array('d',x),array.array('d',ymc_mpf_b_stat[mc]),array.array('d',xerr),array.array('d',ymc_mpf_b_staterr[mc]))
            graph_mpf_purity[mc] = getRatio(graph_mpf_b,graph_mpf_all)
            graph_mpf_purity[mc].SetTitle("b purity")
            graph_mpf_purity[mc].GetXaxis().SetTitle(xtitle)
            graph_mpf_purity[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
            #printGraph("graph_mpf_purity",graph_mpf_purity[mc])

        graph_mpf_stat[mc] = graph_mpf_all
        graph_mpf_stat[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
        

    #print "x      ",x
    #print "pt data",ydata
    if len(ydata) > 0:
        graph_data = ROOT.TGraphErrors(len(x),array.array('d',x),array.array('d',ydata),array.array('d',xerr),array.array('d',ydataerr))
        graph_data.SetMarkerStyle(histogramStyles["Data"].getMarkerStyle())
        graph_data.SetTitle(ytitle)
        graph_data.GetXaxis().SetTitle(xtitle)
        graph_data.Draw("P")
    graph_mc = {}
    for mc in ymc.keys():
        #print "pt mc",mc,ymc[mc]
        graph_mc[mc]   = ROOT.TGraphErrors(len(x),array.array('d',x),array.array('d',ymc[mc]),array.array('d',xerr),array.array('d',ymcerr[mc]))
        graph_mc[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
        graph_mc[mc].SetMarkerColor(histogramStyles[mc].getColor())
        graph_mc[mc].SetTitle(ytitle)
        graph_mc[mc].GetXaxis().SetTitle(xtitle)
        graph_mc[mc].Draw("P")

    #print "mpf data",ydata_mpf
    if len(ydata_mpf) > 0:
        graph_data_mpf = ROOT.TGraphErrors(len(x),array.array('d',x),array.array('d',ydata_mpf),array.array('d',xerr),array.array('d',ydata_mpferr))
        graph_data_mpf.SetMarkerStyle(histogramStyles["Data"].getMarkerStyle()+4)
        graph_data_mpf.SetTitle(ytitle)
        graph_data_mpf.GetXaxis().SetTitle(xtitle)
        graph_data_mpf.Draw("P")
    graph_mc_mpf = {}
    for i,mc in enumerate(ymc_mpf.keys()):
        #print "mpf mc",ymc_mpf[mc]
        graph_mc_mpf[mc] = ROOT.TGraphErrors(len(x),array.array('d',x),array.array('d',ymc_mpf[mc]),array.array('d',xerr),array.array('d',ymc_mpferr[mc]))
        graph_mc_mpf[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle()+4)
        graph_mc_mpf[mc].SetMarkerColor(histogramStyles[mc].getColor())
        graph_mc_mpf[mc].SetTitle(ytitle)
        graph_mc_mpf[mc].GetXaxis().SetTitle(xtitle)
        graph_mc_mpf[mc].Draw("P")

    
    ROOT.gPad.RedrawAxis()

    #cms = ROOT.TLatex(xmin+0.125*(xmax-xmin),1.01*ymax,"CMS preliminary")
    cms = ROOT.TLatex(0.17,0.95,"CMS preliminary")
    cms.SetNDC()
    cms.SetTextSize(20)
    cms.Draw()
    
    lumitext = ROOT.TLatex(0.55,0.95,"%s fb^{-1}"%lumi)
    lumitext.SetNDC()
    lumitext.SetTextSize(20)
    lumitext.Draw()

    cmenergy = ROOT.TLatex(0.75,0.95,"#sqrt{s} = 13 TeV")
    cmenergy.SetNDC()

    cmenergy.SetTextSize(20)
    cmenergy.Draw()

    legend = ROOT.TLegend(0.35,0.1,0.93,0.35)
#   legend.SetHeader("The Legend Title","C")
    legend.SetLineColor(0)
    legend.SetShadowColor(0)
    legend.SetTextSize(0.04)
    if len(ydata) > 0:
        legend.AddEntry(graph_data,"p_{T} balance Data","p")
    for mc in ymc.keys():
        legend.AddEntry(graph_mc[mc],"p_{T} balance %s"%histogramStyles[mc].getLabel(),"p")
    if len(ydata_mpf) > 0:
        legend.AddEntry(graph_data_mpf,"MPF Data","p")
    for mc in ymc_mpf.keys():
        legend.AddEntry(graph_mc_mpf[mc],"MPF %s"%histogramStyles[mc].getLabel(),"p")
    legend.Draw()

    # Fitting
    #fitFunction = ROOT.TF1("linear","[0]*x+[1]",0.099,0.3)
    fitFunction = ROOT.TF1("linear","[0]*x+[1]",0.145,0.31)
    fitFunction.SetLineColor(2)
    fitFunction.SetLineWidth(2)
    ROOT.gStyle.SetOptFit(0)

    if "h_alpha" in hptname:
        if len(ydata) > 0:
            graph_data.Fit(fitFunction,"RQ")
            graph_data_at0 = fitFunction.Eval(0)
            slope_data = (fitFunction.Eval(0.3) - fitFunction.Eval(0))/0.3
            extrapolationFunction1 = fitFunction.Clone()
            extrapolationFunction1.SetRange(0,0.3)
            extrapolationFunction1.SetParameter(0,fitFunction.GetParameter(0))
            extrapolationFunction1.SetParameter(1,fitFunction.GetParameter(1))
            extrapolationFunction1.SetLineStyle(2)
            extrapolationFunction1.Draw("same")
        
        extrapolationFunctions = {}
        for mc in ymc.keys():
            #print "check extrapolationFunctions",mc
            graph_mc[mc].Fit(fitFunction,"RQ")
            graph_mc_at0 = fitFunction.Eval(0)
            slope_mc = (fitFunction.Eval(0.3) - fitFunction.Eval(0))/0.3
            extrapolationFunctions[mc] = fitFunction.Clone()
            extrapolationFunctions[mc].SetRange(0,0.3)
            extrapolationFunctions[mc].SetParameter(0,fitFunction.GetParameter(0))
            extrapolationFunctions[mc].SetParameter(1,fitFunction.GetParameter(1))
            extrapolationFunctions[mc].SetLineStyle(2)
            extrapolationFunctions[mc].Draw("same")

        if len(ydata_mpf) > 0:
            fitFunction.SetLineColor(4)
            graph_data_mpf.Fit(fitFunction,"RQ")
            graph_data_mpf_at0 = fitFunction.Eval(0)
            extrapolationFunction3 = fitFunction.Clone()
            extrapolationFunction3.SetRange(0,0.3)
            extrapolationFunction3.SetParameter(0,fitFunction.GetParameter(0))
            extrapolationFunction3.SetParameter(1,fitFunction.GetParameter(1))
            extrapolationFunction3.SetLineStyle(2)
            extrapolationFunction3.Draw("same")

        extrapolationFunctions_mpf = {}
        for mc in ymc.keys():
            graph_mc_mpf[mc].Fit(fitFunction,"RQ")
            graph_mc_mpf_at0 = fitFunction.Eval(0)
            extrapolationFunctions_mpf[mc] = fitFunction.Clone()
            extrapolationFunctions_mpf[mc].SetRange(0,0.3)
            extrapolationFunctions_mpf[mc].SetParameter(0,fitFunction.GetParameter(0))
            extrapolationFunctions_mpf[mc].SetParameter(1,fitFunction.GetParameter(1))
            extrapolationFunctions_mpf[mc].SetLineStyle(2)
            extrapolationFunctions_mpf[mc].Draw("same")

    btag_ = btag
    if btag == "":
        btag_ = "no btag"
    else:
        tex1 = ROOT.TLatex(0.7,1.07,label_algo)
        tex1.SetTextSize(0.04)
        tex1.Draw()
        if not "gluon" in label_algo and not "quark"in label_algo:
            tex2 = ROOT.TLatex(0.7,1.04,btag_match.group("wp")+" WP")
            tex2.SetTextSize(0.04)
            tex2.Draw()
    tex3 = ROOT.TLatex(0.7,1.01,zpt)
    tex3.SetTextSize(0.04)
    tex3.Draw()
    print
    print("Zpt:",zpt)
    print("btag:",btag_)
    #print "pT balance (Data) at alpha=0 :",graph_data_at0,slope_data
    #print "pT balance (MC) at alpha=0   :",graph_mc_at0,slope_mc
    #print "MPF (Data) at alpha=0        :",graph_data_mpf_at0
    #print "MPF (MC) at alpha=0          :",graph_mc_mpf_at0
    print
    
    if ratioPlot and len(ydata) > 0:
        canvas.cd(2)
        """
        ratiopad.cd()
        #ratiopad.SetFillColor(5)
        ratiopad.SetTopMargin(0)
        ratiopad.SetBottomMargin(bottomMargin*2)
        ratioRange = 0.2
        rframe = ROOT.TH2F("ratioframe","",2,xmin,xmax,2,0.9,1.099999)
        rframe.Reset()
        rframe.SetStats(0)
#        rframe.GetYaxis().SetLabelSize(labelsize)
#        rframe.GetYaxis().SetTitleOffset(frame.GetYaxis().GetTitleOffset())
#        rframe.GetYaxis().SetTitleSize(titlesize)
        rframe.GetYaxis().SetTitle("Data/MC")
#        rframe.GetXaxis().SetLabelSize(labelsize)
#        rframe.GetXaxis().SetTitleSize(titlesize)
#        rframe.GetXaxis().SetTitleOffset(xtitleoffset)
        rframe.GetXaxis().SetTitle(xtitle)
        rframe.Draw()
        """
        zeroline = ROOT.TLine(xmin,1.0,xmax,1.0)
        #zeroline.SetNDC()
        zeroline.SetLineStyle(2)
        zeroline.Draw("L")

        ratios = {}
        ratios_mpf = {}
        extrapolationFunction_r1 = {}
        extrapolationFunction_r2 = {}
        for mc in ymc.keys():
            ratios[mc] = getRatio(graph_data,graph_mc[mc])
            ratios[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
            ratios[mc].SetTitle("Data/MC")
            ratios[mc].GetXaxis().SetTitle(xtitle)
            ratios[mc].Draw("P")

            ratios_mpf[mc] = getRatio(graph_data_mpf,graph_mc_mpf[mc])
            ratios_mpf[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle()+4)
            ratios_mpf[mc].SetTitle("Data/MC")
            ratios_mpf[mc].GetXaxis().SetTitle(xtitle)
            ratios_mpf[mc].Draw("P")

            # Fitting
            #fitFunction = ROOT.TF1("linear","[0]*x+[1]",0.145,0.31)
            fitFunction.SetLineColor(2)

            if "h_alpha" in hptname:
                ratios[mc].Fit(fitFunction,"RQ")
                ratio_at0 = fitFunction.Eval(0)
                extrapolationFunction_r1[mc] = fitFunction.Clone()
                extrapolationFunction_r1[mc].SetRange(0,0.3)
                extrapolationFunction_r1[mc].SetParameter(0,fitFunction.GetParameter(0))
                extrapolationFunction_r1[mc].SetParameter(1,fitFunction.GetParameter(1))
                extrapolationFunction_r1[mc].SetLineStyle(2)
                extrapolationFunction_r1[mc].Draw("same")

                fitFunction.SetLineColor(4)
                ratios_mpf[mc].Fit(fitFunction,"RQ")
                ratio_mpf_at0 = fitFunction.Eval(0)
                extrapolationFunction_r2[mc] = fitFunction.Clone()
                extrapolationFunction_r2[mc].SetRange(0,0.3)
                extrapolationFunction_r2[mc].SetParameter(0,fitFunction.GetParameter(0))
                extrapolationFunction_r2[mc].SetParameter(1,fitFunction.GetParameter(1))
                extrapolationFunction_r2[mc].SetLineStyle(2)
                extrapolationFunction_r2[mc].Draw("same")

        """
#        ratio = graph_data.Clone("Data/MC")
#        ratio.Reset()

#        ratio = ROOT.TGraphErrors(d.GetNbinsX(),)
        d = graph2th1("d",graph_data,10,0,0.5)
        m = graph2th1("m",graph_mc,10,0,0.5)
        ratio = d.Clone("d")
        ratio.Divide(m)
        print "check size",d.GetNbinsX(),m.GetNbinsX(),ratio.GetNbinsX()
        for i in range(1,ratio.GetNbinsX()+1):
            print "ratio",ratio.GetBinCenter(i),ratio.GetBinContent(i),ratio.GetBinError(i)
        """
        """
        print "check histos",d.GetNbinsX(),m.GetNbinsX()
        ratio.Divide(graph2th1("Data",graph_data),graph2th1("MC",graph_mc),"pois")
        for i in range(0,ratio.GetN()):
            x = ROOT.Double()
            y = ROOT.Double()
            ratio.GetPoint(i,x,y)
            print "check div",i,x,y
        """
#        ratio.Draw("P")

    if not os.path.exists(outputdir):
        os.mkdir(outputdir)
    if plot:
        for ending in endings:
            #print "check",os.path.join(outputdir,histogramName+ending)
            canvas.Print(os.path.join(outputdir,histogramName+ending))
        canvas.cd(1)                                                                                                                                             
        ROOT.gPad.SetLogx()
        canvas.cd(2)
        ROOT.gPad.SetLogx()
        for ending in endings:
            canvas.Print(os.path.join(outputdir,histogramName+"_logx"+ending))

    if len(ydata) > 0:
        #print "check btag->",btag,"<-"
        match = btag_re.search(btag)        
        if not match:
            PT_BALANCE_DATA = graph_data
            PT_BALANCE_MC   = graph_mc
            MPF_DATA        = graph_data_mpf
            MPF_MC          = graph_mc_mpf
        else:
            #print "check histoname",histogramName
            pt_balance_ratio_data = getRatio(graph_data,PT_BALANCE_DATA)
            pt_balance_ratio_mc = {}
            for mc in ymc.keys():
                pt_balance_ratio_mc[mc]   = getRatio(graph_mc[mc],PT_BALANCE_MC[mc])
            mpf_ratio_data        = getRatio(graph_data_mpf,MPF_DATA)
            mpf_ratio_mc = {}
            for mc in ymc.keys():
                mpf_ratio_mc[mc]          = getRatio(graph_mc_mpf[mc],MPF_MC[mc])

            pt_balance_ratio = {}
            mpf_ratio_ratio = {}
            for mc in ymc.keys():
                pt_balance_ratio[mc]      = getRatio(pt_balance_ratio_data,pt_balance_ratio_mc[mc])
                mpf_ratio_ratio[mc]       = getRatio(mpf_ratio_data,mpf_ratio_mc[mc])

        
            ratiohistogramName = histogramName+"_bOverLightRatio"
            ytitle = ytitle+" b/light Ratio"
        
            frameRatioMain = ROOT.TH1D(ratiohistogramName+"_bOverLightRatio",";%s;%s"%(xtitle,ytitle),25,xmin,xmax)
            frameRatioMain.SetMaximum(1.1)
            frameRatioMain.SetMinimum(0.9)
            frameRatioMain.SetLabelSize(0.04,"Y")
        
            frameRatioRatio = ROOT.TH1D(ratiohistogramName+"_bOverLightRatio"+"_ratio",";%s;Data/MC"%(xtitle),25,xmin,xmax);
            frameRatioRatio.SetMaximum(1.04999)
            frameRatioRatio.SetMinimum(0.95)
            frameRatioRatio.SetLabelSize(0.04,"Y")
        
            rcanvas = ROOT.tdrDiCanvas(ratiohistogramName+"_bOverLightRatio",frameRatioMain,frameRatioRatio,4)
            rcanvas.cd(1)
            ROOT.gPad.Update()

            pt_balance_ratio_data.SetMarkerStyle(histogramStyles["Data"].getMarkerStyle())
            pt_balance_ratio_data.Draw("P")

            graphs = []
            for mc in ymc.keys():
                pt_balance_ratio_mc[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
                pt_balance_ratio_mc[mc].Draw("P")
                graphs.append(pt_balance_ratio_mc[mc])
                mpf_ratio_data.SetMarkerStyle(histogramStyles["Data"].getMarkerStyle()+4)
                mpf_ratio_data.Draw("P")

                for mc in ymc.keys():
                    mpf_ratio_mc[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle()+4)
                    mpf_ratio_mc[mc].Draw("P")
                    graphs.append(mpf_ratio_mc[mc])
                    
            ROOT.gPad.RedrawAxis()

            tex1 = ROOT.TLatex(0.05,1.05,match.group("algo"))
            tex1.Draw()
            tex2 = ROOT.TLatex(0.05,1.035,match.group("wp")+" WP")
            tex2.Draw()
            tex3 = ROOT.TLatex(0.05,1.02,zpt)
            tex3.Draw()
                
            rlegend = ROOT.TLegend(0.45,0.6,0.93,0.85)
            rlegend.SetLineColor(0)
            rlegend.SetShadowColor(0)
            rlegend.SetTextSize(0.04)
            rlegend.AddEntry(pt_balance_ratio_data,"p_{T} balance Data","p")
            for mc in ymc.keys():
                rlegend.AddEntry(pt_balance_ratio_mc[mc],"p_{T} balance %s"%histogramStyles[mc].getLabel(),"p")
            rlegend.AddEntry(mpf_ratio_data,"MPF Data","p")
            for mc in ymc.keys():
                rlegend.AddEntry(mpf_ratio_mc[mc],"MPF %s"%histogramStyles[mc].getLabel(),"p")
            rlegend.Draw()
        
            rcanvas.cd(2)
            zeroline.Draw("L")

            for mc in ymc.keys():
                pt_balance_ratio[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle())
                pt_balance_ratio[mc].Draw("P")
                graphs.append(pt_balance_ratio[mc])
                
                mpf_ratio_ratio[mc].SetMarkerStyle(histogramStyles[mc].getMarkerStyle()+4)
                mpf_ratio_ratio[mc].Draw("P")
                graphs.append(mpf_ratio_ratio[mc])

            if plot:
                for ending in endings:
                    rcanvas.Print(os.path.join(outputdir,ratiohistogramName+"_bOverLightRatio"+ending))

                
    #.,.
    #print "check keys",ymc.keys()
    #print "check histogramName",histogramName
    KEY = "MC" #"DYJetsToLL"
    subdir = os.path.basename(os.path.dirname(hname))
    if subdir == "analysis":
        subdir = ""
    nameOUT = histogramName+".root"
    if len(subdir) > 0:
        nameOUT = histogramName+"_"+subdir+".root"
    #print "check root",nameOUT,histogramName,subdir
    fOUT = ROOT.TFile.Open(os.path.join(outputdir,nameOUT),"RECREATE")
    fOUT.cd()
    
    if "h_Zpt_RpT" in hname or "h_Zpt_RMPF" in hname:
        #print "check",hname
        
        alpha = hname[hname.find("_alpha")+6:]
        alpha = alpha[:alpha.find("_")]
        eta = hname[hname.find("_eta")+4:]
        
        hNameRpt = "ptchs_zmmjet_a"+alpha
        if len(btag) > 0:
            hNameRpt = "ptchs_zmmjet_"+btag+"_a"+alpha
            hNameRpt = hNameRpt[:hname.find("_eta")-1]
        #print "check hNameRpt",hNameRpt,alpha,eta
        hNameMPF = ""
        mpf_re = re.compile("_(?P<value>RMPF\S*?)_")
        match = mpf_re.search(hname)
        #hNameMPF = "mpfchs_zmmjet_a"+alpha #hNameRpt.replace("ptchs_","mpfchs_")
        if match:
            hNameMPF = match.group("value")
            if hNameMPF == "mpf":
                hNameMPF += "chs"
            if len(btag) > 0:
                hNameMPF += "_zmmjet_"+btag+"_a"+alpha
                hNameMPF = hNameMPF[:hname.find("_eta")-1]
            else:
                hNameMPF += "_zmmjet_a"+alpha
        hNameMPF = hNameMPF.lower()
                #mpfjet1 mpfjetn mpfuncl
        #print "check hNameMPF---",hNameMPF
        if len(ydata) > 0 or len(ydata_mpf) > 0:
            fOUT.mkdir("data")
            fOUT.mkdir("data/eta%s"%eta)
            fOUT.cd("data/eta%s"%eta)
            if len(subdir) > 0:
                fOUT.mkdir("data/eta%s/%s"%(eta,subdir))
                fOUT.cd("data/eta%s/%s"%(eta,subdir))
            if "RpT" in hname:
                graph_data.Write(hNameRpt)
                hstatistics_data = hpt["data"].ProjectionX()
                hstatistics_data.SetStats(0)
                hstatistics_data.Write("statistics"+hNameRpt[hNameRpt.find("_"):])
            if "data" in hmZ:
                hmZ["data"].Write("h_Zpt_mZ")
            if "RMPF" in hname:
                graph_data_mpf.Write(hNameMPF)
                hstatistics_data = hmpf["data"].ProjectionX()
                hstatistics_data.SetStats(0)
                hstatistics_data.Write("statistics"+hNameMPF[hNameMPF.find("_"):])
#        print "check hstatistics_data","statistics_alpha%s_eta%s"%(alpha,eta)

        #print "check graph_mc_mpf",graph_mc_mpf.keys()
        fOUT.mkdir("mc")
        fOUT.mkdir("mc/eta%s"%eta)
        fOUT.cd("mc/eta%s"%eta)
        if len(subdir) > 0:
            fOUT.mkdir("mc/eta%s/%s"%(eta,subdir))
            fOUT.cd("mc/eta%s/%s"%(eta,subdir))
        if KEY in graph_mc:
            #print "check graph_mc WRITE",KEY
            graph_mc[KEY].Write(hNameRpt)
        if KEY in graph_mc_mpf:
            #print "check write hNameMPF",KEY
            graph_mc_mpf[KEY].Write(hNameMPF)
        if KEY in hmZ:
            hmZ[KEY].Write("h_Zpt_mZ")
        if KEY in hmZgen:
            hmZgen[KEY].Write("h_Zpt_mZgen")
        if len(graph_bpurity) > 0:
            graph_bpurity[KEY].Write("bpurity"+hNameRpt[hNameRpt.find("_"):])
        if len(graph_cpurity) > 0:
            graph_cpurity[KEY].Write("cpurity"+hNameRpt[hNameRpt.find("_"):])
        if len(graph_gpurity) > 0:
            graph_gpurity[KEY].Write("gpurity"+hNameRpt[hNameRpt.find("_"):])
        if len(graph_udspurity) > 0:
            graph_udspurity[KEY].Write("udspurity"+hNameRpt[hNameRpt.find("_"):])
#        hstatistics = ROOT.TH1F("statistics","statistics",graph_stat[KEY].GetN(),xmin,xmax)
#        entries = 0
#        for i in range(graph_stat[KEY].GetN()):
#            x = ROOT.Double()
#            y = ROOT.Double()
#            graph_stat[KEY].GetPoint(i,x,y)
#            hstatistics.Fill(x-1.0,y)
#            hstatistics.SetBinError(i+1,math.sqrt(y))
#            entries += y
#            #print "check point",i,x,y
#        hstatistics.SetEntries(entries)
#        hstatistics = graph2th1("statistics",graph_stat[KEY],graph_stat[KEY].GetN(),xmin,xmax)
        if KEY in hpt:
            hstatistics_mc = hpt[KEY].ProjectionX()
            hstatistics_mc.SetStats(0)
            hstatistics_mc.Write("statistics"+hNameRpt[hNameRpt.find("_"):])
        if KEY in hmpf:
            hstatistics_mc = hmpf[KEY].ProjectionX()
            hstatistics_mc.SetStats(0)
            hstatistics_mc.Write("statistics"+hNameMPF[hNameMPF.find("_"):])

        if len(ydata) > 0:
            fOUT.mkdir("ratio")
            fOUT.mkdir("ratio/eta%s"%eta)
            fOUT.cd("ratio/eta%s"%eta)
            ratios[KEY].Write(hNameRpt)
            ratios_mpf[KEY].Write(hNameMPF)
    else:
        fOUT.mkdir("extra")
        fOUT.cd("extra")
        if len(ydata) > 0:
            fOUT.mkdir("extra/data")
            fOUT.cd("extra/data")
            graph_data.Write(hptname)
            graph_data_mpf.Write(hmpfname)
            hstatistics = hpt["data"].ProjectionX()
            hstatistics.Write("statistics"+histogramName[12:])
        fOUT.mkdir("extra/mc")
        fOUT.cd("extra/mc")
        if KEY in graph_mc:
            graph_mc[KEY].Write(hptname)
            graph_mc_mpf[KEY].Write(hmpfname)
            graph_bpurity[KEY].Write("purity"+histogramName[12:])
            printGraph("purity",graph_bpurity[KEY])

            hstatistics = graph2th1("statistics",graph_stat[KEY],graph_stat[KEY].GetN(),xmin,xmax)
            hstatistics_mc = hpt[KEY].ProjectionX()
            hstatistics_mc.Write("statistics"+histogramName[12:])
        
    fOUT.Close()

import math
def getRatio(graph1,graph2):
    nr = graph1.GetN()
    xr = []
    yr = []
    xrerr = []
    yrerr = []
    for i in range(0,nr):
        x = ROOT.Double()
        y1 = ROOT.Double()
        y2 = ROOT.Double()
        
        graph1.GetPoint(i,x,y1)
        graph2.GetPoint(i,x,y2)

        yerr1 = graph1.GetErrorY(i)
        yerr2 = graph2.GetErrorY(i)

        xr.append(float(x))
        ratio = 0
        if not y2 == 0:
            ratio = float(y1)/float(y2)
        yr.append(ratio)
        xrerr.append(0)
        ratioerr = 0
        #print "check ratio",y1,y2
        if y1>0.0001 and y2>0.0001:
            ratioerr = math.sqrt(yerr1*yerr1/(y1*y1) + yerr2*yerr2/(y2*y2))
        yrerr.append(ratioerr)
        #print "check ratio",i,x,ratio,ratioerr
    return ROOT.TGraphErrors(nr,array.array('d',xr),array.array('d',yr),array.array('d',xrerr),array.array('d',yrerr))

def graph2th1(name,graph,N,xmin,xmax):
    h = ROOT.TH1F(name,"",N,xmin,xmax)
    for i in range(0,N):
        x = ROOT.Double()
        y = ROOT.Double()
        graph.GetPoint(i, x, y);
        yerr = graph.GetErrorY(i)
        #print "check graph2th1",i,x,y,yerr
        h.Fill(x,y);
        h.SetBinError(i,yerr)
    return h.Clone(name)

def printGraph(name,graph):
    print("graph",name,"entries=",graph.GetN())
    for i in range(0,graph.GetN()):
        x = ROOT.Double()
        y = ROOT.Double()
        graph.GetPoint(i, x, y);
        print("    ",i,x,y)
                                                                                                                                        
def setStyles(datasets):
    for d in datasets:
        if d.name in histogramStyles.keys():
            d.style = histogramStyles[d.name]

def reorderDatasets(datasets):
    #for d in datasets:
    #    print d.name
    return datasets

def main():

    if len(sys.argv) == 1:
        usage()
        sys.exit()

    multicrabdirs = sys.argv[1:]
    for multicrabdir in multicrabdirs:
        if not os.path.exists(multicrabdir) or not os.path.isdir(multicrabdir):
            usage()
        else:
            print(multicrabdir)

    whitelist = []
#    whitelist = ["DoubleMuon_Run2016G","DoubleMuon_Run2016H","JetsToLL_M_50"]
    whitelist = ["DoubleMuon_Run2018A","DY1JetsToLL_M_50"]
    blacklist = [] #"DY2","DY3","DY4"]
#    blacklist = ["DY1","DY2","DY3","DY4"]
#    blacklist = ["DoubleMuon_Run2017B"]
#    blacklist = ["herwig","DYB"]
#    blacklist = ["DYB"]
#    blacklist = ["DY1","DY2","DY3","DY4","DYB"]
#    blacklist = ["DY1","DY2","DY3","DY4","herwig"]
#    blacklist = ["herwig"]
#    blacklist = ["TTJets"]

    datasets = getDatasetsMany(multicrabdirs,whitelist=whitelist,blacklist=blacklist)
#    for d in datasets:
#        print d.name
#    print "-------------"
#    datasets = removeDatasets("SingleMuon_Run2017C_31Mar2018_v1_299368_302029",datasets)

    datasets = read(datasets)
    #print "check read",len(datasets)
    datasets = mergeExtDatasets(datasets)
    #print "check mergeExtDatasets",len(datasets)
    datasets = normalizeToLumi(datasets)
    #print "check normalizeToLumi",len(datasets)
    datasets = mergeDatasets("Data","_Run201\d\S_",datasets)
    #datasets = mergeDatasets("DYJetsToLL_M_50","DYJetsToLL_M_50_HT\S+",datasets)
    #datasets = mergeDatasets("DYNJetsToLL_M_50","DY\dJetsToLL_M_50$",datasets)
#    datasets = mergeDatasets("DYJetsToLL_M_50_Herwig","DYJetsToLL_M_50_herwig",datasets)
    #datasets = mergeDatasets("DYBJetsToLL_M_50","DYBJetsToLL_M_50",datasets)
#    datasets = mergeDatasets("DYJetsToLL_M_50","DY\SJetsToLL_M_50",datasets)
#    datasets = mergeDatasets("DYJetsToLL_M_50","DYJetsToLL_M_50",datasets)
#    datasets = mergeDatasets("DYJetsToLL","DY\dJetsToLL",datasets)
    #datasets = mergeDatasets("SingleTop","ST_\S+",datasets)
    #datasets = mergeDatasets("WJetsToLNu","WJetsToLNu_\S+",datasets)

#    datasets = mergeDatasets("MC","^DY",datasets)
    datasets = mergeDatasets("MC","^(?!Data).*",datasets)
#    datasets = mergeDatasets("MC","DY\dJetsToLL",datasets)
    #print "check mergeDatasets",len(datasets)

    datasets = reorderDatasets(datasets)
    #print "check reorderDatasets"
    
    for d in datasets:
        print(d.name,d.isData())
#    sys.exit()
#    import tdrstyle
#    style = tdrstyle.TDRStyle()

    ROOT.gROOT.LoadMacro("tdrstyle_mod14.C+")
    ROOT.setTDRStyle()

    ROOT.extraText = "Simulation";
    
    setStyles(datasets)
    #print "check datasets",len(datasets)
#    histogramNames = datasets[0].histo.keys()
#    #print "check histogram names",histogramNames
#    for hname in histogramNames:
#        label = hname
#        if hname in labels.keys():
#            label = labels[hname]
##        plot(datasets,hname,label,"Events",ratioPlot=True,logy=True)

#    jet_responce_re = re.compile("h_alpha_RpT")
#
#    for h in datasets[0].histo.keys():
#        match = jet_responce_re.search(h)
#        if match:
#            print h
#    sys.exit()
#    for h in datasets[0].histo.keys():
#        match = jet_responce_re.search(h)
#        if match:
#            plotJES(datasets,"jet_response",h,"p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
#
#    sys.exit()

#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt30_50","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt30_50_btagCSVV2loose","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt30_50_btagCSVV2medium","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt30_50_btagCSVV2tight","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)

#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt50_70","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt50_70_btagCSVV2loose","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt50_70_btagCSVV2medium","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt50_70_btagCSVV2tight","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)

#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt70_100","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt70_100_btagCSVV2loose","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt70_100_btagCSVV2medium","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt70_100_btagCSVV2tight","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)


#    plotJES(datasets,"jet_response","analysis/h_alpha_RpT_Zpt100_140","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)   
#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt100_140_btagCSVV2loose","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt100_140_btagCSVV2medium","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
#    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt100_140_btagCSVV2tight","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)


    """
    plotJES(datasets,"jet_response","h_Zpt_RpT_alpha10","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_alpha15","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_alpha20","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_alpha25","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_alpha30","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_alpha40","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_alpha50","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_alpha60","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_alpha80","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_alpha100","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_alpha120","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    """
#    plotJES(datasets,"jet_response",os.path.join("analysis","h_Zpt_RpT_alpha30_eta_00_25"),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=True)
#    plotJES(datasets,"jet_response",os.path.join("analysis","h_Zpt_RpT_btagDeepBtight_alpha30_eta_00_25"),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=True)
#    plotJES(datasets,"jet_response",os.path.join("analysis","h_Zpt_RMPFjet1_alpha30_eta_00_25"),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)

    for folder in {"analysis","analysis/PSWeight0","analysis/PSWeight1","analysis/PSWeight2","analysis/PSWeight3"}:
#        for alpha in {25}:
        for alpha in {10,15,20,25,30,40,50,60,80,100,120}:
            for eta in {"00_13","13_19","19_25","00_25"}:
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RpT_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=True)

                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RpT_btagDeepBtight_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=True)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RpT_btagDeepCtight_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=True)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RpT_quarktag_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=True)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RpT_gluontag_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=True)
                
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPF_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFjet1_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFjetn_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFuncl_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)

                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPF_btagDeepBtight_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFjet1_btagDeepBtight_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFjetn_btagDeepBtight_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFuncl_btagDeepBtight_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)

                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPF_btagDeepCtight_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFjet1_btagDeepCtight_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFjetn_btagDeepCtight_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFuncl_btagDeepCtight_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)

                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPF_quarktag_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFjet1_quarktag_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFjetn_quarktag_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFuncl_quarktag_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)

                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPF_gluontag_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFjet1_gluontag_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFjetn_gluontag_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
                plotJES(datasets,"jet_response",os.path.join(folder,"h_Zpt_RMPFuncl_gluontag_alpha%s_eta_%s"%(alpha,eta)),"p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)

    """
    plotJES(datasets,"jet_response","h_Zpt_RpT_btagCSVV2loose_alpha10_eta19","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_btagCSVV2medium_alpha10_eta19","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_btagCSVV2tight_alpha10_eta19","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)

    plotJES(datasets,"jet_response","h_Zpt_RpT_btagCSVV2loose_alpha10_eta25","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_btagCSVV2medium_alpha10_eta25","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)
    plotJES(datasets,"jet_response","h_Zpt_RpT_btagCSVV2tight_alpha10_eta25","p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False,plot=False)



    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt140_200","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt140_200_btagCSVV2loose","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt140_200_btagCSVV2medium","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt140_200_btagCSVV2tight","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)

    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt200_Inf","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt200_Inf_btagCSVV2loose","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt200_Inf_btagCSVV2medium","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
    plotJES(datasets,"jet_response","h_alpha_RpT_Zpt200_Inf_btagCSVV2tight","p_{T}^{Jet2}/p_{T}^{Z}","Jet Response",ratioPlot=True,logy=False)
    """
####    plotPTZvsNev(datasets,"PTZvsNev","analysis/Zpt","p_{T}^{Z} (GeV)","N events",ratioPlot=False,logy=True)

    plotHisto(datasets,"analysis/jet1pt","p_{T}^{leading jet} (GeV)","N events",ratioPlot=True,logy=True)

    #    h_alpha_RpT_btagCSVV2loose_Zpt100_140
    print("Outputdir:",outputdir)
    
if __name__ == "__main__":
    main()
