#!/usr/bin/env python3

import os
import sys
import re
import subprocess

from optparse import OptionParser

import ROOT

def usage():
    print
    print("### Usage:  ",os.path.basename(sys.argv[0]),"<multicrab|multicrab processed>")
    print

def printFile(filename):
    counters = {}
    fIN = ROOT.TFile.Open(filename,'read')
    keys = fIN.GetListOfKeys()
    for key in keys:
        if 'counter' in key.GetName():
            counters[key.GetName()] = fIN.Get(key.GetName())
    printing(counters)

def printDatasets(datasets):
    counters = {}
    for d in datasets:
#        fRF = ROOT.TFile.Open(os.path.join(d.multicrabdir,d.name,"results","histograms.root"))
        cHisto = d.counters #fRF.Get("configInfo/unweighted counters").Clone("unweighted")
        #cHisto.SetDirectory(0)
        #print "check cHisto",d.name,cHisto.GetEntries()
        counters[d.name] = cHisto
        #fRF.Close()
    printing(counters)

def printing(counters):
    # sort
    counternames = []
    if "Data" in counters.keys():
        counternames.append("Data")
    for k in counters.keys():
        if k not in counternames:
            counternames.append(k)

    print
    line = " "*29
    run_re = re.compile(r"_(?P<run>Run20\S+?)_")
    for k in counternames:

        match = run_re.search(k)

        if match:
            line += "{:>16.15}".format(match.group("run"))
        else:
            line += "{:>16.15}".format(k)
    line += "\n"

    sys.stdout.write(line)
        
    for i in range(1,counters[counternames[0]].GetNbinsX()+1):
        line = "    "
        if len(counters[counternames[0]].GetXaxis().GetBinLabel(i)) > 0:
            line += "{:25.24}".format(counters[counternames[0]].GetXaxis().GetBinLabel(i))
            for n in counternames:
                line += str("{:16.1f}".format(counters[n].GetBinContent(i)))
        line += "\n"
        sys.stdout.write(line)
    
def main(opts,args):

    if len(sys.argv) == 1:
        usage()
        sys.exit()

    if os.path.isfile(sys.argv[1]):
        printFile(sys.argv[1])
        sys.exit()

    multicrabdir = sys.argv[1]
    if not os.path.exists(multicrabdir) or not os.path.isdir(multicrabdir):
        usage()
        sys.exit()

    blacklist = []
    whitelist = []
    if opts.includeTasks != 'None':
        whitelist.extend(opts.includeTasks.split(','))
    if opts.excludeTasks != 'None':
        blacklist.extend(opts.excludeTasks.split(','))

    if 'processed' in multicrabdir:
        from plot_v3 import Dataset,getDatasets,read,mergeExtDatasets,mergeDatasets,reorderDatasets

        datasets = getDatasets(multicrabdir,whitelist=whitelist,blacklist=blacklist)
        datasets = read(datasets)
        datasets = mergeExtDatasets(datasets)
        if not opts.no_merge:
            datasets = mergeDatasets("Data",r"_Run20\d\d\S_",datasets)
        #    datasets = mergeDatasets("DYJetsToLL_M_50","DY\S+",datasets)
        datasets = reorderDatasets(datasets)
    
        printDatasets(datasets)
    else:
        from analyse import Dataset,getDatasets
        opts.includeTasks = whitelist
        opts.excludeTasks = blacklist
        datasets = getDatasets(multicrabdir,opts)
        for d in datasets:
            d.PrintCounters()

if __name__ == "__main__":
    parser = OptionParser(usage="Usage: %prog [options]")
    parser.add_option("-i", "--includeTasks", dest="includeTasks", default="None", type="string",
                      help="Only perform action for this dataset(s) [default: \"\"]")
    parser.add_option("-e", "--excludeTasks", dest="excludeTasks", default="None", type="string",
                      help="Exclude this dataset(s) from action [default: \"\"]")
    parser.add_option("-n", "--no-merge", dest="no_merge", default=False, action="store_true",
                      help="Don't merge datasets [default: 'False']")
    (opts, args) = parser.parse_args()

    main(opts, args)

