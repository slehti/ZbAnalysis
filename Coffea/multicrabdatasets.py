#!/usr/bin/env python

import os
import sys
import re
import subprocess
import json

import uproot

import ROOT

root_re = re.compile("(?P<rootfile>([^/]*events_\d+\.root))")

class Dataset:
    def __init__(self,path,run):
        self.run = run
        self.lumi = 0
        self.files = []
        if isinstance(path,list):
            self.name = path[0]
            self.files = path
            self.isMulticrab = False
        else:
            self.name = os.path.basename(path)
            cands = execute("ls %s"%os.path.join(path,"results"))
            for c in cands:
                match = root_re.search(c)
                if match:
                    self.files.append(os.path.join(path,"results",match.group("rootfile")))
            self.isMulticrab = True
        self.isData = False
        if "Run201" in self.name:
            self.isData = True

        #print(self.name,len(self.files),self.isData)

        if len(self.files) == 0:
            print("Dataset contains no root files")
            return

        self.histograms = {}
        if isinstance(path,list):
            self.fPU = None
        else:
            if self.isData:
                self.fPU = os.path.join(path,"results","PileUp.root")
            else:
                self.fPU = os.path.join(path,"results",self.files[0])
        """
        if self.isData:
            self.fPU = ROOT.TFile.Open(os.path.join(path,"results","PileUp.root"))
            self.pileup = self.fPU.Get("pileup").Clone("pileup")
            self.pileup.SetDirectory(0)
            #print "check pu data",self.pileup.GetEntries()                                                                                                     
            #for i in range(1,self.pileup.GetNbinsX()):                                                                                                         
            #    print "check pu bin ",i,self.pileup.GetBinContent(i)                                                                                           
            self.fPU.Close()
        else:
            self.fPU = ROOT.TFile.Open(os.path.join(path,"results",self.files[0]))
            self.pileup = self.fPU.Get("configInfo/pileup").Clone("pileup_mc")
            self.pileup.SetDirectory(0)
            self.pileup.Reset()
            self.fPU.Close()
            for fname in self.files:
                f = ROOT.TFile.Open(os.path.join(path,"results",fname))
                h = f.Get("configInfo/pileup").Clone("pileup")
                self.pileup.Add(h)
                f.Close()

        obj = self.pileup.Clone("pileup")
        obj.SetTitle("skim")
        self.histograms["pileup"] = obj
        """
        self.fRF = ROOT.TFile.Open(self.files[0])
        self.skimCounter = self.fRF.Get("configInfo/SkimCounter").Clone("SkimCounter")
        self.skimCounter.Reset()
        for fname in self.files:
            rf = ROOT.TFile.Open(fname)
            s = rf.Get("configInfo/SkimCounter").Clone("SkimCounter")
            self.skimCounter.Add(s)
            rf.Close()
        obj = self.skimCounter.Clone("skimCounter")
        obj.SetTitle("skim")
        self.histograms["skimCounter"] = obj

    def getPileup(self):
        fPU = self.getPileupfile()
        if fPU is None:
            return None

        with uproot.open(fPU) as fIN:
            if self.isData:
                return fIN['pileup']
            else:
                return fIN['configInfo/pileup']
        return None

#        return self.histograms["pileup"]

    def getPileupfile(self):
        return self.fPU

    def getFileNames(self):
        return self.files

    def getSkimCounter(self):
        return self.skimCounter

    def isMulticrabData(self):
        return self.isMulticrab

    def Print(self):
        print( self.name )
        print( "    is data",self.isData )
        print( "    number of files",len(self.files) )

def execute(cmd):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
    (s_in, s_out) = (p.stdin, p.stdout)

    f = s_out
    ret=[]
    for line in f:
        line = str(line,'utf-8')
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

def getRun(datasetnames):
    run = ""
    run_re = re.compile("(?P<run>Run201\d)")
    for n in datasetnames:
        match = run_re.search(n)
        if match:
            run = match.group("run")
    return run

def getDatasets(multicrabdir,whitelist=[],blacklist=[]):
    datasets = []

    # if rootfiles instead of multicrabdir
    if isinstance(multicrabdir, list):
        run = getRun(multicrabdir)
        datasets.append(Dataset(multicrabdir,run))
        print(run,datasets)
        return datasets

    cands = execute("ls %s"%multicrabdir)

    if len(whitelist) > 0:
        #print "check whitelist 1 ",whitelist,blacklist                                                                                                         
        datasets_whitelist = []
        for d in cands:
            for wl in whitelist:
                wl_re = re.compile(wl)
                match = wl_re.search(d)
                if match:
                    datasets_whitelist.append(d)
                    break
        #print "check whitelist",datasets_whitelist                                                                                                             
        cands = datasets_whitelist

    if len(blacklist) > 0:
        #print "check blacklist 1 ",whitelist,blacklist                                                                                                         
        datasets_blacklist = []
        for d in cands:
            found = False
            for bl in blacklist:
                bl_re = re.compile(bl)
                match = bl_re.search(d)
                if match:
                    found = True
                    break
            if not found:
                datasets_blacklist.append(d)
        cands = datasets_blacklist

    run = getRun(cands)

    for c in cands:
        resultdir = os.path.join(multicrabdir,c,"results")
        if os.path.exists(resultdir):
            datasets.append(Dataset(os.path.join(multicrabdir,c),run))

    return datasets

def getDataPileupROOT(datasets):
    pileup_data = ROOT.TH1F("pileup_data","",100,0,100)
    for d in datasets:
        if d.isData:
            if hasattr(d, 'pileup'):
                pileup_data.Add(d.pileup)
    return pileup_data

def getDataPileup(datasets):
    hMC = None
    for d in datasets:
        if d.isData:
            fPU = d.getPileupfile()
            with uproot.open(fPU) as fIN:
                if hMC == None:
                    hMC = fIN['pileup'].values()
                else:
                    hMC.sum(fIN['pileup'].values())
                #print("check pileup",hMC)
    return hMC

def getDataPileupMulticrab(multicrabdir):
    if isinstance(multicrabdir,list):
        return None
    with uproot.open(os.path.join(multicrabdir,"pileup.root")) as fIN:
        return fIN['pileup']
    return None
    """
    print("check getDataPileupMulticrab",os.path.join(multicrabdir,"pileup.root"))
    fIN = ROOT.TFile.Open(os.path.join(multicrabdir,"pileup.root"))
    pileup_data = fIN.Get("pileup").Clone("pileup_data")
    pileup_data.SetDirectory(0)
    print("check pu data histo",pileup_data)
    return pileup_data
    """
def loadLuminosity(multicrabdir,datasets):
    lumisum = 0
    if isinstance(multicrabdir,list):
        return lumisum

    lumijson = open(os.path.join(multicrabdir,"lumi.json"),'r')
    data = json.load(lumijson)
    for d in datasets:
        if d.name in data.keys():
            d.lumi = data[d.name]
            lumisum += d.lumi
    lumijson.close()
    return lumisum

def getYear(multicrabdir):
    thepath = multicrabdir
    if isinstance(multicrabdir,list):
        thepath = multicrabdir[0]

    year_re = re.compile("Run(?P<year>201\d)\S+_")
    match = year_re.search(thepath)
    if match:
        return match.group("year")

    print("Searching for year in",thepath,". Not found. Used format: Run201\d\S+_")
    return "-1"
