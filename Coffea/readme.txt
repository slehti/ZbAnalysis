python -m venv VirtualTestEnvironment
source VirtualTestEnvironment/bin/activate
pip install wheel
pip install xrootd
pip install numpy==1.20
pip install coffea

python test.py











singularity run -B ${PWD}:/work /cvmfs/unpacked.cern.ch/registry.hub.docker.com/coffeateam/coffea-dask:latest sh /work/test.sh

singularity build --sandbox coffea_test docker-archive://coffea_test.tar
singularity run -B ${PWD}:/work coffea_test sh /work/test.sh
