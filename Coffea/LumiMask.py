import json
import itertools

class LumiMask():
    def __init__(self,name,isdata):
        self.isdata = isdata
        if isdata:
            self.LoadJSON(name)

    def passed(self,event):
        if not self.isdata:
            return [True]*len(event)

        run = event.run
        lumi = event.luminosityBlock

        value = [str(i) in self.lumidata.keys() and (j in itertools.chain.from_iterable(range(self.lumidata[str(i)][k][0],self.lumidata[str(i)][k][1]+1) for k in range(len(self.lumidata[str(i)])))) for i,j in zip(run,lumi)]

        return value

    def LoadJSON(self,fname):
        f = open(fname)
        self.lumidata = json.load(f)
        f.close()
