#!/usr/bin/env python

import sys
import os
import datetime
import numpy

import awkward as ak
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema
from coffea import processor, hist
from coffea import lookup_tools
from coffea import analysis_tools

import multicrabdatasets
import ZbSelection as selection
import hist2root
import PileupWeight
from parsePileUp import *
import LumiMask

import ROOT

#LEPTONFLAVOR = 11
LEPTONFLAVOR = 13


class Analysis(processor.ProcessorABC):
    def __init__(self,year,isData, pu_data, pu_mc):
        self.year = year
        self.isData = isData

#        if isData:
#            parsePileUpJSON2(year)

        if not isData:
            self.pu_data = pu_data
            self.pu_mc   = pu_mc
            self.pu_weight = PileupWeight.PileupWeight(pu_data,pu_mc)

        maskname = "../NanoAODSkim/data/Cert_294927-306462_13TeV_UL2017_Collisions17_GoldenJSON.txt"
        self.lumimask = LumiMask.LumiMask(maskname,self.isData)

        self.book_histograms()
        self.first = True

    def book_histograms(self):
        self.histograms = {}
        self.histograms['counter'] = processor.defaultdict_accumulator(int)

        self.addHistogram('Z_mass', 60, 60, 120)
        if not self.isData:
            self.addHistogram('pu_orig', 100, 1, 100)
            self.cloneHistogram('pu_orig', 'pu_corr')
            self.cloneHistogram('pu_orig', 'pu_data')

        self._accumulator = processor.dict_accumulator(self.histograms)

    def addHistogram(self, name, nbins, binmin, binmax):
        self.histograms[name] = hist.Hist(
            "Events",
            hist.Bin("value", name, nbins, binmin, binmax)
        )

    def cloneHistogram(self, nameOrig, nameClone):
        self.histograms[nameClone] = self.histograms[nameOrig].copy()

    def getArrays(self, histo):
        x = []
        axis  = histo.axis()
        edges = axis.edges()
        for i in range(0,len(edges)-1):
            bincenter = edges[i] + 0.5*(edges[i+1]-edges[i])
            x.append(bincenter)
        y = histo.values()
        return ak.from_numpy(numpy.array(x)),y

    @property
    def accumulator(self):
        return self._accumulator

    def process(self, events):
        out = self.accumulator.identity()

        out['counter']['all events'] += len(events.event)

#        eweight = analysis_tools.Weights(len(events),storeIndividual=True)
#        genw = numpy.ones_like(events['event']) if self.isData else events['genWeight']
#        eweight.add('pileup',genw)
#
#        pu = numpy.ones_like(events['event'])
#        eweight.add('pileup',pu)
#        print("check eweight",eweight.weight())
#
#        out['counter']['event weights'] += len(events.event)

        events = events[selection.triggerSelection(events, self.year, LEPTONFLAVOR)]
        out['counter']['passed trigger'] += len(events.event)

        if self.isData:
            events = events[selection.lumimask(events,self.lumimask)] 
        out['counter']['JSON filter'] += len(events.event)

        events = events[selection.METCleaning(events, self.year)]
        out['counter']['MET cleaning'] += len(events.event)

        events = events[selection.leptonSelection(events, LEPTONFLAVOR)]
        out['counter']['lepton selection'] += len(events.event)

#        print("check1",events.Muon.pt)
#        # official version of rochester
#        #RochesterCorr https://github.com/bfis/coffea/commit/69019e03a98b28e501eb6fb86924af554bf76b01#
#        PWD = os.getcwd()
#        ROOT.gROOT.ProcessLine(f".L {PWD}/{cfname}")
#        roccor = ROOT.RoccoR(f"{PWD}/{txtname}")
#        #RochesterCorr https://github.com/bfis/coffea/commit/69019e03a98b28e501eb6fb86924af554bf76b01#

#        rochester_data = lookup_tools.txt_converters.convert_rochester_file(
#            "roccor.Run2.v5.tgz", loaduncs=True
#            "RoccoR2018.txt.gz", loaduncs=True
#        )
#        rochester = lookup_tools.rochester_lookup.rochester_lookup(rochester_data)
#        data_k = rochester.kScaleDT(
#            events.Muon.charge, events.Muon.pt, events.Muon.eta, events.Muon.phi
#        )
#        data_k = np.array(ak.flatten(data_k))

        Zboson = selection.Zboson(events,LEPTONFLAVOR)
        events = events[selection.Zpt(Zboson,events)]
        out['counter']['Z pt'] += len(events.event)

        jets = selection.jets(events)

        # Weights
        eweight = analysis_tools.Weights(len(events),storeIndividual=True)
        if not self.isData:
            genw = events['genWeight'] #numpy.ones_like(events['event']) #if self.isData else events['genWeight']
            eweight.add('pileup',genw)

        if not self.isData:
            pu = self.pu_weight.getWeight(events.Pileup.nTrueInt)
            eweight.add('pileup',pu)

        mu = 0
        if self.isData:
            pass
            #print("check run,lumi",events.run[:10],events.luminosityBlock[:10])
            #mu = getAvgPU(events.run,events.luminosityBlock)
            #print("check mu",mu[:10])
        else:
            mu = events.Pileup.nTrueInt

        # Plotting
#        plotResponce(events,jets,out)
        Zboson = selection.Zboson(events,LEPTONFLAVOR) # making the Z again using the events after all cuts

        out['Z_mass'].fill(value=Zboson.mass,weight=eweight.weight())
        if not self.isData:
            out['pu_orig'].fill(value=mu)
            out['pu_corr'].fill(value=mu,weight=eweight.weight())
            if self.first:
                x,y = self.getArrays(self.pu_data)
                out['pu_data'].fill(value=x,weight=y)
            self.first = False
        return out

    def postprocess(self, accumulator):
        return accumulator

def usage():
    print
    print( "### Usage:  ",os.path.basename(sys.argv[0]),"<multicrab skim>" )
    print

def printCounters(accumulator):
    print("    Counters")
    for k in accumulator['counter'].keys():
        counter = "     "+k
        while len(counter) < 25:
            counter+=" "
        counter +="%s"%accumulator['counter'][k]
        print(counter)

def main():

    multicrabdir = os.path.abspath(sys.argv[1])
#    if not os.path.exists(multicrabdir):
    if len(sys.argv) == 1:
        usage()
        sys.exit()

    if not os.path.isdir(multicrabdir):
        multicrabdir = sys.argv[1:]
    year = multicrabdatasets.getYear(multicrabdir)

    starttime = datetime.datetime.now().strftime("%Y%m%dT%H%M")
    lepton = "_"

    blacklist = []
    whitelist = []

    if LEPTONFLAVOR == 11:
        lepton+="Electron"
        whitelist = ["DoubleEG","EGamma","DYJets","TTJet"]

    if LEPTONFLAVOR == 13:
        lepton+="Muon"
#        whitelist = ["DoubleMuon","SingleMuon_Run2017H","DYJets","TTJet"]
        whitelist = ["DoubleMuon"]
#        whitelist = ["DYJets"]

    datasets = multicrabdatasets.getDatasets(multicrabdir,whitelist=whitelist,blacklist=blacklist)
    pileup_data = multicrabdatasets.getDataPileupMulticrab(multicrabdir)
    lumi = multicrabdatasets.loadLuminosity(multicrabdir,datasets)

    outputdir = os.getcwd()
    if not isinstance(multicrabdir,list):
        outputdir = os.path.basename(os.path.abspath(multicrabdir))+"_processed"+starttime+lepton
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)

    import time
    t0 = time.time()

    for i,d in enumerate(datasets):
        print("Dataset %s/%s %s"%(i+1,len(datasets),d.name))
        print("isdata,pu",d.isData,pileup_data)
        #if d.isData:
        #    continue
        if d.isMulticrabData():
            subdir = os.path.join(outputdir,d.name,"results")
            if not os.path.exists(subdir):
                os.makedirs(subdir)
        else:
            subdir = outputdir

        samples = {d.name: d.getFileNames()}

        result = processor.run_uproot_job(
            samples,
            "Events",
            Analysis(year,d.isData,pileup_data,d.getPileup()),
            processor.iterative_executor,
            {"schema": NanoAODSchema},
        )

        fOUT = ROOT.TFile.Open(os.path.join(subdir,"histograms.root"),"RECREATE")
        fOUT.cd()
        for key in result.keys():
            histo = hist2root.convert(result[key]).Clone(key)
            histo.Write()
        fOUT.Close()

        printCounters(result)

    dt = time.time()-t0

    print("Processing time %s min %s s"%(int(dt/60),int(dt%60)))
    print("output in",outputdir)

if __name__ == "__main__":
    main()
    #os.system("ls -lt")
    #os.system("pwd")
