#!/usr/bin/env python

import os,sys
import ROOT
import array
import statistics

from optparse import OptionParser

runmin = 379000
runmax = 387000
ymin = -0.1
ymax = 0.1

marker_size = 0.25

def p2g(profile):
    nbinsx = profile.GetNbinsX()
    xmin = profile.GetBinLowEdge(profile.GetMinimumBin())
    xmax = profile.GetBinLowEdge(profile.GetMaximumBin())
    print(nbinsx,xmin,xmax)
    x = []
    y = []
    dx = []
    dy = []
    for i in range(nbinsx):
        xval = profile.GetBinCenter(i)
        yval = profile.GetBinContent(i)
        xerr = 0
        yerr = profile.GetBinError(i)
        if yval > 0:
            x.append(xval)
            y.append(yval)
            dx.append(xerr)
            dy.append(yerr)
            print(xval,yval,yerr)
    graph = ROOT.TGraphErrors(len(x),array.array("d",x),array.array("d",y),array.array("d",dx),array.array("d",dy))
    return graph

def scale(graph):
    x = []
    y = []
    N = graph.GetN()
    for i in range(N):
        x.append(graph.GetPointX(i))
        y.append(graph.GetPointY(i))

    mean_y = statistics.mean(y)
    print("mean",mean_y)
    yscaled = []
    for i in range(N):
        yscaled.append(y[i]-mean_y)
        print("y",y[i],yscaled[i])

    return ROOT.TGraph(N,array.array("d",x),array.array("d",yscaled))

def main(opts, args):

    frame = ROOT.TH2F("frame","",100,runmin,runmax,100,ymin,ymax)
    frame.SetStats(0)
    frame.GetXaxis().SetTitle("Run")
    frame.GetXaxis().SetNoExponent()
    frame.GetXaxis().SetNdivisions(9);
    frame.GetXaxis().SetLabelFont(42);
    frame.GetXaxis().SetLabelOffset(0.002);
    frame.GetXaxis().SetLabelSize(0.02);
    frame.GetXaxis().SetTitleSize(0.06);
    frame.GetXaxis().SetTitleOffset(1);
    frame.GetXaxis().SetTitleFont(42);
    frame.GetYaxis().SetTitle("Diff to mean value")
    frame.GetYaxis().SetLabelFont(42);
    frame.GetYaxis().SetTitleSize(0.06);
    frame.GetYaxis().SetTitleOffset(0.5);
    frame.GetYaxis().SetTitleFont(42);
    frame.Draw()


    rootfile = args[0]
    fIN = ROOT.TFile.Open(rootfile)
    p_nhf = fIN.Get("data/nhf_run_zpt30")
    g_nhf = p2g(p_nhf)
    g_nhf = scale(g_nhf)
    g_nhf.SetMarkerStyle(20)
#    ci = ROOT.TColor.GetColor("#000099")
    g_nhf.SetMarkerColor(8)
    g_nhf.SetMarkerSize(marker_size)
#    g_nhf.Scale(1./0.1)

    p_chf = fIN.Get("data/chf_run_zpt30")
    g_chf = p2g(p_chf)
    g_chf = scale(g_chf)
    g_chf.SetMarkerColor(2)
    g_chf.SetMarkerStyle(20)
    g_chf.SetMarkerSize(marker_size)
    #g_chf.Scale(1./0.65)

    p_nef = fIN.Get("data/nef_run_zpt30")
    g_nef = p2g(p_nef)
    g_nef = scale(g_nef)
    g_nef.SetMarkerColor(4)
    g_nef.SetMarkerStyle(20)
    g_nef.SetMarkerSize(marker_size)
    #g_nef.Scale(1./0.25)

    #p_cef = fIN.Get("data/nef_run_zpt30")
    #g_cef = p2g(p_cef)
    #g_cef.SetMarkerColor(7)
    #g_cef.SetMarkerStyle(20)
    #g_cef.SetMarkerSize(0.5)
    #g_cef.Scale(1./0.25)
    

    p_mpf = fIN.Get("data/mpf_run_zpt30")
    g_mpf = p2g(p_mpf)
    g_mpf = scale(g_mpf)
    g_mpf.SetMarkerColor(1)
    g_mpf.SetMarkerStyle(20)
    g_mpf.SetMarkerSize(marker_size)



    canvas = ROOT.TCanvas("canvas","",1450,500)
    canvas.SetFillColor(0);
    canvas.SetBorderMode(0);
    canvas.SetBorderSize(2);
    canvas.SetTickx(1);
    canvas.SetTicky(1);
    canvas.SetLeftMargin(0.15);
    canvas.SetRightMargin(0.05);
    canvas.SetTopMargin(0.05);
    canvas.SetBottomMargin(0.15)
    canvas.SetFrameFillStyle(0);
    canvas.SetFrameBorderMode(0);
    canvas.SetFrameFillStyle(0);
    canvas.SetFrameBorderMode(0)
    canvas.cd()

    frame.Draw()
    g_nhf.Draw("psame")
    g_chf.Draw("psame")
    g_nef.Draw("psame")
    g_mpf.Draw("psame")

    # legend
    legend = ROOT.TLegend(0.8,0.75,0.93,0.9)
    legend.SetLineColor(0)
    legend.SetShadowColor(0)
    legend.SetTextSize(0.04)
    legend.AddEntry(g_mpf,"MPF","p")
    legend.AddEntry(g_chf,"CHF","p")
    legend.AddEntry(g_nef,"NEF","p")
    legend.AddEntry(g_nhf,"NHF","p")

    #for i,h in enumerate(histos):
    #    legend.AddEntry(h,histolegends[i],"f")
    legend.Draw()

    tex1 = ROOT.TLatex(0.2,0.85,"CMS")
    tex1.SetLineWidth(2)
    tex1.SetNDC()
    tex1.Draw()

    tex2 = ROOT.TLatex(0.2,0.807,"Preliminary")
    tex2.SetNDC()
    tex2.SetTextFont(52)
    tex2.SetTextSize(0.03781513)
    tex2.SetLineWidth(2)
    tex2.Draw()

    # Z+j text
    tex3 = ROOT.TLatex(0.5,0.853,"Z+jet")
    tex3.SetNDC()
    tex3.SetTextSize(0.04201681)
    tex3.SetLineWidth(2)
    tex3.Draw()

    tex4 = ROOT.TLatex(0.7,0.96,"2024")
    tex4.SetNDC()
    tex4.SetTextFont(42)
    tex4.SetTextSize(0.04201681)
    tex4.SetLineWidth(2)
    tex4.Draw()
    tex5 = ROOT.TLatex(0.8,0.96,"(13.6 TeV)")
    tex5.SetNDC()
    tex5.SetTextFont(42)
    tex5.SetTextSize(0.04201681)
    tex5.SetLineWidth(2)
    tex5.Draw()

    # line                                                                                                                                            
    runlines = [["2024C",379415],["2024D",380255],["2024E",380956],["2024F",381984],["2024G",383811],["2024H",385836],["2024I",386423]]
    lines = {}
    texts = {}
    ylinemax = 0.06
    for rl in runlines:
        print(rl)
        lines[rl[0]] = ROOT.TLine(rl[1],ymin,rl[1],ylinemax)
        lines[rl[0]].SetLineStyle(3)
        lines[rl[0]].SetLineWidth(2)
        lines[rl[0]].Draw()

        texts[rl[0]] = ROOT.TLatex(rl[1]-40,ymin+0.02,rl[0])
        texts[rl[0]].SetTextSize(0.02)
        texts[rl[0]].SetTextAngle(90)
        texts[rl[0]].SetLineWidth(2)
        texts[rl[0]].Draw()

    canvas.SaveAs("nhf_vs_run.png")
    canvas.SaveAs("nhf_vs_run.C")

if __name__=="__main__":
    parser = OptionParser(usage="Usage: %prog [options] <rootfile>")
    parser.add_option("-l","--line", dest="line", default="None", type="string",
                      help="Draw a line for run [default: \"\"]")

    (opts, args) = parser.parse_args()

    main(opts, args)
