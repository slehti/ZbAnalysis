#!/usr/bin/env python3
#
# Read NanoAOD skim and make histograms
# 12.10.2018/S.Lehti
#

import os
import sys
import re
import subprocess
import datetime
import json
import psutil

from optparse import OptionParser

import ROOT
import multiprocessing

import setproctitle

MAX_WORKERS = max(multiprocessing.cpu_count()-1,1)

MULTIPROCESSING = True
#MULTIPROCESSING = False

TEST = False
#TEST = True

#LEPTONFLAVOR = 11
#LEPTONFLAVOR = 13
#LEPTONFLAVOR = -13 # emu: single muon trigger

SMEARING = False
#SMEARING = True

PILEUPREWEIGHT = False
PILEUPREWEIGHT = True

MINBIAS_XS_Run2 = 69200
MINBIAS_XS_Run3 = 75300

#if TEST:
#    LEPTONFLAVOR = 13

maxEvents = -1
if TEST:
    maxEvents = 10000
#maxEvents = 10000

pileupfile = {}
pileupfile["2016"] = "pileup_2016.txt"
pileupfile["2017"] = "pileup_2017.txt"
pileupfile["2018"] = "pileup_2018.txt"
pileupfile["2022"] = "pileup_2022.txt"
pileupfile["2023"] = "pileup_2023.txt"
pileupfile["2024"] = "pileup_2024.txt"

def usage():
    print
    print( "### Usage:  ",os.path.basename(sys.argv[0]),"<multicrab skim>" )
    print

root_re = re.compile(r"(?P<rootfile>([^/]*events_\d+\S*\.root))")
#mask_re = re.compile("(?P<lumimask>Cert_\S+\.\w+)")
mask_re = re.compile(r"(?P<lumimask>\S*Collisions\S+\.\w+)")
json_re = re.compile(r"(?P<jsonfile>(files_\S+\.json))")
das_re = re.compile(r"(?P<das>(das\.json))")

def set_proc_name(newname):
    from ctypes import cdll, byref, create_string_buffer
    libc = cdll.LoadLibrary('libc.so.6')
    buff = create_string_buffer(len(newname)+1)
    buff.value = newname.encode('utf-8')
    libc.prctl(15, byref(buff), 0, 0, 0)

"""
xsecs = {}
xsecs["TTJets"] = 831.76 #https://twiki.cern.ch/twiki/bin/view/LHCPhysics/TtbarNNLO#Top_quark_pair_cross_sections_at 
xsecs["ST_tW_top_5f_inclusiveDecays"]            = 35.85 # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["ST_tW_antitop_5f_inclusiveDecays"]        = 35.85 # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["ST_t_channel_top_4f_inclusiveDecays"]     = 136.02# https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["ST_t_channel_antitop_4f_inclusiveDecays"] = 80.95 # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["ST_s_channel_4f_InclusiveDecays"]         = 11.36 # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["DYJetsToLL_M_50_HT_70to100"]    = 209.592
xsecs["DYJetsToLL_M_50_HT_100to200"]   = 181.302 # McM times NNLO/LO ratio of inclusive sample
xsecs["DYJetsToLL_M_50_HT_200to400"]   = 50.4177 # McM times NNLO/LO ratio of inclusive sample
xsecs["DYJetsToLL_M_50_HT_400to600"]   = 6.98314
xsecs["DYJetsToLL_M_50_HT_600to800"]   = 1.6841 
xsecs["DYJetsToLL_M_50_HT_800to1200"]  = 0.775392
xsecs["DYJetsToLL_M_50_HT_1200to2500"] = 0.18622
xsecs["DYJetsToLL_M_50_HT_2500toInf"]  = 0.004384
xsecs["WJetsToLNu_HT_100To200"]   = 1.293e+03*1.2138
xsecs["WJetsToLNu_HT_200To400"]   = 3.86e+02*1.2138
xsecs["WJetsToLNu_HT_400To600"]   = 47.9*1.2138
xsecs["WJetsToLNu_HT_600To800"]   = 12.8*1.2138
xsecs["WJetsToLNu_HT_800To1200"]  = 5.26*1.2138
xsecs["WJetsToLNu_HT_1200To2500"] = 1.33*1.2138
xsecs["WJetsToLNu_HT_2500ToInf"]  = 3.089e-02*1.2138

label = {}
label["R_pT"] = "p_{T} balance"
label["R_MPF"] = "MPF"

def getCrossSection(datasetName):
    if "_ext" in datasetName:
        datasetName = datasetName[:datasetName.find("_ext")]
    return xsecs[datasetName]
"""
class Dataset:
    def __init__(self,path,run,opts): #lepton):
        self.name = os.path.basename(path)
        self.run = run
        year_re = re.compile(r"Run(?P<year>\d\d\d\d)")
        match = year_re.search(self.run)
        if match:
            self.year = int(match.group("year"))

        if hasattr(opts, 'lepton'):
            self.lepton = opts.lepton
        self.isData = False
        if "Run20" in self.name:
            self.isData = True
        self.lumi = 0
        self.lumimask = ""
        self.files = []
        self.das = None
        cands = execute("ls %s"%os.path.join(path,"results"))
        jsonfile = ""
        for c in cands:
            match = root_re.search(c)
            if match:
                self.files.append(os.path.join(path,"results",match.group("rootfile")))
            json_match = json_re.search(c)
            if json_match:
                jsonfile = os.path.join(path,"results",json_match.group("jsonfile"))
            das_match = das_re.search(c)
            if das_match:
                dasfile = os.path.join(path,"results",das_match.group("das"))
                with open(dasfile) as dasIN:
                    self.das = json.loads(dasIN.read())

        if len(self.files) == 0:
            #jsonfile = os.path.join(path,"results","files_%s.json"%self.name)
            if os.path.exists(jsonfile):
                with open(jsonfile, 'r') as fJSON:
                    data = json.loads(fJSON.read())
                    self.files.extend(data['files'])

        if len(self.files) == 0:
            print("Dataset %s contains no root files"%self.name)
            return

        self.histograms = {}

        self.runmin = 0
        self.runmax = 0

        if self.isData:
            #self.fPU = ROOT.TFile.Open(os.path.join(os.path.dirname(path),"PileUp.root"))
            pu_file_data = os.path.join(path,"results","PileUp.root")
            if not os.path.exists(os.path.join(path,"results","PileUp.root")):
                print("No pileup found for %s, using combined pileup for data from pileup.root"%self.name)
                pu_file_data = os.path.join(os.path.dirname(path),"pileup.root")
            self.fPU = ROOT.TFile.Open(pu_file_data)
            pu_histonames = []
            pu_keys = self.fPU.GetListOfKeys()
            for i in range(len(pu_keys)):
                histoname = pu_keys.At(i).GetName()
                if not "_up" in histoname and not "_down" in	histoname:
                    pu_histonames.append(histoname)
            if opts.pileup not in pu_histonames:
                print("Pileup histogram '%s' \033[1m\033[91mnot found\033[0m"%opts.pileup)
                print("    available:")
                for p in pu_histonames:
                    print("       ",p)
                print("    use command line option -p,--pileup <pileup histo name>")
                sys.exit()
            else:
                print("Pileup histogram '%s' \033[1mfound\033[0m"%opts.pileup)
                print("    available:")
                for p in pu_histonames:
                    print("       ",p)

            print("Using pileup histogram '%s'"%opts.pileup)
            self.pileup = self.fPU.Get(opts.pileup).Clone("pileup")
            self.pileup.SetDirectory(0)
            #print("check pu data",pu_file_data,self.pileup.GetEntries(),self.pileup.Integral(),self.pileup.GetNbinsX())
            if self.pileup.Integral() == 0:
                print("Warning, empty data pu histogram") 
                sys.exit()
            #for i in range(1,self.pileup.GetNbinsX()):
            #    print("check pu bin ",i,self.pileup.GetBinContent(i))
            self.fPU.Close()

            self.minbias_xs = 0
            if len(opts.minbias_xs) > 0:
                self.minbias_xs = int(opts.minbias_xs)
            else:
                if self.year < 2020:
                    self.minbias_xs = MINBIAS_XS_Run2
                else:
                    self.minbias_xs = MINBIAS_XS_Run3
                # taking minbias xsec from pileup histogram name 
                if "pileup_" in opts.pileup:
                    self.minbias_xs = int(opts.pileup[7:])
            if not str(self.minbias_xs) in opts.pileup:
                print("\033[1m\033[91mWARNING\033[0m, minbias xsec %s used with pileup %s"%(self.minbias_xs,opts.pileup))
        else:
            pu_file_mc = os.path.join(path,"results","PileUp.root")
            self.fPU = ROOT.TFile.Open(pu_file_mc)
            self.pileup = self.fPU.Get("pileup").Clone("pileup_mc")
            #self.fPU = ROOT.TFile.Open(os.path.join(path,"results",self.files[0]))
            #self.pileup = self.fPU.Get("configInfo/pileup").Clone("pileup_mc")
            self.pileup.SetDirectory(0)
            #print("check pu mc",pu_file_mc,self.pileup.GetEntries(),self.pileup.Integral(),self.pileup.GetNbinsX())
            if self.pileup.Integral() == 0:
                print("Warning, empty mc pu histogram")
                sys.exit()
            #self.pileup.Reset()
            self.fPU.Close()
            #for fname in self.files:
            #    f = ROOT.TFile.Open(os.path.join(path,"results",fname))
            #    h = f.Get("configInfo/pileup").Clone("pileup")
            #    self.pileup.Add(h)
            #    f.Close()
        obj = self.pileup.Clone("pileup")
        obj.SetTitle("pileup")
        self.histograms["pileup"] = obj

        #self.fRF = ROOT.TFile.Open(os.path.join(path,"results",self.files[0]))
        try:
            self.fRF = ROOT.TFile.Open(self.files[0])
        except:
            return

        self.skimCounter = self.fRF.Get("configInfo/SkimCounter").Clone("skimCounter")
        self.skimCounter.Reset()
        for fname in self.files:
            rf = ROOT.TFile.Open(fname)
            s = rf.Get("configInfo/SkimCounter").Clone("skimCounter")
            self.skimCounter.Add(s)
            rf.Close()
        if self.das != None:
            # Adding event count from DAS into the skim counter
            newBins = self.skimCounter.GetNbinsX()+1
            newCounter = ROOT.TH1F("newCounter","",newBins,0,newBins)
            newCounter.GetXaxis().SetBinLabel(1,"DAS:  All events")
            newCounter.SetBinContent(1,self.das['dataset'][0]['nevents'])
            for i in range(1,newBins):
                newCounter.GetXaxis().SetBinLabel(i+1,self.skimCounter.GetXaxis().GetBinLabel(i))
                newCounter.SetBinContent(i+1,self.skimCounter.GetBinContent(i))
            #for i in range(1,newBins+1):
            #    print(newCounter.GetXaxis().GetBinLabel(i),newCounter.GetBinContent(i))
            self.skimCounter = newCounter
            self.skimCounter.SetName("skimCounter")

        """
        if self.fRF.FindObject("skimCounter"):
            self.skimCounter = self.fRF.Get("configInfo/SkimCounter").Clone("skimCounter")
            self.skimCounter.Reset()
            for fname in self.files:
                #rf = ROOT.TFile.Open(os.path.join(path,"results",fname))
                rf = ROOT.TFile.Open(fname)
                #            s = rf.Get("configInfo/skimCounter").Clone("skimCounter")
                s = rf.Get("configInfo/SkimCounter").Clone("skimCounter")
                self.skimCounter.Add(s)
                rf.Close()
        else:
            self.skimCounter = ROOT.TH1F("SkimCounter","",2,0,2)
            self.skimCounter.SetBinContent(1,len(self.files))
            self.skimCounter.GetXaxis().SetBinLabel(1,"Skim: control")
            allevents = 0
            for fname in self.files:
                rf = ROOT.TFile.Open(fname)
                s = rf.Get("Events")
                allevents += s.GetEntries() 
                rf.Close()
            self.skimCounter.SetBinContent(2,allevents)
            self.skimCounter.GetXaxis().SetBinLabel(2,"Skim: All events")   
        """
        obj = self.skimCounter.Clone("skimCounter")
        obj.SetTitle("skim")
        self.histograms["skimCounter"] = obj

        if self.isData:
            cands = execute("ls %s"%os.path.join(path,"inputs/"))
            for c in cands:
                match = mask_re.search(c)
                if match:
                    self.lumimask = os.path.join(path,"inputs/",match.group("lumimask"))
                    if hasattr(opts, 'nibsFile'):
                        self.runmin,self.runmax = getRunMinMax(self.lumimask)
                        self.nib = getNib(self.runmin,self.runmax,opts.nibsFile)

        """
        print ("check",self.fRF.GetName())
        #self.fRF.cd("histograms")
        gDir = self.fRF.CurrentDirectory()
        print(type(gDir))
        #keys = gDir.GetListOfKeys()
        keys = self.fRF.GetListOfKeys()
        print(keys)
        nkeys = len(keys)
        self.fRF.cd()
        self.fRF.Print()
        self.histograms = {}
        for i in range(nkeys):
            keyname = keys.At(i).GetName()
            print("check key",keyname)
            obj = self.fRF.Get(keyname).Clone(keyname)
            obj.Reset()
            obj.SetTitle("skim")
            self.histograms[keyname] = obj
        sys.exit()
        for fname in self.files:
            #print "check file",fname
            rf = ROOT.TFile.Open(os.path.join(path,"results",fname))
            rf.cd()
            for i in range(nkeys):
                keyname = keys.At(i).GetName()
                #s = gDir.Get(os.path.join("histograms",keyname))
                #s = gDir.Get(keyname).Clone(keyname)
                s = rf.Get(os.path.join("histograms",keyname)).Clone(keyname)
                #print "obj",keyname,s.GetEntries(),s.GetMean()
                self.histograms[keyname].Add(s)
            rf.Close()
        """
#        print "check histo",self.histograms["Z pt"].GetEntries(),self.histograms["Z pt"].GetMean()
#        sys.exit()
#            if isinstance(obj, ROOT.TH1F):
#                lsHisto(obj)
#        print "check pu",self.pileup.GetEntries()
#        print "check skimcounter",self.skimCounter.GetBinContent(1),self.skimCounter.GetBinContent(2)
#        self.fRF.Close()
#        if not self.isData:
#            self.xsec = getCrossSection(self.name)

    def getFileNames(self):
        return self.files

    def getSkimCounter(self):
        #return self.skimCounter
        return self.histograms["skimCounter"]

    def Print(self):
        print( self.name )
        print( "    is data",self.isData )
        print( "    number of files",len(self.files) )

    def PrintCounters(self):
        if hasattr(self, 'skimCounter'):
            print( self.name )
            for i in range(1,self.skimCounter.GetNbinsX()+1):
                label = self.skimCounter.GetXaxis().GetBinLabel(i)
                while len(label) < 30:
                    label += ' '
                value = "%s"%self.skimCounter.GetBinContent(i)
                while len(value) < 12:
                    value = ' '+value
                print("   ",label,value)
            print()

def execute(cmd):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
    (s_in, s_out) = (p.stdin, p.stdout)

    f = s_out
    ret=[]
    for line in f:
        line = str(line,'utf-8')
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

def getRun(datasetnames):
    run = ""
    run_re = re.compile(r"(?P<run>Run20\d\d)")
    for n in datasetnames:
        match = run_re.search(n)
        if match:
            run = match.group("run")
    return run

def getRunMinMax(jsonfile):
    with open(jsonfile, "r") as fIN:
        d = json.loads(fIN.read())
        runs_s = d.keys()
        runs_i = list(map(lambda x: int(x), runs_s))

        runmin = min(runs_i)
        runmax = max(runs_i)
        return runmin,runmax
    print("Run min/max not found in",jsonfile)
    sys.exit()

def fibs2json(fibsfile):
    # [run1, run2] | name = [year][era]-nib[N]-fib[M] | lum(/fb) |  start and end time  | runs  | number of LS
    line_re = re.compile(r"\[(?P<runmin>\S+),\s*(?P<runmax>\S+)\]\s*\|\s*(?P<name>\S+)\s*\|")
    fibsjson = {}
    with open(fibsfile) as fIN:
        lines = fIN.readlines()
        for line in lines:
            #print(line)
            match = line_re.search(line)
            if match:
                runmin = match.group("runmin")
                runmax = match.group("runmax")
                label = match.group("name")
                #print("label",label,runmin,runmax)
                fibsjson[label] = [runmin,runmax]
    return fibsjson

def getNib(runmin,runmax,nibfile):
    fibsjson = fibs2json(nibfile)
    keys = fibsjson.keys()
    keys = list(set(keys)) # remove duplicates
    fibs = []
    for key in sorted(keys):
        runminfib = int(fibsjson[key][0])
        runmaxfib = int(fibsjson[key][1])
        if runminfib >= runmin and runmaxfib <= runmax:
            fibs.append(key)
    nibs = ['-'.join(x.split('-')[:-1]) for x in fibs]
    nibs = list(set(nibs)) # remove duplicates
    return nibs[0]

def getDatasets(multicrabdir,opts): #whitelist=[],blacklist=[],lepton=13):
    datasets = []
    cands = execute("ls %s"%multicrabdir)
#    for c in cands:
#        resultdir = os.path.join(multicrabdir,c,"results")
#        if os.path.exists(resultdir):
#            datasets.append(Dataset(os.path.join(multicrabdir,c)))

    whitelist = opts.includeTasks
    if len(whitelist) > 0:
        #print "check whitelist 1 ",whitelist,blacklist
        datasets_whitelist = []
        for d in cands:
            for wl in whitelist:
                wl_re = re.compile(r"%s"%wl)
                match = wl_re.search(d)
                if match:
                    datasets_whitelist.append(d)
                    break
        #print "check whitelist",datasets_whitelist
        cands = datasets_whitelist

    blacklist = opts.excludeTasks
    if len(blacklist) > 0:
        #print "check blacklist 1 ",whitelist,blacklist
        datasets_blacklist = []
        for d in cands:
            found = False
            for bl in blacklist:
                bl_re = re.compile(r"%s"%bl)
                match = bl_re.search(d)
                if match:
                    found = True
                    break
            if not found:
                datasets_blacklist.append(d)
        cands = datasets_blacklist

    run = getRun(cands)

    for c in cands:
        resultdir = os.path.join(multicrabdir,c,"results")
        if os.path.exists(resultdir):
            datasets.append(Dataset(os.path.join(multicrabdir,c),run,opts))
    return datasets

def getPileup(datasets):
    nbinsx = 0
    for d in datasets:
        if d.isData and hasattr(d, 'pileup'):
            nbinsx = d.pileup.GetNbinsX()
            break
    print("Taking pileup from datasets")
    print("Pileup data: n bins =",nbinsx)
    if nbinsx == 0:
        sys.exit(1)

    pileup_data = ROOT.TH1F("pileup_data","",nbinsx,0,nbinsx)
    for d in datasets:
        if d.isData:
            #print d.name
            #for i in range(1,d.pileup.GetNbinsX()):
            #    print "check pu bin ",i,d.pileup.GetBinContent(i)
            if hasattr(d, 'pileup'):
                pileup_data.Add(d.pileup)
    #print "check pileup_data",pileup_data.GetEntries()
    #sys.exit()
    return pileup_data

def getPileupData(multicrabdir):
    if not PILEUPREWEIGHT:
        return None

    print("Taking pileup from %s"%os.path.join(multicrabdir,"pileup.root"))
    fPU = ROOT.TFile.Open(os.path.join(multicrabdir,"pileup.root"))
    pileup_data = fPU.Get("pileup").Clone("pileup_data")
    pileup_data.SetDirectory(0)
    fPU.Close()
    return pileup_data

def loadLuminosity(multicrabdir,datasets):
    lumisum = 0
    lumijson = open(os.path.join(multicrabdir,"lumi.json"),'r')
    data = json.load(lumijson)
    for d in datasets:
        if d.name in data.keys():
            d.lumi = data[d.name]
            lumisum += d.lumi
    lumijson.close()
    return lumisum

def eventloop(year,outputdir,dataset,pileup_data,lumi,txt,lepton,lock=None):

    os.nice(19)

    #print("check eventloop input",year,outputdir,dataset.name,lumi)
    subdir = os.path.join(outputdir,dataset.name)
    #if not dataset.isData:
    #    subdir+="_"+dataset.run
    if not os.path.exists(subdir):
        os.mkdir(subdir)
        os.mkdir(os.path.join(subdir,"results"))

    ROOT.gSystem.Load("lib/libAnalysis.so")

    tchain = ROOT.TChain("Events")
    tchain.SetCacheSize(10000000) # Set cache size to 10 MB (somehow it is not automatically set contrary to ROOT docs)
    for f in dataset.getFileNames():
        tchain.Add(f)

    #if tchain.GetEntries() == 0:
    #    return

    pileup_mc = None
    if PILEUPREWEIGHT and not dataset.isData:
        pileup_mc = dataset.pileup

    gitcommit = execute("git rev-parse HEAD")[0]
    #print("check LEPTONFLAVOR",lepton)
    inputList = ROOT.TList()
    inputList.Add(ROOT.TNamed("outputdir",os.path.join(subdir,"results")))
    inputList.Add(ROOT.TNamed("name",dataset.name))
    inputList.Add(ROOT.TNamed("year","%s"%year))
    inputList.Add(ROOT.TNamed("lumi","%s"%dataset.lumi))
    inputList.Add(ROOT.TNamed("leptonflavor","%s"%lepton))
    inputList.Add(ROOT.TNamed("smearing","%s"%SMEARING))
    inputList.Add(ROOT.TNamed("pileupreweight","%s"%PILEUPREWEIGHT))
    inputList.Add(ROOT.TNamed("gitcommit","%s"%gitcommit))

    inputList.Add(pileup_data)
    if(dataset.isData):
        inputList.Add(ROOT.TNamed("isData","1"))
        inputList.Add(ROOT.TNamed("pileupfile",pileupfile[year]))
        inputList.Add(ROOT.TNamed("minbias_xs","%s"%dataset.minbias_xs))
        inputList.Add(ROOT.TNamed("lumimask",dataset.lumimask))
    else:
        inputList.Add(ROOT.TNamed("isData","0"))
        if PILEUPREWEIGHT:
            #inputList.Add(pileup_data)
            inputList.Add(pileup_mc)
    inputList.Add(dataset.getSkimCounter())

    #if "DoubleMuon" in dataset.name or not dataset.isData:
    #print("check lepton",lepton)
    if lepton == 13:
        if "2016" in year:
            inputList.Add(ROOT.TNamed("trigger","HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ"))
        if not "2016" in year:
            ####inputList.Add(ROOT.TNamed("trigger","HLT_Mu19_TrkIsoVVL_Mu9_TrkIsoVVL_DZ"))
            inputList.Add(ROOT.TNamed("trigger","HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8"))
            #print("check inputList.Add")
        if "2017H" in subdir:
            inputList.Add(ROOT.TNamed("trigger","HLT_HIMu17"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_Mu19_TrkIsoVVL_Mu9_TrkIsoVVL"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_Mu19"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_Mu17"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_IsoMu24_eta2p1"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_DoubleIsoMu17_eta2p1_noDzCut"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_DoubleIsoMu17_eta2p1"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_IsoMu17_eta2p1"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_IsoMu22_eta2p1"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_DoubleMu18NoFiltersNoVtx_v5"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_IsoMu22"))
    #if "DoubleEG" in dataset.name or not dataset.isData:
    if lepton == 11:
        inputList.Add(ROOT.TNamed("trigger","HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL"))
        #inputList.Add(ROOT.TNamed("trigger","HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_Ele17_Ele12_CaloIdL_TrackIdL_IsoVL"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_Ele17_Ele12_CaloIdL_TrackIdL_IsoVL_DZ"))
    if lepton == -13:
        inputList.Add(ROOT.TNamed("trigger","HLT_IsoMu24_eta2p1"))



    # https://twiki.cern.ch/twiki/bin/viewauth/CMS/MissingETOptionalFiltersRun2
    if "2016" in year:
        inputList.Add(ROOT.TNamed("Filters","Flag_goodVertices"))
        inputList.Add(ROOT.TNamed("Filters","Flag_globalSuperTightHalo2016Filter"))
        inputList.Add(ROOT.TNamed("Filters","Flag_HBHENoiseFilter"))
        inputList.Add(ROOT.TNamed("Filters","Flag_HBHENoiseIsoFilter"))
        #inputList.Add(ROOT.TNamed("Filters","EcalDeadCellTriggerPrimitiveFilter"))
        inputList.Add(ROOT.TNamed("Filters","Flag_BadPFMuonFilter"))
        #inputList.Add(ROOT.TNamed("Filters","Flag_BadPFMuonDzFilter"))
        #inputList.Add(ROOT.TNamed("Filters","Flag_BadChargedCandidateFilter"))
        inputList.Add(ROOT.TNamed("Filters","Flag_eeBadScFilter"))

    else:
        inputList.Add(ROOT.TNamed("Filters","Flag_goodVertices"))
        inputList.Add(ROOT.TNamed("Filters","Flag_globalSuperTightHalo2016Filter"))
        inputList.Add(ROOT.TNamed("Filters","Flag_HBHENoiseFilter"))
        inputList.Add(ROOT.TNamed("Filters","Flag_HBHENoiseIsoFilter"))
        inputList.Add(ROOT.TNamed("Filters","Flag_EcalDeadCellTriggerPrimitiveFilter"))
        inputList.Add(ROOT.TNamed("Filters","Flag_BadPFMuonFilter"))
        inputList.Add(ROOT.TNamed("Filters","Flag_BadPFMuonDzFilter"))
        #inputList.Add(ROOT.TNamed("Filters","Flag_hfNoisyHitsFilter"))
        #inputList.Add(ROOT.TNamed("Filters","Flag_BadChargedCandidateFilter"))
        inputList.Add(ROOT.TNamed("Filters","Flag_eeBadScFilter"))
        inputList.Add(ROOT.TNamed("Filters","Flag_ecalBadCalibFilter"))

    #print("check nib",dataset.name,dataset.runmin,dataset.runmax,dataset.nib)
    if(dataset.isData):
        if dataset.nib == '2024B-nib1':
            inputList.Add(ROOT.TNamed("JECs","Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt"))
            inputList.Add(ROOT.TNamed("JECs","Prompt24_Run2024B_nib1_V8M_DATA_L2L3Residual_AK4PFPuppi.txt"))
        if dataset.nib == '2024C-nib1':
            inputList.Add(ROOT.TNamed("JECs","Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt"))
            inputList.Add(ROOT.TNamed("JECs","Prompt24_Run2024C_nib1_V8M_DATA_L2L3Residual_AK4PFPuppi.txt"))
        if dataset.nib == '2024D-nib1':
            inputList.Add(ROOT.TNamed("JECs","Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt"))
            inputList.Add(ROOT.TNamed("JECs","Prompt24_Run2024D_nib1_V8M_DATA_L2L3Residual_AK4PFPuppi.txt"))
        if dataset.nib == '2024Ev1-nib1':
            inputList.Add(ROOT.TNamed("JECs","Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt"))
            inputList.Add(ROOT.TNamed("JECs","Prompt24_Run2024Ev1_nib1_V8M_DATA_L2L3Residual_AK4PFPuppi.txt"))
        if dataset.nib == '2024Ev2-nib1':
            inputList.Add(ROOT.TNamed("JECs","Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt"))
            inputList.Add(ROOT.TNamed("JECs","Prompt24_Run2024Ev2_nib1_V8M_DATA_L2L3Residual_AK4PFPuppi.txt"))
        if dataset.nib == '2024F-nib1':
            inputList.Add(ROOT.TNamed("JECs","Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt"))
            inputList.Add(ROOT.TNamed("JECs","Prompt24_Run2024F_nib1_V8M_DATA_L2L3Residual_AK4PFPuppi.txt"))
        if dataset.nib == '2024F-nib2':
            inputList.Add(ROOT.TNamed("JECs","Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt"))
            inputList.Add(ROOT.TNamed("JECs","Prompt24_Run2024F_nib2_V8M_DATA_L2L3Residual_AK4PFPuppi.txt"))
        if dataset.nib == '2024F-nib3':
            inputList.Add(ROOT.TNamed("JECs","Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt"))
            inputList.Add(ROOT.TNamed("JECs","Prompt24_Run2024F_nib3_V8M_DATA_L2L3Residual_AK4PFPuppi.txt"))
        if dataset.nib == '2024G-nib1':
            inputList.Add(ROOT.TNamed("JECs","Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt"))
            inputList.Add(ROOT.TNamed("JECs","Prompt24_Run2024G_nib1_V8M_DATA_L2L3Residual_AK4PFPuppi.txt"))
        if dataset.nib == '2024G-nib2':
            inputList.Add(ROOT.TNamed("JECs","Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt"))
            inputList.Add(ROOT.TNamed("JECs","Prompt24_Run2024G_nib2_V8M_DATA_L2L3Residual_AK4PFPuppi.txt"))
        if dataset.nib == '2024H-nib1':
            inputList.Add(ROOT.TNamed("JECs","Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt"))
            inputList.Add(ROOT.TNamed("JECs","Prompt24_Run2024H_nib1_V8M_DATA_L2L3Residual_AK4PFPuppi.txt"))
        if dataset.nib == '2024I-nib1':
            inputList.Add(ROOT.TNamed("JECs","Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt"))
            inputList.Add(ROOT.TNamed("JECs","Prompt24_Run2024I_nib1_V8M_DATA_L2L3Residual_AK4PFPuppi.txt"))
    else:
        inputList.Add(ROOT.TNamed("JECs","Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt"))
        inputList.Add(ROOT.TNamed("JECs","JR_Winter22Run3_V1_MC_PtResolution_AK4PFPuppi.txt"))
        inputList.Add(ROOT.TNamed("JECs","Prompt24_2024BC_JRV1M_MC_SF_AK4PFPuppi.txt"))

    for key in dataset.histograms.keys():
        inputList.Add(dataset.histograms[key])

    ## Only used branches
    usedBranches = ["run","luminosityBlock","event"]
    usedBranches.extend(["Pileup_nTrueInt",
                         "nPSWeight",
                         "PSWeight",
                         "LHEWeight_originalXWGTUP",
                         "fixedGridRhoFastjetAll",
                         "Rho_fixedGridRhoFastjetAll",
                         "Rho_fixedGridRhoFastjetCentral",
                         "Rho_fixedGridRhoFastjetCentralChargedPileUp",
                         "PV_npvsGood",
                         "PV_npvs"])
    usedBranches.extend(["nMuon",
                         "Muon_pt",
                         "Muon_eta",
                         "Muon_phi",
                         "Muon_mass",
                         "Muon_charge",
                         "Muon_softId",
                         "Muon_mediumPromptId",
                         "Muon_miniPFRelIso_all",
                         "Muon_pfRelIso03_all",
                         "Muon_pfRelIso04_all",
                         "Muon_nTrackerLayers",
                         "Muon_tightId"])
    usedBranches.extend(["nElectron",
                         "Electron_pt",
                         "Electron_eta",
                         "Electron_phi",
                         "Electron_mass",
                         "Electron_charge",
                         "Electron_cutBased",
                         "Electron_miniPFRelIso_all",
                         "Electron_pfRelIso03_all",
                         "Electron_dxy",
                         "Electron_dz"])
    usedBranches.extend(["nJet",
                         "Jet_area",
                         "Jet_pt",
                         "Jet_eta",
                         "Jet_phi",
                         "Jet_mass",
                         "Jet_rawFactor",
                         "Jet_electronIdx1",
                         "Jet_electronIdx2",
                         "Jet_muonIdx1",
                         "Jet_muonIdx2",
                         "Jet_jetId",
                         "Jet_nElectrons",
                         "Jet_nMuons",
                         "Jet_nConstituents",
                         "Jet_neEmEF",
                         "Jet_neHEF",
                         "Jet_muEF",
                         "Jet_chEmEF",
                         "Jet_chHEF",
                         "Jet_btagDeepFlavB",
                         "Jet_btagDeepFlavCvL",
                         "Jet_btagDeepFlavQG",
                         "Jet_btagDeepB",
                         "Jet_btagDeepCvB",
                         "Jet_btagDeepCvL",
                         "Jet_particleNetAK4_B",
                         "Jet_particleNetAK4_CvsB",
                         "Jet_particleNetAK4_CvsL",
                         "Jet_particleNetAK4_QvsG",
                         "Jet_hadronFlavour",
                         "Jet_partonFlavour"])
    usedBranches.extend(["nGenJet",
                         "GenJet_pt",
                         "GenJet_eta",
                         "GenJet_phi",
                         "GenJet_mass",
                         "GenMET_pt",
                         "GenMET_phi",
                         "nGenPart",
                         "GenPart_pt",
                         "GenPart_eta",
                         "GenPart_phi",
                         "GenPart_mass",
                         "GenPart_pdgId",
                         "GenPart_genPartIdxMother",
                         "GenPart_status"])
    usedBranches.extend(["MET_pt",
                         "MET_phi",
                         "ChsMET_pt",
                         "ChsMET_phi",
                         "PuppiMET_phi",
                         "PuppiMET_pt",
                         "RawMET_pt",
                         "RawMET_phi",
                         "RawPuppiMET_phi",
                         "RawPuppiMET_pt",
                         "MET_MetUnclustEnUpDeltaX",
                         "MET_MetUnclustEnUpDeltaY"])
    usedBranches.extend(["nTrigObj",
                         "TrigObj_pt",
                         "TrigObj_eta",
                         "TrigObj_phi",
                         "TrigObj_id"])

    # trigger and filter branches
    for i in range(len(inputList)):
        obj = inputList.At(i)
        #print("check inputList",obj.GetTitle())
        if obj.GetName() == "trigger" or obj.GetName() == "Filters":
            usedBranches.append(obj.GetTitle())
            #print("check trg",obj.GetTitle())

    disableBranches = []
    disableBranches.extend(["L1_MHT120","L1_MHT150","L1_MHT200"])
    disableBranches.extend(["L1_SingleMu0_Upt10_SQ14_BMTF","L1_SingleMu0_Upt15_SQ14_BMTF","L1_SingleMu0_Upt20_SQ14_BMTF","L1_SingleMu0_Upt25_SQ14_BMTF"])
    usedBranches = [item for item in usedBranches if item not in disableBranches]

    # disable the rest
    branches = tchain.GetListOfBranches()
    for i in range(len(branches)):
        name = branches.At(i).GetName()
        if not name in usedBranches:
            tchain.SetBranchStatus(name,0)

    ######


    tselector = ROOT.Analysis()#("HLT_IsoMu27||HLT_Mu45_eta2p1") #ROOT.TSelector()
    tselector.SetInputList(inputList)

    if lock != None:
        lock.acquire()
        print
        print( txt )
        print( dataset.name )
        print( "  Process ID",os.getpid())
        print( "  Year",year)
        print( "  Lepton",lepton)
        print( "  Trigger OR:")
        for i in range(len(inputList)):
            if inputList.At(i).GetName() == "trigger":
                print( "    %s"%inputList.At(i).GetTitle())
        lock.release()

    if maxEvents > 0:
        tchain.Process(tselector,"",maxEvents)
    else:
        tchain.Process(tselector)

    outputList = tselector.GetOutputList()

    if tselector.GetOutputList().FindObject("unweighted counters"):
        counter_unweighted = tselector.GetOutputList().FindObject("unweighted counters")
        counter_weighted   = tselector.GetOutputList().FindObject("weighted counters")

        if lock != None:
            lock.acquire()
        print
        print( txt)
        print( dataset.name)
        print( "  Process ID",os.getpid())
        printBothCounters(counter_unweighted,counter_weighted)

        if tselector.GetOutputList().FindObject("jetCounter_all"):
            counter_jets_all       = tselector.GetOutputList().FindObject("jetCounter_all")
            counter_jets_btag  = tselector.GetOutputList().FindObject("jetCounter_btag")
            counter_jets_ctag  = tselector.GetOutputList().FindObject("jetCounter_ctag")
            counter_jets_quarktag  = tselector.GetOutputList().FindObject("jetCounter_quarktag")
            counter_jets_gluontag  = tselector.GetOutputList().FindObject("jetCounter_gluontag")
            counter_jets_notag  = tselector.GetOutputList().FindObject("jetCounter_notag")
            counter_jets_qgtLT0tag = tselector.GetOutputList().FindObject("jetCounter_qgtLT0tag")
            printCounter(counter_jets_all)
            printCounter(counter_jets_btag)
            printCounter(counter_jets_ctag)
            printCounter(counter_jets_quarktag)
            printCounter(counter_jets_gluontag)
            printCounter(counter_jets_notag)
            printCounter(counter_jets_qgtLT0tag)

        if lock != None:
            lock.release()
    del outputList
    del tselector

def printCounter(counter):
    print( counter.GetName() )
    for i in range(1,counter.GetNbinsX()+1):
        line = "    ";
        clabel = "{:25.24}".format(counter.GetXaxis().GetBinLabel(i))
        if len(counter.GetXaxis().GetBinLabel(i)) == 0:
            print
            continue
        cvalue = str("{:12.0f}".format(counter.GetBinContent(i)))
        line += "%s %s"%(clabel,cvalue)
        print( line )
    print

def printBothCounters(counter1,counter2):
    for i in range(1,counter1.GetNbinsX()+1):
        line = "    ";
        clabel = "{:25.24}".format(counter1.GetXaxis().GetBinLabel(i))
        if len(counter1.GetXaxis().GetBinLabel(i)) == 0:
            print
            continue
        cvalue1 = str("{:12.0f}".format(counter1.GetBinContent(i)))
        cvalue2 = str("{:12.1f}".format(counter2.GetBinContent(i)))
        line += "%s %s %s"%(clabel,cvalue1,cvalue2)
        print( line )
    print

def getYear(multicrabdir):
    year_re = re.compile(r"Run(?P<year>20\d\d)\S+_")
    match = year_re.search(multicrabdir)
    if match:
        return match.group("year")
    return "-1"

def main(opts, args):

    if len(sys.argv) == 1:
        usage()
        sys.exit()
        
    multicrabdir = os.path.abspath(sys.argv[1])
    if not os.path.exists(multicrabdir) or not os.path.isdir(multicrabdir):
        usage()
        sys.exit()
    year = getYear(multicrabdir)
    #print "check year",year

    time = datetime.datetime.now().strftime("%Y%m%dT%H%M")
    LEPTONFLAVOR = int(opts.lepton)
    #print("check opts.lepton",opts.lepton)
    lepton = "_"
    if LEPTONFLAVOR == 11:
        lepton+="Electron"
    if LEPTONFLAVOR == 13:
        lepton+="Muon"
    if LEPTONFLAVOR == -11:
        lepton+="Electron"
    if LEPTONFLAVOR == -13:
        lepton+="Muon"

    pu_xsec = 0
    if opts.pileup != "pileup":
        pu_xsec = int(opts.pileup[7:])
    if len(opts.minbias_xs) > 0:
        pu_xsec = minbias_xs

    outputmulticrab = os.path.basename(os.path.abspath(multicrabdir))+"_processed_"+time+lepton
    if pu_xsec > 0:
        outputmulticrab = os.path.basename(os.path.abspath(multicrabdir))+"_processed_%s_"%pu_xsec+time+lepton
    if len(opts.name) > 0:
        outputmulticrab = outputmulticrab + "_"+opts.name
    if not os.path.exists(outputmulticrab):
        os.mkdir(outputmulticrab)

    cmd = "cp %s %s"%(os.path.join(multicrabdir,"lumi.json"),outputmulticrab)
    os.system(cmd)

    blacklist = []
    whitelist = []
    if opts.includeTasks != 'None':
        whitelist.extend(opts.includeTasks.split(','))
    if opts.excludeTasks != 'None':
        blacklist.extend(opts.excludeTasks.split(','))
    runonly = []
    if opts.runMCOnly != 'None':
        runonly.extend(opts.runMCOnly.split(','))
        #print("check opts.runMCOnly",opts.runMCOnly,runonly)

#    whitelist = ["DY1JetsToLL_M_50_ext1","DoubleMuon_Run2017D"] #"SingleMuon_Run2017B_31Mar2018_v1_297050_299329","TTJets"]
#    whitelist = ["DoubleMuon_Run2016C","DY1JetsToLL_M_10"] #"SingleMuon_Run2017B_31Mar2018_v1_297050_299329","TTJets"]
#    whitelist = ["DoubleMuon_Run2017B"]
#    if LEPTONFLAVOR == 13:
##        whitelist = ["DoubleMuon","SingleMuon_Run2017H","DYJets","TTJet"]
#        whitelist = ["Muon","DY"]
#        #blacklist = ["DoubleEG","EGamma","SingleMuon"]
#    if LEPTONFLAVOR == 11:
#        #blacklist = ["DoubleMuon","SingleMuon"]
#        whitelist = ["DoubleEG","EGamma","DYJets"]
##        whitelist = ["DoubleEG","EGamma","DYJets","TTJet"]
##        whitelist = ["DoubleEG","EGamma","TTJet"]
##        whitelist = ["DoubleEG_Run2016C","DY1JetsToLL_M_50"]
#
#    if LEPTONFLAVOR == -13:
#        #blacklist = ["DoubleMuon","DoubleEG","EGamma"]
#        whitelist = ["SingleMuon","DYJets"]#,"TTJet"]
#    whitelist = ["DoubleMuon_Run2016G","DoubleMuon_Run2016H","DY1","DY2","DY3","DY4"]
#    whitelist = ["DoubleMuon_Run2016G","DoubleMuon_Run2016H","JetsToLL_M_50"]
    if TEST:
        whitelist = [r"DYJetsToLL_M_50",r"DoubleMuon_Run201\dC"]
#    whitelist = ["DY1JetsToLL_M_50","DoubleEG_Run201\dC"]
#    whitelist = ["DY1JetsToLL_M_50","EGamma_Run201\dC"]
#    whitelist = ["DY3JetsToLL_M_50","DoubleMuon_Run201\dC"]
#    whitelist = ["DoubleMuon_Run20\d\dC"]
#    whitelist = ["DYJets"]
#    whitelist = ["DoubleEG_Run20\d\dD"]
#    whitelist = ["DoubleEG"]
#    whitelist = ["Run2017B"]
#    whitelist = ["Muon1_Run2023D_PromptReco_v1"]
#    whitelist = ["DoubleMuon","DYJetsToLL_M_50"]
#    whitelist = ["Muon_Run20\d\dD"]#,"DYJetsToLL_M_50"]
#    whitelist = ["DYJetsToLL_M_50_ext1"]
#    whitelist = ["PromptNanoAODv12_v3","PromptNanoAODv12_v4","DYJetsToLL_M_50"]
#    whitelist = ["Run20\d\dF","DYJetsToLL_M_50"]
#    whitelist = ["DYJetsToLL_M_50_Summer22EF"]
#    blacklist = ["DYJetsToLL_M_50_herwig7","EG"]
#    whitelist = ["DoubleMuon_Run2017F"]

    blacklist.extend(["DYJetsToLL_M_50_herwig7"])
    blacklist.extend(["DYJetsToLL_M_50_2024_JMENano_ext1_v2"])

    if LEPTONFLAVOR == 11:
        blacklist.extend(["Muon"])
    if LEPTONFLAVOR == 13:
        blacklist.extend(["EG"])
    blacklist.extend(["TT"])

    opts.includeTasks = whitelist
    opts.excludeTasks = blacklist
    print("whitelist",whitelist)
    print("blacklist",blacklist)

    datasets = getDatasets(multicrabdir, opts)#,whitelist=whitelist,blacklist=blacklist,lepton=LEPTONFLAVOR)
    print("Number of datasets: %s"%len(datasets))
    for d in datasets:
        print(d.name)

    if opts.list:
        sys.exit()

    pileup_data = getPileup(datasets)
    #pileup_data = getPileupData(multicrabdir)

    if len(runonly) > 0: # For running MC only with pu determined by -i list_of_datasets
        #print("check runonly",opts.runMCOnly)
        rundatasets = []
        for ronly in runonly:
            only_re = re.compile(ronly)
            for d in datasets:
                match = only_re.search(d.name)
                if match and not d in rundatasets: 
                    rundatasets.append(d)

        if len(rundatasets) == 0:
            print("No datasets to run? Exiting..")
            sys.exit()

        print("Running only")
        datasets = rundatasets
        for d in datasets:
            print(d.name)

    lumi = loadLuminosity(multicrabdir,datasets)

    import time
    t0 = time.time()

    pool = multiprocessing.Pool(MAX_WORKERS)
    pids = []
    results = []
    lock = multiprocessing.Lock()
    for i,d in enumerate(datasets):
        txt = "Dataset %s/%s"%(i+1,len(datasets))
        if MULTIPROCESSING:
            pname = "ana/%s"%(d.name)
            p = multiprocessing.Process(target=eventloop, args=[year,outputmulticrab,d,pileup_data,lumi,txt,LEPTONFLAVOR,lock])
            p.start()
            pids.append(p.pid)

            setproctitle.setproctitle(pname)
#            results.append(pool.apply_async(func=eventloop, args=[outputmulticrab,d,pileup_data,lumi,txt]))
        else:
            eventloop(year,outputmulticrab,d,pileup_data,lumi,txt,LEPTONFLAVOR)

#    if MULTIPROCESSING:
#        pool.close()
#        del results
#        pool.join()
#        print "check pool.join"
#    output = [p.get() for p in results]

    if MULTIPROCESSING:
        import psutil
        nalive = len(pids)
        #print( "\rProcesses running",nalive,)
        i = 0

        os.system('setterm -cursor off')
        while nalive > 0:
            al = 0
            for pid in pids:
                #print("spawn:",psutil.Process(pid).cpu_affinity())
                if psutil.Process(pid).status() != psutil.STATUS_ZOMBIE:
                    al = al+1
            nalive = al
            dt = time.time()-t0
            lock.acquire()
            #sys.stdout.write("\rProcesses running %s/%s, time used %s min %s s             "%(nalive,len(pids),int(dt/60),int(dt%60)))
            #sys.stdout.flush()
            lock.release()
    #else:
    dt = time.time()-t0
    sys.stdout.write("\rFinished. Runtime used %s min %s s             "%(int(dt/60),int(dt%60)))
    sys.stdout.flush()
    print
        
    os.system('setterm -cursor on')
    #os.system('wall Zb analysis finished')
    #os.system('wall outputdir: %s'%outputmulticrab)
    print( "Output written in",outputmulticrab )
    print( "Workdir:",os.getcwd() )

if __name__ == "__main__":

    print(" ".join(sys.argv)) # print multicrab command

    os.nice(10)

    p = psutil.Process()
    CPUcores = p.cpu_affinity()
    CPUcores = CPUcores[1:]
    p.cpu_affinity(CPUcores)
    print("CPUcores in use",p.cpu_affinity())

    parser = OptionParser(usage="Usage: %prog [options]")

    parser.add_option("-i", "--includeTasks", dest="includeTasks", default="None", type="string",
                      help="Only perform action for this dataset(s) [default: \"\"]")
    parser.add_option("-e", "--excludeTasks", dest="excludeTasks", default="None", type="string",
                      help="Exclude this dataset(s) from action [default: \"\"]")
    parser.add_option("--name",dest="name", default="", type="string",
                      help="Ending for the multicrabdir [default: \"\"]")
    parser.add_option("-l", "--lepton",dest="lepton", default="13", type="string",
                      help="Electron (11) or Muon (13) [default: \"13\"]")
    parser.add_option("--list", dest="list", default=False, action="store_true",
                      help="List datasets and exit")
    parser.add_option("-r", "--runMCOnly", dest="runMCOnly", default="None", type="string",
                      help="For running MC only with selected data for pileup_data [default: \"\"]")
    parser.add_option("-n", "--nibsFile", dest="nibsFile", default="", type="string",
                      help="Input nib file [default: \"\"]")
    parser.add_option("-p", "--pileup", dest="pileup", default="pileup", type="string",
                      help="Name of the pileup histogram [default: \"\"]")
    parser.add_option("-x", "--minbias_xs", dest="minbias_xs", default="", type="string",
                      help="Minbias cross section for pileup calculation [default: \"\"]")

    (opts, args) = parser.parse_args()

    #if len(opts.name) > 0:
        #set_proc_name("python3/%s"%opts.name)
    #    setproctitle.setproctitle("ana/%s"%opts.name)
    main(opts, args)

