#include "Lumimask.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <regex>


Lumimask::Lumimask(std::string filename, short verbose){
  runmin = 355000;
  runmax = 400000;
  uint runmin0 = runmin;
  uint runmax0 = runmax;
  lumimaskfile = filename;
  if(filename.size() > 0){
    if(verbose > 0) std::cout << "Using lumi mask " << filename.substr(filename.find_last_of("/\\") + 1) << std::endl;
    std::ifstream fIN(filename, std::ifstream::in);
    std::string line;
    while (std::getline(fIN, line)) {
      parse_line(line);
    }
  }
  if((runmin-runmin0)!=0 || (runmax-runmax0)!=0) {
    std::cout << "Lumimask: Processing runs outside runrange. Change values in Lumimask.cc runmin,runmax" << std::endl;
    std::cout << "Exiting.." << std::endl;
    exit(1);
  }
  if(lumijson.size() == 0){
    std::cout << "Problem with lumijson " << filename << std::endl;
    std::cout << "Exiting.." << std::endl;
    exit(2);
  }
}

Lumimask::~Lumimask(){
  print("Processed lumis");
}

void Lumimask::print(std::string message){
  if(message.size() > 0) std::cout << message << std::endl;
  bool first = true;
  std::cout << "{";
  for(auto const& i: processedlumi){
    uint run = i.first;
    size_t index = 0;
    uint lumiarray[i.second.size()];
    for(auto const& j: i.second){
      lumiarray[index++] = j.first;
    }

    if(first){
      first = false;
    }else{
      std::cout << ", ";
    }
    std::cout << "\"" << run << "\"" << ": [" << compress(lumiarray,i.second.size()) << "]";
  }
  std::cout << "}" << std::endl;
}

std::string Lumimask::compress(uint arr[], size_t n){
  std::string returnstr;
  bool first = true;
  uint i = 0, j = 0;
  while (i < n) {
    j = i;
    while ((j + 1 < n) && (arr[j + 1] == arr[j] + 1)) j++;
    if(first){
      first = false;
    }else{
      returnstr += ", ";
    }
    returnstr += "["+std::to_string(arr[i])+", "+std::to_string(arr[j])+"]";
    i = j + 1;
  }
  return returnstr;
}

void Lumimask::parse_line(std::string line){

  std::smatch match;
  std::regex runs_re("\"(\\d{6})\": (\\[\\[.*?\\]\\])");

  std::smatch match2;
  std::regex lumis_re("\\[(\\d+), (\\d+)\\]");

  while (std::regex_search(line, match, runs_re) && match.size() > 0) {
    std::string matchstr = match.str(0);
    uint run = (uint) std::stoi(match.str(1));
    if(run < runmin) runmin = run;
    if(run > runmax) runmax = run;

    std::string lumiranges = match.str(2);
    while(std::regex_search(lumiranges,match2,lumis_re) && match2.size() > 0) {
      std::string match2str = match2.str(0);
      uint lumibegin = (uint)std::stoi(match2.str(1));
      uint lumiend = (uint)std::stoi(match2.str(2));
      for(uint i = lumibegin; i < lumiend+1; ++i) lumijson[run][i] = 1;
      lumiranges = lumiranges.substr(lumiranges.find(match2str)+match2str.size(),lumiranges.size());
    }
    line = line.substr(line.find(matchstr)+matchstr.size(),line.size());
  }
}

bool Lumimask::filter(uint run, uint lumi){
  processedlumi[run][lumi] = 1;
  if(lumimaskfile.size() == 0) return true;
  
  if(lumijson[run][lumi] == 1) return true;
  return false;
}

std::pair<uint,uint> Lumimask::getRunRange(){
  return std::pair<uint,uint>(runmin,runmax); 
}
