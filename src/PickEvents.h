#ifndef PickEvents_h
#define PickEvents_h

#include <string>
#include <vector>

class PickEvents {
 public:
  PickEvents(size_t NEV);
  PickEvents(size_t NEV,std::string filename);
  ~PickEvents();
  
  void fill(uint run, uint lumi, ulong event);
  void Write(std::string fname);

  bool pick(uint run, uint lumi, ulong event);

 private:
  std::vector<std::string> eventsOUT;
  size_t maxEvents;

  std::vector<std::string> eventsIN;
};
#endif
