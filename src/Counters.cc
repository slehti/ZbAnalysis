#include "Counters.h"
#include <iostream>

Counters::Counters(const char* n){
  name = n;
}

Counters::~Counters(){}

void Counters::book(std::string counterName, long value){
  if(unweightedCounter.count(counterName) == 0){
    counterOrder.push_back(counterName);
    unweightedCounter[counterName] = value;
    weightedCounter[counterName]   = value;
  }
}

void Counters::increment(std::string counterName, double weight){
  book(counterName);
  long sign = long(weight/fabs(weight));
  //std::cout << "check Counters::increment " << counterName << " " << weight << " " << sign << std::endl;
  unweightedCounter[counterName] += sign;
  weightedCounter[counterName] += weight;
}

void Counters::increment(const char* counterName, double weight){
  this->increment(std::string(counterName),weight);
}

double Counters::getCount(const char* counterName){
  return unweightedCounter[counterName];
}

void Counters::print(){
  std::cout << this->getName() << std::endl;
  for(std::vector<std::string>::const_iterator cname = counterOrder.begin(); cname != counterOrder.end(); cname++){
    std::string line = *cname;
    while (line.size() < 25) line+=' ';
    std::cout << "    " << line << " " << unweightedCounter[*cname] << std::endl;
  }
}

#include <bits/stdc++.h> 
void Counters::printBothCounters(){
  std::cout << this->getName() << std::endl;
  for(std::vector<std::string>::const_iterator cname = counterOrder.begin(); cname != counterOrder.end(); cname++){
    std::string line = "    ";
    line+=*cname;
    if(cname->size() > 0){
      while (line.size() < 25) line+=' ';
      std::stringstream ss1;
      ss1 << unweightedCounter[*cname];
      line+=ss1.str();
      while (line.size() < 40) line+=' ';
      std::stringstream ss2;
      ss2 << weightedCounter[*cname];
      line+=ss2.str();
      //    line+="%f"%(weightedCounter[*cname]);
    }
    std::cout << line << std::endl;
  }
}

std::string Counters::getName(){return name;}

TH1D* Counters::getHisto(std::string newname){
  int n = counterOrder.size();
  TH1D* histo = new TH1D(newname.c_str(),"",n,0,n-1);
  for(size_t i = 0; i < counterOrder.size(); i++){
    histo->GetXaxis()->SetBinLabel(i+1,counterOrder[i].c_str());
    histo->SetBinContent(i+1,unweightedCounter[counterOrder[i]]);
  }
  return histo;
}

TH1D* Counters::getWeightedHisto(std::string newname){
  int n = counterOrder.size();
  TH1D* histo = new TH1D(newname.c_str(),"",n,0,n-1);
  for(size_t i = 0; i < counterOrder.size(); i++){
    histo->GetXaxis()->SetBinLabel(i+1,counterOrder[i].c_str());
    histo->SetBinContent(i+1,weightedCounter[counterOrder[i]]);
  }
  return histo;
}
