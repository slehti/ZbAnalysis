#include "PickEvents.h"
#include <iostream>
#include <fstream>
#include <sstream>

PickEvents::PickEvents(size_t NEV = 0){
  maxEvents = NEV;
  eventsIN.clear();
  eventsOUT.clear();
}
PickEvents::PickEvents(size_t NEV,std::string filename){
  maxEvents = NEV;
  eventsIN.clear();
  eventsOUT.clear();

  std::string line;
  std::ifstream fIN(filename);
  if(fIN.is_open()) {
    while ( getline (fIN,line) ) {
      //std::cout << line << std::endl;
      eventsIN.push_back(line);
    }
    fIN.close();

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "\033[1;31mPicking " << eventsIN.size() << " events from file " << filename << "\033[0m" << std::endl;
    std::cout << std::endl;
  }
}

PickEvents::~PickEvents(){}
  
void PickEvents::fill(uint run, uint lumi, ulong event){
  if(maxEvents < 0 || eventsOUT.size() < maxEvents){

    std::stringstream ss;
    ss << run;
    ss << ":";
    ss << lumi;
    ss << ":";
    ss << event;
    std::string str_ev = ss.str();
    eventsOUT.push_back(str_ev);
  }
}
void PickEvents::Write(std::string fname){
  if(eventsOUT.size() > 0){
    std::ofstream fOUT;
    fOUT.open (fname);
    for(std::vector<std::string>::const_iterator i = eventsOUT.begin(); i!= eventsOUT.end(); ++i){
      fOUT << *i << std::endl;
    }
    fOUT.close();
  }
}

bool PickEvents::pick(uint run, uint lumi, ulong event){

  if(eventsIN.size() == 0) return true;

  std::stringstream ss;
    ss << run;
    ss << ":";
    ss << lumi;
    ss << ":";
    ss << event;
  std::string str_ev = ss.str();
  //std::cout << "PickEvents " << run << " " << lumi << " " << event << std::endl;
  for(std::vector<std::string>::const_iterator i = eventsIN.begin(); i!= eventsIN.end(); ++i){
    //std::cout << "       PickEvents " << *i << " " << str_ev << " " << i->compare(str_ev) << " " << std::endl;
    if(i->compare(str_ev) == 0) return true;
  }
  return false;
}
