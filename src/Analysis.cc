#include "Analysis.h"
#include <iostream>
#include <regex>
#include <iomanip>
#include <iterator>
#include <map>
#include <algorithm>
#include <math.h>

#include "Math/VectorUtil.h"
#include "TMath.h"
#include "TVector3.h"
#include "TStyle.h"

#include "parsePileUpJSON2.h"

Analysis::Analysis()
  : BaseSelector()
{
  time(&timer);

  //std::cout << "check Analysis::Analysis " << dummy << std::endl;
  cMain = new Counters("main counter");
  cJetLabel = new Counters("jet label");
  cJetLabel_btag = new Counters("jet label btagDeepBtight");
  cJetLabel_ctag = new Counters("jet label btagDeepCtight");
  cJetLabel_gluontag = new Counters("jet label gluontag");
  cJetLabel_quarktag = new Counters("jet label quarktag");
  cJetLabel_notag = new Counters("jet not tagged");
  cJetLabel_qgtLT0tag = new Counters("jet label q/gtag failed (-1)");

  //  TList* inputList = this->GetInputList();
  //  std::cout << "check name " << inputList->GetSize() << std::endl;
  //  for(const auto&& obj: *GetListOfPrimitives())
  //    obj->Write();
  /*
  TNamed* name = (TNamed*)inputList->FindObject("name");
  const char* datasetName = name->GetTitle();
  std::cout << "check name " << datasetName << std::endl;
  */
  //  HLT_test = {fReader, "HLT_IsoMu27"};
  //fOUT = TFile::Open("histograms.root","RECREATE");

  pickEvents = new PickEvents(10,"pickevents.txt");
  //pickEvents = new PickEvents(-1);

  l2resHistograms = new L2ResHistograms();
}

Analysis::~Analysis() {
  /*
  delete l1rc;
  delete l1;
  delete l2;
  delete l3;
  delete l2l3;
  delete jec_noL2L3;
  delete jec_withL2L3;
  */
  /*
  cMain->print();
  
  fOUT->cd();
  fOUT->mkdir("analysis");
  fOUT->cd("analysis");

  h_RpT->Write();
  h_RMPF->Write();
  fOUT->Write();
  fOUT->ls();
  fOUT->Close();
  */
}

void Analysis::printStatus(){
  time_t now;
  time(&now);
  double timediff = difftime(now,timer);
  std::string unit = "h";
  timediff = timediff/3600.;

  long allevents = cMain->getCount("all events");
  long frac = 100*allevents/maxEvents;
  double timeleft = timediff*(float(maxEvents)/allevents-1);
  if(timeleft < 1) {
    timeleft = timeleft*60;
    unit = "min";
  }
  if(int(cMain->getCount("all events"))%100000 == 0){
    if(int(cMain->getCount("all events")) == 0){
      std::cout << "    "<< datasetName << ", begin processing. Maxevents = " << maxEvents << endl;
    }else{
      std::cout << "    "<< datasetName << ", time used " << timediff << "h, left " << timeleft << unit <<", processed " << allevents << "/" << maxEvents << "(" << frac << "%)                                        " << endl;
    }
  }
}

void Analysis::Begin(TTree* tree) {
  //std::cout << "check Analysis::Begin" << std::endl;
  /*
  TList* inputList = this->GetInputList();
  std::cout << "check B size " << inputList->GetSize() << std::endl;
  TNamed* name = (TNamed*)inputList->FindObject("name");
  const char* datasetName = name->GetTitle();
  std::cout << "check name " << datasetName << std::endl;
  */

  //  HLT_test = {fReader, "HLT_IsoMu27"};
  //h_ZbosonPt   = new TH1D('Z pt','',20,0,200);
  //h_ZbosonPt_b = new TH1D('Z pt (b jet)','',20,0,200);
  //h_alpha      = new TH1D('alpha','',100,0,1);
  //h_bdiscr     = new TH1D('b discr','',100,0,1);

  datasetName = this->GetParameter<TNamed*>("name")->GetTitle();
  year = this->GetParameter<TNamed*>("year")->GetTitle();
  //std::cout << "check dset,year " << datasetName << " " << year << std::endl;  
  std::string resultDir = std::string(this->GetParameter<TNamed*>("outputdir")->GetTitle());
  //std::cout << "check outputdir " << resultDir << std::endl; 
  isData = (std::string(this->GetParameter<TNamed*>("isData")->GetTitle()) == '1');

  pu_data = (TH1D*)this->GetParameter<TH1D*>("pileup_data");

  pileupreweightingON = (std::string(this->GetParameter<TNamed*>("pileupreweight")->GetTitle()) == "True");
  if(!isData){
    if(pileupreweightingON) std::cout << datasetName << " PU reweighting ON" << std::endl;
    else std::cout << datasetName << " PU reweigting OFF" << std::endl;
  }
  if(pileupreweightingON){
    //TH1D* pu_data = (TH1D*)this->GetParameter<TH1D*>("pileup_data");
    TH1D* pu_mc   = (TH1D*)this->GetParameter<TH1D*>("pileup_mc");
    if(!isData){
      pileupWeight = new PileupWeight(pu_data,pu_mc);

      pu_mc_reweighted = (TH1D*)pu_mc->Clone("pileup_mc_reweighted");
      pu_mc_reweighted->Reset();

      h_pu_weight = pileupWeight->getPUWeightHistogram();
    }
  }

  if(isData){
    const char* lumimaskfile = this->GetParameter<TNamed*>("lumimask")->GetTitle();
    //std::cout << "check lumimaskfile " << lumimaskfile << std::endl;
    lumimask = new Lumimask(lumimaskfile);
  }
  gitCommit = this->GetParameter<TNamed*>("gitcommit")->GetTitle();

  TH1D* skimCounter = (TH1D*)this->GetParameter<TH1D*>("skimCounter");
  maxEvents = skimCounter->GetBinContent(skimCounter->GetNbinsX());
  for(int i = 1; i < skimCounter->GetNbinsX()+1; ++i){
    cMain->book(skimCounter->GetXaxis()->GetBinLabel(i),skimCounter->GetBinContent(i));
    //std::cout << "  " << skimCounter->GetXaxis()->GetBinLabel(i) << " " << skimCounter->GetBinContent(i) << std::endl;
  }

  triggerNames = this->GetParameters("trigger");
  //std::cout << "  Trigger OR:" << std::endl;
  //for(size_t i = 0; i < triggerNames.size(); ++i){
  //  std::cout << "    " << triggerNames[i] << std::endl;
  //}

  JECs = this->GetParameters("JECs");
  //std::cout << "  Input JECs:" << std::endl;
  //for(size_t i = 0; i < JECs.size(); ++i){
  //  std::cout << "    " << JECs[i] << std::endl;
  //}

  // Rochester corrections to muons
  // https://twiki.cern.ch/twiki/bin/viewauth/CMS/RochcorMuon
  // https://twiki.cern.ch/twiki/pub/CMS/RochcorMuon/roccor.Run2.v5.tgz
  std::smatch match;
  std::regex runs_re("_Run(\\d+\\S+?)_");
  std::string runs = "";
  if (std::regex_search(resultDir, match, runs_re) && match.size() > 0) {
    runs = match.str(1);
    runs = runs.replace(0,4,"");
  }

  std::string run = "";
  std::string s_datasetName = std::string(datasetName);
  //std::cout << "check s_datasetName " << s_datasetName << std::endl;
  if (std::regex_search(s_datasetName, match, runs_re) && match.size() > 0) {
    run = match.str(1);
    //    run = run.replace(0,4,"");
  }
  //std::cout << "check run " << run << std::endl;
  std::string rochesterCorrection = "";
  if(std::string(year).compare("2016") == 0) {
    if(std::string(runs).compare("BCDEF") == 0) rochesterCorrection = std::string("roccor.Run2.v5/RoccoR2016aUL.txt");
    if(std::string(runs).compare("FGH") == 0)   rochesterCorrection = std::string("roccor.Run2.v5/RoccoR2016bUL.txt");
  }
  if(std::string(year).compare("2017") == 0) rochesterCorrection = std::string("roccor.Run2.v5/RoccoR2017UL.txt");
  if(std::string(year).compare("2018") == 0) rochesterCorrection = std::string("roccor.Run2.v5/RoccoR2018UL.txt");
  //std::cout << "check rochesterCorrection " << rochesterCorrection << std::endl;
  rcFound = false;
  if ( access( rochesterCorrection.c_str(), F_OK ) != -1 ){
    std::cout << "Using RochesterCorrection " << rochesterCorrection << std::endl;
    rcFound = true;
    rc.init(rochesterCorrection);
    srand (static_cast <unsigned> (time(0)));
  }else std::cout << "Not using RochesterCorrection " << std::endl;

  eventWeight = 1;
  lumi = strtof(this->GetParameter<TNamed*>("lumi")->GetTitle(),0);
  //std::cout << "check lumi " << lumi << std::endl;
  //if(!isData) {
      //float lumi = strtof(this->GetParameter<TNamed*>("lumi")->GetTitle(),0);
      //float xsec = strtof(this->GetParameter<TNamed*>("xsec")->GetTitle(),0);
      //    std::string s_xsec = strtof(this->GetParameter<TNamed*>("xsec")->GetTitle(),0);
      //int nevents = skimCounter->GetBinContent(1);
      //eventWeight = lumi*xsec/nevents;
      //std::cout << "check lumi,xsec,nevents " << lumi << " " << xsec << " " << nevents << std::endl;
      //std::cout << "check eventWeight " << eventWeight << std::endl;
  //}
  if(isData){
    const char* pileupfile = this->GetParameter<TNamed*>("pileupfile")->GetTitle();
    double minbias_xs = strtof(this->GetParameter<TNamed*>("minbias_xs")->GetTitle(),0);
    parsePileUpJSON2(std::string(pileupfile),minbias_xs);
  }

  std::string jvyear = year;
  if(isData) jvyear = run;
  jetvetoMap = new JetvetoMap(jvyear,isData);

  leptonflavor = strtof(this->GetParameter<TNamed*>("leptonflavor")->GetTitle(),0);

  smearingON = (std::string(this->GetParameter<TNamed*>("smearing")->GetTitle()) == "True");
  if(!isData){
    if(smearingON) std::cout << datasetName << " Smearing ON" << std::endl;
    else std::cout << datasetName << " Smearing OFF" << std::endl;
  }
  npsWeights = 1;
  //if(!isData) npsWeights = 5;
  //psWeights = new float[npsWeights];

  fOUT = TFile::Open((resultDir+"/histograms.root").c_str(),"RECREATE");
  fOUT->mkdir("analysis");
  fOUT->cd("analysis");
  /*
  if(!isData){
    h_psweight0 = new TH1D("hPSWeight0","",300,0,3);
    h_psweight1 = new TH1D("hPSWeight1","",300,0,3);
    h_psweight2 = new TH1D("hPSWeight2","",300,0,3);
    h_psweight3 = new TH1D("hPSWeight3","",300,0,3);
  }
  */
  Double_t mubins[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100};
  Int_t  nmubins = sizeof(mubins)/sizeof(Double_t) - 1;

  h_Mu               = new TH1D("Mu","",nmubins,mubins);
  h_RhoVsMu          = new TH2D("RhoVsMu","",nmubins,mubins,nmubins,mubins);
  hprof_RhoVsMu      = new TProfile("hprof_RhoVsMu","",nmubins,mubins);
  h_NpvVsMu          = new TH2D("NpvVsMu","",nmubins,mubins,nmubins,mubins);
  h_Mu0              = new TH1D("Mu0","",nmubins,mubins);
  h_RhoVsMu0         = new TH2D("RhoVsMu0","",nmubins,mubins,nmubins,mubins);
  h_NpvVsMu0         = new TH2D("NpvVsMu0","",nmubins,mubins,nmubins,mubins);

  h_mu_noPUrw         = (TH1D*)h_Mu->Clone("h_mu_noPUrw");
  h_npvall_noPUrw     = (TH1D*)h_Mu->Clone("h_npvall_noPUrw");
  h_npvgood_noPUrw    = (TH1D*)h_Mu->Clone("h_npvgood_noPUrw");
  h_rho_noPUrw        = (TH1D*)h_Mu->Clone("h_rho_noPUrw");
  h_rho_central_noPUrw = (TH1D*)h_Mu->Clone("h_rho_central_noPUrw");
  h_rho_centralChargedPileUp_noPUrw = (TH1D*)h_Mu->Clone("h_rho_centralChargedPileUp_noPUrw");
  h_mu_afterPUrw      = (TH1D*)h_Mu->Clone("h_mu_afterPUrw");
  h_npvall_afterPUrw  = (TH1D*)h_Mu->Clone("h_npvall_afterPUrw");
  h_npvgood_afterPUrw = (TH1D*)h_Mu->Clone("h_npvgood_afterPUrw");
  h_rho_afterPUrw     = (TH1D*)h_Mu->Clone("h_rho_afterPUrw");
  h_rho_central_afterPUrw = (TH1D*)h_Mu->Clone("h_rho_central_afterPUrw");
  h_rho_centralChargedPileUp_afterPUrw = (TH1D*)h_Mu->Clone("h_rho_centralChargedPileUp_afterPUrw");

  if(isData){
    // Time stability of JEC
    std::pair<uint,uint> runrange = lumimask->getRunRange();
    double runmin = runrange.first - 0.5;
    double runmax = runrange.second + 0.5;
    int runsteps = int(runmax - runmin);
    hprof_db_run_zpt15   = new TProfile("db_run_zpt15",";Run;DB;",runsteps,runmin,runmax);
    hprof_db_run_zpt30   = new TProfile("db_run_zpt30",";Run;DB;",runsteps,runmin,runmax);
    hprof_db_run_zpt50   = new TProfile("db_run_zpt50",";Run;DB;",runsteps,runmin,runmax);
    hprof_db_run_zpt110  = new TProfile("db_run_zpt110",";Run;DB;",runsteps,runmin,runmax);
    hprof_mpf_run_zpt15  = new TProfile("mpf_run_zpt15",";Run;MPF;",runsteps,runmin,runmax);
    hprof_mpf_run_zpt30  = new TProfile("mpf_run_zpt30",";Run;MPF;",runsteps,runmin,runmax);
    hprof_mpf_run_zpt50  = new TProfile("mpf_run_zpt50",";Run;MPF;",runsteps,runmin,runmax);
    hprof_mpf_run_zpt110 = new TProfile("mpf_run_zpt110",";Run;MPF;",runsteps,runmin,runmax);

    hprof_db_run_jeta34   = new TProfile("db_run_jeta34",";Run;DB;",runsteps,runmin,runmax);
    hprof_db_run_jeta45   = new TProfile("db_run_jeta45",";Run;DB;",runsteps,runmin,runmax);
    hprof_mpf_run_jeta34   = new TProfile("mpf_run_jeta34",";Run;MPF;",runsteps,runmin,runmax);
    hprof_mpf_run_jeta45   = new TProfile("mpf_run_jeta45",";Run;MPF;",runsteps,runmin,runmax);

    hprof_chHEF_run_zpt15   = new TProfile("chf_run_zpt15",";Run;CHF;",runsteps,runmin,runmax);
    hprof_neHEF_run_zpt15   = new TProfile("nhf_run_zpt15",";Run;NHF;",runsteps,runmin,runmax);
    hprof_chEmEF_run_zpt15  = new TProfile("cef_run_zpt15",";Run;CEF;",runsteps,runmin,runmax);
    hprof_neEmEF_run_zpt15  = new TProfile("nef_run_zpt15",";Run;NEF;",runsteps,runmin,runmax);
    hprof_muEF_run_zpt15    = new TProfile("mef_run_zpt15",";Run;MEF;",runsteps,runmin,runmax);

    hprof_chHEF_run_zpt30   = new TProfile("chf_run_zpt30",";Run;CHF;",runsteps,runmin,runmax);
    hprof_neHEF_run_zpt30   = new TProfile("nhf_run_zpt30",";Run;NHF;",runsteps,runmin,runmax);
    hprof_chEmEF_run_zpt30  = new TProfile("cef_run_zpt30",";Run;CEF;",runsteps,runmin,runmax);
    hprof_neEmEF_run_zpt30  = new TProfile("nef_run_zpt30",";Run;NEF;",runsteps,runmin,runmax);
    hprof_muEF_run_zpt30    = new TProfile("mef_run_zpt30",";Run;MEF;",runsteps,runmin,runmax);

    hprof_chHEF_run_zpt50   = new TProfile("chf_run_zpt50",";Run;CHF;",runsteps,runmin,runmax);
    hprof_neHEF_run_zpt50   = new TProfile("nhf_run_zpt50",";Run;NHF;",runsteps,runmin,runmax);
    hprof_chEmEF_run_zpt50  = new TProfile("cef_run_zpt50",";Run;CEF;",runsteps,runmin,runmax);
    hprof_neEmEF_run_zpt50  = new TProfile("nef_run_zpt50",";Run;NEF;",runsteps,runmin,runmax);
    hprof_muEF_run_zpt50    = new TProfile("mef_run_zpt50",";Run;MEF;",runsteps,runmin,runmax);

    hprof_chHEF_run_zpt110  = new TProfile("chf_run_zpt110",";Run;CHF;",runsteps,runmin,runmax);
    hprof_neHEF_run_zpt110  = new TProfile("nhf_run_zpt110",";Run;NHF;",runsteps,runmin,runmax);
    hprof_chEmEF_run_zpt110 = new TProfile("cef_run_zpt110",";Run;CEF;",runsteps,runmin,runmax);
    hprof_neEmEF_run_zpt110 = new TProfile("nef_run_zpt110",";Run;NEF;",runsteps,runmin,runmax);
    hprof_muEF_run_zpt110   = new TProfile("mef_run_zpt110",";Run;MEF;",runsteps,runmin,runmax);

    hprof_db_run_JetPt15   = new TProfile("db_run_JetPt15",";Run;DB;",runsteps,runmin,runmax);
    hprof_db_run_JetPt30   = new TProfile("db_run_JetPt30",";Run;DB;",runsteps,runmin,runmax);
    hprof_db_run_JetPt50   = new TProfile("db_run_JetPt50",";Run;DB;",runsteps,runmin,runmax);
    hprof_db_run_JetPt110  = new TProfile("db_run_JetPt110",";Run;DB;",runsteps,runmin,runmax);
    hprof_mpf_run_JetPt15  = new TProfile("mpf_run_JetPt15",";Run;MPF;",runsteps,runmin,runmax);
    hprof_mpf_run_JetPt30  = new TProfile("mpf_run_JetPt30",";Run;MPF;",runsteps,runmin,runmax);
    hprof_mpf_run_JetPt50  = new TProfile("mpf_run_JetPt50",";Run;MPF;",runsteps,runmin,runmax);
    hprof_mpf_run_JetPt110 = new TProfile("mpf_run_JetPt110",";Run;MPF;",runsteps,runmin,runmax);

    hprof_chHEF_run_JetPt15   = new TProfile("chf_run_JetPt15",";Run;CHF;",runsteps,runmin,runmax);
    hprof_neHEF_run_JetPt15   = new TProfile("nhf_run_JetPt15",";Run;NHF;",runsteps,runmin,runmax);
    hprof_chEmEF_run_JetPt15  = new TProfile("cef_run_JetPt15",";Run;CEF;",runsteps,runmin,runmax);
    hprof_neEmEF_run_JetPt15  = new TProfile("nef_run_JetPt15",";Run;NEF;",runsteps,runmin,runmax);
    hprof_muEF_run_JetPt15    = new TProfile("mef_run_JetPt15",";Run;MEF;",runsteps,runmin,runmax);

    hprof_chHEF_run_JetPt30   = new TProfile("chf_run_JetPt30",";Run;CHF;",runsteps,runmin,runmax);
    hprof_neHEF_run_JetPt30   = new TProfile("nhf_run_JetPt30",";Run;NHF;",runsteps,runmin,runmax);
    hprof_chEmEF_run_JetPt30  = new TProfile("cef_run_JetPt30",";Run;CEF;",runsteps,runmin,runmax);
    hprof_neEmEF_run_JetPt30  = new TProfile("nef_run_JetPt30",";Run;NEF;",runsteps,runmin,runmax);
    hprof_muEF_run_JetPt30    = new TProfile("mef_run_JetPt30",";Run;MEF;",runsteps,runmin,runmax);

    hprof_chHEF_run_JetPt50   = new TProfile("chf_run_JetPt50",";Run;CHF;",runsteps,runmin,runmax);
    hprof_neHEF_run_JetPt50   = new TProfile("nhf_run_JetPt50",";Run;NHF;",runsteps,runmin,runmax);
    hprof_chEmEF_run_JetPt50  = new TProfile("cef_run_JetPt50",";Run;CEF;",runsteps,runmin,runmax);
    hprof_neEmEF_run_JetPt50  = new TProfile("nef_run_JetPt50",";Run;NEF;",runsteps,runmin,runmax);
    hprof_muEF_run_JetPt50    = new TProfile("mef_run_JetPt50",";Run;MEF;",runsteps,runmin,runmax);

    hprof_chHEF_run_JetPt110  = new TProfile("chf_run_JetPt110",";Run;CHF;",runsteps,runmin,runmax);
    hprof_neHEF_run_JetPt110  = new TProfile("nhf_run_JetPt110",";Run;NHF;",runsteps,runmin,runmax);
    hprof_chEmEF_run_JetPt110 = new TProfile("cef_run_JetPt110",";Run;CEF;",runsteps,runmin,runmax);
    hprof_neEmEF_run_JetPt110 = new TProfile("nef_run_JetPt110",";Run;NEF;",runsteps,runmin,runmax);
    hprof_muEF_run_JetPt110   = new TProfile("mef_run_JetPt110",";Run;MEF;",runsteps,runmin,runmax);

    hprof_db_run_PtAve15   = new TProfile("db_run_PtAve15",";Run;DB;",runsteps,runmin,runmax);
    hprof_db_run_PtAve30   = new TProfile("db_run_PtAve30",";Run;DB;",runsteps,runmin,runmax);
    hprof_db_run_PtAve50   = new TProfile("db_run_PtAve50",";Run;DB;",runsteps,runmin,runmax);
    hprof_db_run_PtAve110  = new TProfile("db_run_PtAve110",";Run;DB;",runsteps,runmin,runmax);
    hprof_mpf_run_PtAve15  = new TProfile("mpf_run_PtAve15",";Run;MPF;",runsteps,runmin,runmax);
    hprof_mpf_run_PtAve30  = new TProfile("mpf_run_PtAve30",";Run;MPF;",runsteps,runmin,runmax);
    hprof_mpf_run_PtAve50  = new TProfile("mpf_run_PtAve50",";Run;MPF;",runsteps,runmin,runmax);
    hprof_mpf_run_PtAve110 = new TProfile("mpf_run_PtAve110",";Run;MPF;",runsteps,runmin,runmax);

    hprof_chHEF_run_PtAve15   = new TProfile("chf_run_PtAve15",";Run;CHF;",runsteps,runmin,runmax);
    hprof_neHEF_run_PtAve15   = new TProfile("nhf_run_PtAve15",";Run;NHF;",runsteps,runmin,runmax);
    hprof_chEmEF_run_PtAve15  = new TProfile("cef_run_PtAve15",";Run;CEF;",runsteps,runmin,runmax);
    hprof_neEmEF_run_PtAve15  = new TProfile("nef_run_PtAve15",";Run;NEF;",runsteps,runmin,runmax);
    hprof_muEF_run_PtAve15    = new TProfile("mef_run_PtAve15",";Run;MEF;",runsteps,runmin,runmax);

    hprof_chHEF_run_PtAve30   = new TProfile("chf_run_PtAve30",";Run;CHF;",runsteps,runmin,runmax);
    hprof_neHEF_run_PtAve30   = new TProfile("nhf_run_PtAve30",";Run;NHF;",runsteps,runmin,runmax);
    hprof_chEmEF_run_PtAve30  = new TProfile("cef_run_PtAve30",";Run;CEF;",runsteps,runmin,runmax);
    hprof_neEmEF_run_PtAve30  = new TProfile("nef_run_PtAve30",";Run;NEF;",runsteps,runmin,runmax);
    hprof_muEF_run_PtAve30    = new TProfile("mef_run_PtAve30",";Run;MEF;",runsteps,runmin,runmax);

    hprof_chHEF_run_PtAve50   = new TProfile("chf_run_PtAve50",";Run;CHF;",runsteps,runmin,runmax);
    hprof_neHEF_run_PtAve50   = new TProfile("nhf_run_PtAve50",";Run;NHF;",runsteps,runmin,runmax);
    hprof_chEmEF_run_PtAve50  = new TProfile("cef_run_PtAve50",";Run;CEF;",runsteps,runmin,runmax);
    hprof_neEmEF_run_PtAve50  = new TProfile("nef_run_PtAve50",";Run;NEF;",runsteps,runmin,runmax);
    hprof_muEF_run_PtAve50    = new TProfile("mef_run_PtAve50",";Run;MEF;",runsteps,runmin,runmax);

    hprof_chHEF_run_PtAve110  = new TProfile("chf_run_PtAve110",";Run;CHF;",runsteps,runmin,runmax);
    hprof_neHEF_run_PtAve110  = new TProfile("nhf_run_PtAve110",";Run;NHF;",runsteps,runmin,runmax);
    hprof_chEmEF_run_PtAve110 = new TProfile("cef_run_PtAve110",";Run;CEF;",runsteps,runmin,runmax);
    hprof_neEmEF_run_PtAve110 = new TProfile("nef_run_PtAve110",";Run;NEF;",runsteps,runmin,runmax);
    hprof_muEF_run_PtAve110   = new TProfile("mef_run_PtAve110",";Run;MEF;",runsteps,runmin,runmax);

    hprof_mz_run_zpt15   = new TProfile("mz_run_zpt15",";Run;mZ;",runsteps,runmin,runmax);
    hprof_mz_run_zpt30   = new TProfile("mz_run_zpt30",";Run;mZ;",runsteps,runmin,runmax);
    hprof_mz_run_jeta34  = new TProfile("mz_run_jeta34",";Run;mZ;",runsteps,runmin,runmax);
    hprof_mz_run_jeta45  = new TProfile("mz_run_jeta45",";Run;mZ;",runsteps,runmin,runmax);
    hprof_mz_run_zpt50   = new TProfile("mz_run_zpt50",";Run;mZ;",runsteps,runmin,runmax);
    hprof_mz_run_zpt110  = new TProfile("mz_run_zpt110",";Run;mZ;",runsteps,runmin,runmax);
    hprof_mz_run_zpt0    = new TProfile("mz_run_zpt0",";Run;mZ;",runsteps,runmin,runmax);
    hprof_nz_run_zpt15   = new TH1D("nz_run_zpt15",";Run;NZ;",runsteps,runmin,runmax);
    hprof_nz_run_zpt30   = new TH1D("nz_run_zpt30",";Run;NZ;",runsteps,runmin,runmax);
    hprof_nz_run_jeta34  = new TH1D("nz_run_jeta34",";Run;NZ;",runsteps,runmin,runmax);
    hprof_nz_run_jeta45  = new TH1D("nz_run_jeta45",";Run;NZ;",runsteps,runmin,runmax);
    hprof_nz_run_zpt50   = new TH1D("nz_run_zpt50",";Run;NZ;",runsteps,runmin,runmax);
    hprof_nz_run_zpt110  = new TH1D("nz_run_zpt110",";Run;NZ;",runsteps,runmin,runmax);
    hprof_nz_run_zpt0    = new TH1D("nz_run_zpt0",";Run;NZ;",runsteps,runmin,runmax);
    hprof_nz_run_incl    = new TH1D("nz_run_incl",";Run;events;",runsteps,runmin,runmax);

    hprof_mz_run_JetPt15   = new TProfile("mz_run_JetPt15",";Run;mZ;",runsteps,runmin,runmax);
    hprof_mz_run_JetPt30   = new TProfile("mz_run_JetPt30",";Run;mZ;",runsteps,runmin,runmax);
    hprof_mz_run_JetPt50   = new TProfile("mz_run_JetPt50",";Run;mZ;",runsteps,runmin,runmax);
    hprof_mz_run_JetPt110  = new TProfile("mz_run_JetPt110",";Run;mZ;",runsteps,runmin,runmax);
    hprof_nz_run_JetPt15   = new TH1D("nz_run_JetPt15",";Run;NZ;",runsteps,runmin,runmax);
    hprof_nz_run_JetPt30   = new TH1D("nz_run_JetPt30",";Run;NZ;",runsteps,runmin,runmax);
    hprof_nz_run_JetPt50   = new TH1D("nz_run_JetPt50",";Run;NZ;",runsteps,runmin,runmax);
    hprof_nz_run_JetPt110  = new TH1D("nz_run_JetPt110",";Run;NZ;",runsteps,runmin,runmax);

    hprof_mz_run_PtAve15   = new TProfile("mz_run_PtAve15",";Run;mZ;",runsteps,runmin,runmax);
    hprof_mz_run_PtAve30   = new TProfile("mz_run_PtAve30",";Run;mZ;",runsteps,runmin,runmax);
    hprof_mz_run_PtAve50   = new TProfile("mz_run_PtAve50",";Run;mZ;",runsteps,runmin,runmax);
    hprof_mz_run_PtAve110  = new TProfile("mz_run_PtAve110",";Run;mZ;",runsteps,runmin,runmax);
    hprof_nz_run_PtAve15   = new TH1D("nz_run_PtAve15",";Run;NZ;",runsteps,runmin,runmax);
    hprof_nz_run_PtAve30   = new TH1D("nz_run_PtAve30",";Run;NZ;",runsteps,runmin,runmax);
    hprof_nz_run_PtAve50   = new TH1D("nz_run_PtAve50",";Run;NZ;",runsteps,runmin,runmax);
    hprof_nz_run_PtAve110  = new TH1D("nz_run_PtAve110",";Run;NZ;",runsteps,runmin,runmax);
  }

  h_njets            = new TH1D("njets","",50,0,50);
  h_njets_sele       = new TH1D("njets_selected","",50,0,50);
  h_njets_aas        = new TH1D("njets_aas","",50,0,50);
  h_njets_id         = new TH1D("njets_id","",50,0,50);
  h_njets_pt         = new TH1D("njets_pt","",50,0,50);

  h_Jet_jetId        = new TH1D("Jet_jetId","",10,-1,9);
  h_Jet_electronIdx1 = new TH1D("Jet_electronIdx1","",10,-1,9);
  h_Jet_electronIdx2 = new TH1D("Jet_electronIdx2","",10,-1,9);
  h_Jet_muonIdx1     = new TH1D("Jet_muonIdx1","",10,-1,9);
  h_Jet_muonIdx2     = new TH1D("Jet_muonIdx2","",10,-1,9);
  h_Jet_nElectrons   = new TH1D("Jet_nElectrons","",10,-1,9);
  h_Jet_nMuons       = new TH1D("Jet_nMuons","",10,-1,9);
  h_Jet_puId         = new TH1D("Jet_puId","",10,-1,9);

  //Double_t zptbins[] = {0,50,100,150,200,250,300,350,400,450,500};
  Double_t zptbins[] = {12, 15, 20, 25, 30, 35, 40, 45, 50, 60, 70, 85, 105, 130, 175, 230, 300, 400, 500, 700, 1000, 1500};
  Int_t  nzptbins = sizeof(zptbins)/sizeof(Double_t) - 1;

  //  h_jet1pt   = new TH1D("jet1pt","",nzptbins,zptbins);
  h_jet1pt   = new TH1D("jet1pt","",1750,0,1750);
  h_jet1eta  = new TH1D("jet1eta","",120,-6,6);
  h_jet2pt   = new TH1D("jet2pt","",1750,0,1750);
  h_jet2eta  = new TH1D("jet2eta","",120,-6,6);

  h_jetseta_pt20  = (TH1D*) h_jet1eta->Clone("h_jetseta_pt20");
  h_jetseta_pt30  = (TH1D*) h_jet1eta->Clone("h_jetseta_pt30");
  h_jetseta_pt40  = (TH1D*) h_jet1eta->Clone("h_jetseta_pt40");

  h_jet_smearOff = new TH1D("jetpt_smearOff","",250,0,250);
  h_jet_smearOn  = new TH1D("jetpt_smearOn","",250,0,250);
  h_jet_smearFactor = new TH1D("jetpt_smearFactor","",300,0.7,1.3);

  h_jetseta_pt20  = (TH1D*) h_jet1eta->Clone("h_jetseta_pt20");
  h_jetseta_pt30  = (TH1D*) h_jet1eta->Clone("h_jetseta_pt30");
  h_jetseta_pt40  = (TH1D*) h_jet1eta->Clone("h_jetseta_pt40");

  h_lep1pt   = new TH1D("lep1pt","",1750,0,1750);
  h_lep1eta  = new TH1D("lep1eta","",100,-5,5);
  h_lep2pt   = new TH1D("lep2pt","",1750,0,1750);
  h_lep2eta  = new TH1D("lep2eta","",100,-5,5);

  h_lep1pt_hltoveroffline  = new TH1D("lep1pt_hltoveroffline","",200,0,2);
  h_lep2pt_hltoveroffline  = new TH1D("lep2pt_hltoveroffline","",200,0,2);

  h_phibb    = new TH1D("phibb","",128,0,6.4);

  //  Double_t ptbins[] = {1, 15, 21, 28, 37, 49, 64, 84, 114, 153, 196, 272, 330, 395, 468, 548, 686, 846, 1032, 1248, 1588, 2000, 2500, 3103, 3832, 4713, 5777, 7000};
  Double_t ptbins[] = {1, 5, 6, 8, 10, 12, 15, 18, 21, 24, 28, 32, 37, 43, 49, 56, 64, 74, 84, 97, 114, 133, 153, 174, 196, 220, 245, 272, 300, 330, 362, 395, 430, 468, 507, 548, 592, 638, 686, 737, 790, 846, 905, 967, 1032, 1101, 1172, 1248, 1327, 1410, 1497, 1588, 1684, 1784, 1890, 2000, 2116, 2238, 2366, 2500, 2640, 2787, 2941, 3103, 3273, 3450, 3637, 3832, 4037, 4252, 4477, 4713, 4961, 5220, 5492, 5777, 6076, 6389, 6717, 7000};

  Int_t  nptbins = sizeof(ptbins)/sizeof(Double_t) - 1;
  /*
  h_JetPt_chHEF  = new TProfile("JetPt_chHEF","",nptbins,ptbins);
  h_JetPt_neHEF  = new TProfile("JetPt_neHEF","",nptbins,ptbins);
  h_JetPt_neEmEF = new TProfile("JetPt_neEmE","",nptbins,ptbins);
  h_JetPt_chEmEF = new TProfile("JetPt_chEmEF","",nptbins,ptbins);
  h_JetPt_muEF   = new TProfile("JetPt_muEF","",nptbins,ptbins);
  */
  h_jet1cpt  = new TH1D("jet1cpt","",250,0,250);
  h_jet1ceta = new TH1D("jet1ceta","",50,-2.5,2.5);
  h_jet2cpt  = new TH1D("jet2cpt","",250,0,250);
  h_jet2ceta = new TH1D("jet2ceta","",50,-2.5,2.5);

  h_jet2etapt = new TH2D("jet2etapt","",600,-3.0,3.0,100,0,100);

  h_Zpt      = new TH1D("Zpt","",nzptbins,zptbins);
  h_Zeta     = new TH1D("Zeta","",100,-5,5);
  h_leptonpt = new TH1D("leptonpt","",250,0,250);
  h_leptoneta= new TH1D("leptoneta","",100,-5,5);
  h_Zpt_a10  = new TH1D("Zpt_a10","alpha<0.10",nzptbins,zptbins);
  h_Zpt_a15  = new TH1D("Zpt_a15","alpha<0.15",nzptbins,zptbins);
  h_Zpt_a20  = new TH1D("Zpt_a20","alpha<0.20",nzptbins,zptbins);
  h_Zpt_a30  = new TH1D("Zpt_a30","alpha<0.30",nzptbins,zptbins);
  h_Zpt_a40  = new TH1D("Zpt_a40","alpha<0.40",nzptbins,zptbins);
  h_Zpt_a50  = new TH1D("Zpt_a50","alpha<0.50",nzptbins,zptbins);
  h_Zpt_a60  = new TH1D("Zpt_a60","alpha<0.60",nzptbins,zptbins);
  h_Zpt_a80  = new TH1D("Zpt_a80","alpha<0.80",nzptbins,zptbins);
  h_Zpt_a100 = new TH1D("Zpt_a100","alpha<1.0",nzptbins,zptbins);

  //Double_t bins_alpha_[] = {0.0,0.1,0.15,0.2,0.25,0.3,0.4,0.5,0.6,0.8,1.0,1.2};
  //Double_t bins_alpha_[] = {0.0,0.001,0.05,0.1,0.15,0.2,0.25,0.3,0.4,0.5,0.6,0.8,1.0,1.2};
  Double_t bins_alpha_[] = {0.3,1.0};
  nbins_alpha = sizeof(bins_alpha_)/sizeof(Double_t);
  bins_alpha = new Double_t[nbins_alpha];
  for(size_t i = 0; i < nbins_alpha+1; i++) bins_alpha[i] = bins_alpha_[i];

  Double_t bins_pt_[] = {15,30};
  nbins_pt = sizeof(bins_pt_)/sizeof(Double_t);
  bins_pt = new Double_t[nbins_pt];
  for(size_t i = 0; i < nbins_pt+1; i++) bins_pt[i] = bins_pt_[i];
  //std::cout << "check bins " << nbins_alpha << std::endl;
  //for(size_t i = 0; i < nbins_alpha; i++) std::cout << "      check bins  " << bins_alpha[i] << std::endl;

  //  bins_alpha = bins_alpha_;

  //Double_t binsy[] = {-0.5,-0.4,-0.3,-0.2,-0.1,0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5};
  //Int_t  nbinsy = sizeof(binsy)/sizeof(Double_t) - 1;

//  Double_t binsy[] = {-0.5,-0.45,-0.4,-0.35,-0.3,-0.25,-0.2,-0.15,-0.1,-0.05,0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1,1.05,1.1,1.15,1.2,1.25,1.3,1.35,1.4,1.45,1.5,1.55,1.6,1.65,1.7,1.75,1.8,1.85,1.9,1.95,2,2.05,2.1,2.15,2.2,2.25,2.3,2.35,2.4,2.45,2.5};
  /*
  Double_t binsy[] = {-1.51,-1.49,-1.47,-1.45,-1.43,-1.41,-1.39,-1.37,-1.35,-1.33,-1.31,-1.29,-1.27,-1.25,-1.23,-1.21,-1.19,-1.17,-1.15,-1.13,-1.11,-1.09,-1.07,-1.05,-1.03,-1.01,-0.99,-0.97,-0.95,-0.93,-0.91,-0.89,-0.87,-0.85,-0.83,-0.81,-0.79,-0.77,-0.75,-0.73,-0.71,-0.69,-0.67,-0.65,-0.63,-0.61,-0.59,-0.57,-0.55,-0.53,-0.51,-0.49,-0.47,-0.45,-0.43,-0.41,-0.39,-0.37,-0.35,-0.33,-0.31,-0.29,-0.27,-0.25,-0.23,-0.21,-0.19,-0.17,-0.15,-0.13,-0.11,-0.09,-0.07,-0.05,-0.03,-0.01,0.01,0.03,0.05,0.07,0.09,0.11,0.13,0.15,0.17,0.19,0.21,0.23,0.25,0.27,0.29,0.31,0.33,0.35,0.37,0.39,0.41,0.43,0.45,0.47,0.49,0.51,0.53,0.55,0.57,0.59,0.61,0.63,0.65,0.67,0.69,0.71,0.73,0.75,0.77,0.79,0.81,0.83,0.85,0.87,0.89,0.91,0.93,0.95,0.97,0.99,1.01,1.03,1.05,1.07,1.09,1.11,1.13,1.15,1.17,1.19,1.21,1.23,1.25,1.27,1.29,1.31,1.33,1.35,1.37,1.39,1.41,1.43,1.45,1.47,1.49,1.51,1.53,1.55,1.57,1.59,1.61,1.63,1.65,1.67,1.69,1.71,1.73,1.75,1.77,1.79,1.81,1.83,1.85,1.87,1.89,1.91,1.93,1.95,1.97,1.99,2.01,2.03,2.05,2.07,2.09,2.11,2.13,2.15,2.17,2.19,2.21,2.23,2.25,2.27,2.29,2.31,2.33,2.35,2.37,2.39,2.41,2.43,2.45,2.47,2.49,2.51,2.53,2.55,2.57,2.59,2.61,2.63,2.65,2.67,2.69,2.71,2.73,2.75,2.77,2.79,2.81,2.83,2.85,2.87,2.89,2.91,2.93,2.95,2.97,2.99,3.01,3.03,3.05,3.07,3.09,3.11,3.13,3.15,3.17,3.19,3.21,3.23,3.25,3.27,3.29,3.31,3.33,3.35,3.37,3.39,3.41,3.43,3.45,3.47,3.49,3.51};
  UInt_t  nbinsy = sizeof(binsy)/sizeof(Double_t) - 1;
  */
  Double_t binsy[] = {-3.99,-3.97,-3.95,-3.93,-3.91,-3.89,-3.87,-3.85,-3.83,-3.81,-3.79,-3.77,-3.75,-3.73,-3.71,-3.69,-3.67,-3.65,-3.63,-3.61,-3.59,-3.57,-3.55,-3.53,-3.51,-3.49,-3.47,-3.45,-3.43,-3.41,-3.39,-3.37,-3.35,-3.33,-3.31,-3.29,-3.27,-3.25,-3.23,-3.21,-3.19,-3.17,-3.15,-3.13,-3.11,-3.09,-3.07,-3.05,-3.03,-3.01,-2.99,-2.97,-2.95,-2.93,-2.91,-2.89,-2.87,-2.85,-2.83,-2.81,-2.79,-2.77,-2.75,-2.73,-2.71,-2.69,-2.67,-2.65,-2.63,-2.61,-2.59,-2.57,-2.55,-2.53,-2.51,-2.49,-2.47,-2.45,-2.43,-2.41,-2.39,-2.37,-2.35,-2.33,-2.31,-2.29,-2.27,-2.25,-2.23,-2.21,-2.19,-2.17,-2.15,-2.13,-2.11,-2.09,-2.07,-2.05,-2.03,-2.01,-1.99,-1.97,-1.95,-1.93,-1.91,-1.89,-1.87,-1.85,-1.83,-1.81,-1.79,-1.77,-1.75,-1.73,-1.71,-1.69,-1.67,-1.65,-1.63,-1.61,-1.59,-1.57,-1.55,-1.53,-1.51,-1.49,-1.47,-1.45,-1.43,-1.41,-1.39,-1.37,-1.35,-1.33,-1.31,-1.29,-1.27,-1.25,-1.23,-1.21,-1.19,-1.17,-1.15,-1.13,-1.11,-1.09,-1.07,-1.05,-1.03,-1.01,-0.99,-0.97,-0.95,-0.93,-0.91,-0.89,-0.87,-0.85,-0.83,-0.81,-0.79,-0.77,-0.75,-0.73,-0.71,-0.69,-0.67,-0.65,-0.63,-0.61,-0.59,-0.57,-0.55,-0.53,-0.51,-0.49,-0.47,-0.45,-0.43,-0.41,-0.39,-0.37,-0.35,-0.33,-0.31,-0.29,-0.27,-0.25,-0.23,-0.21,-0.19,-0.17,-0.15,-0.13,-0.11,-0.09,-0.07,-0.05,-0.03,-0.01,0.01,0.03,0.05,0.07,0.09,0.11,0.13,0.15,0.17,0.19,0.21,0.23,0.25,0.27,0.29,0.31,0.33,0.35,0.37,0.39,0.41,0.43,0.45,0.47,0.49,0.51,0.53,0.55,0.57,0.59,0.61,0.63,0.65,0.67,0.69,0.71,0.73,0.75,0.77,0.79,0.81,0.83,0.85,0.87,0.89,0.91,0.93,0.95,0.97,0.99,1.01,1.03,1.05,1.07,1.09,1.11,1.13,1.15,1.17,1.19,1.21,1.23,1.25,1.27,1.29,1.31,1.33,1.35,1.37,1.39,1.41,1.43,1.45,1.47,1.49,1.51,1.53,1.55,1.57,1.59,1.61,1.63,1.65,1.67,1.69,1.71,1.73,1.75,1.77,1.79,1.81,1.83,1.85,1.87,1.89,1.91,1.93,1.95,1.97,1.99,2.01,2.03,2.05,2.07,2.09,2.11,2.13,2.15,2.17,2.19,2.21,2.23,2.25,2.27,2.29,2.31,2.33,2.35,2.37,2.39,2.41,2.43,2.45,2.47,2.49,2.51,2.53,2.55,2.57,2.59,2.61,2.63,2.65,2.67,2.69,2.71,2.73,2.75,2.77,2.79,2.81,2.83,2.85,2.87,2.89,2.91,2.93,2.95,2.97,2.99,3.01,3.03,3.05,3.07,3.09,3.11,3.13,3.15,3.17,3.19,3.21,3.23,3.25,3.27,3.29,3.31,3.33,3.35,3.37,3.39,3.41,3.43,3.45,3.47,3.49,3.51,3.53,3.55,3.57,3.59,3.61,3.63,3.65,3.67,3.69,3.71,3.73,3.75,3.77,3.79,3.81,3.83,3.85,3.87,3.89,3.91,3.93,3.95,3.97,3.99,4.01,4.03,4.05,4.07,4.09,4.11,4.13,4.15,4.17,4.19,4.21,4.23,4.25,4.27,4.29,4.31,4.33,4.35,4.37,4.39,4.41,4.43,4.45,4.47,4.49,4.51,4.53,4.55,4.57,4.59,4.61,4.63,4.65,4.67,4.69,4.71,4.73,4.75,4.77,4.79,4.81,4.83,4.85,4.87,4.89,4.91,4.93,4.95,4.97,4.99,5.01,5.03,5.05,5.07,5.09,5.11,5.13,5.15,5.17,5.19,5.21,5.23,5.25,5.27,5.29,5.31,5.33,5.35,5.37,5.39,5.41,5.43,5.45,5.47,5.49,5.51,5.53,5.55,5.57,5.59,5.61,5.63,5.65,5.67,5.69,5.71,5.73,5.75,5.77,5.79,5.81,5.83,5.85,5.87,5.89,5.91,5.93,5.95,5.97,5.99};
  UInt_t  nbinsy = sizeof(binsy)/sizeof(Double_t) - 1;

  //Double_t etabins[] = {0, 0.261, 0.522, 0.783, 1.044, 1.305, 1.566, 1.74, 1.93, 2.043, 2.172, 2.322, 2.5, 2.65, 2.853, 2.964, 3.139, 3.489, 3.839, 5.191};
  //Double_t etabins[] = {0., 0.261, 0.522, 0.783, 1.044, 1.305, 1.479, 1.653, 1.93, 2.172, 2.322, 2.5, 2.65, 2.853, 2.964, 3.139, 3.314, 3.489, 3.839, 4.013, 4.583, 5.191};
  Double_t etabins[] = {0, 0.087, 0.174, 0.261, 0.348, 0.435, 0.522, 0.609, 0.696, 0.783, 0.879, 0.957, 1.044, 1.131, 1.218, 1.305, 1.392, 1.479, 1.566, 1.653, 1.74, 1.83, 1.93, 2.043, 2.172, 2.322, 2.5, 2.65, 2.853, 2.964, 3.139, 3.314, 3.489, 3.664, 3.839, 4.013, 4.191, 4.363, 4.538, 4.716, 4.889, 5.191};
  UInt_t netabins = sizeof(etabins)/sizeof(Double_t) - 1;

  /*
  Double_t bins_eta_[] = {0.0,1.3,1.9,2.5};
  nbins_eta = sizeof(bins_eta_)/sizeof(Double_t) - 1;
  bins_eta = new Double_t[nbins_eta];
  for(size_t i = 0; i < nbins_eta+1; i++) bins_eta[i] = bins_eta_[i];
  */
  
  /*
  Int_t  nbinsy = 201;
  Double_t vmin = -1.51;
  Double_t step = 0.02;
  Double_t binsy[nbinsy];
  for(Int_t i = 0; i < nbinsy+1; ++i){
    binsy[i] = vmin + step*i;
    std::cout << binsy[i] << ",";
  }
  std::cout << std::endl;
< */
  //  bins_eta = {"0.0_1.3","1.3_1.9","1.9_2.5","0.0_2.5"};
  bins_eta = {"0.0_1.3","0.0_2.5","2.5_3.0","3.0_5.0","4.7_5.0"};
  cuts = {"0_30","30_50","50_70","70_100","100_140","140_200","200_Inf"};

  //h_alpha      = new TH1D("alpha","",nbinsy,binsy);

  //h_alphaZpt = new TH1D*[cuts.size()];
  //for(size_t i = 0; i < cuts.size(); ++i){
  //  h_alphaZpt[i] = (TH1D*)h_alpha->Clone(("alpha_Zpt"+cuts[i]).c_str());
  //}
  /*
  h_RpT        = new TH1D("R_pT","",nbins_alpha,bins_alpha);
  h_RMPF       = new TH1D("R_MPF","",nbins_alpha,bins_alpha);
  */
  h_METorig    = new TH1D("METorig","",100,0,100);
  h_MET        = new TH1D("MET","",100,0,100);

  /*
  h_alpha_RpT  = new TH2D("h_alpha_RpT","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF = new TH2D("h_alpha_RMPF","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_genb  = new TH2D("h_alpha_RpT_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_genb = new TH2D("h_alpha_RMPF_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);

  h_alpha_RpT_btagCSVV2loose = new TH2D("h_alpha_RpT_btagCSVV2loose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCSVV2loose = new TH2D("h_alpha_RMPF_btagCSVV2loose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagCSVV2medium = new TH2D("h_alpha_RpT_btagCSVV2medium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCSVV2medium = new TH2D("h_alpha_RMPF_btagCSVV2medium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagCSVV2tight = new TH2D("h_alpha_RpT_btagCSVV2tight","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCSVV2tight = new TH2D("h_alpha_RMPF_btagCSVV2tight","",nbins_alpha,bins_alpha,nbinsy,binsy);

  h_alpha_RpT_btagCSVV2loose_genb = new TH2D("h_alpha_RpT_btagCSVV2loose_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCSVV2loose_genb = new TH2D("h_alpha_RMPF_btagCSVV2loose_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagCSVV2medium_genb = new TH2D("h_alpha_RpT_btagCSVV2medium_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCSVV2medium_genb = new TH2D("h_alpha_RMPF_btagCSVV2medium_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagCSVV2tight_genb = new TH2D("h_alpha_RpT_btagCSVV2tight_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCSVV2tight_genb = new TH2D("h_alpha_RMPF_btagCSVV2tight_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);

  h_alpha_RpT_btagCMVAloose = new TH2D("h_alpha_RpT_btagCMVAloose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCMVAloose = new TH2D("h_alpha_RMPF_btagCMVAloose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagCMVAmedium = new TH2D("h_alpha_RpT_btagCMVAmedium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCMVAmedium = new TH2D("h_alpha_RMPF_btagCMVAmedium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagCMVAtight = new TH2D("h_alpha_RpT_btagCMVAtight","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCMVAtight = new TH2D("h_alpha_RMPF_btagCMVAtight","",nbins_alpha,bins_alpha,nbinsy,binsy);

  h_alpha_RpT_btagDeepBloose = new TH2D("h_alpha_RpT_btagDeepBloose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepBloose = new TH2D("h_alpha_RMPF_btagDeepBloose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepBmedium = new TH2D("h_alpha_RpT_btagDeepBmedium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepBmedium = new TH2D("h_alpha_RMPF_DeepBmedium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btag = new TH2D("h_alpha_RpT_btag","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btag = new TH2D("h_alpha_RMPF_btag","",nbins_alpha,bins_alpha,nbinsy,binsy);

  h_alpha_RpT_btagDeepBloose_genb = new TH2D("h_alpha_RpT_btagDeepBloose_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepBloose_genb = new TH2D("h_alpha_RMPF_btagDeepBloose_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepBmedium_genb = new TH2D("h_alpha_RpT_btagDeepBmedium_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepBmedium_genb = new TH2D("h_alpha_RMPF_DeepBmedium_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btag_genb = new TH2D("h_alpha_RpT_btag_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btag_genb = new TH2D("h_alpha_RMPF_btag_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);

  h_alpha_RpT_btagDeepFlavBloose = new TH2D("h_alpha_RpT_btagDeepFlavBloose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepFlavBloose = new TH2D("h_alpha_RMPF_btagDeepFlavBloose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepFlavBmedium = new TH2D("h_alpha_RpT_btagDeepFlavBmedium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepFlavBmedium = new TH2D("h_alpha_RMPF_DeepFlavBmedium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepFlavBtight = new TH2D("h_alpha_RpT_btagDeepFlavBtight","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepFlavBtight = new TH2D("h_alpha_RMPF_btagDeepFlavBtight","",nbins_alpha,bins_alpha,nbinsy,binsy);
  
  h_alpha_RpT_btagDeepFlavBloose_genb = new TH2D("h_alpha_RpT_btagDeepFlavBloose_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepFlavBloose_genb = new TH2D("h_alpha_RMPF_btagDeepFlavBloose_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepFlavBmedium_genb = new TH2D("h_alpha_RpT_btagDeepFlavBmedium_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepFlavBmedium_genb = new TH2D("h_alpha_RMPF_DeepFlavBmedium_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepFlavBtight_genb = new TH2D("h_alpha_RpT_btagDeepFlavBtight_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepFlavBtight_genb = new TH2D("h_alpha_RMPF_btagDeepFlavBtight_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);


  h_alpha_RpT_Zpt = new TH2D*[cuts.size()];
  h_alpha_RMPF_Zpt = new TH2D*[cuts.size()];
  h_alpha_RpT_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RMPF_Zpt_genb = new TH2D*[cuts.size()];

  h_alpha_RpT_btagCSVV2loose_Zpt = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagCSVV2loose_Zpt = new TH2D*[cuts.size()];
  h_alpha_RpT_btagCSVV2medium_Zpt = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagCSVV2medium_Zpt = new TH2D*[cuts.size()];
  h_alpha_RpT_btagCSVV2tight_Zpt = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagCSVV2tight_Zpt = new TH2D*[cuts.size()];
  h_alpha_RpT_btagCSVV2loose_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagCSVV2loose_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RpT_btagCSVV2medium_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagCSVV2medium_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RpT_btagCSVV2tight_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagCSVV2tight_Zpt_genb = new TH2D*[cuts.size()];

  h_alpha_RpT_btagDeepBloose_Zpt = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagDeepBloose_Zpt = new TH2D*[cuts.size()];
  h_alpha_RpT_btagDeepBmedium_Zpt = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagDeepBmedium_Zpt = new TH2D*[cuts.size()];
  h_alpha_RpT_btag_Zpt = new TH2D*[cuts.size()];
  h_alpha_RMPF_btag_Zpt = new TH2D*[cuts.size()];
  h_alpha_RpT_btagDeepBloose_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagDeepBloose_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RpT_btagDeepBmedium_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagDeepBmedium_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RpT_btag_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RMPF_btag_Zpt_genb = new TH2D*[cuts.size()];

  h_alpha_RpT_btagDeepFlavBloose_Zpt = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagDeepFlavBloose_Zpt = new TH2D*[cuts.size()];
  h_alpha_RpT_btagDeepFlavBmedium_Zpt = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagDeepFlavBmedium_Zpt = new TH2D*[cuts.size()];
  h_alpha_RpT_btagDeepFlavBtight_Zpt = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagDeepFlavBtight_Zpt = new TH2D*[cuts.size()];
  h_alpha_RpT_btagDeepFlavBloose_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagDeepFlavBloose_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RpT_btagDeepFlavBmedium_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagDeepFlavBmedium_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RpT_btagDeepFlavBtight_Zpt_genb = new TH2D*[cuts.size()];
  h_alpha_RMPF_btagDeepFlavBtight_Zpt_genb = new TH2D*[cuts.size()];

  for(size_t i = 0; i < cuts.size(); ++i){
    h_alpha_RpT_Zpt[i] = (TH2D*)h_alpha_RpT->Clone(("h_alpha_RpT_Zpt"+cuts[i]).c_str());
    h_alpha_RMPF_Zpt[i] = (TH2D*)h_alpha_RMPF->Clone(("h_alpha_RMPF_Zpt"+cuts[i]).c_str());
    h_alpha_RpT_Zpt_genb[i] = (TH2D*)h_alpha_RpT->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_genb").c_str());
    h_alpha_RMPF_Zpt_genb[i] = (TH2D*)h_alpha_RMPF->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_genb").c_str());

    h_alpha_RpT_btagCSVV2loose_Zpt[i] = (TH2D*)h_alpha_RpT_btagCSVV2loose->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagCSVV2loose").c_str());
    h_alpha_RMPF_btagCSVV2loose_Zpt[i] = (TH2D*)h_alpha_RMPF_btagCSVV2loose->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagCSVV2loose").c_str());
    h_alpha_RpT_btagCSVV2medium_Zpt[i] = (TH2D*)h_alpha_RpT_btagCSVV2medium->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagCSVV2medium").c_str());
    h_alpha_RMPF_btagCSVV2medium_Zpt[i] = (TH2D*)h_alpha_RMPF_btagCSVV2medium->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagCSVV2medium").c_str());
    h_alpha_RpT_btagCSVV2tight_Zpt[i] = (TH2D*)h_alpha_RpT_btagCSVV2tight->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagCSVV2tight").c_str());
    h_alpha_RMPF_btagCSVV2tight_Zpt[i] = (TH2D*)h_alpha_RMPF_btagCSVV2tight->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagCSVV2tight").c_str());
    h_alpha_RpT_btagCSVV2loose_Zpt_genb[i] = (TH2D*)h_alpha_RpT_btagCSVV2loose->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagCSVV2loose"+"_genb").c_str());
    h_alpha_RMPF_btagCSVV2loose_Zpt_genb[i] = (TH2D*)h_alpha_RMPF_btagCSVV2loose->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagCSVV2loose"+"_genb").c_str());
    h_alpha_RpT_btagCSVV2medium_Zpt_genb[i] = (TH2D*)h_alpha_RpT_btagCSVV2medium->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagCSVV2medium"+"_genb").c_str());
    h_alpha_RMPF_btagCSVV2medium_Zpt_genb[i] = (TH2D*)h_alpha_RMPF_btagCSVV2medium->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagCSVV2medium"+"_genb").c_str());
    h_alpha_RpT_btagCSVV2tight_Zpt_genb[i] = (TH2D*)h_alpha_RpT_btagCSVV2tight->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagCSVV2tight"+"_genb").c_str());
    h_alpha_RMPF_btagCSVV2tight_Zpt_genb[i] = (TH2D*)h_alpha_RMPF_btagCSVV2tight->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagCSVV2tight"+"_genb").c_str());

    h_alpha_RpT_btagDeepBloose_Zpt[i] = (TH2D*)h_alpha_RpT_btagDeepBloose->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeeploose").c_str());
    h_alpha_RMPF_btagDeepBloose_Zpt[i] = (TH2D*)h_alpha_RMPF_btagDeepBloose->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepBloose").c_str());
    h_alpha_RpT_btagDeepBmedium_Zpt[i] = (TH2D*)h_alpha_RpT_btagDeepBmedium->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepBmedium").c_str());
    h_alpha_RMPF_btagDeepBmedium_Zpt[i] = (TH2D*)h_alpha_RMPF_btagDeepBmedium->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepBmedium").c_str());
    h_alpha_RpT_btag_Zpt[i] = (TH2D*)h_alpha_RpT_btag->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btag").c_str());
    h_alpha_RMPF_btag_Zpt[i] = (TH2D*)h_alpha_RMPF_btag->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btag").c_str());
    h_alpha_RpT_btagDeepBloose_Zpt_genb[i] = (TH2D*)h_alpha_RpT_btagDeepBloose->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepBloose"+"_genb").c_str());
    h_alpha_RMPF_btagDeepBloose_Zpt_genb[i] = (TH2D*)h_alpha_RMPF_btagDeepBloose->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepBloose"+"_genb").c_str());
    h_alpha_RpT_btagDeepBmedium_Zpt_genb[i] = (TH2D*)h_alpha_RpT_btagDeepBmedium->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepBmedium"+"_genb").c_str());
    h_alpha_RMPF_btagDeepBmedium_Zpt_genb[i] = (TH2D*)h_alpha_RMPF_btagDeepBmedium->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepBmedium"+"_genb").c_str());
    h_alpha_RpT_btag_Zpt_genb[i] = (TH2D*)h_alpha_RpT_btag->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btag"+"_genb").c_str());
    h_alpha_RMPF_btag_Zpt_genb[i] = (TH2D*)h_alpha_RMPF_btag->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btag"+"_genb").c_str());

    h_alpha_RpT_btagDeepFlavBloose_Zpt[i] = (TH2D*)h_alpha_RpT_btagDeepFlavBloose->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepFlavBloose").c_str());
    h_alpha_RMPF_btagDeepFlavBloose_Zpt[i] = (TH2D*)h_alpha_RMPF_btagDeepFlavBloose->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepFlavBloose").c_str());
    h_alpha_RpT_btagDeepFlavBmedium_Zpt[i] = (TH2D*)h_alpha_RpT_btagDeepFlavBmedium->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepFlavBmedium").c_str());
    h_alpha_RMPF_btagDeepFlavBmedium_Zpt[i] = (TH2D*)h_alpha_RMPF_btagDeepFlavBmedium->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepFlavBmedium").c_str());
    h_alpha_RpT_btagDeepFlavBtight_Zpt[i] = (TH2D*)h_alpha_RpT_btagDeepFlavBtight->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepFlavBtight").c_str());
    h_alpha_RMPF_btagDeepFlavBtight_Zpt[i] = (TH2D*)h_alpha_RMPF_btagDeepFlavBtight->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepFlavBtight").c_str());
    h_alpha_RpT_btagDeepFlavBloose_Zpt_genb[i] = (TH2D*)h_alpha_RpT_btagDeepFlavBloose->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepFlavBloose"+"_genb").c_str());
    h_alpha_RMPF_btagDeepFlavBloose_Zpt_genb[i] = (TH2D*)h_alpha_RMPF_btagDeepFlavBloose->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepFlavBloose"+"_genb").c_str());
    h_alpha_RpT_btagDeepFlavBmedium_Zpt_genb[i] = (TH2D*)h_alpha_RpT_btagDeepFlavBmedium->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepFlavBmedium"+"_genb").c_str());
    h_alpha_RMPF_btagDeepFlavBmedium_Zpt_genb[i] = (TH2D*)h_alpha_RMPF_btagDeepFlavBmedium->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepFlavBmedium"+"_genb").c_str());
    h_alpha_RpT_btagDeepFlavBtight_Zpt_genb[i] = (TH2D*)h_alpha_RpT_btagDeepFlavBtight->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepFlavBtight"+"_genb").c_str());
    h_alpha_RMPF_btagDeepFlavBtight_Zpt_genb[i] = (TH2D*)h_alpha_RMPF_btagDeepFlavBtight->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepFlavBtight"+"_genb").c_str());
  }
  */
  //Double_t massbins[] = {50,60,70,80,90,100,110,120,130,140,150};
  Double_t massbins[] = {70.0,70.5,71.0,71.5,72.0,72.5,73.0,73.5,74.0,74.5,75.0,75.5,76.0,76.5,77.0,77.5,78.0,78.5,79.0,79.5,80.0,80.5,81.0,81.5,82.0,82.5,83.0,83.5,84.0,84.5,85.0,85.5,86.0,86.5,87.0,87.5,88.0,88.5,89.0,89.5,90.0,90.5,91.0,91.5,92.0,92.5,93.0,93.5,94.0,94.5,95.0,95.5,96.0,96.5,97.0,97.5,98.0,98.5,99.0,99.5,100.0,100.5,101.0,101.5,102.0,102.5,103.0,103.5,104.0,104.5,105.0,105.5,106.0,106.5,107.0,107.5,108.0,108.5,109.0,109.5,110.0,110.5,111.0,111.5,112.0,112.5,113.0,113.5,114.0,114.5,115.0,115.5,116.0,116.5,117.0,117.5,118.0,118.5,119.0,119.5,120.0};
  UInt_t  nmassbins = sizeof(massbins)/sizeof(Double_t) - 1;
  h_Zmass = new TH1D("Zmass","",nmassbins,massbins);
  h_Mmumu = new TH1D("mumu_mass","",500,0,500);

  h_Zpt_mZ_template = new TH2D("h_Zpt_mZ_temp","",nzptbins,zptbins,nmassbins,massbins);
  Double_t rhobins[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50};
  UInt_t  nrhobins = sizeof(rhobins)/sizeof(Double_t) - 1;
  h_Zpt_Rho_template = new TH2D("h_Zpt_Rho_temp","",nzptbins,zptbins,nrhobins,rhobins);
  Double_t efbins[] = {0.0,0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.1,0.11,0.12,0.13,0.14,0.15,0.16,0.17,0.18,0.19,0.2,0.21,0.22,0.23,0.24,0.25,0.26,0.27,0.28,0.29,0.3,0.31,0.32,0.33,0.34,0.35,0.36,0.37,0.38,0.39,0.4,0.41,0.42,0.43,0.44,0.45,0.46,0.47,0.48,0.49,0.5,0.51,0.52,0.53,0.54,0.55,0.56,0.57,0.58,0.59,0.6,0.61,0.62,0.63,0.64,0.65,0.66,0.67,0.68,0.69,0.7,0.71,0.72,0.73,0.74,0.75,0.76,0.77,0.78,0.79,0.8,0.81,0.82,0.83,0.84,0.85,0.86,0.87,0.88,0.89,0.9,0.91,0.92,0.93,0.94,0.95,0.96,0.97,0.98,0.99,1.0};
  UInt_t  nefbins = sizeof(efbins)/sizeof(Double_t) - 1;
  h_Zpt_ef_template = new TH2D("h_Zpt_EF_temp","",nzptbins,zptbins,nefbins,efbins);
  Double_t jnpfbins[] = {-5,-4.9,-4.8,-4.7,-4.6,-4.5,-4.4,-4.3,-4.2,-4.1,-4.0,-3.9,-3.8,-3.7,-3.6,-3.5,-3.4,-3.3,-3.2,-3.1,-3.0,-2.9,-2.8,-2.7,-2.6,-2.5,-2.4,-2.3,-2.2,-2.1,-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1.0,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4.0,4.1,4.2,4.3,4.4,4.5,4.6,4.7,4.8,4.9,5.0};
  UInt_t  njnpfbins = sizeof(jnpfbins)/sizeof(Double_t) - 1;
  h_Zpt_jnpf_template = new TH2D("h_Zpt_JNPF_temp","",nzptbins,zptbins,njnpfbins,jnpfbins);

  h_pTGenJet_JESRpTGen = new TH2D("h_pTGenJet_JESRpTGen","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRpTGen_genb = new TH2D("h_pTGenJet_JESRpTGen_genb","",nzptbins,zptbins,nbinsy,binsy); 
  h_pTGenJet_JESRpTGen_genc = new TH2D("h_pTGenJet_JESRpTGen_genc","",nzptbins,zptbins,nbinsy,binsy); 
  h_pTGenJet_JESRpTGen_geng = new TH2D("h_pTGenJet_JESRpTGen_geng","",nzptbins,zptbins,nbinsy,binsy); 
  h_pTGenJet_JESRpTGen_genuds = new TH2D("h_pTGenJet_JESRpTGen_genuds","",nzptbins,zptbins,nbinsy,binsy); 
  h_pTGenJet_JESRpTGen_unclassified = new TH2D("h_pTGenJet_JESRpTGen_unclassified","",nzptbins,zptbins,nbinsy,binsy);

  h_pTGenJet_JESRMPFGen = new TH2D("h_pTGenJet_JESRMPFGen","",nzptbins,zptbins,nbinsy,binsy); 
  h_pTGenJet_JESRMPFGen_genb = new TH2D("h_pTGenJet_JESRMPFGen_genb","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRMPFGen_genc = new TH2D("h_pTGenJet_JESRMPFGen_genc","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRMPFGen_geng = new TH2D("h_pTGenJet_JESRMPFGen_geng","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRMPFGen_genuds = new TH2D("h_pTGenJet_JESRMPFGen_genuds","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRMPFGen_unclassified = new TH2D("h_pTGenJet_JESRMPFGen_unclassified","",nzptbins,zptbins,nbinsy,binsy);
  /*
  h_pTGenJet_JESRpTGen = new TH2D("h_pTGenJet_JESRpTGen","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRpTGen_genb = new TH2D("h_pTGenJet_JESRpTGen_genb","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRpTGen_genc = new TH2D("h_pTGenJet_JESRpTGen_genc","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRpTGen_geng = new TH2D("h_pTGenJet_JESRpTGen_geng","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRpTGen_genuds = new TH2D("h_pTGenJet_JESRpTGen_genuds","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRpTGen_unclassified = new TH2D("h_pTGenJet_JESRpTGen_unclassified","",nzptbins,zptbins,nbinsy,binsy);

  h_pTGenJet_JESRMPFGen = new TH2D("h_pTGenJet_JESRMPFGen","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRMPFGen_genb = new TH2D("h_pTGenJet_JESRMPFGen_genb","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRMPFGen_genc = new TH2D("h_pTGenJet_JESRMPFGen_genc","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRMPFGen_geng = new TH2D("h_pTGenJet_JESRMPFGen_geng","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRMPFGen_genuds = new TH2D("h_pTGenJet_JESRMPFGen_genuds","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRMPFGen_unclassified = new TH2D("h_pTGenJet_JESRMPFGen_unclassified","",nzptbins,zptbins,nbinsy,binsy);
  */
  /*
  Double_t ymin = -1.5, ymax = 3.5;
  h_pTGenJet_JESRpTGen->BuildOptions(ymin,ymax,"i");
  h_pTGenJet_JESRpTGen_genb->BuildOptions(ymin,ymax,"i"); 
  h_pTGenJet_JESRpTGen_genc->BuildOptions(ymin,ymax,"i");
  h_pTGenJet_JESRpTGen_geng->BuildOptions(ymin,ymax,"i");
  h_pTGenJet_JESRpTGen_genuds->BuildOptions(ymin,ymax,"i");
  h_pTGenJet_JESRpTGen_unclassified->BuildOptions(ymin,ymax,"i");
  h_pTGenJet_JESRMPFGen->BuildOptions(ymin,ymax,"i");
  h_pTGenJet_JESRMPFGen_genb->BuildOptions(ymin,ymax,"i");
  h_pTGenJet_JESRMPFGen_genc->BuildOptions(ymin,ymax,"i");
  h_pTGenJet_JESRMPFGen_geng->BuildOptions(ymin,ymax,"i");
  h_pTGenJet_JESRMPFGen_genuds->BuildOptions(ymin,ymax,"i");
  h_pTGenJet_JESRMPFGen_unclassified->BuildOptions(ymin,ymax,"i");
  */
  h_pTGenZ_JESRpTGen        = new TH2D("h_pTGenZ_JESRpTGen","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRpTGen_genb   = new TH2D("h_pTGenZ_JESRpTGen_genb","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRpTGen_genc   = new TH2D("h_pTGenZ_JESRpTGen_genc","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRpTGen_geng   = new TH2D("h_pTGenZ_JESRpTGen_geng","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRpTGen_genuds = new TH2D("h_pTGenZ_JESRpTGen_genuds","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRpTGen_unclassified = new TH2D("h_pTGenZ_JESRpTGen_unclassified","",nzptbins,zptbins,nbinsy,binsy);

  h_pTGenZ_JESRMPFGen        = new TH2D("h_pTGenZ_JESRMPFGen","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRMPFGen_genb   = new TH2D("h_pTGenZ_JESRMPFGen_genb","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRMPFGen_genc   = new TH2D("h_pTGenZ_JESRMPFGen_genc","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRMPFGen_geng   = new TH2D("h_pTGenZ_JESRMPFGen_geng","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRMPFGen_genuds = new TH2D("h_pTGenZ_JESRMPFGen_genuds","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRMPFGen_unclassified = new TH2D("h_pTGenZ_JESRMPFGen_unclassified","",nzptbins,zptbins,nbinsy,binsy);

  h_Zpt_R_template = new TH2D("h_Zpt_R_temp","",nzptbins,zptbins,nbinsy,binsy);
  h_Zpt_R_template->GetXaxis()->SetTitle("p_{T}^{Z}");
  h3D_Zpt_R_eta_template = new TH3D("h3D_Zpt_R_eta_temp","",nzptbins,zptbins,nbinsy,binsy,netabins,etabins);
  h3D_Zpt_R_eta_template->GetXaxis()->SetTitle("p_{T}^{Z}");
  h3D_Zpt_R_mu_template = new TProfile2D("h3D_Zpt_R_mu_temp","",nzptbins,zptbins,nbinsy,binsy);//,nmubins,mubins);
  h3D_Zpt_R_mu_template->GetXaxis()->SetTitle("p_{T}^{Z}");

  tprof2D_eta_Zpt_RMPF_template = new TProfile2D("p2D_eta_Zpt_temp","",netabins,etabins,nzptbins,zptbins);

  h_Zpt_leptonpt_template = new TH2D("h_Zpt_leptonpt_temp","",nzptbins,zptbins,200,0,200);
  h_JetPt_QGL_template = new TH2D("h_JetPt_QGL_temp","",nptbins,ptbins,nefbins,efbins);
  h_Zpt_QGL_template = new TH2D("h_Zpt_QGL_temp","",nzptbins,zptbins,nefbins,efbins);
  h_JetPt_muEF_template = new TH2D("h_JetPt_muEF_temp","",nptbins,ptbins,nefbins,efbins);
  h_JetPt_chEmEF_template = new TH2D("h_JetPt_chEmEF_temp","",nptbins,ptbins,nefbins,efbins);
  h_JetPt_chHEF_template = new TH2D("h_JetPt_chHEF_temp","",nptbins,ptbins,nefbins,efbins);
  h_JetPt_neEmEF_template = new TH2D("h_JetPt_neEmEF_temp","",nptbins,ptbins,nefbins,efbins);
  h_JetPt_neHEF_template = new TH2D("h_JetPt_neHEF_temp","",nptbins,ptbins,nefbins,efbins);

  h_PtAve_QGL_template = new TH2D("h_PtAve_QGL_temp","",nptbins,ptbins,nefbins,efbins);
  h_PtAve_muEF_template = new TH2D("h_PtAve_muEF_temp","",nptbins,ptbins,nefbins,efbins);
  h_PtAve_chEmEF_template = new TH2D("h_PtAve_chEmEF_temp","",nptbins,ptbins,nefbins,efbins);
  h_PtAve_chHEF_template = new TH2D("h_PtAve_chHEF_temp","",nptbins,ptbins,nefbins,efbins);
  h_PtAve_neEmEF_template = new TH2D("h_PtAve_neEmEF_temp","",nptbins,ptbins,nefbins,efbins);
  h_PtAve_neHEF_template = new TH2D("h_PtAve_neHEF_temp","",nptbins,ptbins,nefbins,efbins);

  h_Zpt_Mu_Rho_template      = new TProfile2D("h3D_Zpt_Mu_Rho_temp","",nzptbins,zptbins,nmubins,mubins);//,nmubins,mubins);
  h_Zpt_Mu_NPVgood_template  = new TProfile2D("h3D_Zpt_Mu_NPVgood_temp","",nzptbins,zptbins,nmubins,mubins);//,nmubins,mubins);
  h_Zpt_Mu_NPVall_template   = new TProfile2D("h3D_Zpt_Mu_NPVall_temp","",nzptbins,zptbins,nmubins,mubins);//,nmubins,mubins);
  h_Zpt_Mu_Mu_template       = new TProfile2D("h3D_Zpt_Mu_Mu_temp","",nzptbins,zptbins,nmubins,mubins);//,nmubins,mubins);
  h_Pt_Mu_RpT_template       = new TProfile2D("h3D_Pt_Mu_RpT_temp","",nzptbins,zptbins,nmubins,mubins);//,nbinsy,binsy);

  h_Mu_RpT_template      = new TH2D("h_Mu_RpT_temp","",nmubins,mubins,nbinsy,binsy);
  h_Mu_RMPF_template     = new TH2D("h_Mu_RMPF_temp","",nmubins,mubins,nbinsy,binsy);
  h_JetPt_RMPF_template     = new TH2D("h_JetPt_RMPF_temp","",nzptbins,zptbins,nbinsy,binsy);
  h_PtAve_RMPF_template     = new TH2D("h_PtAve_RMPF_temp","",nzptbins,zptbins,nbinsy,binsy);
  h_Mu_RMPFjet1_template = new TH2D("h_Mu_RMPFjet1_temp","",nmubins,mubins,nbinsy,binsy);
  h_JetPt_RMPFjet1_template = new TH2D("h_JetPt_RMPFjet1_temp","",nzptbins,zptbins,nbinsy,binsy);
  h_PtAve_RMPFjet1_template = new TH2D("h_PtAve_RMPFjet1_temp","",nzptbins,zptbins,nbinsy,binsy);
  h_Mu_RMPFjetn_template = new TH2D("h_Mu_RMPFjetn_temp","",nmubins,mubins,nbinsy,binsy);
  h_JetPt_RMPFjetn_template = new TH2D("h_JetPt_RMPFjetn_temp","",nzptbins,zptbins,nbinsy,binsy);
  h_PtAve_RMPFjetn_template = new TH2D("h_PtAve_RMPFjetn_temp","",nzptbins,zptbins,nbinsy,binsy);
  h_Mu_RMPFuncl_template = new TH2D("h_Mu_RMPFuncl_temp","",nmubins,mubins,nbinsy,binsy);
  h_JetPt_RMPFuncl_template = new TH2D("h_JetPt_RMPFuncl_temp","",nzptbins,zptbins,nbinsy,binsy);
  h_PtAve_RMPFuncl_template = new TH2D("h_PtAve_RMPFuncl_temp","",nzptbins,zptbins,nbinsy,binsy);

  h_Mu_ptdiff_template      = new TH2D("h_Mu_ptdiff_temp","",nmubins,mubins,nefbins,efbins);

  h_JetPt_RpT_template   = new TH2D("h_JetPt_RpT_temp","",nzptbins,zptbins,nbinsy,binsy);
  h_PtAve_RpT_template   = new TH2D("h_PtAve_RpT_temp","",nzptbins,zptbins,nbinsy,binsy);

  gStyle->SetOptStat(111111);
  h_pTgen_R_template = new TH2D("h_pTgen_R_temp","",nzptbins,zptbins,nbinsy,binsy);
  h_pTgen_R_template->GetXaxis()->SetTitle("p_{T}^{leading jet(gen)}");
  size_t nbins = nbins_alpha*bins_eta.size()*npsWeights;
  size_t nbinsj = nbins_pt*bins_eta.size()*npsWeights;
  size_t nbins3D = nbins_alpha*npsWeights;
  size_t nbinsj3D = nbins_pt*npsWeights;

  h_Zpt_mZ = new TH2D*[nbins];
  h_Zpt_mZgen = new TH2D*[nbins];
  h_Zpt_Rho = new TH2D*[nbins];
  h_JetPt_Rho = new TH2D*[nbins];
  h_PtAve_Rho = new TH2D*[nbins];

  h_Zpt_chHEF = new TH2D*[nbins];
  h_Zpt_neHEF = new TH2D*[nbins];
  h_Zpt_chEmEF = new TH2D*[nbins];
  h_Zpt_neEmEF = new TH2D*[nbins];
  h_Zpt_muEF = new TH2D*[nbins];

  h_Zpt_RpT = new TH2D*[nbins];
  h_Zpt_RMPF  = new TH2D*[nbins];
  h_Zpt_RMPFx = new TH2D*[nbins];
  h_Zpt_RMPFjet1  = new TH2D*[nbins];
  h_Zpt_RMPFjet1x = new TH2D*[nbins];
  h_Zpt_RMPFjetn  = new TH2D*[nbins];
  h_Zpt_RMPFuncl  = new TH2D*[nbins];
  h_Zpt_RMPFfromType1  = new TH2D*[nbins];
  h_Zpt_RMPFPF  = new TH2D*[nbins];

  h3D_Zpt_RMPF_mu = new TProfile2D*[nbins3D];
  h3D_Zpt_RMPFx_mu = new TProfile2D*[nbins3D];
  h3D_Zpt_RMPFj2pt_mu = new TProfile2D*[nbinsj3D];
  h3D_Zpt_RMPFj2ptx_mu = new TProfile2D*[nbinsj3D];

  tprof2D_eta_Zpt_RMPF = new TProfile2D*[nbins3D];
  tprof2D_eta_Zpt_RMPFx = new TProfile2D*[nbins3D];

  h3D_Zpt_RMPF_eta = new TH3D*[nbins3D];
  h3D_Zpt_RMPFx_eta = new TH3D*[nbins3D];
  h3D_Zpt_RMPFj2pt_eta = new TH3D*[nbinsj3D];
  h3D_Zpt_RMPFj2ptx_eta = new TH3D*[nbinsj3D];

  h_Zpt_RMPFj2pt  = new TH2D*[nbinsj];
  h_Zpt_RMPFj2pt_genb  = new TH2D*[nbinsj];
  h_Zpt_RMPFj2pt_genc  = new TH2D*[nbinsj];
  h_Zpt_RMPFj2pt_genuds  = new TH2D*[nbinsj];
  h_Zpt_RMPFj2pt_geng  = new TH2D*[nbinsj];
  h_Zpt_RMPFj2pt_unclassified  = new TH2D*[nbinsj];

  h_Zpt_RMPFjet1j2pt  = new TH2D*[nbinsj];
  h_Zpt_RMPFjet1j2pt_genb  = new TH2D*[nbinsj];
  h_Zpt_RMPFjet1j2pt_genc  = new TH2D*[nbinsj];
  h_Zpt_RMPFjet1j2pt_genuds  = new TH2D*[nbinsj];
  h_Zpt_RMPFjet1j2pt_geng  = new TH2D*[nbinsj];
  h_Zpt_RMPFjet1j2pt_unclassified  = new TH2D*[nbinsj];

  h_Zpt_RMPFj2ptx = new TH2D*[nbins];
  h_Zpt_RMPFjet1j2ptx = new TH2D*[nbins];

  h_JetPt_QGL = new TH2D*[nbins];
  h_JetPt_QGL_genb = new TH2D*[nbins];
  h_JetPt_QGL_genc = new TH2D*[nbins];
  h_JetPt_QGL_genuds = new TH2D*[nbins];
  h_JetPt_QGL_geng = new TH2D*[nbins];
  h_JetPt_QGL_unclassified = new TH2D*[nbins];
  h_Zpt_QGL = new TH2D*[nbins];
  h_Zpt_QGL_genb = new TH2D*[nbins];
  h_Zpt_QGL_genc = new TH2D*[nbins];
  h_Zpt_QGL_genuds = new TH2D*[nbins];
  h_Zpt_QGL_geng = new TH2D*[nbins];
  h_Zpt_QGL_unclassified = new TH2D*[nbins];
  h_JetPt_muEF = new TH2D*[nbins];
  h_JetPt_chEmEF = new TH2D*[nbins];
  h_JetPt_chHEF = new TH2D*[nbins];
  h_JetPt_neEmEF = new TH2D*[nbins];
  h_JetPt_neHEF = new TH2D*[nbins];

  h_PtAve_QGL = new TH2D*[nbins];
  h_PtAve_muEF = new TH2D*[nbins];
  h_PtAve_chEmEF = new TH2D*[nbins];
  h_PtAve_chHEF = new TH2D*[nbins];
  h_PtAve_neEmEF = new TH2D*[nbins];
  h_PtAve_neHEF = new TH2D*[nbins];

  h_Zpt_Mu_Rho = new TProfile2D*[nbins];
  h_Zpt_Mu_NPVgood = new TProfile2D*[nbins];
  h_Zpt_Mu_NPVall = new TProfile2D*[nbins];
  h_Zpt_Mu_Mu = new TProfile2D*[nbins];

  h_ZpT_Mu_RpT = new TProfile2D*[nbins];
  h_ZpT_Mu_RMPF = new TProfile2D*[nbins];
  h_ZpT_Mu_RMPFjet1 = new TProfile2D*[nbins];
  h_ZpT_Mu_RMPFjetn = new TProfile2D*[nbins];
  h_ZpT_Mu_RMPFuncl = new TProfile2D*[nbins];

  h_ZpT_Mu_offset = new TProfile2D*[nbins];
  h_ZpT_Rho_offset = new TProfile2D*[nbins];
  h_ZpT_NPVgood_offset = new TProfile2D*[nbins];
  h_ZpT_NPVall_offset = new TProfile2D*[nbins];

  h_ZpT_NPVg_RpT = new TProfile2D*[nbins];
  h_ZpT_NPVg_RMPF = new TProfile2D*[nbins];
  h_ZpT_NPVg_RMPFjet1 = new TProfile2D*[nbins];
  h_ZpT_NPVg_RMPFjetn = new TProfile2D*[nbins];
  h_ZpT_NPVg_RMPFuncl = new TProfile2D*[nbins];

  h_JetPt_Mu_RpT = new TProfile2D*[nbins];
  h_JetPt_Mu_RMPF = new TProfile2D*[nbins];
  h_JetPt_Mu_RMPFjet1 = new TProfile2D*[nbins];
  h_JetPt_Mu_RMPFjetn = new TProfile2D*[nbins];
  h_JetPt_Mu_RMPFuncl = new TProfile2D*[nbins];

  h_JetPt_NPVg_RpT = new TProfile2D*[nbins];
  h_JetPt_NPVg_RMPF = new TProfile2D*[nbins];
  h_JetPt_NPVg_RMPFjet1 = new TProfile2D*[nbins];
  h_JetPt_NPVg_RMPFjetn = new TProfile2D*[nbins];
  h_JetPt_NPVg_RMPFuncl = new TProfile2D*[nbins];

  h_PtAve_Mu_RpT = new TProfile2D*[nbins];
  h_PtAve_Mu_RMPF = new TProfile2D*[nbins];
  h_PtAve_Mu_RMPFjet1 = new TProfile2D*[nbins];
  h_PtAve_Mu_RMPFjetn = new TProfile2D*[nbins];
  h_PtAve_Mu_RMPFuncl = new TProfile2D*[nbins];

  h_PtAve_NPVg_RpT = new TProfile2D*[nbins];
  h_PtAve_NPVg_RMPF = new TProfile2D*[nbins];
  h_PtAve_NPVg_RMPFjet1 = new TProfile2D*[nbins];
  h_PtAve_NPVg_RMPFjetn = new TProfile2D*[nbins];
  h_PtAve_NPVg_RMPFuncl = new TProfile2D*[nbins];

  h_Mu_RpT = new TH2D*[nbins];
  h_JetPt_RpT = new TH2D*[nbins];
  h_PtAve_RpT = new TH2D*[nbins];
  h_Mu_RMPF = new TH2D*[nbins];
  h_JetPt_RMPF = new TH2D*[nbins];
  h_PtAve_RMPF = new TH2D*[nbins];
  h_Mu_RMPFjet1 = new TH2D*[nbins];
  h_JetPt_RMPFjet1 = new TH2D*[nbins];
  h_PtAve_RMPFjet1 = new TH2D*[nbins];
  h_Mu_RMPFjetn = new TH2D*[nbins];
  h_JetPt_RMPFjetn = new TH2D*[nbins];
  h_PtAve_RMPFjetn = new TH2D*[nbins];
  h_Mu_RMPFuncl = new TH2D*[nbins];
  h_JetPt_RMPFuncl = new TH2D*[nbins];
  h_PtAve_RMPFuncl = new TH2D*[nbins];

  h_Mu_ptdiff = new TH2D*[nbins];

  h_Zpt_JNPF = new TH2D*[nbins];

  /*
  h_Zpt_RpT_btagCSVV2loose = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2loose = new TH2D*[nbins];
  h_Zpt_RpT_btagCSVV2medium = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2medium = new TH2D*[nbins];
  h_Zpt_RpT_btagCSVV2tight = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2tight = new TH2D*[nbins];
  */
  h_Zpt_RpT_genb = new TH2D*[nbins];
  h_Zpt_RMPF_genb = new TH2D*[nbins];
  h_Zpt_RMPFjet1_genb  = new TH2D*[nbins];
  h_Zpt_RMPFjetn_genb  = new TH2D*[nbins];
  h_Zpt_RMPFuncl_genb  = new TH2D*[nbins];
  /*
  h_Zpt_RpT_btagCSVV2loose_genb = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2loose_genb = new TH2D*[nbins];
  h_Zpt_RpT_btagCSVV2medium_genb = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2medium_genb = new TH2D*[nbins];
  h_Zpt_RpT_btagCSVV2tight_genb = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2tight_genb = new TH2D*[nbins];
  */
  h_Zpt_RpT_genc = new TH2D*[nbins];
  h_Zpt_RMPF_genc = new TH2D*[nbins];
  h_Zpt_RMPFjet1_genc  = new TH2D*[nbins];
  h_Zpt_RMPFjetn_genc  = new TH2D*[nbins];
  h_Zpt_RMPFuncl_genc  = new TH2D*[nbins];
  /*
  h_Zpt_RpT_btagCSVV2loose_genc = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2loose_genc = new TH2D*[nbins];
  h_Zpt_RpT_btagCSVV2medium_genc = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2medium_genc = new TH2D*[nbins];
  h_Zpt_RpT_btagCSVV2tight_genc = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2tight_genc = new TH2D*[nbins];
  */
  h_Zpt_RpT_genuds = new TH2D*[nbins];
  h_Zpt_RMPF_genuds = new TH2D*[nbins];
  h_Zpt_RMPFjet1_genuds  = new TH2D*[nbins];
  h_Zpt_RMPFjetn_genuds  = new TH2D*[nbins];
  h_Zpt_RMPFuncl_genuds  = new TH2D*[nbins];

  h_Zpt_RpT_gens = new TH2D*[nbins];
  h_Zpt_RMPF_gens = new TH2D*[nbins];
  h_Zpt_RMPFjet1_gens  = new TH2D*[nbins];
  h_Zpt_RMPFjetn_gens  = new TH2D*[nbins];
  h_Zpt_RMPFuncl_gens  = new TH2D*[nbins];

  h_Zpt_RpT_genud = new TH2D*[nbins];
  h_Zpt_RMPF_genud = new TH2D*[nbins];
  h_Zpt_RMPFjet1_genud  = new TH2D*[nbins];
  h_Zpt_RMPFjetn_genud  = new TH2D*[nbins];
  h_Zpt_RMPFuncl_genud  = new TH2D*[nbins];

  /*
  h_Zpt_RpT_btagCSVV2loose_genuds = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2loose_genuds = new TH2D*[nbins];
  h_Zpt_RpT_btagCSVV2medium_genuds = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2medium_genuds = new TH2D*[nbins];
  h_Zpt_RpT_btagCSVV2tight_genuds = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2tight_genuds = new TH2D*[nbins];
  */
  h_Zpt_RpT_geng = new TH2D*[nbins];
  h_Zpt_RMPF_geng = new TH2D*[nbins];
  h_Zpt_RMPFjet1_geng  = new TH2D*[nbins];
  h_Zpt_RMPFjetn_geng  = new TH2D*[nbins];
  h_Zpt_RMPFuncl_geng  = new TH2D*[nbins];
  /*
  h_Zpt_RpT_btagCSVV2loose_geng = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2loose_geng = new TH2D*[nbins];
  h_Zpt_RpT_btagCSVV2medium_geng = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2medium_geng = new TH2D*[nbins];
  h_Zpt_RpT_btagCSVV2tight_geng = new TH2D*[nbins];
  h_Zpt_RMPF_btagCSVV2tight_geng = new TH2D*[nbins];
  */
  h_Zpt_RpT_unclassified = new TH2D*[nbins];
  h_Zpt_RMPF_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFjet1_unclassified  = new TH2D*[nbins];
  h_Zpt_RMPFjetn_unclassified  = new TH2D*[nbins];
  h_Zpt_RMPFuncl_unclassified  = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepBloose = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepBloose = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepBmedium = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepBmedium = new TH2D*[nbins];
  h_Zpt_RpT_btag = new TH2D*[nbins];
  h_Zpt_RMPF_btag = new TH2D*[nbins];
  h_Zpt_RMPFjet1_btag = new TH2D*[nbins];
  h_Zpt_RMPFjetn_btag = new TH2D*[nbins];
  h_Zpt_RMPFuncl_btag = new TH2D*[nbins];

  h_JetPt_QGL_btag = new TH2D*[nbins];
  h_JetPt_QGL_btag_genb = new TH2D*[nbins];
  h_JetPt_QGL_btag_genc = new TH2D*[nbins];
  h_JetPt_QGL_btag_genuds = new TH2D*[nbins];
  h_JetPt_QGL_btag_geng = new TH2D*[nbins];
  h_JetPt_QGL_btag_unclassified = new TH2D*[nbins];
  h_Zpt_QGL_btag = new TH2D*[nbins];
  h_Zpt_QGL_btag_genb = new TH2D*[nbins];
  h_Zpt_QGL_btag_genc = new TH2D*[nbins];
  h_Zpt_QGL_btag_genuds = new TH2D*[nbins];
  h_Zpt_QGL_btag_geng = new TH2D*[nbins];
  h_Zpt_QGL_btag_unclassified = new TH2D*[nbins];
  h_JetPt_muEF_btag = new TH2D*[nbins];
  h_JetPt_chEmEF_btag = new TH2D*[nbins];
  h_JetPt_chHEF_btag = new TH2D*[nbins];
  h_JetPt_neEmEF_btag = new TH2D*[nbins];
  h_JetPt_neHEF_btag = new TH2D*[nbins];

  h_PtAve_QGL_btag = new TH2D*[nbins];
  h_PtAve_muEF_btag = new TH2D*[nbins];
  h_PtAve_chEmEF_btag = new TH2D*[nbins];
  h_PtAve_chHEF_btag = new TH2D*[nbins];
  h_PtAve_neEmEF_btag = new TH2D*[nbins];
  h_PtAve_neHEF_btag = new TH2D*[nbins];

  h_Zpt_Mu_Rho_btag = new TProfile2D*[nbins];
  h_Zpt_Mu_NPVgood_btag = new TProfile2D*[nbins];
  h_Zpt_Mu_NPVall_btag = new TProfile2D*[nbins];
  h_Zpt_Mu_Mu_btag = new TProfile2D*[nbins];
  h_Mu_RpT_btag = new TH2D*[nbins];
  h_JetPt_RpT_btag = new TH2D*[nbins];
  h_PtAve_RpT_btag = new TH2D*[nbins];
  h_Mu_RMPF_btag = new TH2D*[nbins];
  h_JetPt_RMPF_btag = new TH2D*[nbins];
  h_PtAve_RMPF_btag = new TH2D*[nbins];
  h_Mu_RMPFjet1_btag = new TH2D*[nbins];
  h_JetPt_RMPFjet1_btag = new TH2D*[nbins];
  h_PtAve_RMPFjet1_btag = new TH2D*[nbins];
  h_Mu_RMPFjetn_btag = new TH2D*[nbins];
  h_JetPt_RMPFjetn_btag = new TH2D*[nbins];
  h_PtAve_RMPFjetn_btag = new TH2D*[nbins];
  h_Mu_RMPFuncl_btag = new TH2D*[nbins];
  h_JetPt_RMPFuncl_btag = new TH2D*[nbins];
  h_PtAve_RMPFuncl_btag = new TH2D*[nbins];
  
  //h_Zpt_RpT_btagDeepBloose_genb = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepBloose_genb = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepBmedium_genb = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepBmedium_genb = new TH2D*[nbins];
  h_Zpt_RpT_btag_genb = new TH2D*[nbins];
  h_Zpt_RMPF_btag_genb = new TH2D*[nbins];
  h_Zpt_RMPFjet1_btag_genb = new TH2D*[nbins];
  h_Zpt_RMPFjetn_btag_genb = new TH2D*[nbins];
  h_Zpt_RMPFuncl_btag_genb = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepBloose_genc = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepBloose_genc = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepBmedium_genc = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepBmedium_genc = new TH2D*[nbins];
  h_Zpt_RpT_btag_genc = new TH2D*[nbins];
  h_Zpt_RMPF_btag_genc = new TH2D*[nbins];
  h_Zpt_RMPFjet1_btag_genc = new TH2D*[nbins];
  h_Zpt_RMPFjetn_btag_genc = new TH2D*[nbins];
  h_Zpt_RMPFuncl_btag_genc = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepBloose_genuds = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepBloose_genuds = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepBmedium_genuds = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepBmedium_genuds = new TH2D*[nbins];
  h_Zpt_RpT_btag_genuds = new TH2D*[nbins];
  h_Zpt_RMPF_btag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFjet1_btag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFjetn_btag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFuncl_btag_genuds = new TH2D*[nbins];

  h_Zpt_RpT_btag_gens = new TH2D*[nbins];
  h_Zpt_RMPF_btag_gens = new TH2D*[nbins];
  h_Zpt_RMPFjet1_btag_gens = new TH2D*[nbins];
  h_Zpt_RMPFjetn_btag_gens = new TH2D*[nbins];
  h_Zpt_RMPFuncl_btag_gens = new TH2D*[nbins];

  h_Zpt_RpT_btag_genud = new TH2D*[nbins];
  h_Zpt_RMPF_btag_genud = new TH2D*[nbins];
  h_Zpt_RMPFjet1_btag_genud = new TH2D*[nbins];
  h_Zpt_RMPFjetn_btag_genud = new TH2D*[nbins];
  h_Zpt_RMPFuncl_btag_genud = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepBloose_geng = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepBloose_geng = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepBmedium_geng = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepBmedium_geng = new TH2D*[nbins];
  h_Zpt_RpT_btag_geng = new TH2D*[nbins];
  h_Zpt_RMPF_btag_geng = new TH2D*[nbins];
  h_Zpt_RMPFjet1_btag_geng = new TH2D*[nbins];
  h_Zpt_RMPFjetn_btag_geng = new TH2D*[nbins];
  h_Zpt_RMPFuncl_btag_geng = new TH2D*[nbins];

  h_Zpt_RpT_btag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPF_btag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFjet1_btag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFjetn_btag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFuncl_btag_unclassified = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepCloose = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepCloose = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepCmedium = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepCmedium = new TH2D*[nbins];
  h_Zpt_RpT_ctag = new TH2D*[nbins];
  h_Zpt_RMPF_ctag = new TH2D*[nbins];
  h_Zpt_RMPFjet1_ctag = new TH2D*[nbins];
  h_Zpt_RMPFjetn_ctag = new TH2D*[nbins];
  h_Zpt_RMPFuncl_ctag = new TH2D*[nbins];

  h_JetPt_QGL_ctag = new TH2D*[nbins];
  h_JetPt_QGL_ctag_genb = new TH2D*[nbins];
  h_JetPt_QGL_ctag_genc = new TH2D*[nbins];
  h_JetPt_QGL_ctag_genuds = new TH2D*[nbins];
  h_JetPt_QGL_ctag_geng = new TH2D*[nbins];
  h_JetPt_QGL_ctag_unclassified = new TH2D*[nbins];
  h_Zpt_QGL_ctag = new TH2D*[nbins];
  h_Zpt_QGL_ctag_genb = new TH2D*[nbins];
  h_Zpt_QGL_ctag_genc = new TH2D*[nbins];
  h_Zpt_QGL_ctag_genuds = new TH2D*[nbins];
  h_Zpt_QGL_ctag_geng = new TH2D*[nbins];
  h_Zpt_QGL_ctag_unclassified = new TH2D*[nbins];
  h_JetPt_muEF_ctag = new TH2D*[nbins];
  h_JetPt_chEmEF_ctag = new TH2D*[nbins];
  h_JetPt_chHEF_ctag = new TH2D*[nbins];
  h_JetPt_neEmEF_ctag = new TH2D*[nbins];
  h_JetPt_neHEF_ctag = new TH2D*[nbins];

  h_PtAve_QGL_ctag = new TH2D*[nbins];
  h_PtAve_muEF_ctag = new TH2D*[nbins];
  h_PtAve_chEmEF_ctag = new TH2D*[nbins];
  h_PtAve_chHEF_ctag = new TH2D*[nbins];
  h_PtAve_neEmEF_ctag = new TH2D*[nbins];
  h_PtAve_neHEF_ctag = new TH2D*[nbins];

  h_Zpt_Mu_Rho_ctag = new TProfile2D*[nbins];
  h_Zpt_Mu_NPVgood_ctag = new TProfile2D*[nbins];
  h_Zpt_Mu_NPVall_ctag = new TProfile2D*[nbins];
  h_Zpt_Mu_Mu_ctag = new TProfile2D*[nbins];
  h_Mu_RpT_ctag = new TH2D*[nbins];
  h_JetPt_RpT_ctag = new TH2D*[nbins];
  h_PtAve_RpT_ctag = new TH2D*[nbins];
  h_Mu_RMPF_ctag = new TH2D*[nbins];
  h_JetPt_RMPF_ctag = new TH2D*[nbins];
  h_PtAve_RMPF_ctag = new TH2D*[nbins];
  h_Mu_RMPFjet1_ctag = new TH2D*[nbins];
  h_JetPt_RMPFjet1_ctag = new TH2D*[nbins];
  h_PtAve_RMPFjet1_ctag = new TH2D*[nbins];
  h_Mu_RMPFjetn_ctag = new TH2D*[nbins];
  h_JetPt_RMPFjetn_ctag = new TH2D*[nbins];
  h_PtAve_RMPFjetn_ctag = new TH2D*[nbins];
  h_Mu_RMPFuncl_ctag = new TH2D*[nbins];
  h_JetPt_RMPFuncl_ctag = new TH2D*[nbins];
  h_PtAve_RMPFuncl_ctag = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepCloose_genb = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepCloose_genb = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepCmedium_genb = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepCmedium_genb = new TH2D*[nbins];
  h_Zpt_RpT_ctag_genb = new TH2D*[nbins];
  h_Zpt_RMPF_ctag_genb = new TH2D*[nbins];
  h_Zpt_RMPFjet1_ctag_genb = new TH2D*[nbins];
  h_Zpt_RMPFjetn_ctag_genb = new TH2D*[nbins];
  h_Zpt_RMPFuncl_ctag_genb = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepCloose_genc = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepCloose_genc = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepCmedium_genc = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepCmedium_genc = new TH2D*[nbins];
  h_Zpt_RpT_ctag_genc = new TH2D*[nbins];
  h_Zpt_RMPF_ctag_genc = new TH2D*[nbins];
  h_Zpt_RMPFjet1_ctag_genc = new TH2D*[nbins];
  h_Zpt_RMPFjetn_ctag_genc = new TH2D*[nbins];
  h_Zpt_RMPFuncl_ctag_genc = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepCloose_genuds = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepCloose_genuds = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepCmedium_genuds = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepCmedium_genuds = new TH2D*[nbins];
  h_Zpt_RpT_ctag_genuds = new TH2D*[nbins];
  h_Zpt_RMPF_ctag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFjet1_ctag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFjetn_ctag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFuncl_ctag_genuds = new TH2D*[nbins];

  h_Zpt_RpT_ctag_gens = new TH2D*[nbins];
  h_Zpt_RMPF_ctag_gens = new TH2D*[nbins];
  h_Zpt_RMPFjet1_ctag_gens = new TH2D*[nbins];
  h_Zpt_RMPFjetn_ctag_gens = new TH2D*[nbins];
  h_Zpt_RMPFuncl_ctag_gens = new TH2D*[nbins];

  h_Zpt_RpT_ctag_genud = new TH2D*[nbins];
  h_Zpt_RMPF_ctag_genud = new TH2D*[nbins];
  h_Zpt_RMPFjet1_ctag_genud = new TH2D*[nbins];
  h_Zpt_RMPFjetn_ctag_genud = new TH2D*[nbins];
  h_Zpt_RMPFuncl_ctag_genud = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepCloose_geng = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepCloose_geng = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepCmedium_geng = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepCmedium_geng = new TH2D*[nbins];
  h_Zpt_RpT_ctag_geng = new TH2D*[nbins];
  h_Zpt_RMPF_ctag_geng = new TH2D*[nbins];
  h_Zpt_RMPFjet1_ctag_geng = new TH2D*[nbins];
  h_Zpt_RMPFjetn_ctag_geng = new TH2D*[nbins];
  h_Zpt_RMPFuncl_ctag_geng = new TH2D*[nbins];

  h_Zpt_RpT_ctag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPF_ctag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFjet1_ctag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFjetn_ctag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFuncl_ctag_unclassified = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepFlavBloose = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepFlavBloose = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepFlavBmedium = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepFlavBmedium = new TH2D*[nbins];
  h_Zpt_RpT_btagDeepFlavBtight = new TH2D*[nbins];
  h_Zpt_RMPF_btagDeepFlavBtight = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepFlavBloose_genb = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepFlavBloose_genb = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepFlavBmedium_genb = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepFlavBmedium_genb = new TH2D*[nbins];
  h_Zpt_RpT_btagDeepFlavBtight_genb = new TH2D*[nbins];
  h_Zpt_RMPF_btagDeepFlavBtight_genb = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepFlavBloose_genc = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepFlavBloose_genc = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepFlavBmedium_genc = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepFlavBmedium_genc = new TH2D*[nbins];
  h_Zpt_RpT_btagDeepFlavBtight_genc = new TH2D*[nbins];
  h_Zpt_RMPF_btagDeepFlavBtight_genc = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepFlavBloose_genuds = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepFlavBloose_genuds = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepFlavBmedium_genuds = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepFlavBmedium_genuds = new TH2D*[nbins];
  h_Zpt_RpT_btagDeepFlavBtight_genuds = new TH2D*[nbins];
  h_Zpt_RMPF_btagDeepFlavBtight_genuds = new TH2D*[nbins];

  //h_Zpt_RpT_btagDeepFlavBloose_geng = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepFlavBloose_geng = new TH2D*[nbins];
  //h_Zpt_RpT_btagDeepFlavBmedium_geng = new TH2D*[nbins];
  //h_Zpt_RMPF_btagDeepFlavBmedium_geng = new TH2D*[nbins];
  h_Zpt_RpT_btagDeepFlavBtight_geng = new TH2D*[nbins];
  h_Zpt_RMPF_btagDeepFlavBtight_geng = new TH2D*[nbins];

  h_Zpt_RpT_btagDeepFlavBtight_unclassified = new TH2D*[nbins];
  h_Zpt_RMPF_btagDeepFlavBtight_unclassified = new TH2D*[nbins];

  h_Zpt_RpT_gluontag = new TH2D*[nbins];
  h_Zpt_RMPF_gluontag = new TH2D*[nbins];
  h_Zpt_RMPFjet1_gluontag = new TH2D*[nbins];
  h_Zpt_RMPFjetn_gluontag = new TH2D*[nbins];
  h_Zpt_RMPFuncl_gluontag = new TH2D*[nbins];

  h_JetPt_QGL_gluontag = new TH2D*[nbins];
  h_JetPt_QGL_gluontag_genb = new TH2D*[nbins];
  h_JetPt_QGL_gluontag_genc = new TH2D*[nbins];
  h_JetPt_QGL_gluontag_genuds = new TH2D*[nbins];
  h_JetPt_QGL_gluontag_geng = new TH2D*[nbins];
  h_JetPt_QGL_gluontag_unclassified = new TH2D*[nbins];
  h_Zpt_QGL_gluontag = new TH2D*[nbins];
  h_Zpt_QGL_gluontag_genb = new TH2D*[nbins];
  h_Zpt_QGL_gluontag_genc = new TH2D*[nbins];
  h_Zpt_QGL_gluontag_genuds = new TH2D*[nbins];
  h_Zpt_QGL_gluontag_geng = new TH2D*[nbins];
  h_Zpt_QGL_gluontag_unclassified = new TH2D*[nbins];
  h_JetPt_muEF_gluontag = new TH2D*[nbins];
  h_JetPt_chEmEF_gluontag = new TH2D*[nbins];
  h_JetPt_chHEF_gluontag = new TH2D*[nbins];
  h_JetPt_neEmEF_gluontag = new TH2D*[nbins];
  h_JetPt_neHEF_gluontag = new TH2D*[nbins];

  h_PtAve_QGL_gluontag = new TH2D*[nbins];
  h_PtAve_muEF_gluontag = new TH2D*[nbins];
  h_PtAve_chEmEF_gluontag = new TH2D*[nbins];
  h_PtAve_chHEF_gluontag = new TH2D*[nbins];
  h_PtAve_neEmEF_gluontag = new TH2D*[nbins];
  h_PtAve_neHEF_gluontag = new TH2D*[nbins];

  h_Zpt_Mu_Rho_gluontag = new TProfile2D*[nbins];
  h_Zpt_Mu_NPVgood_gluontag = new TProfile2D*[nbins];
  h_Zpt_Mu_NPVall_gluontag = new TProfile2D*[nbins];
  h_Zpt_Mu_Mu_gluontag = new TProfile2D*[nbins];
  h_Mu_RpT_gluontag = new TH2D*[nbins];
  h_JetPt_RpT_gluontag = new TH2D*[nbins];
  h_PtAve_RpT_gluontag = new TH2D*[nbins];
  h_Mu_RMPF_gluontag = new TH2D*[nbins];
  h_JetPt_RMPF_gluontag = new TH2D*[nbins];
  h_PtAve_RMPF_gluontag = new TH2D*[nbins];
  h_Mu_RMPFjet1_gluontag = new TH2D*[nbins];
  h_JetPt_RMPFjet1_gluontag = new TH2D*[nbins];
  h_PtAve_RMPFjet1_gluontag = new TH2D*[nbins];
  h_Mu_RMPFjetn_gluontag = new TH2D*[nbins];
  h_JetPt_RMPFjetn_gluontag = new TH2D*[nbins];
  h_PtAve_RMPFjetn_gluontag = new TH2D*[nbins];
  h_Mu_RMPFuncl_gluontag = new TH2D*[nbins];
  h_JetPt_RMPFuncl_gluontag = new TH2D*[nbins];
  h_PtAve_RMPFuncl_gluontag = new TH2D*[nbins];

  h_Zpt_RpT_gluontag_genb = new TH2D*[nbins];
  h_Zpt_RMPF_gluontag_genb = new TH2D*[nbins];
  h_Zpt_RMPFjet1_gluontag_genb = new TH2D*[nbins];
  h_Zpt_RMPFjetn_gluontag_genb = new TH2D*[nbins];
  h_Zpt_RMPFuncl_gluontag_genb = new TH2D*[nbins];
  h_Zpt_RpT_gluontag_genc = new TH2D*[nbins];
  h_Zpt_RMPF_gluontag_genc = new TH2D*[nbins];
  h_Zpt_RMPFjet1_gluontag_genc = new TH2D*[nbins];
  h_Zpt_RMPFjetn_gluontag_genc = new TH2D*[nbins];
  h_Zpt_RMPFuncl_gluontag_genc = new TH2D*[nbins];
  h_Zpt_RpT_gluontag_genuds = new TH2D*[nbins];
  h_Zpt_RMPF_gluontag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFjet1_gluontag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFjetn_gluontag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFuncl_gluontag_genuds = new TH2D*[nbins];
  h_Zpt_RpT_gluontag_gens = new TH2D*[nbins];
  h_Zpt_RMPF_gluontag_gens = new TH2D*[nbins];
  h_Zpt_RMPFjet1_gluontag_gens = new TH2D*[nbins];
  h_Zpt_RMPFjetn_gluontag_gens = new TH2D*[nbins];
  h_Zpt_RMPFuncl_gluontag_gens = new TH2D*[nbins];
  h_Zpt_RpT_gluontag_genud = new TH2D*[nbins];
  h_Zpt_RMPF_gluontag_genud = new TH2D*[nbins];
  h_Zpt_RMPFjet1_gluontag_genud = new TH2D*[nbins];
  h_Zpt_RMPFjetn_gluontag_genud = new TH2D*[nbins];
  h_Zpt_RMPFuncl_gluontag_genud = new TH2D*[nbins];
  h_Zpt_RpT_gluontag_geng = new TH2D*[nbins];
  h_Zpt_RMPF_gluontag_geng = new TH2D*[nbins];
  h_Zpt_RMPFjet1_gluontag_geng = new TH2D*[nbins];
  h_Zpt_RMPFjetn_gluontag_geng = new TH2D*[nbins];
  h_Zpt_RMPFuncl_gluontag_geng = new TH2D*[nbins];
  h_Zpt_RpT_gluontag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPF_gluontag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFjet1_gluontag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFjetn_gluontag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFuncl_gluontag_unclassified = new TH2D*[nbins];
  
  h_Zpt_RpT_quarktag = new TH2D*[nbins];
  h_Zpt_RMPF_quarktag = new TH2D*[nbins];
  h_Zpt_RMPFjet1_quarktag = new TH2D*[nbins];
  h_Zpt_RMPFjetn_quarktag = new TH2D*[nbins];
  h_Zpt_RMPFuncl_quarktag = new TH2D*[nbins];

  h_JetPt_QGL_quarktag = new TH2D*[nbins];
  h_JetPt_QGL_quarktag_genb = new TH2D*[nbins];
  h_JetPt_QGL_quarktag_genc = new TH2D*[nbins];
  h_JetPt_QGL_quarktag_genuds = new TH2D*[nbins];
  h_JetPt_QGL_quarktag_geng = new TH2D*[nbins];
  h_JetPt_QGL_quarktag_unclassified = new TH2D*[nbins];
  h_Zpt_QGL_quarktag = new TH2D*[nbins];
  h_Zpt_QGL_quarktag_genb = new TH2D*[nbins];
  h_Zpt_QGL_quarktag_genc = new TH2D*[nbins];
  h_Zpt_QGL_quarktag_genuds = new TH2D*[nbins];
  h_Zpt_QGL_quarktag_geng = new TH2D*[nbins];
  h_Zpt_QGL_quarktag_unclassified = new TH2D*[nbins];
  h_JetPt_muEF_quarktag = new TH2D*[nbins];
  h_JetPt_chEmEF_quarktag = new TH2D*[nbins];
  h_JetPt_chHEF_quarktag = new TH2D*[nbins];
  h_JetPt_neEmEF_quarktag = new TH2D*[nbins];
  h_JetPt_neHEF_quarktag = new TH2D*[nbins];

  h_PtAve_QGL_quarktag = new TH2D*[nbins];
  h_PtAve_muEF_quarktag = new TH2D*[nbins];
  h_PtAve_chEmEF_quarktag = new TH2D*[nbins];
  h_PtAve_chHEF_quarktag = new TH2D*[nbins];
  h_PtAve_neEmEF_quarktag = new TH2D*[nbins];
  h_PtAve_neHEF_quarktag = new TH2D*[nbins];

  h_Zpt_Mu_Rho_quarktag = new TProfile2D*[nbins];
  h_Zpt_Mu_NPVgood_quarktag = new TProfile2D*[nbins];
  h_Zpt_Mu_NPVall_quarktag = new TProfile2D*[nbins];
  h_Zpt_Mu_Mu_quarktag = new TProfile2D*[nbins];
  h_Mu_RpT_quarktag = new TH2D*[nbins];
  h_JetPt_RpT_quarktag = new TH2D*[nbins];
  h_PtAve_RpT_quarktag = new TH2D*[nbins];
  h_Mu_RMPF_quarktag = new TH2D*[nbins];
  h_JetPt_RMPF_quarktag = new TH2D*[nbins];
  h_PtAve_RMPF_quarktag = new TH2D*[nbins];
  h_Mu_RMPFjet1_quarktag = new TH2D*[nbins];
  h_JetPt_RMPFjet1_quarktag = new TH2D*[nbins];
  h_PtAve_RMPFjet1_quarktag = new TH2D*[nbins];
  h_Mu_RMPFjetn_quarktag = new TH2D*[nbins];
  h_JetPt_RMPFjetn_quarktag = new TH2D*[nbins];
  h_PtAve_RMPFjetn_quarktag = new TH2D*[nbins];
  h_Mu_RMPFuncl_quarktag = new TH2D*[nbins];
  h_JetPt_RMPFuncl_quarktag = new TH2D*[nbins];
  h_PtAve_RMPFuncl_quarktag = new TH2D*[nbins];

  h_Zpt_RpT_quarktag_genb = new TH2D*[nbins];
  h_Zpt_RMPF_quarktag_genb = new TH2D*[nbins];
  h_Zpt_RMPFjet1_quarktag_genb = new TH2D*[nbins];
  h_Zpt_RMPFjetn_quarktag_genb = new TH2D*[nbins];
  h_Zpt_RMPFuncl_quarktag_genb = new TH2D*[nbins];
  h_Zpt_RpT_quarktag_genc = new TH2D*[nbins];
  h_Zpt_RMPF_quarktag_genc = new TH2D*[nbins];
  h_Zpt_RMPFjet1_quarktag_genc = new TH2D*[nbins];
  h_Zpt_RMPFjetn_quarktag_genc = new TH2D*[nbins];
  h_Zpt_RMPFuncl_quarktag_genc = new TH2D*[nbins];
  h_Zpt_RpT_quarktag_genuds = new TH2D*[nbins];
  h_Zpt_RMPF_quarktag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFjet1_quarktag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFjetn_quarktag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFuncl_quarktag_genuds = new TH2D*[nbins];
  h_Zpt_RpT_quarktag_geng = new TH2D*[nbins];
  h_Zpt_RMPF_quarktag_geng = new TH2D*[nbins];
  h_Zpt_RMPFjet1_quarktag_geng = new TH2D*[nbins];
  h_Zpt_RMPFjetn_quarktag_geng = new TH2D*[nbins];
  h_Zpt_RMPFuncl_quarktag_geng = new TH2D*[nbins];
  h_Zpt_RpT_quarktag_gens = new TH2D*[nbins];
  h_Zpt_RMPF_quarktag_gens = new TH2D*[nbins];
  h_Zpt_RMPFjet1_quarktag_gens = new TH2D*[nbins];
  h_Zpt_RMPFjetn_quarktag_gens = new TH2D*[nbins];
  h_Zpt_RMPFuncl_quarktag_gens = new TH2D*[nbins];
  h_Zpt_RpT_quarktag_genud = new TH2D*[nbins];
  h_Zpt_RMPF_quarktag_genud = new TH2D*[nbins];
  h_Zpt_RMPFjet1_quarktag_genud = new TH2D*[nbins];
  h_Zpt_RMPFjetn_quarktag_genud = new TH2D*[nbins];
  h_Zpt_RMPFuncl_quarktag_genud = new TH2D*[nbins];
  h_Zpt_RpT_quarktag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPF_quarktag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFjet1_quarktag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFjetn_quarktag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFuncl_quarktag_unclassified = new TH2D*[nbins];

  h_Zpt_RpT_notag = new TH2D*[nbins];
  h_Zpt_RMPF_notag = new TH2D*[nbins];
  h_Zpt_RMPFjet1_notag = new TH2D*[nbins];
  h_Zpt_RMPFjetn_notag = new TH2D*[nbins];
  h_Zpt_RMPFuncl_notag = new TH2D*[nbins];

  h_JetPt_QGL_notag = new TH2D*[nbins];
  h_JetPt_QGL_notag_genb = new TH2D*[nbins];
  h_JetPt_QGL_notag_genc = new TH2D*[nbins];
  h_JetPt_QGL_notag_genuds = new TH2D*[nbins];
  h_JetPt_QGL_notag_geng = new TH2D*[nbins];
  h_JetPt_QGL_notag_unclassified = new TH2D*[nbins];
  h_Zpt_QGL_notag = new TH2D*[nbins];
  h_Zpt_QGL_notag_genb = new TH2D*[nbins];
  h_Zpt_QGL_notag_genc = new TH2D*[nbins];
  h_Zpt_QGL_notag_genuds = new TH2D*[nbins];
  h_Zpt_QGL_notag_geng = new TH2D*[nbins];
  h_Zpt_QGL_notag_unclassified = new TH2D*[nbins];
  h_JetPt_muEF_notag = new TH2D*[nbins];
  h_JetPt_chEmEF_notag = new TH2D*[nbins];
  h_JetPt_chHEF_notag = new TH2D*[nbins];
  h_JetPt_neEmEF_notag = new TH2D*[nbins];
  h_JetPt_neHEF_notag = new TH2D*[nbins];

  h_PtAve_QGL_notag = new TH2D*[nbins];
  h_PtAve_muEF_notag = new TH2D*[nbins];
  h_PtAve_chEmEF_notag = new TH2D*[nbins];
  h_PtAve_chHEF_notag = new TH2D*[nbins];
  h_PtAve_neEmEF_notag = new TH2D*[nbins];
  h_PtAve_neHEF_notag = new TH2D*[nbins];

  h_Zpt_Mu_Rho_notag = new TProfile2D*[nbins];
  h_Zpt_Mu_NPVgood_notag = new TProfile2D*[nbins];
  h_Zpt_Mu_NPVall_notag = new TProfile2D*[nbins];
  h_Zpt_Mu_Mu_notag = new TProfile2D*[nbins];
  h_Mu_RpT_notag = new TH2D*[nbins];
  h_JetPt_RpT_notag = new TH2D*[nbins];
  h_PtAve_RpT_notag = new TH2D*[nbins];
  h_Mu_RMPF_notag = new TH2D*[nbins];
  h_JetPt_RMPF_notag = new TH2D*[nbins];
  h_PtAve_RMPF_notag = new TH2D*[nbins];
  h_Mu_RMPFjet1_notag = new TH2D*[nbins];
  h_JetPt_RMPFjet1_notag = new TH2D*[nbins];
  h_PtAve_RMPFjet1_notag = new TH2D*[nbins];
  h_Mu_RMPFjetn_notag = new TH2D*[nbins];
  h_JetPt_RMPFjetn_notag = new TH2D*[nbins];
  h_PtAve_RMPFjetn_notag = new TH2D*[nbins];
  h_Mu_RMPFuncl_notag = new TH2D*[nbins];
  h_JetPt_RMPFuncl_notag = new TH2D*[nbins];
  h_PtAve_RMPFuncl_notag = new TH2D*[nbins];

  h_Zpt_RpT_notag_genb = new TH2D*[nbins];
  h_Zpt_RMPF_notag_genb = new TH2D*[nbins];
  h_Zpt_RMPFjet1_notag_genb = new TH2D*[nbins];
  h_Zpt_RMPFjetn_notag_genb = new TH2D*[nbins];
  h_Zpt_RMPFuncl_notag_genb = new TH2D*[nbins];
  h_Zpt_RpT_notag_genc = new TH2D*[nbins];
  h_Zpt_RMPF_notag_genc = new TH2D*[nbins];
  h_Zpt_RMPFjet1_notag_genc = new TH2D*[nbins];
  h_Zpt_RMPFjetn_notag_genc = new TH2D*[nbins];
  h_Zpt_RMPFuncl_notag_genc = new TH2D*[nbins];
  h_Zpt_RpT_notag_genuds = new TH2D*[nbins];
  h_Zpt_RMPF_notag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFjet1_notag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFjetn_notag_genuds = new TH2D*[nbins];
  h_Zpt_RMPFuncl_notag_genuds = new TH2D*[nbins];
  h_Zpt_RpT_notag_gens = new TH2D*[nbins];
  h_Zpt_RMPF_notag_gens = new TH2D*[nbins];
  h_Zpt_RMPFjet1_notag_gens = new TH2D*[nbins];
  h_Zpt_RMPFjetn_notag_gens = new TH2D*[nbins];
  h_Zpt_RMPFuncl_notag_gens = new TH2D*[nbins];
  h_Zpt_RpT_notag_genud = new TH2D*[nbins];
  h_Zpt_RMPF_notag_genud = new TH2D*[nbins];
  h_Zpt_RMPFjet1_notag_genud = new TH2D*[nbins];
  h_Zpt_RMPFjetn_notag_genud = new TH2D*[nbins];
  h_Zpt_RMPFuncl_notag_genud = new TH2D*[nbins];
  h_Zpt_RpT_notag_geng = new TH2D*[nbins];
  h_Zpt_RMPF_notag_geng = new TH2D*[nbins];
  h_Zpt_RMPFjet1_notag_geng = new TH2D*[nbins];
  h_Zpt_RMPFjetn_notag_geng = new TH2D*[nbins];
  h_Zpt_RMPFuncl_notag_geng = new TH2D*[nbins];
  h_Zpt_RpT_notag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPF_notag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFjet1_notag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFjetn_notag_unclassified = new TH2D*[nbins];
  h_Zpt_RMPFuncl_notag_unclassified = new TH2D*[nbins];


  h_Zpt_RZ        = new TH2D*[nbins];

  h_Zpt_RBal                     = new TH2D*[nbins];
  h_Zpt_Resp                     = new TH2D*[nbins];
  h_Zpt_RGMPF                    = new TH2D*[nbins];
  h_Zpt_RGenjet1                 = new TH2D*[nbins];
  h_Zpt_RGenjetn                 = new TH2D*[nbins];
  h_Zpt_RGenuncl                 = new TH2D*[nbins];

  h_JetPt_RBal                   = new TH2D*[nbins];
  h_JetPt_RGenjet1               = new TH2D*[nbins];
  h_JetPt_RGenjetn               = new TH2D*[nbins];
  h_JetPt_RGenuncl               = new TH2D*[nbins];

  h_PtAve_RBal                   = new TH2D*[nbins];
  h_PtAve_RGenjet1               = new TH2D*[nbins];
  h_PtAve_RGenjetn               = new TH2D*[nbins];
  h_PtAve_RGenuncl               = new TH2D*[nbins];

  h_Zpt_RBal_btag      = new TH2D*[nbins];
  h_Zpt_Resp_btag      = new TH2D*[nbins];
  h_Zpt_RGMPF_btag     = new TH2D*[nbins];
  h_Zpt_RGenjet1_btag  = new TH2D*[nbins];
  h_Zpt_RGenjetn_btag  = new TH2D*[nbins];
  h_Zpt_RGenuncl_btag  = new TH2D*[nbins];
  h_Zpt_RBal_ctag      = new TH2D*[nbins];
  h_Zpt_Resp_ctag      = new TH2D*[nbins];
  h_Zpt_RGMPF_ctag     = new TH2D*[nbins];
  h_Zpt_RGenjet1_ctag  = new TH2D*[nbins];
  h_Zpt_RGenjetn_ctag  = new TH2D*[nbins];
  h_Zpt_RGenuncl_ctag  = new TH2D*[nbins];
  h_Zpt_RBal_gluontag            = new TH2D*[nbins];
  h_Zpt_Resp_gluontag            = new TH2D*[nbins];
  h_Zpt_RGMPF_gluontag           = new TH2D*[nbins];
  h_Zpt_RGenjet1_gluontag        = new TH2D*[nbins];
  h_Zpt_RGenjetn_gluontag        = new TH2D*[nbins];
  h_Zpt_RGenuncl_gluontag        = new TH2D*[nbins];
  h_Zpt_RBal_quarktag            = new TH2D*[nbins];
  h_Zpt_Resp_quarktag            = new TH2D*[nbins];
  h_Zpt_RGMPF_quarktag           = new TH2D*[nbins];
  h_Zpt_RGenjet1_quarktag        = new TH2D*[nbins];
  h_Zpt_RGenjetn_quarktag        = new TH2D*[nbins];
  h_Zpt_RGenuncl_quarktag        = new TH2D*[nbins];
  h_Zpt_RBal_notag               = new TH2D*[nbins];
  h_Zpt_Resp_notag               = new TH2D*[nbins];
  h_Zpt_RGMPF_notag              = new TH2D*[nbins];
  h_Zpt_RGenjet1_notag           = new TH2D*[nbins];
  h_Zpt_RGenjetn_notag           = new TH2D*[nbins];
  h_Zpt_RGenuncl_notag           = new TH2D*[nbins];

  h_Zpt_RBal_genb                     = new TH2D*[nbins];
  h_Zpt_Resp_genb                     = new TH2D*[nbins];
  h_Zpt_RGMPF_genb                    = new TH2D*[nbins];
  h_Zpt_RGenjet1_genb                 = new TH2D*[nbins];
  h_Zpt_RGenjetn_genb                 = new TH2D*[nbins];
  h_Zpt_RGenuncl_genb                 = new TH2D*[nbins];
  h_Zpt_RBal_btag_genb      = new TH2D*[nbins];
  h_Zpt_Resp_btag_genb      = new TH2D*[nbins];
  h_Zpt_RGMPF_btag_genb     = new TH2D*[nbins];
  h_Zpt_RGenjet1_btag_genb  = new TH2D*[nbins];
  h_Zpt_RGenjetn_btag_genb  = new TH2D*[nbins];
  h_Zpt_RGenuncl_btag_genb  = new TH2D*[nbins];
  h_Zpt_RBal_ctag_genb      = new TH2D*[nbins];
  h_Zpt_Resp_ctag_genb      = new TH2D*[nbins];
  h_Zpt_RGMPF_ctag_genb     = new TH2D*[nbins];
  h_Zpt_RGenjet1_ctag_genb  = new TH2D*[nbins];
  h_Zpt_RGenjetn_ctag_genb  = new TH2D*[nbins];
  h_Zpt_RGenuncl_ctag_genb  = new TH2D*[nbins];
  h_Zpt_RBal_gluontag_genb            = new TH2D*[nbins];
  h_Zpt_Resp_gluontag_genb            = new TH2D*[nbins];
  h_Zpt_RGMPF_gluontag_genb           = new TH2D*[nbins];
  h_Zpt_RGenjet1_gluontag_genb        = new TH2D*[nbins];
  h_Zpt_RGenjetn_gluontag_genb        = new TH2D*[nbins];
  h_Zpt_RGenuncl_gluontag_genb        = new TH2D*[nbins];
  h_Zpt_RBal_quarktag_genb            = new TH2D*[nbins];
  h_Zpt_Resp_quarktag_genb            = new TH2D*[nbins];
  h_Zpt_RGMPF_quarktag_genb           = new TH2D*[nbins];
  h_Zpt_RGenjet1_quarktag_genb        = new TH2D*[nbins];
  h_Zpt_RGenjetn_quarktag_genb        = new TH2D*[nbins];
  h_Zpt_RGenuncl_quarktag_genb        = new TH2D*[nbins];
  h_Zpt_RBal_notag_genb               = new TH2D*[nbins];
  h_Zpt_Resp_notag_genb               = new TH2D*[nbins];
  h_Zpt_RGMPF_notag_genb              = new TH2D*[nbins];
  h_Zpt_RGenjet1_notag_genb           = new TH2D*[nbins];
  h_Zpt_RGenjetn_notag_genb           = new TH2D*[nbins];
  h_Zpt_RGenuncl_notag_genb           = new TH2D*[nbins];

  h_Zpt_RBal_genc                     = new TH2D*[nbins];
  h_Zpt_Resp_genc                     = new TH2D*[nbins];
  h_Zpt_RGMPF_genc                    = new TH2D*[nbins];
  h_Zpt_RGenjet1_genc                 = new TH2D*[nbins];
  h_Zpt_RGenjetn_genc                 = new TH2D*[nbins];
  h_Zpt_RGenuncl_genc                 = new TH2D*[nbins];
  h_Zpt_RBal_btag_genc      = new TH2D*[nbins];
  h_Zpt_Resp_btag_genc      = new TH2D*[nbins];
  h_Zpt_RGMPF_btag_genc     = new TH2D*[nbins];
  h_Zpt_RGenjet1_btag_genc  = new TH2D*[nbins];
  h_Zpt_RGenjetn_btag_genc  = new TH2D*[nbins];
  h_Zpt_RGenuncl_btag_genc  = new TH2D*[nbins];
  h_Zpt_RBal_ctag_genc      = new TH2D*[nbins];
  h_Zpt_Resp_ctag_genc      = new TH2D*[nbins];
  h_Zpt_RGMPF_ctag_genc     = new TH2D*[nbins];
  h_Zpt_RGenjet1_ctag_genc  = new TH2D*[nbins];
  h_Zpt_RGenjetn_ctag_genc  = new TH2D*[nbins];
  h_Zpt_RGenuncl_ctag_genc  = new TH2D*[nbins];
  h_Zpt_RBal_gluontag_genc            = new TH2D*[nbins];
  h_Zpt_Resp_gluontag_genc            = new TH2D*[nbins];
  h_Zpt_RGMPF_gluontag_genc           = new TH2D*[nbins];
  h_Zpt_RGenjet1_gluontag_genc        = new TH2D*[nbins];
  h_Zpt_RGenjetn_gluontag_genc        = new TH2D*[nbins];
  h_Zpt_RGenuncl_gluontag_genc        = new TH2D*[nbins];
  h_Zpt_RBal_quarktag_genc            = new TH2D*[nbins];
  h_Zpt_Resp_quarktag_genc            = new TH2D*[nbins];
  h_Zpt_RGMPF_quarktag_genc           = new TH2D*[nbins];
  h_Zpt_RGenjet1_quarktag_genc        = new TH2D*[nbins];
  h_Zpt_RGenjetn_quarktag_genc        = new TH2D*[nbins];
  h_Zpt_RGenuncl_quarktag_genc        = new TH2D*[nbins];
  h_Zpt_RBal_notag_genc               = new TH2D*[nbins];
  h_Zpt_Resp_notag_genc               = new TH2D*[nbins];
  h_Zpt_RGMPF_notag_genc              = new TH2D*[nbins];
  h_Zpt_RGenjet1_notag_genc           = new TH2D*[nbins];
  h_Zpt_RGenjetn_notag_genc           = new TH2D*[nbins];
  h_Zpt_RGenuncl_notag_genc           = new TH2D*[nbins];

  h_Zpt_RBal_genuds                     = new TH2D*[nbins];
  h_Zpt_Resp_genuds                     = new TH2D*[nbins];
  h_Zpt_RGMPF_genuds                    = new TH2D*[nbins];
  h_Zpt_RGenjet1_genuds                 = new TH2D*[nbins];
  h_Zpt_RGenjetn_genuds                 = new TH2D*[nbins];
  h_Zpt_RGenuncl_genuds                 = new TH2D*[nbins];
  h_Zpt_RBal_btag_genuds      = new TH2D*[nbins];
  h_Zpt_Resp_btag_genuds      = new TH2D*[nbins];
  h_Zpt_RGMPF_btag_genuds     = new TH2D*[nbins];
  h_Zpt_RGenjet1_btag_genuds  = new TH2D*[nbins];
  h_Zpt_RGenjetn_btag_genuds  = new TH2D*[nbins];
  h_Zpt_RGenuncl_btag_genuds  = new TH2D*[nbins];
  h_Zpt_RBal_ctag_genuds      = new TH2D*[nbins];
  h_Zpt_Resp_ctag_genuds      = new TH2D*[nbins];
  h_Zpt_RGMPF_ctag_genuds     = new TH2D*[nbins];
  h_Zpt_RGenjet1_ctag_genuds  = new TH2D*[nbins];
  h_Zpt_RGenjetn_ctag_genuds  = new TH2D*[nbins];
  h_Zpt_RGenuncl_ctag_genuds  = new TH2D*[nbins];
  h_Zpt_RBal_gluontag_genuds            = new TH2D*[nbins];
  h_Zpt_Resp_gluontag_genuds            = new TH2D*[nbins];
  h_Zpt_RGMPF_gluontag_genuds           = new TH2D*[nbins];
  h_Zpt_RGenjet1_gluontag_genuds        = new TH2D*[nbins];
  h_Zpt_RGenjetn_gluontag_genuds        = new TH2D*[nbins];
  h_Zpt_RGenuncl_gluontag_genuds        = new TH2D*[nbins];
  h_Zpt_RBal_quarktag_genuds            = new TH2D*[nbins];
  h_Zpt_Resp_quarktag_genuds            = new TH2D*[nbins];
  h_Zpt_RGMPF_quarktag_genuds           = new TH2D*[nbins];
  h_Zpt_RGenjet1_quarktag_genuds        = new TH2D*[nbins];
  h_Zpt_RGenjetn_quarktag_genuds        = new TH2D*[nbins];
  h_Zpt_RGenuncl_quarktag_genuds        = new TH2D*[nbins];
  h_Zpt_RBal_notag_genuds               = new TH2D*[nbins];
  h_Zpt_Resp_notag_genuds               = new TH2D*[nbins];
  h_Zpt_RGMPF_notag_genuds              = new TH2D*[nbins];
  h_Zpt_RGenjet1_notag_genuds           = new TH2D*[nbins];
  h_Zpt_RGenjetn_notag_genuds           = new TH2D*[nbins];
  h_Zpt_RGenuncl_notag_genuds           = new TH2D*[nbins];

  h_Zpt_RBal_geng                     = new TH2D*[nbins];
  h_Zpt_Resp_geng                     = new TH2D*[nbins];
  h_Zpt_RGMPF_geng                    = new TH2D*[nbins];
  h_Zpt_RGenjet1_geng                 = new TH2D*[nbins];
  h_Zpt_RGenjetn_geng                 = new TH2D*[nbins];
  h_Zpt_RGenuncl_geng                 = new TH2D*[nbins];
  h_Zpt_RBal_btag_geng      = new TH2D*[nbins];
  h_Zpt_Resp_btag_geng      = new TH2D*[nbins];
  h_Zpt_RGMPF_btag_geng     = new TH2D*[nbins];
  h_Zpt_RGenjet1_btag_geng  = new TH2D*[nbins];
  h_Zpt_RGenjetn_btag_geng  = new TH2D*[nbins];
  h_Zpt_RGenuncl_btag_geng  = new TH2D*[nbins];
  h_Zpt_RBal_ctag_geng      = new TH2D*[nbins];
  h_Zpt_Resp_ctag_geng      = new TH2D*[nbins];
  h_Zpt_RGMPF_ctag_geng     = new TH2D*[nbins];
  h_Zpt_RGenjet1_ctag_geng  = new TH2D*[nbins];
  h_Zpt_RGenjetn_ctag_geng  = new TH2D*[nbins];
  h_Zpt_RGenuncl_ctag_geng  = new TH2D*[nbins];
  h_Zpt_RBal_gluontag_geng            = new TH2D*[nbins];
  h_Zpt_Resp_gluontag_geng            = new TH2D*[nbins];
  h_Zpt_RGMPF_gluontag_geng           = new TH2D*[nbins];
  h_Zpt_RGenjet1_gluontag_geng        = new TH2D*[nbins];
  h_Zpt_RGenjetn_gluontag_geng        = new TH2D*[nbins];
  h_Zpt_RGenuncl_gluontag_geng        = new TH2D*[nbins];
  h_Zpt_RBal_quarktag_geng            = new TH2D*[nbins];
  h_Zpt_Resp_quarktag_geng            = new TH2D*[nbins];
  h_Zpt_RGMPF_quarktag_geng           = new TH2D*[nbins];
  h_Zpt_RGenjet1_quarktag_geng        = new TH2D*[nbins];
  h_Zpt_RGenjetn_quarktag_geng        = new TH2D*[nbins];
  h_Zpt_RGenuncl_quarktag_geng        = new TH2D*[nbins];
  h_Zpt_RBal_notag_geng               = new TH2D*[nbins];
  h_Zpt_Resp_notag_geng               = new TH2D*[nbins];
  h_Zpt_RGMPF_notag_geng              = new TH2D*[nbins];
  h_Zpt_RGenjet1_notag_geng           = new TH2D*[nbins];
  h_Zpt_RGenjetn_notag_geng           = new TH2D*[nbins];
  h_Zpt_RGenuncl_notag_geng           = new TH2D*[nbins];

  h_Zpt_RBal_unclassified                     = new TH2D*[nbins];
  h_Zpt_Resp_unclassified                     = new TH2D*[nbins];
  h_Zpt_RGMPF_unclassified                    = new TH2D*[nbins];
  h_Zpt_RGenjet1_unclassified                 = new TH2D*[nbins];
  h_Zpt_RGenjetn_unclassified                 = new TH2D*[nbins];
  h_Zpt_RGenuncl_unclassified                 = new TH2D*[nbins];
  h_Zpt_RBal_btag_unclassified      = new TH2D*[nbins];
  h_Zpt_Resp_btag_unclassified      = new TH2D*[nbins];
  h_Zpt_RGMPF_btag_unclassified     = new TH2D*[nbins];
  h_Zpt_RGenjet1_btag_unclassified  = new TH2D*[nbins];
  h_Zpt_RGenjetn_btag_unclassified  = new TH2D*[nbins];
  h_Zpt_RGenuncl_btag_unclassified  = new TH2D*[nbins];
  h_Zpt_RBal_ctag_unclassified      = new TH2D*[nbins];
  h_Zpt_Resp_ctag_unclassified      = new TH2D*[nbins];
  h_Zpt_RGMPF_ctag_unclassified     = new TH2D*[nbins];
  h_Zpt_RGenjet1_ctag_unclassified  = new TH2D*[nbins];
  h_Zpt_RGenjetn_ctag_unclassified  = new TH2D*[nbins];
  h_Zpt_RGenuncl_ctag_unclassified  = new TH2D*[nbins];
  h_Zpt_RBal_gluontag_unclassified            = new TH2D*[nbins];
  h_Zpt_Resp_gluontag_unclassified            = new TH2D*[nbins];
  h_Zpt_RGMPF_gluontag_unclassified           = new TH2D*[nbins];
  h_Zpt_RGenjet1_gluontag_unclassified        = new TH2D*[nbins];
  h_Zpt_RGenjetn_gluontag_unclassified        = new TH2D*[nbins];
  h_Zpt_RGenuncl_gluontag_unclassified        = new TH2D*[nbins];
  h_Zpt_RBal_quarktag_unclassified            = new TH2D*[nbins];
  h_Zpt_Resp_quarktag_unclassified            = new TH2D*[nbins];
  h_Zpt_RGMPF_quarktag_unclassified           = new TH2D*[nbins];
  h_Zpt_RGenjet1_quarktag_unclassified        = new TH2D*[nbins];
  h_Zpt_RGenjetn_quarktag_unclassified        = new TH2D*[nbins];
  h_Zpt_RGenuncl_quarktag_unclassified        = new TH2D*[nbins];
  h_Zpt_RBal_notag_unclassified               = new TH2D*[nbins];
  h_Zpt_Resp_notag_unclassified               = new TH2D*[nbins];
  h_Zpt_RGMPF_notag_unclassified              = new TH2D*[nbins];
  h_Zpt_RGenjet1_notag_unclassified           = new TH2D*[nbins];
  h_Zpt_RGenjetn_notag_unclassified           = new TH2D*[nbins];
  h_Zpt_RGenuncl_notag_unclassified           = new TH2D*[nbins];

  
  h_Zpt_RZ_genb                = new TH2D*[nbins];
  h_Zpt_RZ_genc                = new TH2D*[nbins];
  h_Zpt_RZ_genuds              = new TH2D*[nbins];
  h_Zpt_RZ_geng                = new TH2D*[nbins];
  h_Zpt_RZ_unclassified        = new TH2D*[nbins];


  h_pTgen_Resp                       = new TH2D*[nbins];
  h_pTgen_Resp_btag        = new TH2D*[nbins];
  h_pTgen_Resp_ctag        = new TH2D*[nbins];
  h_pTgen_Resp_gluontag              = new TH2D*[nbins];
  h_pTgen_Resp_quarktag              = new TH2D*[nbins];
  h_pTgen_Resp_notag                 = new TH2D*[nbins];

  h_pTgen_Resp_genb                  = new TH2D*[nbins];
  h_pTgen_Resp_btag_genb   = new TH2D*[nbins];
  h_pTgen_Resp_ctag_genb   = new TH2D*[nbins];
  h_pTgen_Resp_gluontag_genb         = new TH2D*[nbins];
  h_pTgen_Resp_quarktag_genb         = new TH2D*[nbins];
  h_pTgen_Resp_notag_genb            = new TH2D*[nbins];
  
  h_pTgen_Resp_genc                  = new TH2D*[nbins];
  h_pTgen_Resp_btag_genc   = new TH2D*[nbins];
  h_pTgen_Resp_ctag_genc   = new TH2D*[nbins];
  h_pTgen_Resp_gluontag_genc         = new TH2D*[nbins];
  h_pTgen_Resp_quarktag_genc         = new TH2D*[nbins];
  h_pTgen_Resp_notag_genc            = new TH2D*[nbins];

  h_pTgen_Resp_genuds                = new TH2D*[nbins];
  h_pTgen_Resp_btag_genuds = new TH2D*[nbins];
  h_pTgen_Resp_ctag_genuds = new TH2D*[nbins];
  h_pTgen_Resp_gluontag_genuds       = new TH2D*[nbins];
  h_pTgen_Resp_quarktag_genuds       = new TH2D*[nbins];
  h_pTgen_Resp_notag_genuds          = new TH2D*[nbins];

  h_pTgen_Resp_geng                  = new TH2D*[nbins];
  h_pTgen_Resp_btag_geng   = new TH2D*[nbins];
  h_pTgen_Resp_ctag_geng   = new TH2D*[nbins];
  h_pTgen_Resp_gluontag_geng         = new TH2D*[nbins];
  h_pTgen_Resp_quarktag_geng         = new TH2D*[nbins];
  h_pTgen_Resp_notag_geng            = new TH2D*[nbins];

  h_pTgen_Resp_unclassified                  = new TH2D*[nbins];
  h_pTgen_Resp_btag_unclassified   = new TH2D*[nbins];
  h_pTgen_Resp_ctag_unclassified   = new TH2D*[nbins];
  h_pTgen_Resp_gluontag_unclassified         = new TH2D*[nbins];
  h_pTgen_Resp_quarktag_unclassified         = new TH2D*[nbins];
  h_pTgen_Resp_notag_unclassified            = new TH2D*[nbins];

  h_Zpt_leptonm_pt = (TH2D*)h_Zpt_leptonpt_template->Clone("h_Zpt_leptonm_pt");
  h_Zpt_leptonp_pt = (TH2D*)h_Zpt_leptonpt_template->Clone("h_Zpt_leptonp_pt");

  //std::cout << "nbins booking " << nbins << " " << nbins_alpha  << " " << bins_eta.size() << " " << npsWeights << std::endl;
  h_Zpt_Rho_1 = (TH2D*)h_Zpt_Rho_template->Clone("h_Zpt_rho_1");
  h_Zpt_Rho_2 = (TH2D*)h_Zpt_Rho_template->Clone("h_Zpt_rho_2");
  for(size_t i = 0; i < nbinsj; ++i){
    //nbinsj = nbins_pt*bins_eta.size()*npsWeights;
    size_t ipt = i%(nbins_pt*bins_eta.size())%nbins_pt;
    size_t ieta = i%(nbins_pt*bins_eta.size())/nbins_pt;
    size_t ipsw = i/(nbins_pt*bins_eta.size());

    std::string s_pt = std::string("_pt")+std::to_string(int(bins_pt[ipt]));
    std::string s_eta = std::string("_eta_")+std::regex_replace(bins_eta[ieta], std::regex("\\."), "");
    //    std::string s_psw = "";
    //    if(ipsw > 0) s_psw = "/PSWeight"+std::to_string(ipsw-1)+"/";
    fOUT->cd();
    fOUT->cd("analysis");
    if(ipsw > 0){
      std::string s_psw = "analysis/PSWeight"+std::to_string(ipsw-1);

      if(!fOUT->cd(s_psw.c_str())) {
        fOUT->mkdir(s_psw.c_str());
      }
      fOUT->cd(s_psw.c_str());
    }
    std::string s = s_pt+s_eta;
    //std::cout << "check nbinsj s " << i << " " << s << std::endl;

    h_Zpt_RMPFj2pt[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFj2pt"+s).c_str());
    h_Zpt_RMPFj2pt_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFj2pt"+s+"_genb").c_str());
    h_Zpt_RMPFj2pt_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFj2pt"+s+"_genc").c_str());
    h_Zpt_RMPFj2pt_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFj2pt"+s+"_genuds").c_str());
    h_Zpt_RMPFj2pt_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFj2pt"+s+"_geng").c_str());
    h_Zpt_RMPFj2pt_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFj2pt"+s+"_uncl").c_str());

    h_Zpt_RMPFjet1j2pt[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1j2pt"+s).c_str());
    h_Zpt_RMPFjet1j2pt_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1j2pt"+s+"_genb").c_str());
    h_Zpt_RMPFjet1j2pt_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1j2pt"+s+"_genc").c_str());
    h_Zpt_RMPFjet1j2pt_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1j2pt"+s+"_genuds").c_str());
    h_Zpt_RMPFjet1j2pt_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1j2pt"+s+"_geng").c_str());
    h_Zpt_RMPFjet1j2pt_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1j2pt"+s+"_uncl").c_str());

    h_Zpt_RMPFj2ptx[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFj2ptx"+s).c_str());
    h_Zpt_RMPFjet1j2ptx[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1j2ptx"+s).c_str());
  }
  for(size_t i = 0; i < nbinsj3D; ++i){
    //nbinsj = nbins_pt*npsWeights;
    size_t ipt = i%nbins_pt;
    //    size_t ieta = i%(nbins_pt*bins_eta.size())/nbins_pt;
    size_t ipsw = i/nbins_pt;

    std::string s_pt = std::string("_pt")+std::to_string(int(bins_pt[ipt]));
    std::string s_psw = "";
    //if(ipsw > 0) s_psw = "/PSWeight"+std::to_string(ipsw-1)+"/";
    fOUT->cd();
    fOUT->cd("analysis");
    if(ipsw > 0){
      std::string s_psw = "analysis/PSWeight"+std::to_string(ipsw-1);

      if(!fOUT->cd(s_psw.c_str())) {
        fOUT->mkdir(s_psw.c_str());
      }
      fOUT->cd(s_psw.c_str());
    }
    std::string s = s_pt;
    //std::cout << "check nbinsj3D s " << i << " " << s << std::endl;
    h3D_Zpt_RMPFj2pt_mu[i] = (TProfile2D*)h3D_Zpt_R_mu_template->Clone(("h3D_Zpt_RMPFj2pt_mu"+s).c_str());
    h3D_Zpt_RMPFj2ptx_mu[i] = (TProfile2D*)h3D_Zpt_R_mu_template->Clone(("h3D_Zpt_RMPFj2ptx_mu"+s).c_str());
    h3D_Zpt_RMPFj2pt_eta[i] = (TH3D*)h3D_Zpt_R_eta_template->Clone(("h3D_Zpt_RMPFj2pt_eta"+s).c_str());
    h3D_Zpt_RMPFj2ptx_eta[i] = (TH3D*)h3D_Zpt_R_eta_template->Clone(("h3D_Zpt_RMPFj2ptx_eta"+s).c_str());
  }
  for(size_t i = 0; i < nbins; ++i){
    //nbins = nbins_alpha*bins_eta.size()*npsWeights;
    size_t ialpha = i%(nbins_alpha*bins_eta.size())%nbins_alpha;
    size_t ieta = i%(nbins_alpha*bins_eta.size())/nbins_alpha;
    size_t ipsw = i/(nbins_alpha*bins_eta.size());
    
    std::string s_alpha = std::string("_alpha")+std::to_string(int(100*bins_alpha[ialpha]));
    std::string s_eta = std::string("_eta_")+std::regex_replace(bins_eta[ieta], std::regex("\\."), "");
    std::string s_psw = "";
    //if(ipsw > 0) s_psw = "/PSWeight"+std::to_string(ipsw-1)+"/";
    fOUT->cd();
    fOUT->cd("analysis");
    if(ipsw > 0){
      std::string s_psw = "analysis/PSWeight"+std::to_string(ipsw-1);

      if(!fOUT->cd(s_psw.c_str())) {
        fOUT->mkdir(s_psw.c_str());
      }
      fOUT->cd(s_psw.c_str());
    }
    std::string s = s_alpha+s_eta;
    //std::cout << "check s " << i << " " << s << std::endl;
    h_Zpt_mZ[i] = (TH2D*)h_Zpt_mZ_template->Clone(("h_Zpt_mZ"+s).c_str());
    if(!isData) h_Zpt_mZgen[i] = (TH2D*)h_Zpt_mZ_template->Clone(("h_Zpt_mZgen"+s).c_str());

    h_Zpt_Rho[i] = (TH2D*)h_Zpt_Rho_template->Clone(("h_Zpt_rho"+s).c_str());
    h_JetPt_Rho[i] = (TH2D*)h_Zpt_Rho_template->Clone(("h_JetPt_rho"+s).c_str());
    h_PtAve_Rho[i] = (TH2D*)h_Zpt_Rho_template->Clone(("h_PtAve_rho"+s).c_str());

    h_Zpt_chHEF[i]  = (TH2D*)h_Zpt_ef_template->Clone(("h_Zpt_chHEF"+s).c_str());
    h_Zpt_neHEF[i]  = (TH2D*)h_Zpt_ef_template->Clone(("h_Zpt_neHEF"+s).c_str());
    h_Zpt_chEmEF[i] = (TH2D*)h_Zpt_ef_template->Clone(("h_Zpt_chEmEF"+s).c_str());
    h_Zpt_neEmEF[i] = (TH2D*)h_Zpt_ef_template->Clone(("h_Zpt_neEmEF"+s).c_str());
    h_Zpt_muEF[i]   = (TH2D*)h_Zpt_ef_template->Clone(("h_Zpt_muEF"+s).c_str());

    h_Zpt_JNPF[i]   = (TH2D*)h_Zpt_jnpf_template->Clone(("h_Zpt_JNPF"+s).c_str());

    h_Zpt_RpT[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT"+s).c_str());
    h_Zpt_RMPF[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF"+s).c_str());
    h_Zpt_RMPFx[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFx"+s).c_str());
    h_Zpt_RMPFjet1[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1"+s).c_str());
    h_Zpt_RMPFjet1x[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1x"+s).c_str());
    h_Zpt_RMPFjetn[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn"+s).c_str());
    h_Zpt_RMPFuncl[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl"+s).c_str());
    h_Zpt_RMPFfromType1[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFfromType1"+s).c_str());
    h_Zpt_RMPFPF[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFPF"+s).c_str());

    h_JetPt_QGL[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL"+s).c_str());
    if(!isData){
    h_JetPt_QGL_genb[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL"+s+"_genb").c_str());
    h_JetPt_QGL_genc[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL"+s+"_genc").c_str());
    h_JetPt_QGL_genuds[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL"+s+"_genuds").c_str());
    h_JetPt_QGL_geng[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL"+s+"_geng").c_str());
    h_JetPt_QGL_unclassified[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL"+s+"_unclassified").c_str());
    }
    h_Zpt_QGL[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL"+s).c_str());
    if(!isData){
    h_Zpt_QGL_genb[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL"+s+"_genb").c_str());
    h_Zpt_QGL_genc[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL"+s+"_genc").c_str());
    h_Zpt_QGL_genuds[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL"+s+"_genuds").c_str());
    h_Zpt_QGL_geng[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL"+s+"_geng").c_str());
    h_Zpt_QGL_unclassified[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL"+s+"_unclassified").c_str());
    }
    h_JetPt_muEF[i] = (TH2D*)h_JetPt_muEF_template->Clone(("h_JetPt_muEF"+s).c_str());
    h_JetPt_chEmEF[i] = (TH2D*)h_JetPt_chEmEF_template->Clone(("h_JetPt_chEmEF"+s).c_str());
    h_JetPt_chHEF[i] = (TH2D*)h_JetPt_chHEF_template->Clone(("h_JetPt_chHEF"+s).c_str());
    h_JetPt_neEmEF[i] = (TH2D*)h_JetPt_neEmEF_template->Clone(("h_JetPt_neEmEF"+s).c_str());
    h_JetPt_neHEF[i] = (TH2D*)h_JetPt_neHEF_template->Clone(("h_JetPt_neHEF"+s).c_str());

    h_PtAve_QGL[i] = (TH2D*)h_PtAve_QGL_template->Clone(("h_PtAve_QGL"+s).c_str());
    h_PtAve_muEF[i] = (TH2D*)h_PtAve_muEF_template->Clone(("h_PtAve_muEF"+s).c_str());
    h_PtAve_chEmEF[i] = (TH2D*)h_PtAve_chEmEF_template->Clone(("h_PtAve_chEmEF"+s).c_str());
    h_PtAve_chHEF[i] = (TH2D*)h_PtAve_chHEF_template->Clone(("h_PtAve_chHEF"+s).c_str());
    h_PtAve_neEmEF[i] = (TH2D*)h_PtAve_neEmEF_template->Clone(("h_PtAve_neEmEF"+s).c_str());
    h_PtAve_neHEF[i] = (TH2D*)h_PtAve_neHEF_template->Clone(("h_PtAve_neHEF"+s).c_str());

    h_Zpt_Mu_Rho[i] = (TProfile2D*)h_Zpt_Mu_Rho_template->Clone(("h3D_Zpt_Mu_Rho"+s).c_str());
    h_Zpt_Mu_NPVgood[i] = (TProfile2D*)h_Zpt_Mu_NPVgood_template->Clone(("h3D_Zpt_Mu_NPVgood"+s).c_str());
    h_Zpt_Mu_NPVall[i] = (TProfile2D*)h_Zpt_Mu_NPVall_template->Clone(("h3D_Zpt_Mu_NPVall"+s).c_str());
    h_Zpt_Mu_Mu[i] = (TProfile2D*)h_Zpt_Mu_Mu_template->Clone(("h3D_Zpt_Mu_Mu"+s).c_str());

    h_ZpT_Mu_RpT[i]      = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_Mu_RpT"+s).c_str());
    h_ZpT_Mu_RMPF[i]     = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_Mu_RMPF"+s).c_str());
    h_ZpT_Mu_RMPFjet1[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_Mu_RMPFjet1"+s).c_str());
    h_ZpT_Mu_RMPFjetn[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_Mu_RMPFjetn"+s).c_str());
    h_ZpT_Mu_RMPFuncl[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_Mu_RMPFuncl"+s).c_str());

    h_ZpT_Mu_offset[i]    = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_Mu_offset"+s).c_str());
    h_ZpT_Rho_offset[i]    = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_Rho_offset"+s).c_str());
    h_ZpT_NPVgood_offset[i]    = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_NPVgood_offset"+s).c_str());
    h_ZpT_NPVall_offset[i]    = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_NPVall_offset"+s).c_str());

    h_ZpT_NPVg_RpT[i]      = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_NPVg_RpT"+s).c_str());
    h_ZpT_NPVg_RMPF[i]     = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_NPVg_RMPF"+s).c_str());
    h_ZpT_NPVg_RMPFjet1[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_NPVg_RMPFjet1"+s).c_str());
    h_ZpT_NPVg_RMPFjetn[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_NPVg_RMPFjetn"+s).c_str());
    h_ZpT_NPVg_RMPFuncl[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_Zpt_NPVg_RMPFuncl"+s).c_str());

    h_JetPt_Mu_RpT[i]      = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_JetPt_Mu_RpT"+s).c_str());
    h_JetPt_Mu_RMPF[i]     = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_JetPt_Mu_RMPF"+s).c_str());
    h_JetPt_Mu_RMPFjet1[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_JetPt_Mu_RMPFjet1"+s).c_str());
    h_JetPt_Mu_RMPFjetn[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_JetPt_Mu_RMPFjetn"+s).c_str());
    h_JetPt_Mu_RMPFuncl[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_JetPt_Mu_RMPFuncl"+s).c_str());

    h_JetPt_NPVg_RpT[i]      = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_JetPt_NPVg_RpT"+s).c_str());
    h_JetPt_NPVg_RMPF[i]     = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_JetPt_NPVg_RMPF"+s).c_str());
    h_JetPt_NPVg_RMPFjet1[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_JetPt_NPVg_RMPFjet1"+s).c_str());
    h_JetPt_NPVg_RMPFjetn[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_JetPt_NPVg_RMPFjetn"+s).c_str());
    h_JetPt_NPVg_RMPFuncl[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_JetPt_NPVg_RMPFuncl"+s).c_str());

    h_PtAve_Mu_RpT[i]      = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_PtAve_Mu_RpT"+s).c_str());
    h_PtAve_Mu_RMPF[i]     = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_PtAve_Mu_RMPF"+s).c_str());
    h_PtAve_Mu_RMPFjet1[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_PtAve_Mu_RMPFjet1"+s).c_str());
    h_PtAve_Mu_RMPFjetn[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_PtAve_Mu_RMPFjetn"+s).c_str());
    h_PtAve_Mu_RMPFuncl[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_PtAve_Mu_RMPFuncl"+s).c_str());

    h_PtAve_NPVg_RpT[i]      = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_PtAve_NPVg_RpT"+s).c_str());
    h_PtAve_NPVg_RMPF[i]     = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_PtAve_NPVg_RMPF"+s).c_str());
    h_PtAve_NPVg_RMPFjet1[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_PtAve_NPVg_RMPFjet1"+s).c_str());
    h_PtAve_NPVg_RMPFjetn[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_PtAve_NPVg_RMPFjetn"+s).c_str());
    h_PtAve_NPVg_RMPFuncl[i] = (TProfile2D*)h_Pt_Mu_RpT_template->Clone(("h3D_PtAve_NPVg_RMPFuncl"+s).c_str());

    h_Mu_RpT[i] = (TH2D*)h_Mu_RpT_template->Clone(("h_Mu_RpT"+s).c_str());
    h_JetPt_RpT[i] = (TH2D*)h_JetPt_RpT_template->Clone(("h_JetPt_RpT"+s).c_str());
    h_PtAve_RpT[i] = (TH2D*)h_PtAve_RpT_template->Clone(("h_PtAve_RpT"+s).c_str());
    h_Mu_RMPF[i] = (TH2D*)h_Mu_RMPF_template->Clone(("h_Mu_RMPF"+s).c_str());
    h_JetPt_RMPF[i] = (TH2D*)h_JetPt_RMPF_template->Clone(("h_JetPt_RMPF"+s).c_str());
    h_PtAve_RMPF[i] = (TH2D*)h_PtAve_RMPF_template->Clone(("h_PtAve_RMPF"+s).c_str());
    h_Mu_RMPFjet1[i] = (TH2D*)h_Mu_RMPFjet1_template->Clone(("h_Mu_RMPFjet1"+s).c_str());
    h_JetPt_RMPFjet1[i] = (TH2D*)h_JetPt_RMPFjet1_template->Clone(("h_JetPt_RMPFjet1"+s).c_str());
    h_PtAve_RMPFjet1[i] = (TH2D*)h_PtAve_RMPFjet1_template->Clone(("h_PtAve_RMPFjet1"+s).c_str());
    h_Mu_RMPFjetn[i] = (TH2D*)h_Mu_RMPFjetn_template->Clone(("h_Mu_RMPFjetn"+s).c_str());
    h_JetPt_RMPFjetn[i] = (TH2D*)h_JetPt_RMPFjetn_template->Clone(("h_JetPt_RMPFjetn"+s).c_str());
    h_PtAve_RMPFjetn[i] = (TH2D*)h_PtAve_RMPFjetn_template->Clone(("h_PtAve_RMPFjetn"+s).c_str());
    h_Mu_RMPFuncl[i] = (TH2D*)h_Mu_RMPFuncl_template->Clone(("h_Mu_RMPFuncl"+s).c_str());
    h_JetPt_RMPFuncl[i] = (TH2D*)h_JetPt_RMPFuncl_template->Clone(("h_JetPt_RMPFuncl"+s).c_str());
    h_PtAve_RMPFuncl[i] = (TH2D*)h_PtAve_RMPFuncl_template->Clone(("h_PtAve_RMPFuncl"+s).c_str());

    h_Mu_ptdiff[i] = (TH2D*)h_Mu_ptdiff_template->Clone(("h_Mu_ptdiff"+s).c_str());

    //h_Zpt_RpT_btagCSVV2loose[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2loose"+s).c_str());
    //h_Zpt_RMPF_btagCSVV2loose[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2loose"+s).c_str());
    //h_Zpt_RpT_btagCSVV2medium[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2medium"+s).c_str());
    //h_Zpt_RMPF_btagCSVV2medium[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2medium"+s).c_str());
    //h_Zpt_RpT_btagCSVV2tight[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2tight"+s).c_str());
    //h_Zpt_RMPF_btagCSVV2tight[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2tight"+s).c_str());
    if(!isData){
    h_Zpt_RpT_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT"+s+"_genb").c_str());
    h_Zpt_RMPF_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF"+s+"_genb").c_str());
    h_Zpt_RMPFjet1_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1"+s+"_genb").c_str());
    h_Zpt_RMPFjetn_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn"+s+"_genb").c_str());
    h_Zpt_RMPFuncl_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl"+s+"_genb").c_str());
    //h_Zpt_RpT_btagCSVV2loose_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2loose"+s+"_genb").c_str());
    //h_Zpt_RMPF_btagCSVV2loose_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2loose"+s+"_genb").c_str());
    //h_Zpt_RpT_btagCSVV2medium_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2medium"+s+"_genb").c_str());
    //h_Zpt_RMPF_btagCSVV2medium_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2medium"+s+"_genb").c_str());
    //h_Zpt_RpT_btagCSVV2tight_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2tight"+s+"_genb").c_str());
    //h_Zpt_RMPF_btagCSVV2tight_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2tight"+s+"_genb").c_str());

    h_Zpt_RpT_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT"+s+"_genc").c_str());
    h_Zpt_RMPF_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF"+s+"_genc").c_str());
    h_Zpt_RMPFjet1_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1"+s+"_genc").c_str());
    h_Zpt_RMPFjetn_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn"+s+"_genc").c_str());
    h_Zpt_RMPFuncl_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl"+s+"_genc").c_str());
    //h_Zpt_RpT_btagCSVV2loose_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2loose"+s+"_genc").c_str());
    //h_Zpt_RMPF_btagCSVV2loose_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2loose"+s+"_genc").c_str());
    //h_Zpt_RpT_btagCSVV2medium_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2medium"+s+"_genc").c_str());
    //h_Zpt_RMPF_btagCSVV2medium_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2medium"+s+"_genc").c_str());
    //h_Zpt_RpT_btagCSVV2tight_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2tight"+s+"_genc").c_str());
    //h_Zpt_RMPF_btagCSVV2tight_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2tight"+s+"_genc").c_str());

    h_Zpt_RpT_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT"+s+"_genuds").c_str());
    h_Zpt_RMPF_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF"+s+"_genuds").c_str());
    h_Zpt_RMPFjet1_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1"+s+"_genuds").c_str());
    h_Zpt_RMPFjetn_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn"+s+"_genuds").c_str());
    h_Zpt_RMPFuncl_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl"+s+"_genuds").c_str());

    h_Zpt_RpT_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT"+s+"_gens").c_str());
    h_Zpt_RMPF_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF"+s+"_gens").c_str());
    h_Zpt_RMPFjet1_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1"+s+"_gens").c_str());
    h_Zpt_RMPFjetn_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn"+s+"_gens").c_str());
    h_Zpt_RMPFuncl_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl"+s+"_gens").c_str());

    h_Zpt_RpT_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT"+s+"_genud").c_str());
    h_Zpt_RMPF_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF"+s+"_genud").c_str());
    h_Zpt_RMPFjet1_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1"+s+"_genud").c_str());
    h_Zpt_RMPFjetn_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn"+s+"_genud").c_str());
    h_Zpt_RMPFuncl_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl"+s+"_genud").c_str());

    //h_Zpt_RpT_btagCSVV2loose_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2loose"+s+"_genuds").c_str());
    //h_Zpt_RMPF_btagCSVV2loose_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2loose"+s+"_genuds").c_str());
    //h_Zpt_RpT_btagCSVV2medium_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2medium"+s+"_genuds").c_str());
    //h_Zpt_RMPF_btagCSVV2medium_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2medium"+s+"_genuds").c_str());
    //h_Zpt_RpT_btagCSVV2tight_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2tight"+s+"_genuds").c_str());
    //h_Zpt_RMPF_btagCSVV2tight_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2tight"+s+"_genuds").c_str());

    h_Zpt_RpT_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT"+s+"_geng").c_str());
    h_Zpt_RMPF_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF"+s+"_geng").c_str());
    h_Zpt_RMPFjet1_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1"+s+"_geng").c_str());
    h_Zpt_RMPFjetn_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn"+s+"_geng").c_str());
    h_Zpt_RMPFuncl_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl"+s+"_geng").c_str());
    //h_Zpt_RpT_btagCSVV2loose_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2loose"+s+"_geng").c_str());
    //h_Zpt_RMPF_btagCSVV2loose_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2loose"+s+"_geng").c_str());
    //h_Zpt_RpT_btagCSVV2medium_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2medium"+s+"_geng").c_str());
    //h_Zpt_RMPF_btagCSVV2medium_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2medium"+s+"_geng").c_str());
    //h_Zpt_RpT_btagCSVV2tight_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2tight"+s+"_geng").c_str());
    //h_Zpt_RMPF_btagCSVV2tight_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2tight"+s+"_geng").c_str());

    h_Zpt_RpT_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT"+s+"_unclassified").c_str());
    h_Zpt_RMPF_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF"+s+"_unclassified").c_str());
    h_Zpt_RMPFjet1_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1"+s+"_unclassified").c_str());
    h_Zpt_RMPFjetn_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn"+s+"_unclassified").c_str());
    h_Zpt_RMPFuncl_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl"+s+"_unclassified").c_str());
    }

    //h_Zpt_RpT_btagDeepBloose[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBloose"+s).c_str());
    //h_Zpt_RMPF_btagDeepBloose[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBloose"+s).c_str());
    //h_Zpt_RpT_btagDeepBmedium[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBmedium"+s).c_str());
    //h_Zpt_RMPF_btagDeepBmedium[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBmedium"+s).c_str());
    h_Zpt_RpT_btag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btag"+s).c_str());
    h_Zpt_RMPF_btag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btag"+s).c_str());
    h_Zpt_RMPFjet1_btag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_btag"+s).c_str());
    h_Zpt_RMPFjetn_btag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_btag"+s).c_str());
    h_Zpt_RMPFuncl_btag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_btag"+s).c_str());

    h_JetPt_QGL_btag[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_btag"+s).c_str());
    if(!isData){
    h_JetPt_QGL_btag_genb[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_btag"+s+"_genb").c_str());
    h_JetPt_QGL_btag_genc[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_btag"+s+"_genc").c_str());
    h_JetPt_QGL_btag_genuds[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_btag"+s+"_genuds").c_str());
    h_JetPt_QGL_btag_geng[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_btag"+s+"_geng").c_str());
    h_JetPt_QGL_btag_unclassified[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_btag"+s+"_unclassified").c_str());
    }
    h_Zpt_QGL_btag[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_btag"+s).c_str());
    if(!isData){
    h_Zpt_QGL_btag_genb[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_btag"+s+"_genb").c_str());
    h_Zpt_QGL_btag_genc[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_btag"+s+"_genc").c_str());
    h_Zpt_QGL_btag_genuds[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_btag"+s+"_genuds").c_str());
    h_Zpt_QGL_btag_geng[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_btag"+s+"_geng").c_str());
    h_Zpt_QGL_btag_unclassified[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_btag"+s+"_unclassified").c_str());
    }
    h_JetPt_muEF_btag[i] = (TH2D*)h_JetPt_muEF_template->Clone(("h_JetPt_muEF_btag"+s).c_str());
    h_JetPt_chEmEF_btag[i] = (TH2D*)h_JetPt_chEmEF_template->Clone(("h_JetPt_chEmEF_btag"+s).c_str());
    h_JetPt_chHEF_btag[i] = (TH2D*)h_JetPt_chHEF_template->Clone(("h_JetPt_chHEF_btag"+s).c_str());
    h_JetPt_neEmEF_btag[i] = (TH2D*)h_JetPt_neEmEF_template->Clone(("h_JetPt_neEmEF_btag"+s).c_str());
    h_JetPt_neHEF_btag[i] = (TH2D*)h_JetPt_neHEF_template->Clone(("h_JetPt_neHEF_btag"+s).c_str());

    h_PtAve_QGL_btag[i] = (TH2D*)h_PtAve_QGL_template->Clone(("h_PtAve_QGL_btag"+s).c_str());
    h_PtAve_muEF_btag[i] = (TH2D*)h_PtAve_muEF_template->Clone(("h_PtAve_muEF_btag"+s).c_str());
    h_PtAve_chEmEF_btag[i] = (TH2D*)h_PtAve_chEmEF_template->Clone(("h_PtAve_chEmEF_btag"+s).c_str());
    h_PtAve_chHEF_btag[i] = (TH2D*)h_PtAve_chHEF_template->Clone(("h_PtAve_chHEF_btag"+s).c_str());
    h_PtAve_neEmEF_btag[i] = (TH2D*)h_PtAve_neEmEF_template->Clone(("h_PtAve_neEmEF_btag"+s).c_str());
    h_PtAve_neHEF_btag[i] = (TH2D*)h_PtAve_neHEF_template->Clone(("h_PtAve_neHEF_btag"+s).c_str());

    h_Zpt_Mu_Rho_btag[i] = (TProfile2D*)h_Zpt_Mu_Rho_template->Clone(("h3D_Zpt_Mu_Rho_btag"+s).c_str());
    h_Zpt_Mu_NPVgood_btag[i] = (TProfile2D*)h_Zpt_Mu_NPVgood_template->Clone(("h3D_Zpt_Mu_NPVgood_btag"+s).c_str());
    h_Zpt_Mu_NPVall_btag[i] = (TProfile2D*)h_Zpt_Mu_NPVall_template->Clone(("h3D_Zpt_Mu_NPVall_btag"+s).c_str());
    h_Zpt_Mu_Mu_btag[i] = (TProfile2D*)h_Zpt_Mu_Mu_template->Clone(("h3D_Zpt_Mu_Mu_btag"+s).c_str());
    h_Mu_RpT_btag[i] = (TH2D*)h_Mu_RpT_template->Clone(("h_Mu_RpT_btag"+s).c_str());
    h_JetPt_RpT_btag[i] = (TH2D*)h_JetPt_RpT_template->Clone(("h_JetPt_RpT_btag"+s).c_str());
    h_PtAve_RpT_btag[i] = (TH2D*)h_PtAve_RpT_template->Clone(("h_PtAve_RpT_btag"+s).c_str());
    h_Mu_RMPF_btag[i] = (TH2D*)h_Mu_RMPF_template->Clone(("h_Mu_RMPF_btag"+s).c_str());
    h_JetPt_RMPF_btag[i] = (TH2D*)h_JetPt_RMPF_template->Clone(("h_JetPt_RMPF_btag"+s).c_str());
    h_PtAve_RMPF_btag[i] = (TH2D*)h_PtAve_RMPF_template->Clone(("h_PtAve_RMPF_btag"+s).c_str());
    h_Mu_RMPFjet1_btag[i] = (TH2D*)h_Mu_RMPFjet1_template->Clone(("h_Mu_RMPFjet1_btag"+s).c_str());
    h_JetPt_RMPFjet1_btag[i] = (TH2D*)h_JetPt_RMPFjet1_template->Clone(("h_JetPt_RMPFjet1_btag"+s).c_str());
    h_PtAve_RMPFjet1_btag[i] = (TH2D*)h_PtAve_RMPFjet1_template->Clone(("h_PtAve_RMPFjet1_btag"+s).c_str());
    h_Mu_RMPFjetn_btag[i] = (TH2D*)h_Mu_RMPFjetn_template->Clone(("h_Mu_RMPFjetn_btag"+s).c_str());
    h_JetPt_RMPFjetn_btag[i] = (TH2D*)h_JetPt_RMPFjetn_template->Clone(("h_JetPt_RMPFjetn_btag"+s).c_str());
    h_PtAve_RMPFjetn_btag[i] = (TH2D*)h_PtAve_RMPFjetn_template->Clone(("h_PtAve_RMPFjetn_btag"+s).c_str());
    h_Mu_RMPFuncl_btag[i] = (TH2D*)h_Mu_RMPFuncl_template->Clone(("h_Mu_RMPFuncl_btag"+s).c_str());
    h_JetPt_RMPFuncl_btag[i] = (TH2D*)h_JetPt_RMPFuncl_template->Clone(("h_JetPt_RMPFuncl_btag"+s).c_str());
    h_PtAve_RMPFuncl_btag[i] = (TH2D*)h_PtAve_RMPFuncl_template->Clone(("h_PtAve_RMPFuncl_btag"+s).c_str());
    if(!isData){
    //h_Zpt_RpT_btagDeepBloose_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBloose"+s+"_genb").c_str());
    //h_Zpt_RMPF_btagDeepBloose_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBloose"+s+"_genb").c_str());
    //h_Zpt_RpT_btagDeepBmedium_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBmedium"+s+"_genb").c_str());
    //h_Zpt_RMPF_btagDeepBmedium_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBmedium"+s+"_genb").c_str());
    h_Zpt_RpT_btag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btag"+s+"_genb").c_str());
    h_Zpt_RMPF_btag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btag"+s+"_genb").c_str());
    h_Zpt_RMPFjet1_btag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_btag"+s+"_genb").c_str());
    h_Zpt_RMPFjetn_btag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_btag"+s+"_genb").c_str());
    h_Zpt_RMPFuncl_btag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_btag"+s+"_genb").c_str());

    //h_Zpt_RpT_btagDeepBloose_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBloose"+s+"_genc").c_str());
    //h_Zpt_RMPF_btagDeepBloose_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBloose"+s+"_genc").c_str());
    //h_Zpt_RpT_btagDeepBmedium_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBmedium"+s+"_genc").c_str());
    //h_Zpt_RMPF_btagDeepBmedium_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBmedium"+s+"_genc").c_str());
    h_Zpt_RpT_btag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btag"+s+"_genc").c_str());
    h_Zpt_RMPF_btag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btag"+s+"_genc").c_str());
    h_Zpt_RMPFjet1_btag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_btag"+s+"_genc").c_str());
    h_Zpt_RMPFjetn_btag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_btag"+s+"_genc").c_str());
    h_Zpt_RMPFuncl_btag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_btag"+s+"_genc").c_str());

    //h_Zpt_RpT_btagDeepBloose_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBloose"+s+"_genuds").c_str());
    //h_Zpt_RMPF_btagDeepBloose_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBloose"+s+"_genuds").c_str());
    //h_Zpt_RpT_btagDeepBmedium_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBmedium"+s+"_genuds").c_str());
    //h_Zpt_RMPF_btagDeepBmedium_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBmedium"+s+"_genuds").c_str());
    h_Zpt_RpT_btag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btag"+s+"_genuds").c_str());
    h_Zpt_RMPF_btag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btag"+s+"_genuds").c_str());
    h_Zpt_RMPFjet1_btag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_btag"+s+"_genuds").c_str());
    h_Zpt_RMPFjetn_btag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_btag"+s+"_genuds").c_str());
    h_Zpt_RMPFuncl_btag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_btag"+s+"_genuds").c_str());

    h_Zpt_RpT_btag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btag"+s+"_gens").c_str());
    h_Zpt_RMPF_btag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btag"+s+"_gens").c_str());
    h_Zpt_RMPFjet1_btag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_btag"+s+"_gens").c_str());
    h_Zpt_RMPFjetn_btag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_btag"+s+"_gens").c_str());
    h_Zpt_RMPFuncl_btag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_btag"+s+"_gens").c_str());

    h_Zpt_RpT_btag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btag"+s+"_genud").c_str());
    h_Zpt_RMPF_btag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btag"+s+"_genud").c_str());
    h_Zpt_RMPFjet1_btag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_btag"+s+"_genud").c_str());
    h_Zpt_RMPFjetn_btag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_btag"+s+"_genud").c_str());
    h_Zpt_RMPFuncl_btag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_btag"+s+"_genud").c_str());

    //h_Zpt_RpT_btagDeepBloose_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBloose"+s+"_geng").c_str());
    //h_Zpt_RMPF_btagDeepBloose_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBloose"+s+"_geng").c_str());
    //h_Zpt_RpT_btagDeepBmedium_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBmedium"+s+"_geng").c_str());
    //h_Zpt_RMPF_btagDeepBmedium_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBmedium"+s+"_geng").c_str());
    h_Zpt_RpT_btag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btag"+s+"_geng").c_str());
    h_Zpt_RMPF_btag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btag"+s+"_geng").c_str());
    h_Zpt_RMPFjet1_btag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_btag"+s+"_geng").c_str());
    h_Zpt_RMPFjetn_btag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_btag"+s+"_geng").c_str());
    h_Zpt_RMPFuncl_btag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_btag"+s+"_geng").c_str());

    h_Zpt_RpT_btag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btag"+s+"_unclassified").c_str());
    h_Zpt_RMPF_btag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btag"+s+"_unclassified").c_str());
    h_Zpt_RMPFjet1_btag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_btag"+s+"_unclassified").c_str());
    h_Zpt_RMPFjetn_btag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_btag"+s+"_unclassified").c_str());
    h_Zpt_RMPFuncl_btag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_btag"+s+"_unclassified").c_str());
    }
    
    //h_Zpt_RpT_btagDeepCloose[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCloose"+s).c_str());
    //h_Zpt_RMPF_btagDeepCloose[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCloose"+s).c_str());
    //h_Zpt_RpT_btagDeepCmedium[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCmedium"+s).c_str());
    //h_Zpt_RMPF_btagDeepCmedium[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCmedium"+s).c_str());
    h_Zpt_RpT_ctag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_ctag"+s).c_str());
    h_Zpt_RMPF_ctag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_ctag"+s).c_str());
    h_Zpt_RMPFjet1_ctag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_ctag"+s).c_str());
    h_Zpt_RMPFjetn_ctag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_ctag"+s).c_str());
    h_Zpt_RMPFuncl_ctag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_ctag"+s).c_str());

    h_JetPt_QGL_ctag[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_ctag"+s).c_str());
    if(!isData){
    h_JetPt_QGL_ctag_genb[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_ctag"+s+"_genb").c_str());
    h_JetPt_QGL_ctag_genc[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_ctag"+s+"_genc").c_str());
    h_JetPt_QGL_ctag_genuds[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_ctag"+s+"_genuds").c_str());
    h_JetPt_QGL_ctag_geng[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_ctag"+s+"_geng").c_str());
    h_JetPt_QGL_ctag_unclassified[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_ctag"+s+"_unclassified").c_str());
    }
    h_Zpt_QGL_ctag[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_ctag"+s).c_str());
    if(!isData){
    h_Zpt_QGL_ctag_genb[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_ctag"+s+"_genb").c_str());
    h_Zpt_QGL_ctag_genc[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_ctag"+s+"_genc").c_str());
    h_Zpt_QGL_ctag_genuds[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_ctag"+s+"_genuds").c_str());
    h_Zpt_QGL_ctag_geng[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_ctag"+s+"_geng").c_str());
    h_Zpt_QGL_ctag_unclassified[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_ctag"+s+"_unclassified").c_str());
    }
    h_JetPt_muEF_ctag[i] = (TH2D*)h_JetPt_muEF_template->Clone(("h_JetPt_muEF_ctag"+s).c_str());
    h_JetPt_chEmEF_ctag[i] = (TH2D*)h_JetPt_chEmEF_template->Clone(("h_JetPt_chEmEF_ctag"+s).c_str());
    h_JetPt_chHEF_ctag[i] = (TH2D*)h_JetPt_chHEF_template->Clone(("h_JetPt_chHEF_ctag"+s).c_str());
    h_JetPt_neEmEF_ctag[i] = (TH2D*)h_JetPt_neEmEF_template->Clone(("h_JetPt_neEmEF_ctag"+s).c_str());
    h_JetPt_neHEF_ctag[i] = (TH2D*)h_JetPt_neHEF_template->Clone(("h_JetPt_neHEF_ctag"+s).c_str());

    h_PtAve_QGL_ctag[i] = (TH2D*)h_PtAve_QGL_template->Clone(("h_PtAve_QGL_ctag"+s).c_str());
    h_PtAve_muEF_ctag[i] = (TH2D*)h_PtAve_chEmEF_template->Clone(("h_PtAve_muEF_ctag"+s).c_str());
    h_PtAve_chEmEF_ctag[i] = (TH2D*)h_PtAve_chEmEF_template->Clone(("h_PtAve_chEmEF_ctag"+s).c_str());
    h_PtAve_chHEF_ctag[i] = (TH2D*)h_PtAve_chHEF_template->Clone(("h_PtAve_chHEF_ctag"+s).c_str());
    h_PtAve_neEmEF_ctag[i] = (TH2D*)h_PtAve_neEmEF_template->Clone(("h_PtAve_neEmEF_ctag"+s).c_str());
    h_PtAve_neHEF_ctag[i] = (TH2D*)h_PtAve_neHEF_template->Clone(("h_PtAve_neHEF_ctag"+s).c_str());

    h_Zpt_Mu_Rho_ctag[i] = (TProfile2D*)h_Zpt_Mu_Rho_template->Clone(("h3D_Zpt_Mu_Rho_ctag"+s).c_str());
    h_Zpt_Mu_NPVgood_ctag[i] = (TProfile2D*)h_Zpt_Mu_NPVgood_template->Clone(("h3D_Zpt_Mu_NPVgood_ctag"+s).c_str());
    h_Zpt_Mu_NPVall_ctag[i] = (TProfile2D*)h_Zpt_Mu_NPVall_template->Clone(("h3D_Zpt_Mu_NPVall_ctag"+s).c_str());
    h_Zpt_Mu_Mu_ctag[i] = (TProfile2D*)h_Zpt_Mu_Mu_template->Clone(("h3D_Zpt_Mu_Mu_ctag"+s).c_str());
    h_Mu_RpT_ctag[i] = (TH2D*)h_Mu_RpT_template->Clone(("h_Mu_RpT_ctag"+s).c_str());
    h_JetPt_RpT_ctag[i] = (TH2D*)h_JetPt_RpT_template->Clone(("h_JetPt_RpT_ctag"+s).c_str());
    h_PtAve_RpT_ctag[i] = (TH2D*)h_PtAve_RpT_template->Clone(("h_PtAve_RpT_ctag"+s).c_str());
    h_Mu_RMPF_ctag[i] = (TH2D*)h_Mu_RMPF_template->Clone(("h_Mu_RMPF_ctag"+s).c_str());
    h_JetPt_RMPF_ctag[i] = (TH2D*)h_JetPt_RMPF_template->Clone(("h_JetPt_RMPF_ctag"+s).c_str());
    h_PtAve_RMPF_ctag[i] = (TH2D*)h_PtAve_RMPF_template->Clone(("h_PtAve_RMPF_ctag"+s).c_str());
    h_Mu_RMPFjet1_ctag[i] = (TH2D*)h_Mu_RMPFjet1_template->Clone(("h_Mu_RMPFjet1_ctag"+s).c_str());
    h_JetPt_RMPFjet1_ctag[i] = (TH2D*)h_JetPt_RMPFjet1_template->Clone(("h_JetPt_RMPFjet1_ctag"+s).c_str());
    h_PtAve_RMPFjet1_ctag[i] = (TH2D*)h_PtAve_RMPFjet1_template->Clone(("h_PtAve_RMPFjet1_ctag"+s).c_str());
    h_Mu_RMPFjetn_ctag[i] = (TH2D*)h_Mu_RMPFjetn_template->Clone(("h_Mu_RMPFjetn_ctag"+s).c_str());
    h_JetPt_RMPFjetn_ctag[i] = (TH2D*)h_JetPt_RMPFjetn_template->Clone(("h_JetPt_RMPFjetn_ctag"+s).c_str());
    h_PtAve_RMPFjetn_ctag[i] = (TH2D*)h_PtAve_RMPFjetn_template->Clone(("h_PtAve_RMPFjetn_ctag"+s).c_str());
    h_Mu_RMPFuncl_ctag[i] = (TH2D*)h_Mu_RMPFuncl_template->Clone(("h_Mu_RMPFuncl_ctag"+s).c_str());
    h_JetPt_RMPFuncl_ctag[i] = (TH2D*)h_JetPt_RMPFuncl_template->Clone(("h_JetPt_RMPFuncl_ctag"+s).c_str());
    h_PtAve_RMPFuncl_ctag[i] = (TH2D*)h_PtAve_RMPFuncl_template->Clone(("h_PtAve_RMPFuncl_ctag"+s).c_str());
    if(!isData){
    //h_Zpt_RpT_btagDeepCloose_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCloose"+s+"_genb").c_str());
    //h_Zpt_RMPF_btagDeepCloose_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCloose"+s+"_genb").c_str());
    //h_Zpt_RpT_btagDeepCmedium_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCmedium"+s+"_genb").c_str());
    //h_Zpt_RMPF_btagDeepCmedium_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCmedium"+s+"_genb").c_str());
    h_Zpt_RpT_ctag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_ctag"+s+"_genb").c_str());
    h_Zpt_RMPF_ctag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_ctag"+s+"_genb").c_str());
    h_Zpt_RMPFjet1_ctag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_ctag"+s+"_genb").c_str());
    h_Zpt_RMPFjetn_ctag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_ctag"+s+"_genb").c_str());
    h_Zpt_RMPFuncl_ctag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_ctag"+s+"_genb").c_str());

    //h_Zpt_RpT_btagDeepCloose_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCloose"+s+"_genc").c_str());
    //h_Zpt_RMPF_btagDeepCloose_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCloose"+s+"_genc").c_str());
    //h_Zpt_RpT_btagDeepCmedium_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCmedium"+s+"_genc").c_str());
    //h_Zpt_RMPF_btagDeepCmedium_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCmedium"+s+"_genc").c_str());
    h_Zpt_RpT_ctag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_ctag"+s+"_genc").c_str());
    h_Zpt_RMPF_ctag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_ctag"+s+"_genc").c_str());
    h_Zpt_RMPFjet1_ctag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_ctag"+s+"_genc").c_str());
    h_Zpt_RMPFjetn_ctag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_ctag"+s+"_genc").c_str());
    h_Zpt_RMPFuncl_ctag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_ctag"+s+"_genc").c_str());

    //h_Zpt_RpT_btagDeepCloose_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCloose"+s+"_genuds").c_str());
    //h_Zpt_RMPF_btagDeepCloose_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCloose"+s+"_genuds").c_str());
    //h_Zpt_RpT_btagDeepCmedium_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCmedium"+s+"_genuds").c_str());
    //h_Zpt_RMPF_btagDeepCmedium_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCmedium"+s+"_genuds").c_str());
    h_Zpt_RpT_ctag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_ctag"+s+"_genuds").c_str());
    h_Zpt_RMPF_ctag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_ctag"+s+"_genuds").c_str());
    h_Zpt_RMPFjet1_ctag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_ctag"+s+"_genuds").c_str());
    h_Zpt_RMPFjetn_ctag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_ctag"+s+"_genuds").c_str());
    h_Zpt_RMPFuncl_ctag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_ctag"+s+"_genuds").c_str());

    h_Zpt_RpT_ctag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_ctag"+s+"_gens").c_str());
    h_Zpt_RMPF_ctag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_ctag"+s+"_gens").c_str());
    h_Zpt_RMPFjet1_ctag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_ctag"+s+"_gens").c_str());
    h_Zpt_RMPFjetn_ctag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_ctag"+s+"_gens").c_str());
    h_Zpt_RMPFuncl_ctag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_ctag"+s+"_gens").c_str());

    h_Zpt_RpT_ctag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_ctag"+s+"_genud").c_str());
    h_Zpt_RMPF_ctag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_ctag"+s+"_genud").c_str());
    h_Zpt_RMPFjet1_ctag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_ctag"+s+"_genud").c_str());
    h_Zpt_RMPFjetn_ctag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_ctag"+s+"_genud").c_str());
    h_Zpt_RMPFuncl_ctag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_ctag"+s+"_genud").c_str());

    //h_Zpt_RpT_btagDeepCloose_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCloose"+s+"_geng").c_str());
    //h_Zpt_RMPF_btagDeepCloose_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCloose"+s+"_geng").c_str());
    //h_Zpt_RpT_btagDeepCmedium_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCmedium"+s+"_geng").c_str());
    //h_Zpt_RMPF_btagDeepCmedium_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCmedium"+s+"_geng").c_str());
    h_Zpt_RpT_ctag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_ctag"+s+"_geng").c_str());
    h_Zpt_RMPF_ctag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_ctag"+s+"_geng").c_str());
    h_Zpt_RMPFjet1_ctag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_ctag"+s+"_geng").c_str());
    h_Zpt_RMPFjetn_ctag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_ctag"+s+"_geng").c_str());
    h_Zpt_RMPFuncl_ctag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_ctag"+s+"_geng").c_str());

    h_Zpt_RpT_ctag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_ctag"+s+"_unclassified").c_str());
    h_Zpt_RMPF_ctag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_ctag"+s+"_unclassified").c_str());
    h_Zpt_RMPFjet1_ctag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_ctag"+s+"_unclassified").c_str());
    h_Zpt_RMPFjetn_ctag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_ctag"+s+"_unclassified").c_str());
    h_Zpt_RMPFuncl_ctag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_ctag"+s+"_unclassified").c_str());
    }
    /*
    //h_Zpt_RpT_btagDeepFlavBloose[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBloose"+s).c_str());
    //h_Zpt_RMPF_btagDeepFlavBloose[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBloose"+s).c_str());
    //h_Zpt_RpT_btagDeepFlavBmedium[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBmedium"+s).c_str());
    //h_Zpt_RMPF_btagDeepFlavBmedium[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBmedium"+s).c_str());
    h_Zpt_RpT_btagDeepFlavBtight[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBtight"+s).c_str());
    h_Zpt_RMPF_btagDeepFlavBtight[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBtight"+s).c_str());

    //h_Zpt_RpT_btagDeepFlavBloose_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBloose"+s+"_genb").c_str());
    //h_Zpt_RMPF_btagDeepFlavBloose_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBloose"+s+"_genb").c_str());
    //h_Zpt_RpT_btagDeepFlavBmedium_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBmedium"+s+"_genb").c_str());
    //h_Zpt_RMPF_btagDeepFlavBmedium_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBmedium"+s+"_genb").c_str());
    h_Zpt_RpT_btagDeepFlavBtight_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBtight"+s+"_genb").c_str());
    h_Zpt_RMPF_btagDeepFlavBtight_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBtight"+s+"_genb").c_str());

    //h_Zpt_RpT_btagDeepFlavBloose_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBloose"+s+"_genc").c_str());
    //h_Zpt_RMPF_btagDeepFlavBloose_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBloose"+s+"_genc").c_str());
    //h_Zpt_RpT_btagDeepFlavBmedium_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBmedium"+s+"_genc").c_str());
    //h_Zpt_RMPF_btagDeepFlavBmedium_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBmedium"+s+"_genc").c_str());
    h_Zpt_RpT_btagDeepFlavBtight_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBtight"+s+"_genc").c_str());
    h_Zpt_RMPF_btagDeepFlavBtight_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBtight"+s+"_genc").c_str());

    //h_Zpt_RpT_btagDeepFlavBloose_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBloose"+s+"_genuds").c_str());
    //h_Zpt_RMPF_btagDeepFlavBloose_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBloose"+s+"_genuds").c_str());
    //h_Zpt_RpT_btagDeepFlavBmedium_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBmedium"+s+"_genuds").c_str());
    //h_Zpt_RMPF_btagDeepFlavBmedium_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBmedium"+s+"_genuds").c_str());
    h_Zpt_RpT_btagDeepFlavBtight_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBtight"+s+"_genuds").c_str());
    h_Zpt_RMPF_btagDeepFlavBtight_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBtight"+s+"_genuds").c_str());

    //h_Zpt_RpT_btagDeepFlavBloose_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBloose"+s+"_geng").c_str());
    //h_Zpt_RMPF_btagDeepFlavBloose_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBloose"+s+"_geng").c_str());
    //h_Zpt_RpT_btagDeepFlavBmedium_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBmedium"+s+"_geng").c_str());
    //h_Zpt_RMPF_btagDeepFlavBmedium_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBmedium"+s+"_geng").c_str());
    h_Zpt_RpT_btagDeepFlavBtight_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBtight"+s+"_geng").c_str());
    h_Zpt_RMPF_btagDeepFlavBtight_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBtight"+s+"_geng").c_str());
    */

    h_Zpt_RpT_gluontag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_gluontag"+s).c_str());
    h_Zpt_RMPF_gluontag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_gluontag"+s).c_str());
    h_Zpt_RMPFjet1_gluontag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_gluontag"+s).c_str()); 
    h_Zpt_RMPFjetn_gluontag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_gluontag"+s).c_str()); 
    h_Zpt_RMPFuncl_gluontag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_gluontag"+s).c_str());

    h_JetPt_QGL_gluontag[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_gluontag"+s).c_str());
    if(!isData){
    h_JetPt_QGL_gluontag_genb[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_gluontag"+s+"_genb").c_str());
    h_JetPt_QGL_gluontag_genc[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_gluontag"+s+"_genc").c_str());
    h_JetPt_QGL_gluontag_genuds[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_gluontag"+s+"_genuds").c_str());
    h_JetPt_QGL_gluontag_geng[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_gluontag"+s+"_geng").c_str());
    h_JetPt_QGL_gluontag_unclassified[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_gluontag"+s+"_unclassified").c_str());
    }
    h_Zpt_QGL_gluontag[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_gluontag"+s).c_str());
    if(!isData){
    h_Zpt_QGL_gluontag_genb[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_gluontag"+s+"_genb").c_str());
    h_Zpt_QGL_gluontag_genc[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_gluontag"+s+"_genc").c_str());
    h_Zpt_QGL_gluontag_genuds[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_gluontag"+s+"_genuds").c_str());
    h_Zpt_QGL_gluontag_geng[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_gluontag"+s+"_geng").c_str());
    h_Zpt_QGL_gluontag_unclassified[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_gluontag"+s+"_unclassified").c_str());
    }
    h_JetPt_muEF_gluontag[i] = (TH2D*)h_JetPt_muEF_template->Clone(("h_JetPt_muEF_gluontag"+s).c_str());
    h_JetPt_chEmEF_gluontag[i] = (TH2D*)h_JetPt_chEmEF_template->Clone(("h_JetPt_chEmEF_gluontag"+s).c_str());
    h_JetPt_chHEF_gluontag[i] = (TH2D*)h_JetPt_chHEF_template->Clone(("h_JetPt_chHEF_gluontag"+s).c_str());
    h_JetPt_neEmEF_gluontag[i] = (TH2D*)h_JetPt_neEmEF_template->Clone(("h_JetPt_neEmEF_gluontag"+s).c_str());
    h_JetPt_neHEF_gluontag[i] = (TH2D*)h_JetPt_neHEF_template->Clone(("h_JetPt_neHEF_gluontag"+s).c_str());

    h_PtAve_QGL_gluontag[i] = (TH2D*)h_PtAve_QGL_template->Clone(("h_PtAve_QGL_gluontag"+s).c_str());
    h_PtAve_muEF_gluontag[i] = (TH2D*)h_PtAve_muEF_template->Clone(("h_PtAve_muEF_gluontag"+s).c_str());
    h_PtAve_chEmEF_gluontag[i] = (TH2D*)h_PtAve_chEmEF_template->Clone(("h_PtAve_chEmEF_gluontag"+s).c_str());
    h_PtAve_chHEF_gluontag[i] = (TH2D*)h_PtAve_chHEF_template->Clone(("h_PtAve_chHEF_gluontag"+s).c_str());
    h_PtAve_neEmEF_gluontag[i] = (TH2D*)h_PtAve_neEmEF_template->Clone(("h_PtAve_neEmEF_gluontag"+s).c_str());
    h_PtAve_neHEF_gluontag[i] = (TH2D*)h_PtAve_neHEF_template->Clone(("h_PtAve_neHEF_gluontag"+s).c_str());

    h_Zpt_Mu_Rho_gluontag[i] = (TProfile2D*)h_Zpt_Mu_Rho_template->Clone(("h3D_Zpt_Mu_Rho_gluontag"+s).c_str());
    h_Zpt_Mu_NPVgood_gluontag[i] = (TProfile2D*)h_Zpt_Mu_NPVgood_template->Clone(("h3D_Zpt_Mu_NPVgood_gluontag"+s).c_str());
    h_Zpt_Mu_NPVall_gluontag[i] = (TProfile2D*)h_Zpt_Mu_NPVall_template->Clone(("h3D_Zpt_Mu_NPVall_gluontag"+s).c_str());
    h_Zpt_Mu_Mu_gluontag[i] = (TProfile2D*)h_Zpt_Mu_Mu_template->Clone(("h3D_Zpt_Mu_Mu_gluontag"+s).c_str());
    h_Mu_RpT_gluontag[i] = (TH2D*)h_Mu_RpT_template->Clone(("h_Mu_RpT_gluontag"+s).c_str());
    h_JetPt_RpT_gluontag[i] = (TH2D*)h_JetPt_RpT_template->Clone(("h_JetPt_RpT_gluontag"+s).c_str());
    h_PtAve_RpT_gluontag[i] = (TH2D*)h_PtAve_RpT_template->Clone(("h_PtAve_RpT_gluontag"+s).c_str());
    h_Mu_RMPF_gluontag[i] = (TH2D*)h_Mu_RMPF_template->Clone(("h_Mu_RMPF_gluontag"+s).c_str());
    h_JetPt_RMPF_gluontag[i] = (TH2D*)h_JetPt_RMPF_template->Clone(("h_JetPt_RMPF_gluontag"+s).c_str());
    h_PtAve_RMPF_gluontag[i] = (TH2D*)h_PtAve_RMPF_template->Clone(("h_PtAve_RMPF_gluontag"+s).c_str());
    h_Mu_RMPFjet1_gluontag[i] = (TH2D*)h_Mu_RMPFjet1_template->Clone(("h_Mu_RMPFjet1_gluontag"+s).c_str());
    h_JetPt_RMPFjet1_gluontag[i] = (TH2D*)h_JetPt_RMPFjet1_template->Clone(("h_JetPt_RMPFjet1_gluontag"+s).c_str());
    h_PtAve_RMPFjet1_gluontag[i] = (TH2D*)h_PtAve_RMPFjet1_template->Clone(("h_PtAve_RMPFjet1_gluontag"+s).c_str());
    h_Mu_RMPFjetn_gluontag[i] = (TH2D*)h_Mu_RMPFjetn_template->Clone(("h_Mu_RMPFjetn_gluontag"+s).c_str());
    h_JetPt_RMPFjetn_gluontag[i] = (TH2D*)h_JetPt_RMPFjetn_template->Clone(("h_JetPt_RMPFjetn_gluontag"+s).c_str());
    h_PtAve_RMPFjetn_gluontag[i] = (TH2D*)h_PtAve_RMPFjetn_template->Clone(("h_PtAve_RMPFjetn_gluontag"+s).c_str());
    h_Mu_RMPFuncl_gluontag[i] = (TH2D*)h_Mu_RMPFuncl_template->Clone(("h_Mu_RMPFuncl_gluontag"+s).c_str());
    h_JetPt_RMPFuncl_gluontag[i] = (TH2D*)h_JetPt_RMPFuncl_template->Clone(("h_JetPt_RMPFuncl_gluontag"+s).c_str());
    h_PtAve_RMPFuncl_gluontag[i] = (TH2D*)h_PtAve_RMPFuncl_template->Clone(("h_PtAve_RMPFuncl_gluontag"+s).c_str());
    if(!isData){
    h_Zpt_RpT_gluontag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_gluontag"+s+"_genb").c_str());
    h_Zpt_RMPF_gluontag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_gluontag"+s+"_genb").c_str());
    h_Zpt_RMPFjet1_gluontag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_gluontag"+s+"_genb").c_str());
    h_Zpt_RMPFjetn_gluontag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_gluontag"+s+"_genb").c_str());
    h_Zpt_RMPFuncl_gluontag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_gluontag"+s+"_genb").c_str());

    h_Zpt_RpT_gluontag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_gluontag"+s+"_genc").c_str());
    h_Zpt_RMPF_gluontag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_gluontag"+s+"_genc").c_str());
    h_Zpt_RMPFjet1_gluontag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_gluontag"+s+"_genc").c_str());
    h_Zpt_RMPFjetn_gluontag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_gluontag"+s+"_genc").c_str());
    h_Zpt_RMPFuncl_gluontag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_gluontag"+s+"_genc").c_str());

    h_Zpt_RpT_gluontag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_gluontag"+s+"_genuds").c_str());
    h_Zpt_RMPF_gluontag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_gluontag"+s+"_genuds").c_str());
    h_Zpt_RMPFjet1_gluontag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_gluontag"+s+"_genuds").c_str());
    h_Zpt_RMPFjetn_gluontag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_gluontag"+s+"_genuds").c_str());
    h_Zpt_RMPFuncl_gluontag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_gluontag"+s+"_genuds").c_str());

    h_Zpt_RpT_gluontag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_gluontag"+s+"_gens").c_str());
    h_Zpt_RMPF_gluontag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_gluontag"+s+"_gens").c_str());
    h_Zpt_RMPFjet1_gluontag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_gluontag"+s+"_gens").c_str());
    h_Zpt_RMPFjetn_gluontag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_gluontag"+s+"_gens").c_str());
    h_Zpt_RMPFuncl_gluontag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_gluontag"+s+"_gens").c_str());

    h_Zpt_RpT_gluontag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_gluontag"+s+"_genud").c_str());
    h_Zpt_RMPF_gluontag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_gluontag"+s+"_genud").c_str());
    h_Zpt_RMPFjet1_gluontag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_gluontag"+s+"_genud").c_str());
    h_Zpt_RMPFjetn_gluontag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_gluontag"+s+"_genud").c_str());
    h_Zpt_RMPFuncl_gluontag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_gluontag"+s+"_genud").c_str());

    h_Zpt_RpT_gluontag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_gluontag"+s+"_geng").c_str());
    h_Zpt_RMPF_gluontag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_gluontag"+s+"_geng").c_str());
    h_Zpt_RMPFjet1_gluontag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_gluontag"+s+"_geng").c_str());
    h_Zpt_RMPFjetn_gluontag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_gluontag"+s+"_geng").c_str());
    h_Zpt_RMPFuncl_gluontag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_gluontag"+s+"_geng").c_str());

    h_Zpt_RpT_gluontag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_gluontag"+s+"_unclassified").c_str());
    h_Zpt_RMPF_gluontag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_gluontag"+s+"_unclassified").c_str());
    h_Zpt_RMPFjet1_gluontag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_gluontag"+s+"_unclassified").c_str());
    h_Zpt_RMPFjetn_gluontag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_gluontag"+s+"_unclassified").c_str());
    h_Zpt_RMPFuncl_gluontag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_gluontag"+s+"_unclassified").c_str());
    }

    h_Zpt_RpT_quarktag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s).c_str());
    h_Zpt_RMPF_quarktag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s).c_str());
    h_Zpt_RMPFjet1_quarktag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_quarktag"+s).c_str());
    h_Zpt_RMPFjetn_quarktag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_quarktag"+s).c_str());
    h_Zpt_RMPFuncl_quarktag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_quarktag"+s).c_str());

    h_JetPt_QGL_quarktag[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_quarktag"+s).c_str());
    if(!isData){
    h_JetPt_QGL_quarktag_genb[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_quarktag"+s+"_genb").c_str());
    h_JetPt_QGL_quarktag_genc[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_quarktag"+s+"_genc").c_str());
    h_JetPt_QGL_quarktag_genuds[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_quarktag"+s+"_genuds").c_str());
    h_JetPt_QGL_quarktag_geng[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_quarktag"+s+"_geng").c_str());
    h_JetPt_QGL_quarktag_unclassified[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_quarktag"+s+"_unclassified").c_str());
    }
    h_Zpt_QGL_quarktag[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_quarktag"+s).c_str());
    if(!isData){
    h_Zpt_QGL_quarktag_genb[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_quarktag"+s+"_genb").c_str());
    h_Zpt_QGL_quarktag_genc[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_quarktag"+s+"_genc").c_str());
    h_Zpt_QGL_quarktag_genuds[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_quarktag"+s+"_genuds").c_str());
    h_Zpt_QGL_quarktag_geng[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_quarktag"+s+"_geng").c_str());
    h_Zpt_QGL_quarktag_unclassified[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_quarktag"+s+"_unclassified").c_str());
    }
    h_JetPt_muEF_quarktag[i] = (TH2D*)h_JetPt_muEF_template->Clone(("h_JetPt_muEF_quarktag"+s).c_str());
    h_JetPt_chEmEF_quarktag[i] = (TH2D*)h_JetPt_chEmEF_template->Clone(("h_JetPt_chEmEF_quarktag"+s).c_str());
    h_JetPt_chHEF_quarktag[i] = (TH2D*)h_JetPt_chHEF_template->Clone(("h_JetPt_chHEF_quarktag"+s).c_str());
    h_JetPt_neEmEF_quarktag[i] = (TH2D*)h_JetPt_neEmEF_template->Clone(("h_JetPt_neEmEF_quarktag"+s).c_str());
    h_JetPt_neHEF_quarktag[i] = (TH2D*)h_JetPt_neHEF_template->Clone(("h_JetPt_neHEF_quarktag"+s).c_str());

    h_PtAve_QGL_quarktag[i] = (TH2D*)h_PtAve_QGL_template->Clone(("h_PtAve_QGL_quarktag"+s).c_str());
    h_PtAve_muEF_quarktag[i] = (TH2D*)h_PtAve_muEF_template->Clone(("h_PtAve_muEF_quarktag"+s).c_str());
    h_PtAve_chEmEF_quarktag[i] = (TH2D*)h_PtAve_chEmEF_template->Clone(("h_PtAve_chEmEF_quarktag"+s).c_str());
    h_PtAve_chHEF_quarktag[i] = (TH2D*)h_PtAve_chHEF_template->Clone(("h_PtAve_chHEF_quarktag"+s).c_str());
    h_PtAve_neEmEF_quarktag[i] = (TH2D*)h_PtAve_neEmEF_template->Clone(("h_PtAve_neEmEF_quarktag"+s).c_str());
    h_PtAve_neHEF_quarktag[i] = (TH2D*)h_PtAve_neHEF_template->Clone(("h_PtAve_neHEF_quarktag"+s).c_str());

    h_Zpt_Mu_Rho_quarktag[i] = (TProfile2D*)h_Zpt_Mu_Rho_template->Clone(("h3D_Zpt_Mu_Rho_quarktag"+s).c_str());
    h_Zpt_Mu_NPVgood_quarktag[i] = (TProfile2D*)h_Zpt_Mu_NPVgood_template->Clone(("h3D_Zpt_Mu_NPVgood_quarktag"+s).c_str());
    h_Zpt_Mu_NPVall_quarktag[i] = (TProfile2D*)h_Zpt_Mu_NPVall_template->Clone(("h3D_Zpt_Mu_NPVall_quarktag"+s).c_str());
    h_Zpt_Mu_Mu_quarktag[i] = (TProfile2D*)h_Zpt_Mu_Mu_template->Clone(("h3D_Zpt_Mu_Mu_quarktag"+s).c_str());
    h_Mu_RpT_quarktag[i] = (TH2D*)h_Mu_RpT_template->Clone(("h_Mu_RpT_quarktag"+s).c_str());
    h_JetPt_RpT_quarktag[i] = (TH2D*)h_JetPt_RpT_template->Clone(("h_JetPt_RpT_quarktag"+s).c_str());
    h_PtAve_RpT_quarktag[i] = (TH2D*)h_PtAve_RpT_template->Clone(("h_PtAve_RpT_quarktag"+s).c_str());
    h_Mu_RMPF_quarktag[i] = (TH2D*)h_Mu_RMPF_template->Clone(("h_Mu_RMPF_quarktag"+s).c_str());
    h_JetPt_RMPF_quarktag[i] = (TH2D*)h_JetPt_RMPF_template->Clone(("h_JetPt_RMPF_quarktag"+s).c_str());
    h_PtAve_RMPF_quarktag[i] = (TH2D*)h_PtAve_RMPF_template->Clone(("h_PtAve_RMPF_quarktag"+s).c_str());
    h_Mu_RMPFjet1_quarktag[i] = (TH2D*)h_Mu_RMPFjet1_template->Clone(("h_Mu_RMPFjet1_quarktag"+s).c_str());
    h_JetPt_RMPFjet1_quarktag[i] = (TH2D*)h_JetPt_RMPFjet1_template->Clone(("h_JetPt_RMPFjet1_quarktag"+s).c_str());
    h_PtAve_RMPFjet1_quarktag[i] = (TH2D*)h_PtAve_RMPFjet1_template->Clone(("h_PtAve_RMPFjet1_quarktag"+s).c_str());
    h_Mu_RMPFjetn_quarktag[i] = (TH2D*)h_Mu_RMPFjetn_template->Clone(("h_Mu_RMPFjetn_quarktag"+s).c_str());
    h_JetPt_RMPFjetn_quarktag[i] = (TH2D*)h_JetPt_RMPFjetn_template->Clone(("h_JetPt_RMPFjetn_quarktag"+s).c_str());
    h_PtAve_RMPFjetn_quarktag[i] = (TH2D*)h_PtAve_RMPFjetn_template->Clone(("h_PtAve_RMPFjetn_quarktag"+s).c_str());
    h_Mu_RMPFuncl_quarktag[i] = (TH2D*)h_Mu_RMPFuncl_template->Clone(("h_Mu_RMPFuncl_quarktag"+s).c_str());
    h_JetPt_RMPFuncl_quarktag[i] = (TH2D*)h_JetPt_RMPFuncl_template->Clone(("h_JetPt_RMPFuncl_quarktag"+s).c_str());
    h_PtAve_RMPFuncl_quarktag[i] = (TH2D*)h_PtAve_RMPFuncl_template->Clone(("h_PtAve_RMPFuncl_quarktag"+s).c_str());
    if(!isData){
    h_Zpt_RpT_quarktag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s+"_genb").c_str());
    h_Zpt_RMPF_quarktag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s+"_genb").c_str());
    h_Zpt_RMPFjet1_quarktag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_quarktag"+s+"_genb").c_str());
    h_Zpt_RMPFjetn_quarktag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_quarktag"+s+"_genb").c_str());
    h_Zpt_RMPFuncl_quarktag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_quarktag"+s+"_genb").c_str());

    h_Zpt_RpT_quarktag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s+"_genc").c_str());
    h_Zpt_RMPF_quarktag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s+"_genc").c_str());
    h_Zpt_RMPFjet1_quarktag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_quarktag"+s+"_genc").c_str());
    h_Zpt_RMPFjetn_quarktag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_quarktag"+s+"_genc").c_str());
    h_Zpt_RMPFuncl_quarktag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_quarktag"+s+"_genc").c_str());

    h_Zpt_RpT_quarktag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s+"_genuds").c_str());
    h_Zpt_RMPF_quarktag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s+"_genuds").c_str());
    h_Zpt_RMPFjet1_quarktag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_quarktag"+s+"_genuds").c_str());
    h_Zpt_RMPFjetn_quarktag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_quarktag"+s+"_genuds").c_str());
    h_Zpt_RMPFuncl_quarktag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_quarktag"+s+"_genuds").c_str());

    h_Zpt_RpT_quarktag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s+"_geng").c_str());
    h_Zpt_RMPF_quarktag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s+"_geng").c_str());
    h_Zpt_RMPFjet1_quarktag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_quarktag"+s+"_geng").c_str());
    h_Zpt_RMPFjetn_quarktag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_quarktag"+s+"_geng").c_str());
    h_Zpt_RMPFuncl_quarktag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_quarktag"+s+"_geng").c_str());

    h_Zpt_RpT_quarktag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s+"_gens").c_str());
    h_Zpt_RMPF_quarktag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s+"_gens").c_str());
    h_Zpt_RMPFjet1_quarktag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_quarktag"+s+"_gens").c_str());
    h_Zpt_RMPFjetn_quarktag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_quarktag"+s+"_gens").c_str());
    h_Zpt_RMPFuncl_quarktag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_quarktag"+s+"_gens").c_str());

    h_Zpt_RpT_quarktag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s+"_genud").c_str());
    h_Zpt_RMPF_quarktag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s+"_genud").c_str());
    h_Zpt_RMPFjet1_quarktag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_quarktag"+s+"_genud").c_str());
    h_Zpt_RMPFjetn_quarktag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_quarktag"+s+"_genud").c_str());
    h_Zpt_RMPFuncl_quarktag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_quarktag"+s+"_genud").c_str());

    h_Zpt_RpT_quarktag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s+"_unclassified").c_str());
    h_Zpt_RMPF_quarktag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s+"_unclassified").c_str());
    h_Zpt_RMPFjet1_quarktag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_quarktag"+s+"_unclassified").c_str());
    h_Zpt_RMPFjetn_quarktag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_quarktag"+s+"_unclassified").c_str());
    h_Zpt_RMPFuncl_quarktag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_quarktag"+s+"_unclassified").c_str());
    }
    h_Zpt_RpT_notag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_notag"+s).c_str());
    h_Zpt_RMPF_notag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_notag"+s).c_str());
    h_Zpt_RMPFjet1_notag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_notag"+s).c_str());
    h_Zpt_RMPFjetn_notag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_notag"+s).c_str());
    h_Zpt_RMPFuncl_notag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_notag"+s).c_str());

    h_JetPt_QGL_notag[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_notag"+s).c_str());
    if(!isData){
    h_JetPt_QGL_notag_genb[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_notag"+s+"_genb").c_str());
    h_JetPt_QGL_notag_genc[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_notag"+s+"_genc").c_str());
    h_JetPt_QGL_notag_genuds[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_notag"+s+"_genuds").c_str());
    h_JetPt_QGL_notag_geng[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_notag"+s+"_geng").c_str());
    h_JetPt_QGL_notag_unclassified[i] = (TH2D*)h_JetPt_QGL_template->Clone(("h_JetPt_QGL_notag"+s+"_unclassified").c_str());
    }
    h_Zpt_QGL_notag[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_notag"+s).c_str());
    if(!isData){
    h_Zpt_QGL_notag_genb[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_notag"+s+"_genb").c_str());
    h_Zpt_QGL_notag_genc[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_notag"+s+"_genc").c_str());
    h_Zpt_QGL_notag_genuds[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_notag"+s+"_genuds").c_str());
    h_Zpt_QGL_notag_geng[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_notag"+s+"_geng").c_str());
    h_Zpt_QGL_notag_unclassified[i] = (TH2D*)h_Zpt_QGL_template->Clone(("h_Zpt_QGL_notag"+s+"_unclassified").c_str());
    }
    h_JetPt_muEF_notag[i] = (TH2D*)h_JetPt_muEF_template->Clone(("h_JetPt_muEF_notag"+s).c_str());
    h_JetPt_chEmEF_notag[i] = (TH2D*)h_JetPt_chEmEF_template->Clone(("h_JetPt_chEmEF_notag"+s).c_str());
    h_JetPt_chHEF_notag[i] = (TH2D*)h_JetPt_chHEF_template->Clone(("h_JetPt_chHEF_notag"+s).c_str());
    h_JetPt_neEmEF_notag[i] = (TH2D*)h_JetPt_neEmEF_template->Clone(("h_JetPt_neEmEF_notag"+s).c_str());
    h_JetPt_neHEF_notag[i] = (TH2D*)h_JetPt_neHEF_template->Clone(("h_JetPt_neHEF_notag"+s).c_str());

    h_PtAve_QGL_notag[i] = (TH2D*)h_PtAve_QGL_template->Clone(("h_PtAve_QGL_notag"+s).c_str());
    h_PtAve_muEF_notag[i] = (TH2D*)h_PtAve_muEF_template->Clone(("h_PtAve_muEF_notag"+s).c_str());
    h_PtAve_chEmEF_notag[i] = (TH2D*)h_PtAve_chEmEF_template->Clone(("h_PtAve_chEmEF_notag"+s).c_str());
    h_PtAve_chHEF_notag[i] = (TH2D*)h_PtAve_chHEF_template->Clone(("h_PtAve_chHEF_notag"+s).c_str());
    h_PtAve_neEmEF_notag[i] = (TH2D*)h_PtAve_neEmEF_template->Clone(("h_PtAve_neEmEF_notag"+s).c_str());
    h_PtAve_neHEF_notag[i] = (TH2D*)h_PtAve_neHEF_template->Clone(("h_PtAve_neHEF_notag"+s).c_str());

    h_Zpt_Mu_Rho_notag[i] = (TProfile2D*)h_Zpt_Mu_Rho_template->Clone(("h3D_Zpt_Mu_Rho_notag"+s).c_str());
    h_Zpt_Mu_NPVgood_notag[i] = (TProfile2D*)h_Zpt_Mu_NPVgood_template->Clone(("h3D_Zpt_Mu_NPVgood_notag"+s).c_str());
    h_Zpt_Mu_NPVall_notag[i] = (TProfile2D*)h_Zpt_Mu_NPVall_template->Clone(("h3D_Zpt_Mu_NPVall_notag"+s).c_str());
    h_Zpt_Mu_Mu_notag[i] = (TProfile2D*)h_Zpt_Mu_Mu_template->Clone(("h3D_Zpt_Mu_Mu_notag"+s).c_str());
    h_Mu_RpT_notag[i] = (TH2D*)h_Mu_RpT_template->Clone(("h_Mu_RpT_notag"+s).c_str());
    h_JetPt_RpT_notag[i] = (TH2D*)h_JetPt_RpT_template->Clone(("h_JetPt_RpT_notag"+s).c_str());
    h_PtAve_RpT_notag[i] = (TH2D*)h_PtAve_RpT_template->Clone(("h_PtAve_RpT_notag"+s).c_str());
    h_Mu_RMPF_notag[i] = (TH2D*)h_Mu_RMPF_template->Clone(("h_Mu_RMPF_notag"+s).c_str());
    h_JetPt_RMPF_notag[i] = (TH2D*)h_JetPt_RMPF_template->Clone(("h_JetPt_RMPF_notag"+s).c_str());
    h_PtAve_RMPF_notag[i] = (TH2D*)h_PtAve_RMPF_template->Clone(("h_PtAve_RMPF_notag"+s).c_str());
    h_Mu_RMPFjet1_notag[i] = (TH2D*)h_Mu_RMPFjet1_template->Clone(("h_Mu_RMPFjet1_notag"+s).c_str());
    h_JetPt_RMPFjet1_notag[i] = (TH2D*)h_JetPt_RMPFjet1_template->Clone(("h_JetPt_RMPFjet1_notag"+s).c_str());
    h_PtAve_RMPFjet1_notag[i] = (TH2D*)h_PtAve_RMPFjet1_template->Clone(("h_PtAve_RMPFjet1_notag"+s).c_str());
    h_Mu_RMPFjetn_notag[i] = (TH2D*)h_Mu_RMPFjetn_template->Clone(("h_Mu_RMPFjetn_notag"+s).c_str());
    h_JetPt_RMPFjetn_notag[i] = (TH2D*)h_JetPt_RMPFjetn_template->Clone(("h_JetPt_RMPFjetn_notag"+s).c_str());
    h_PtAve_RMPFjetn_notag[i] = (TH2D*)h_PtAve_RMPFjetn_template->Clone(("h_PtAve_RMPFjetn_notag"+s).c_str());
    h_Mu_RMPFuncl_notag[i] = (TH2D*)h_Mu_RMPFuncl_template->Clone(("h_Mu_RMPFuncl_notag"+s).c_str());
    h_JetPt_RMPFuncl_notag[i] = (TH2D*)h_JetPt_RMPFuncl_template->Clone(("h_JetPt_RMPFuncl_notag"+s).c_str());
    h_PtAve_RMPFuncl_notag[i] = (TH2D*)h_PtAve_RMPFuncl_template->Clone(("h_PtAve_RMPFuncl_notag"+s).c_str());
    if(!isData){
    h_Zpt_RpT_notag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_notag"+s+"_genb").c_str());
    h_Zpt_RMPF_notag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_notag"+s+"_genb").c_str());
    h_Zpt_RMPFjet1_notag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_notag"+s+"_genb").c_str());
    h_Zpt_RMPFjetn_notag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_notag"+s+"_genb").c_str());
    h_Zpt_RMPFuncl_notag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_notag"+s+"_genb").c_str());

    h_Zpt_RpT_notag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_notag"+s+"_genc").c_str());
    h_Zpt_RMPF_notag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_notag"+s+"_genc").c_str());
    h_Zpt_RMPFjet1_notag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_notag"+s+"_genc").c_str());
    h_Zpt_RMPFjetn_notag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_notag"+s+"_genc").c_str());
    h_Zpt_RMPFuncl_notag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_notag"+s+"_genc").c_str());

    h_Zpt_RpT_notag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_notag"+s+"_genuds").c_str());
    h_Zpt_RMPF_notag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_notag"+s+"_genuds").c_str());
    h_Zpt_RMPFjet1_notag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_notag"+s+"_genuds").c_str());
    h_Zpt_RMPFjetn_notag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_notag"+s+"_genuds").c_str());
    h_Zpt_RMPFuncl_notag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_notag"+s+"_genuds").c_str());

    h_Zpt_RpT_notag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_notag"+s+"_gens").c_str());
    h_Zpt_RMPF_notag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_notag"+s+"_gens").c_str());
    h_Zpt_RMPFjet1_notag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_notag"+s+"_gens").c_str());
    h_Zpt_RMPFjetn_notag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_notag"+s+"_gens").c_str());
    h_Zpt_RMPFuncl_notag_gens[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_notag"+s+"_gens").c_str());

    h_Zpt_RpT_notag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_notag"+s+"_genud").c_str());
    h_Zpt_RMPF_notag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_notag"+s+"_genud").c_str());
    h_Zpt_RMPFjet1_notag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_notag"+s+"_genud").c_str());
    h_Zpt_RMPFjetn_notag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_notag"+s+"_genud").c_str());
    h_Zpt_RMPFuncl_notag_genud[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_notag"+s+"_genud").c_str());
    
    h_Zpt_RpT_notag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_notag"+s+"_geng").c_str());
    h_Zpt_RMPF_notag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_notag"+s+"_geng").c_str());
    h_Zpt_RMPFjet1_notag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_notag"+s+"_geng").c_str());
    h_Zpt_RMPFjetn_notag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_notag"+s+"_geng").c_str());
    h_Zpt_RMPFuncl_notag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_notag"+s+"_geng").c_str());

    h_Zpt_RpT_notag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RpT_notag"+s+"_unclassified").c_str());
    h_Zpt_RMPF_notag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_notag"+s+"_unclassified").c_str());
    h_Zpt_RMPFjet1_notag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_notag"+s+"_unclassified").c_str());
    h_Zpt_RMPFjetn_notag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_notag"+s+"_unclassified").c_str());
    h_Zpt_RMPFuncl_notag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_notag"+s+"_unclassified").c_str());
    }    
    
    h_Zpt_RZ[i]          = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RZ"+s).c_str());


    h_Zpt_RBal[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal"+s).c_str());
    h_Zpt_Resp[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp"+s).c_str());
    if(!isData){
    h_Zpt_RGMPF[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF"+s).c_str());
    h_Zpt_RGenjet1[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1"+s).c_str());
    h_Zpt_RGenjetn[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn"+s).c_str());
    h_Zpt_RGenuncl[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl"+s).c_str());
    }
    h_JetPt_RBal[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_JetPt_RBal"+s).c_str());
    if(!isData){
    h_JetPt_RGenjet1[i] = (TH2D*)h_Zpt_R_template->Clone(("h_JetPt_RGenjet1"+s).c_str());
    h_JetPt_RGenjetn[i] = (TH2D*)h_Zpt_R_template->Clone(("h_JetPt_RGenjetn"+s).c_str());
    h_JetPt_RGenuncl[i] = (TH2D*)h_Zpt_R_template->Clone(("h_JetPt_RGenuncl"+s).c_str());
    }
    h_PtAve_RBal[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_PtAve_RBal"+s).c_str());
    if(!isData){
    h_PtAve_RGenjet1[i] = (TH2D*)h_Zpt_R_template->Clone(("h_PtAve_RGenjet1"+s).c_str());
    h_PtAve_RGenjetn[i] = (TH2D*)h_Zpt_R_template->Clone(("h_PtAve_RGenjetn"+s).c_str());
    h_PtAve_RGenuncl[i] = (TH2D*)h_Zpt_R_template->Clone(("h_PtAve_RGenuncl"+s).c_str());
    }
    h_Zpt_RBal_btag[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_btag"+s).c_str());
    h_Zpt_Resp_btag[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_btag"+s).c_str());
    if(!isData){
    h_Zpt_RGMPF_btag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_btag"+s).c_str());
    h_Zpt_RGenjet1_btag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_btag"+s).c_str());
    h_Zpt_RGenjetn_btag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_btag"+s).c_str());
    h_Zpt_RGenuncl_btag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_btag"+s).c_str());
    }
    h_Zpt_RBal_ctag[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_ctag"+s).c_str());
    h_Zpt_Resp_ctag[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_ctag"+s).c_str());
    if(!isData){
    h_Zpt_RGMPF_ctag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_ctag"+s).c_str());
    h_Zpt_RGenjet1_ctag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_ctag"+s).c_str());
    h_Zpt_RGenjetn_ctag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_ctag"+s).c_str());
    h_Zpt_RGenuncl_ctag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_ctag"+s).c_str());
    }
    h_Zpt_RBal_gluontag[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_gluontag"+s).c_str());
    h_Zpt_Resp_gluontag[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_gluontag"+s).c_str());
    if(!isData){
    h_Zpt_RGMPF_gluontag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_gluontag"+s).c_str());
    h_Zpt_RGenjet1_gluontag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_gluontag"+s).c_str());
    h_Zpt_RGenjetn_gluontag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_gluontag"+s).c_str());
    h_Zpt_RGenuncl_gluontag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_gluontag"+s).c_str());
    }
    h_Zpt_RBal_quarktag[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_quarktag"+s).c_str());
    h_Zpt_Resp_quarktag[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_quarktag"+s).c_str());
    if(!isData){
    h_Zpt_RGMPF_quarktag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_quarktag"+s).c_str());
    h_Zpt_RGenjet1_quarktag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_quarktag"+s).c_str());
    h_Zpt_RGenjetn_quarktag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_quarktag"+s).c_str());
    h_Zpt_RGenuncl_quarktag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_quarktag"+s).c_str());
    }
    h_Zpt_RBal_notag[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_notag"+s).c_str());
    h_Zpt_Resp_notag[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_notag"+s).c_str());
    if(!isData){
    h_Zpt_RGMPF_notag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_notag"+s).c_str());
    h_Zpt_RGenjet1_notag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_notag"+s).c_str());
    h_Zpt_RGenjetn_notag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_notag"+s).c_str());
    h_Zpt_RGenuncl_notag[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_notag"+s).c_str());
    }
    if(!isData){
    h_Zpt_RBal_genb[i]   = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal"+s+"_genb").c_str());
    h_Zpt_Resp_genb[i]     = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp"+s+"_genb").c_str());
    h_Zpt_RGMPF_genb[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF"+s+"_genb").c_str());
    h_Zpt_RGenjet1_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1"+s+"_genb").c_str());
    h_Zpt_RGenjetn_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn"+s+"_genb").c_str());
    h_Zpt_RGenuncl_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl"+s+"_genb").c_str());

    h_Zpt_RBal_btag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_btag"+s+"_genb").c_str());
    h_Zpt_Resp_btag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_btag"+s+"_genb").c_str());
    h_Zpt_RGMPF_btag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_btag"+s+"_genb").c_str());
    h_Zpt_RGenjet1_btag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_btag"+s+"_genb").c_str());
    h_Zpt_RGenjetn_btag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_btag"+s+"_genb").c_str());
    h_Zpt_RGenuncl_btag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_btag"+s+"_genb").c_str());

    h_Zpt_RBal_ctag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_ctag"+s+"_genb").c_str());
    h_Zpt_Resp_ctag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_ctag"+s+"_genb").c_str());
    h_Zpt_RGMPF_ctag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_ctag"+s+"_genb").c_str());
    h_Zpt_RGenjet1_ctag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_ctag"+s+"_genb").c_str());
    h_Zpt_RGenjetn_ctag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_ctag"+s+"_genb").c_str());
    h_Zpt_RGenuncl_ctag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_ctag"+s+"_genb").c_str());
   
    h_Zpt_RBal_gluontag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_gluontag"+s+"_genb").c_str());
    h_Zpt_Resp_gluontag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_gluontag"+s+"_genb").c_str());
    h_Zpt_RGMPF_gluontag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_gluontag"+s+"_genb").c_str());
    h_Zpt_RGenjet1_gluontag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_gluontag"+s+"_genb").c_str());
    h_Zpt_RGenjetn_gluontag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_gluontag"+s+"_genb").c_str());
    h_Zpt_RGenuncl_gluontag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_gluontag"+s+"_genb").c_str());

    h_Zpt_RBal_quarktag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_quarktag"+s+"_genb").c_str());
    h_Zpt_Resp_quarktag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_quarktag"+s+"_genb").c_str());
    h_Zpt_RGMPF_quarktag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_quarktag"+s+"_genb").c_str());
    h_Zpt_RGenjet1_quarktag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_quarktag"+s+"_genb").c_str());
    h_Zpt_RGenjetn_quarktag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_quarktag"+s+"_genb").c_str());
    h_Zpt_RGenuncl_quarktag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_quarktag"+s+"_genb").c_str());

    h_Zpt_RBal_notag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_notag"+s+"_genb").c_str());
    h_Zpt_Resp_notag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_notag"+s+"_genb").c_str());
    h_Zpt_RGMPF_notag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_notag"+s+"_genb").c_str());
    h_Zpt_RGenjet1_notag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_notag"+s+"_genb").c_str());
    h_Zpt_RGenjetn_notag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_notag"+s+"_genb").c_str());
    h_Zpt_RGenuncl_notag_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_notag"+s+"_genb").c_str());

    h_Zpt_RBal_genc[i]   = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal"+s+"_genc").c_str());
    h_Zpt_Resp_genc[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp"+s+"_genc").c_str());
    h_Zpt_RGMPF_genc[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF"+s+"_genc").c_str());
    h_Zpt_RGenjet1_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1"+s+"_genc").c_str());
    h_Zpt_RGenjetn_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn"+s+"_genc").c_str());
    h_Zpt_RGenuncl_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl"+s+"_genc").c_str());

    h_Zpt_RBal_btag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_btag"+s+"_genc").c_str());
    h_Zpt_Resp_btag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_btag"+s+"_genc").c_str());
    h_Zpt_RGMPF_btag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_btag"+s+"_genc").c_str());
    h_Zpt_RGenjet1_btag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_btag"+s+"_genc").c_str());
    h_Zpt_RGenjetn_btag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_btag"+s+"_genc").c_str());
    h_Zpt_RGenuncl_btag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_btag"+s+"_genc").c_str());

    h_Zpt_RBal_ctag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_ctag"+s+"_genc").c_str());
    h_Zpt_Resp_ctag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_ctag"+s+"_genc").c_str());
    h_Zpt_RGMPF_ctag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_ctag"+s+"_genc").c_str());
    h_Zpt_RGenjet1_ctag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_ctag"+s+"_genc").c_str());
    h_Zpt_RGenjetn_ctag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_ctag"+s+"_genc").c_str());
    h_Zpt_RGenuncl_ctag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_ctag"+s+"_genc").c_str());

    h_Zpt_RBal_gluontag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_gluontag"+s+"_genc").c_str());
    h_Zpt_Resp_gluontag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_gluontag"+s+"_genc").c_str());
    h_Zpt_RGMPF_gluontag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_gluontag"+s+"_genc").c_str());
    h_Zpt_RGenjet1_gluontag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_gluontag"+s+"_genc").c_str());
    h_Zpt_RGenjetn_gluontag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_gluontag"+s+"_genc").c_str());
    h_Zpt_RGenuncl_gluontag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_gluontag"+s+"_genc").c_str());

    h_Zpt_RBal_quarktag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_quarktag"+s+"_genc").c_str());
    h_Zpt_Resp_quarktag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_quarktag"+s+"_genc").c_str());
    h_Zpt_RGMPF_quarktag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_quarktag"+s+"_genc").c_str());
    h_Zpt_RGenjet1_quarktag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_quarktag"+s+"_genc").c_str());
    h_Zpt_RGenjetn_quarktag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_quarktag"+s+"_genc").c_str());
    h_Zpt_RGenuncl_quarktag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_quarktag"+s+"_genc").c_str());

    h_Zpt_RBal_notag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_notag"+s+"_genc").c_str());
    h_Zpt_Resp_notag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_notag"+s+"_genc").c_str());
    h_Zpt_RGMPF_notag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_notag"+s+"_genc").c_str());
    h_Zpt_RGenjet1_notag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_notag"+s+"_genc").c_str());
    h_Zpt_RGenjetn_notag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_notag"+s+"_genc").c_str());
    h_Zpt_RGenuncl_notag_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_notag"+s+"_genc").c_str());

    h_Zpt_RBal_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal"+s+"_genuds").c_str());
    h_Zpt_Resp_genuds[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp"+s+"_genuds").c_str());
    h_Zpt_RGMPF_genuds[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF"+s+"_genuds").c_str());
    h_Zpt_RGenjet1_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1"+s+"_genuds").c_str());
    h_Zpt_RGenjetn_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn"+s+"_genuds").c_str());
    h_Zpt_RGenuncl_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl"+s+"_genuds").c_str());

    h_Zpt_RBal_btag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_btag"+s+"_genuds").c_str());
    h_Zpt_Resp_btag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_btag"+s+"_genuds").c_str());
    h_Zpt_RGMPF_btag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_btag"+s+"_genuds").c_str());
    h_Zpt_RGenjet1_btag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_btag"+s+"_genuds").c_str());
    h_Zpt_RGenjetn_btag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_btag"+s+"_genuds").c_str());
    h_Zpt_RGenuncl_btag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_btag"+s+"_genuds").c_str());

    h_Zpt_RBal_ctag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_ctag"+s+"_genuds").c_str());
    h_Zpt_Resp_ctag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_ctag"+s+"_genuds").c_str());
    h_Zpt_RGMPF_ctag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_ctag"+s+"_genuds").c_str());
    h_Zpt_RGenjet1_ctag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_ctag"+s+"_genuds").c_str());
    h_Zpt_RGenjetn_ctag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_ctag"+s+"_genuds").c_str());
    h_Zpt_RGenuncl_ctag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_ctag"+s+"_genuds").c_str());

    h_Zpt_RBal_gluontag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_gluontag"+s+"_genuds").c_str());
    h_Zpt_Resp_gluontag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_gluontag"+s+"_genuds").c_str());
    h_Zpt_RGMPF_gluontag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_gluontag"+s+"_genuds").c_str());
    h_Zpt_RGenjet1_gluontag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_gluontag"+s+"_genuds").c_str());
    h_Zpt_RGenjetn_gluontag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_gluontag"+s+"_genuds").c_str());
    h_Zpt_RGenuncl_gluontag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_gluontag"+s+"_genuds").c_str());

    h_Zpt_RBal_quarktag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_quarktag"+s+"_genuds").c_str());
    h_Zpt_Resp_quarktag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_quarktag"+s+"_genuds").c_str());
    h_Zpt_RGMPF_quarktag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_quarktag"+s+"_genuds").c_str());
    h_Zpt_RGenjet1_quarktag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_quarktag"+s+"_genuds").c_str());
    h_Zpt_RGenjetn_quarktag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_quarktag"+s+"_genuds").c_str());
    h_Zpt_RGenuncl_quarktag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_quarktag"+s+"_genuds").c_str());

    h_Zpt_RBal_notag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_notag"+s+"_genuds").c_str());
    h_Zpt_Resp_notag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_notag"+s+"_genuds").c_str());
    h_Zpt_RGMPF_notag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_notag"+s+"_genuds").c_str());
    h_Zpt_RGenjet1_notag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_notag"+s+"_genuds").c_str());
    h_Zpt_RGenjetn_notag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_notag"+s+"_genuds").c_str());
    h_Zpt_RGenuncl_notag_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_notag"+s+"_genuds").c_str());

    h_Zpt_RBal_geng[i]   = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal"+s+"_geng").c_str());
    h_Zpt_Resp_geng[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp"+s+"_geng").c_str());
    h_Zpt_RGMPF_geng[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF"+s+"_geng").c_str());
    h_Zpt_RGenjet1_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1"+s+"_geng").c_str());
    h_Zpt_RGenjetn_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn"+s+"_geng").c_str());
    h_Zpt_RGenuncl_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl"+s+"_geng").c_str());

    h_Zpt_RBal_btag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_btag"+s+"_geng").c_str());
    h_Zpt_Resp_btag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_btag"+s+"_geng").c_str());
    h_Zpt_RGMPF_btag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_btag"+s+"_geng").c_str());
    h_Zpt_RGenjet1_btag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_btag"+s+"_geng").c_str());
    h_Zpt_RGenjetn_btag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_btag"+s+"_geng").c_str());
    h_Zpt_RGenuncl_btag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_btag"+s+"_geng").c_str());

    h_Zpt_RBal_ctag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_ctag"+s+"_geng").c_str());
    h_Zpt_Resp_ctag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_ctag"+s+"_geng").c_str());
    h_Zpt_RGMPF_ctag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_ctag"+s+"_geng").c_str());
    h_Zpt_RGenjet1_ctag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_ctag"+s+"_geng").c_str());
    h_Zpt_RGenjetn_ctag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_ctag"+s+"_geng").c_str());
    h_Zpt_RGenuncl_ctag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_ctag"+s+"_geng").c_str());

    h_Zpt_RBal_gluontag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_gluontag"+s+"_geng").c_str());
    h_Zpt_Resp_gluontag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_gluontag"+s+"_geng").c_str());
    h_Zpt_RGMPF_gluontag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_gluontag"+s+"_geng").c_str());
    h_Zpt_RGenjet1_gluontag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_gluontag"+s+"_geng").c_str());
    h_Zpt_RGenjetn_gluontag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_gluontag"+s+"_geng").c_str());
    h_Zpt_RGenuncl_gluontag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_gluontag"+s+"_geng").c_str());

    h_Zpt_RBal_quarktag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_quarktag"+s+"_geng").c_str());
    h_Zpt_Resp_quarktag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_quarktag"+s+"_geng").c_str());
    h_Zpt_RGMPF_quarktag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_quarktag"+s+"_geng").c_str());
    h_Zpt_RGenjet1_quarktag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_quarktag"+s+"_geng").c_str());
    h_Zpt_RGenjetn_quarktag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_quarktag"+s+"_geng").c_str());
    h_Zpt_RGenuncl_quarktag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_quarktag"+s+"_geng").c_str());

    h_Zpt_RBal_notag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_notag"+s+"_geng").c_str());
    h_Zpt_Resp_notag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_notag"+s+"_geng").c_str());
    h_Zpt_RGMPF_notag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_notag"+s+"_geng").c_str());
    h_Zpt_RGenjet1_notag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_notag"+s+"_geng").c_str());
    h_Zpt_RGenjetn_notag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_notag"+s+"_geng").c_str());
    h_Zpt_RGenuncl_notag_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_notag"+s+"_geng").c_str());

    h_Zpt_RBal_unclassified[i]   = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal"+s+"_unclassified").c_str());
    h_Zpt_Resp_unclassified[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp"+s+"_unclassified").c_str());
    h_Zpt_RGMPF_unclassified[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF"+s+"_unclassified").c_str());
    h_Zpt_RGenjet1_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1"+s+"_unclassified").c_str());
    h_Zpt_RGenjetn_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn"+s+"_unclassified").c_str());
    h_Zpt_RGenuncl_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl"+s+"_unclassified").c_str());

    h_Zpt_RBal_btag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_btag"+s+"_unclassified").c_str());
    h_Zpt_Resp_btag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_btag"+s+"_unclassified").c_str());
    h_Zpt_RGMPF_btag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_btag"+s+"_unclassified").c_str());
    h_Zpt_RGenjet1_btag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_btag"+s+"_unclassified").c_str());
    h_Zpt_RGenjetn_btag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_btag"+s+"_unclassified").c_str());
    h_Zpt_RGenuncl_btag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_btag"+s+"_unclassified").c_str());

    h_Zpt_RBal_ctag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_ctag"+s+"_unclassified").c_str());
    h_Zpt_Resp_ctag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_ctag"+s+"_unclassified").c_str());
    h_Zpt_RGMPF_ctag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_ctag"+s+"_unclassified").c_str());
    h_Zpt_RGenjet1_ctag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_ctag"+s+"_unclassified").c_str());
    h_Zpt_RGenjetn_ctag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_ctag"+s+"_unclassified").c_str());
    h_Zpt_RGenuncl_ctag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_ctag"+s+"_unclassified").c_str());

    h_Zpt_RBal_gluontag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_gluontag"+s+"_unclassified").c_str());
    h_Zpt_Resp_gluontag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_gluontag"+s+"_unclassified").c_str());
    h_Zpt_RGMPF_gluontag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_gluontag"+s+"_unclassified").c_str());
    h_Zpt_RGenjet1_gluontag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_gluontag"+s+"_unclassified").c_str());
    h_Zpt_RGenjetn_gluontag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_gluontag"+s+"_unclassified").c_str());
    h_Zpt_RGenuncl_gluontag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_gluontag"+s+"_unclassified").c_str());

    h_Zpt_RBal_quarktag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_quarktag"+s+"_unclassified").c_str());
    h_Zpt_Resp_quarktag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_quarktag"+s+"_unclassified").c_str());
    h_Zpt_RGMPF_quarktag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_quarktag"+s+"_unclassified").c_str());
    h_Zpt_RGenjet1_quarktag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_quarktag"+s+"_unclassified").c_str());
    h_Zpt_RGenjetn_quarktag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_quarktag"+s+"_unclassified").c_str());
    h_Zpt_RGenuncl_quarktag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_quarktag"+s+"_unclassified").c_str());

    h_Zpt_RBal_notag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RBal_notag"+s+"_unclassified").c_str());
    h_Zpt_Resp_notag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_Resp_notag"+s+"_unclassified").c_str());
    h_Zpt_RGMPF_notag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF_notag"+s+"_unclassified").c_str());
    h_Zpt_RGenjet1_notag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1_notag"+s+"_unclassified").c_str());
    h_Zpt_RGenjetn_notag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn_notag"+s+"_unclassified").c_str());
    h_Zpt_RGenuncl_notag_unclassified[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl_notag"+s+"_unclassified").c_str());
    }
    /*    
    h_Zpt_RGMPF_genb[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF"+s+"_genb").c_str());
    h_Zpt_RGenjet1_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1"+s+"_genb").c_str());
    h_Zpt_RGenjetn_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn"+s+"_genb").c_str());
    h_Zpt_RGenuncl_genb[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl"+s+"_genb").c_str());
    h_Zpt_RJet1_genb[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RJet1"+s+"_genb").c_str());
    h_Zpt_RZ_genb[i]       = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RZ"+s+"_genb").c_str());

    h_Zpt_RGMPF_genc[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF"+s+"_genc").c_str());
    h_Zpt_RGenjet1_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1"+s+"_genc").c_str());
    h_Zpt_RGenjetn_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn"+s+"_genc").c_str());
    h_Zpt_RGenuncl_genc[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl"+s+"_genc").c_str());
    h_Zpt_RJet1_genc[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RJet1"+s+"_genc").c_str());
    h_Zpt_RZ_genc[i]       = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RZ"+s+"_genc").c_str());

    h_Zpt_RGMPF_genuds[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF"+s+"_genuds").c_str());
    h_Zpt_RGenjet1_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1"+s+"_genuds").c_str());
    h_Zpt_RGenjetn_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn"+s+"_genuds").c_str());
    h_Zpt_RGenuncl_genuds[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl"+s+"_genuds").c_str());
    h_Zpt_RJet1_genuds[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RJet1"+s+"_genuds").c_str());
    h_Zpt_RZ_genuds[i]       = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RZ"+s+"_genuds").c_str());

    h_Zpt_RGMPF_geng[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGMPF"+s+"_geng").c_str());
    h_Zpt_RGenjet1_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjet1"+s+"_geng").c_str());
    h_Zpt_RGenjetn_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenjetn"+s+"_geng").c_str());
    h_Zpt_RGenuncl_geng[i] = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RGenuncl"+s+"_geng").c_str());
    h_Zpt_RJet1_geng[i]    = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RJet1"+s+"_geng").c_str());
    h_Zpt_RZ_geng[i]       = (TH2D*)h_Zpt_R_template->Clone(("h_Zpt_RZ"+s+"_geng").c_str());
    */
    if(!isData){
    h_pTgen_Resp[i]                       = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp"+s).c_str());
    h_pTgen_Resp_btag[i]        = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_btag"+s).c_str());
    h_pTgen_Resp_ctag[i]        = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_ctag"+s).c_str());
    h_pTgen_Resp_gluontag[i]              = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_gluontag"+s).c_str());
    h_pTgen_Resp_quarktag[i]              = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_quarktag"+s).c_str());
    h_pTgen_Resp_notag[i]              = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_notag"+s).c_str());
    
    h_pTgen_Resp_genb[i]                  = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp"+s+"_genb").c_str());
    h_pTgen_Resp_btag_genb[i]   = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_btag"+s+"_genb").c_str());
    h_pTgen_Resp_ctag_genb[i]   = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_ctag"+s+"_genb").c_str());
    h_pTgen_Resp_gluontag_genb[i]         = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_gluontag"+s+"_genb").c_str());
    h_pTgen_Resp_quarktag_genb[i]         = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_quarktag"+s+"_genb").c_str());
    h_pTgen_Resp_notag_genb[i]         = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_notag"+s+"_genb").c_str());

    h_pTgen_Resp_genc[i]                  = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp"+s+"_genc").c_str());
    h_pTgen_Resp_btag_genc[i]   = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_btag"+s+"_genc").c_str());
    h_pTgen_Resp_ctag_genc[i]   = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_ctag"+s+"_genc").c_str());
    h_pTgen_Resp_gluontag_genc[i]         = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_gluontag"+s+"_genc").c_str());
    h_pTgen_Resp_quarktag_genc[i]         = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_quarktag"+s+"_genc").c_str());
    h_pTgen_Resp_notag_genc[i]         = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_notag"+s+"_genc").c_str());

    h_pTgen_Resp_genuds[i]                = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp"+s+"_genuds").c_str());
    h_pTgen_Resp_btag_genuds[i] = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_btag"+s+"_genuds").c_str());
    h_pTgen_Resp_ctag_genuds[i] = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_ctag"+s+"_genuds").c_str());
    h_pTgen_Resp_gluontag_genuds[i]       = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_gluontag"+s+"_genuds").c_str());
    h_pTgen_Resp_quarktag_genuds[i]       = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_quarktag"+s+"_genuds").c_str());
    h_pTgen_Resp_notag_genuds[i]       = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_notag"+s+"_genuds").c_str());

    h_pTgen_Resp_geng[i]                  = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp"+s+"_geng").c_str());
    h_pTgen_Resp_btag_geng[i]   = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_btag"+s+"_geng").c_str());
    h_pTgen_Resp_ctag_geng[i]   = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_ctag"+s+"_geng").c_str());
    h_pTgen_Resp_gluontag_geng[i]         = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_gluontag"+s+"_geng").c_str());
    h_pTgen_Resp_quarktag_geng[i]         = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_quarktag"+s+"_geng").c_str());
    h_pTgen_Resp_notag_geng[i]         = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_notag"+s+"_geng").c_str());

    h_pTgen_Resp_unclassified[i]                  = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp"+s+"_unclassified").c_str());
    h_pTgen_Resp_btag_unclassified[i]   = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_btag"+s+"_unclassified").c_str());
    h_pTgen_Resp_ctag_unclassified[i]   = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_ctag"+s+"_unclassified").c_str());
    h_pTgen_Resp_gluontag_unclassified[i]         = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_gluontag"+s+"_unclassified").c_str());
    h_pTgen_Resp_quarktag_unclassified[i]         = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_quarktag"+s+"_unclassified").c_str());
    h_pTgen_Resp_notag_unclassified[i]         = (TH2D*)h_pTgen_R_template->Clone(("h_pTgen_Resp_notag"+s+"_unclassified").c_str());
    }
  }
  for(size_t i = 0; i < nbins3D; ++i){
    //nbins = nbins_alpha*npsWeights;
    size_t ialpha = i%nbins_alpha;
    //    size_t ieta = i%(nbins_alpha*bins_eta.size())/nbins_alpha;
    size_t ipsw = i/nbins_alpha;

    std::string s_alpha = std::string("_alpha")+std::to_string(int(100*bins_alpha[ialpha]));
    //    std::string s_eta = std::string("_eta_")+std::regex_replace(bins_eta[ieta], std::regex("\\."), "");
    //std::string s_psw = "";
    //if(ipsw > 0) s_psw = "/PSWeight"+std::to_string(ipsw-1)+"/";
    fOUT->cd();
    fOUT->cd("analysis");
    /*
    if(ipsw > 0){
      std::string s_psw = "analysis/PSWeight"+std::to_string(ipsw-1);

      if(!fOUT->cd(s_psw.c_str())) {
        fOUT->mkdir(s_psw.c_str());
      }
      fOUT->cd(s_psw.c_str());
    }
    */
    std::string s = s_alpha;
    //std::cout << "check s " << i << " " << s << std::endl;
    h3D_Zpt_RMPF_mu[i] = (TProfile2D*)h3D_Zpt_R_mu_template->Clone(("h3D_Zpt_RMPF_mu"+s).c_str());
    h3D_Zpt_RMPFx_mu[i] = (TProfile2D*)h3D_Zpt_R_mu_template->Clone(("h3D_Zpt_RMPFx_mu"+s).c_str());
    h3D_Zpt_RMPF_eta[i] = (TH3D*)h3D_Zpt_R_eta_template->Clone(("h3D_Zpt_RMPF_eta"+s).c_str());
    h3D_Zpt_RMPFx_eta[i] = (TH3D*)h3D_Zpt_R_eta_template->Clone(("h3D_Zpt_RMPFx_eta"+s).c_str());

    tprof2D_eta_Zpt_RMPF[i] = (TProfile2D*)tprof2D_eta_Zpt_RMPF_template->Clone(("p2D_eta_Zpt_RMPF"+s).c_str());
    tprof2D_eta_Zpt_RMPFx[i] = (TProfile2D*)tprof2D_eta_Zpt_RMPF_template->Clone(("p2D_eta_Zpt_RMPFx"+s).c_str());
  }

  //std::cout << "check1" << std::endl;
  cMain->book("pileup<=100");
  cMain->book("all events");
  cMain->book("lumimask");
  cMain->book("trigger");
  cMain->book("MET cleaning");
  cMain->book("2-3 leptons");
  cMain->book("2-3 e or 2-3 mu");
  cMain->book("lepton charge");
  cMain->book("Z boson");
  cMain->book("lepton pt cuts");
  cMain->book("jets");
  cMain->book("leading jet");
  cMain->book("jetvetomap");
  cMain->book("phiBB");
  cMain->book("leading jet jetId");
  cMain->book("  0-jet == muon");
  cMain->book("  leading jet == 0-jet");
  //  cMain->book("subleading jet");
  cMain->book("  alpha0.3");
  cMain->book("  alpha1.0");
  cMain->book("alpha");
  cMain->book("RpT");
  cMain->book("RpT+MPF cuts");
  //cMain->book("Plotting");
  cMain->book("btagCSVV2loose");
  cMain->book("btagCSVV2medium");
  cMain->book("btagCSVV2tight");
  cMain->book("btagDeepBloose");
  cMain->book("btagDeepBmedium");
  cMain->book("btagDeepBtight");
  cMain->book("btagDeepFlavBloose");
  cMain->book("btagDeepFlavBmedium");
  cMain->book("btagDeepFlavBtight");

  cMain->book("bveto");

  cJetLabel->book("all jets");
  cJetLabel->book("b jets");
  cJetLabel->book("c jets");
  cJetLabel->book("uds jets");
  cJetLabel->book("gluon jets");
  cJetLabel->book("unclassified jets");

  cJetLabel_btag->book("all jets");
  cJetLabel_btag->book("b jets");
  cJetLabel_btag->book("c jets");
  cJetLabel_btag->book("uds jets");
  cJetLabel_btag->book("gluon jets");
  cJetLabel_btag->book("unclassified jets");

  cJetLabel_ctag->book("all jets");
  cJetLabel_ctag->book("b jets");
  cJetLabel_ctag->book("c jets");
  cJetLabel_ctag->book("uds jets");
  cJetLabel_ctag->book("gluon jets");
  cJetLabel_ctag->book("unclassified jets");

  cJetLabel_gluontag->book("all jets");
  cJetLabel_gluontag->book("b jets");
  cJetLabel_gluontag->book("c jets");
  cJetLabel_gluontag->book("uds jets");
  cJetLabel_gluontag->book("gluon jets");
  cJetLabel_gluontag->book("unclassified jets");

  cJetLabel_quarktag->book("all jets");
  cJetLabel_quarktag->book("b jets");
  cJetLabel_quarktag->book("c jets");
  cJetLabel_quarktag->book("uds jets");
  cJetLabel_quarktag->book("gluon jets");
  cJetLabel_quarktag->book("unclassified jets");

  cJetLabel_notag->book("all jets");
  cJetLabel_notag->book("b jets");
  cJetLabel_notag->book("c jets");
  cJetLabel_notag->book("uds jets");
  cJetLabel_notag->book("gluon jets");
  cJetLabel_notag->book("unclassified jets");

  cJetLabel_qgtLT0tag->book("all jets");
  cJetLabel_qgtLT0tag->book("b jets");
  cJetLabel_qgtLT0tag->book("c jets");
  cJetLabel_qgtLT0tag->book("uds jets");
  cJetLabel_qgtLT0tag->book("gluon jets");
  cJetLabel_qgtLT0tag->book("unclassified jets");
  /*
  TList* inputList = this->GetInputList();
  for(int i = 0; i < inputList->GetEntries(); ++i){
    //std::cout << "check inputlist " << inputList->At(i)->GetTitle() << " " << std::string(inputList->At(i)->GetTitle()).find("skim") << std::endl;
    if(std::string(inputList->At(i)->GetTitle()).find("skim") == 0){
      inputList->At(i)->Write();
      //std::cout << "check skim" << std::endl;
    }
  }
  */
  //exit(0);
  //cMain->print();  
  //Init(tree);
  //std::cout << "check end" << std::endl;

  l2resHistograms->book();
}

void Analysis::SlaveBegin(TTree* tree) {
  //Init(tree);
  /*
  //  tree->SetMakeClass(1); // Use the decomposed object mode.                                                                                 
  const char* trg = this->GetParameter<const char*>("trigger");

  std::vector<std::string> triggerNames;
  std::string s_trg = std::string(trg);
  size_t pos = s_trg.find("||");
  if (pos == s_trg.size()){
    triggerNames.push_back(s_trg);
  }else{
    while (pos < s_trg.size()){
      pos = s_trg.find("||");
      triggerNames.push_back(s_trg.substr(0,pos));
      s_trg = s_trg.substr(pos+2,s_trg.size());
    }
  }

  triggerBit = new bool[triggerNames.size()];
  
  bool trgDecision = false;
  std::map<std::string,TTreeReaderValue<Bool_t> > trgMap;
  for(size_t i = 0; i < triggerNames.size(); ++i){
    std::cout << "check trgMap" << triggerNames[i].c_str() << std::endl;
    //    trgMap[triggerNames[i].c_str()] = {fReader, triggerNames[i].c_str()};
    //    TTreeReaderValue<Bool_t> trgBit = {fReader, triggerNames[i].c_str()};
    tree->SetBranchAddress(triggerNames[i].c_str(),&triggerBit[i]);
  }
  */
}

void Analysis::Terminate() {
  //const char* datasetName = this->GetParameter<TNamed*>("name")->GetTitle();
  //std::cout << "check Analysis::Terminate " << datasetName << std::endl;

  //cMain->printBothCounters();
  //std::cout << std::endl;
  //  TList* lOUT = new TList();
  //  lOUT->Add(cMain->getHisto());
  //  lOUT->Add(cMain->getWeightedHisto());
  fOutput->Add(cMain->getHisto());
  fOutput->Add(cMain->getWeightedHisto());
  if(!isData) {
    fOutput->Add(cJetLabel->getHisto("jetCounter_all"));
    fOutput->Add(cJetLabel_btag->getHisto("jetCounter_btag"));
    fOutput->Add(cJetLabel_ctag->getHisto("jetCounter_ctag"));
    fOutput->Add(cJetLabel_quarktag->getHisto("jetCounter_quarktag"));
    fOutput->Add(cJetLabel_gluontag->getHisto("jetCounter_gluontag"));
    fOutput->Add(cJetLabel_notag->getHisto("jetCounter_notag"));
    fOutput->Add(cJetLabel_qgtLT0tag->getHisto("jetCounter_qgtLT0tag"));
  }
  //  fOutput->Add(cJetLabel->getWeightedHisto());

  fOUT->cd();
  fOUT->mkdir("configInfo");
  fOUT->cd("configInfo");

  std::time_t t = std::time(nullptr);
  std::string tm = "produced: " + std::string(std::asctime(std::localtime(&t)));
  TNamed* timestamp = new TNamed(tm.c_str(),"");
  timestamp->Write();

  std::string gc = "git commit:";
  gc += std::string(gitCommit);
  TNamed* gitcmmt = new TNamed(gc.c_str(),gitCommit);
  gitcmmt->Write();


  TH1D* h_counters = cMain->getHisto();
  h_counters->Write();
  TH1D* h_wcounters = cMain->getWeightedHisto();
  h_wcounters->Write();

  if(!isData){
    TH1D* h_jetlabel = cJetLabel->getHisto("jet labels, unweighted");
    h_jetlabel->Write();
    TH1D* h_wjetlabel = cJetLabel->getWeightedHisto("jet labels, weighted");
    h_wjetlabel->Write();

    TH1D* h_jetlabel_btag = cJetLabel_btag->getHisto("jet labels btagDeepBtight, unweighted");
    h_jetlabel_btag->Write();
    TH1D* h_wjetlabel_btag = cJetLabel_btag->getWeightedHisto("jet labels btagDeepBtight, weighted");
    h_wjetlabel_btag->Write();

    TH1D* h_jetlabel_ctag = cJetLabel_ctag->getHisto("jet labels btagDeepCtight, unweighted");
    h_jetlabel_ctag->Write();
    TH1D* h_wjetlabel_ctag = cJetLabel_ctag->getWeightedHisto("jet labels btagDeepCtight, weighted");
    h_wjetlabel_ctag->Write();

    TH1D* h_jetlabel_gluontag = cJetLabel_gluontag->getHisto("jet labels gluontag, unweighted");
    h_jetlabel_gluontag->Write();
    TH1D* h_wjetlabel_gluontag = cJetLabel_gluontag->getWeightedHisto("jet labels gluontag, weighted");
    h_wjetlabel_gluontag->Write();

    TH1D* h_jetlabel_quarktag = cJetLabel_quarktag->getHisto("jet labels quarktag, unweighted");
    h_jetlabel_quarktag->Write();
    TH1D* h_wjetlabel_quarktag = cJetLabel_quarktag->getWeightedHisto("jet labels quarktag, weighted");
    h_wjetlabel_quarktag->Write();

    TH1D* h_jetlabel_notag = cJetLabel_notag->getHisto("jet not tagged, unweighted");
    h_jetlabel_notag->Write();
    TH1D* h_wjetlabel_notag = cJetLabel_notag->getWeightedHisto("jet not tagged, weighted");
    h_wjetlabel_notag->Write();
 
    TH1D* h_jetlabel_qgtLT0tag = cJetLabel_qgtLT0tag->getHisto("jet labels q/gtag failed, unweighted");
    h_jetlabel_qgtLT0tag->Write();
    TH1D* h_wjetlabel_qgtLT0tag = cJetLabel_qgtLT0tag->getWeightedHisto("jet labels q/gtag failed, weighted");
    h_wjetlabel_qgtLT0tag->Write();
  }

  TH1D* h_isdata = new TH1D("isdata","",1,0,1);
  h_isdata->SetBinContent(1,int(isData));
  h_isdata->Write();

  if(isData){
    TH1D* h_lumi = new TH1D("lumi","",1,0,1);
    h_lumi->SetBinContent(1,lumi);
    h_lumi->Write();
  }
  pu_data->Write();
  if(pileupreweightingON && !isData){
    //TH1D* pu_data = pileupWeight->getPUData();
    //pu_data->Write();
    TH1D* pu_mc = pileupWeight->getPUMC();
    pu_mc->Write();
    pu_mc_reweighted->Write();
    h_pu_weight->Write();
  }

  fOUT->cd("analysis");
  /*
  if(!isData){
    h_psweight0->Write();
    h_psweight1->Write();
    h_psweight2->Write();
    h_psweight3->Write();
  }
  */
  h_Mu->Write();
  h_RhoVsMu->Write();
  h_RhoVsMu->Write();
  h_NpvVsMu->Write();
  h_Mu0->Write();
  h_RhoVsMu0->Write();
  h_NpvVsMu0->Write();

  h_mu_noPUrw->Write();
  h_npvall_noPUrw->Write();
  h_npvgood_noPUrw->Write();
  h_rho_noPUrw->Write();
  h_rho_central_noPUrw->Write();
  h_rho_centralChargedPileUp_noPUrw->Write();
  h_mu_afterPUrw->Write();
  h_npvall_afterPUrw->Write();
  h_npvgood_afterPUrw->Write();
  h_rho_afterPUrw->Write();
  h_rho_central_afterPUrw->Write();   
  h_rho_centralChargedPileUp_afterPUrw->Write();

  h_METorig->Write();
  h_MET->Write();
  /*
  h_JetPt_chHEF->Write();
  h_JetPt_neHEF->Write();
  h_JetPt_neEmEF->Write();
  h_JetPt_chEmEF->Write();
  h_JetPt_muEF->Write();
  */
  if(isData){
    hprof_db_run_zpt15->Write();
    hprof_db_run_zpt30->Write();
    hprof_db_run_zpt50->Write();
    hprof_db_run_zpt110->Write();
    hprof_mpf_run_zpt15->Write();
    hprof_mpf_run_zpt30->Write();
    hprof_mpf_run_zpt50->Write();
    hprof_mpf_run_zpt110->Write();

    hprof_db_run_jeta34->Write();
    hprof_db_run_jeta45->Write();
    hprof_mpf_run_jeta34->Write();
    hprof_mpf_run_jeta45->Write();

    hprof_chHEF_run_zpt15->Write();
    hprof_neHEF_run_zpt15->Write();
    hprof_chEmEF_run_zpt15->Write();
    hprof_neEmEF_run_zpt15->Write();
    hprof_muEF_run_zpt15->Write();

    hprof_chHEF_run_zpt30->Write();
    hprof_neHEF_run_zpt30->Write();
    hprof_chEmEF_run_zpt30->Write();
    hprof_neEmEF_run_zpt30->Write();
    hprof_muEF_run_zpt30->Write();

    hprof_chHEF_run_zpt50->Write();
    hprof_neHEF_run_zpt50->Write();
    hprof_chEmEF_run_zpt50->Write();
    hprof_neEmEF_run_zpt50->Write();
    hprof_muEF_run_zpt50->Write();

    hprof_chHEF_run_zpt110->Write();
    hprof_neHEF_run_zpt110->Write();
    hprof_chEmEF_run_zpt110->Write();
    hprof_neEmEF_run_zpt110->Write();
    hprof_muEF_run_zpt110->Write();

    hprof_db_run_JetPt15->Write();
    hprof_db_run_JetPt30->Write();
    hprof_db_run_JetPt50->Write();
    hprof_db_run_JetPt110->Write();
    hprof_mpf_run_JetPt15->Write();
    hprof_mpf_run_JetPt30->Write();
    hprof_mpf_run_JetPt50->Write();
    hprof_mpf_run_JetPt110->Write();

    hprof_chHEF_run_JetPt15->Write();
    hprof_neHEF_run_JetPt15->Write();
    hprof_chEmEF_run_JetPt15->Write();
    hprof_neEmEF_run_JetPt15->Write();
    hprof_muEF_run_zpt15->Write();

    hprof_chHEF_run_JetPt30->Write();
    hprof_neHEF_run_JetPt30->Write();
    hprof_chEmEF_run_JetPt30->Write();
    hprof_neEmEF_run_JetPt30->Write();
    hprof_muEF_run_JetPt30->Write();

    hprof_chHEF_run_JetPt50->Write();
    hprof_neHEF_run_JetPt50->Write();
    hprof_chEmEF_run_JetPt50->Write();
    hprof_neEmEF_run_JetPt50->Write();
    hprof_muEF_run_JetPt50->Write();

    hprof_chHEF_run_JetPt110->Write();
    hprof_neHEF_run_JetPt110->Write();
    hprof_chEmEF_run_JetPt110->Write();
    hprof_neEmEF_run_JetPt110->Write();
    hprof_muEF_run_JetPt110->Write();

    hprof_db_run_PtAve15->Write();
    hprof_db_run_PtAve30->Write();
    hprof_db_run_PtAve50->Write();
    hprof_db_run_PtAve110->Write();
    hprof_mpf_run_PtAve15->Write();
    hprof_mpf_run_PtAve30->Write();
    hprof_mpf_run_PtAve50->Write();
    hprof_mpf_run_PtAve110->Write();

    hprof_chHEF_run_PtAve15->Write();
    hprof_neHEF_run_PtAve15->Write();
    hprof_chEmEF_run_PtAve15->Write();
    hprof_neEmEF_run_PtAve15->Write();
    hprof_muEF_run_zpt15->Write();

    hprof_chHEF_run_PtAve30->Write();
    hprof_neHEF_run_PtAve30->Write();
    hprof_chEmEF_run_PtAve30->Write();
    hprof_neEmEF_run_PtAve30->Write();
    hprof_muEF_run_PtAve30->Write();

    hprof_chHEF_run_PtAve50->Write();
    hprof_neHEF_run_PtAve50->Write();
    hprof_chEmEF_run_PtAve50->Write();
    hprof_neEmEF_run_PtAve50->Write();
    hprof_muEF_run_PtAve50->Write();

    hprof_chHEF_run_PtAve110->Write();
    hprof_neHEF_run_PtAve110->Write();
    hprof_chEmEF_run_PtAve110->Write();
    hprof_neEmEF_run_PtAve110->Write();
    hprof_muEF_run_PtAve110->Write();

    hprof_mz_run_zpt15->Write();
    hprof_mz_run_zpt30->Write();
    hprof_mz_run_jeta34->Write();
    hprof_mz_run_jeta45->Write();
    hprof_mz_run_zpt50->Write();
    hprof_mz_run_zpt110->Write();
    hprof_mz_run_zpt0->Write();
    hprof_nz_run_zpt15->Write();
    hprof_nz_run_zpt30->Write();
    hprof_nz_run_jeta34->Write();
    hprof_nz_run_jeta45->Write();
    hprof_nz_run_zpt50->Write();
    hprof_nz_run_zpt110->Write();
    hprof_nz_run_zpt0->Write();
    hprof_nz_run_incl->Write();

    hprof_mz_run_JetPt15->Write();
    hprof_mz_run_JetPt30->Write();
    hprof_mz_run_JetPt50->Write();
    hprof_mz_run_JetPt110->Write();
    hprof_nz_run_JetPt15->Write();
    hprof_nz_run_JetPt30->Write();
    hprof_nz_run_JetPt50->Write();
    hprof_nz_run_JetPt110->Write();

    hprof_mz_run_PtAve15->Write();
    hprof_mz_run_PtAve30->Write();
    hprof_mz_run_PtAve50->Write();
    hprof_mz_run_PtAve110->Write();
    hprof_nz_run_PtAve15->Write();
    hprof_nz_run_PtAve30->Write();
    hprof_nz_run_PtAve50->Write();
    hprof_nz_run_PtAve110->Write();
  }

  h_njets->Write();
  h_njets_sele->Write();
  h_njets_aas->Write();
  h_njets_id->Write();
  h_njets_pt->Write();

  h_Jet_jetId->Write();
  h_Jet_electronIdx1->Write();
  h_Jet_electronIdx2->Write();
  h_Jet_muonIdx1->Write();
  h_Jet_muonIdx2->Write();
  h_Jet_nElectrons->Write();
  h_Jet_nMuons->Write();
  h_Jet_puId->Write();

  h_jet1pt->Write();
  h_jet1eta->Write();
  h_jet2pt->Write();
  h_jet2eta->Write();

  h_jetseta_pt20->Write();
  h_jetseta_pt30->Write();
  h_jetseta_pt40->Write();

  h_jet_smearOff->Write();
  h_jet_smearOn->Write();
  h_jet_smearFactor->Write();

  h_jetseta_pt20->Write();
  h_jetseta_pt30->Write();
  h_jetseta_pt40->Write();

  h_lep1pt->Write();
  h_lep1eta->Write();
  h_lep2pt->Write();
  h_lep2eta->Write();

  h_lep1pt_hltoveroffline->Write();
  h_lep2pt_hltoveroffline->Write();

  h_phibb->Write();

  h_jet1cpt->Write();
  h_jet1ceta->Write();
  h_jet2cpt->Write();
  h_jet2ceta->Write();

  h_jet2etapt->Write();
  
  h_Zpt->Write();
  h_Zeta->Write();
  h_leptonpt->Write();
  h_leptoneta->Write();
  h_Zpt_a10->Write();
  h_Zpt_a15->Write();
  h_Zpt_a20->Write();
  h_Zpt_a30->Write();
  h_Zpt_a40->Write();
  h_Zpt_a50->Write();
  h_Zpt_a60->Write();
  h_Zpt_a80->Write();
  h_Zpt_a100->Write();
  h_Zmass->Write();
  h_Mmumu->Write();
  //h_alpha->Write();
  //for(size_t i = 0; i < cuts.size(); ++i){
  //  h_alphaZpt[i]->Write();
  //}

  //h_RpT->Write();
  //h_RMPF->Write();

  /*
  h_alpha_RpT->Write();
  h_alpha_RMPF->Write();
  h_alpha_RpT_genb->Write();
  h_alpha_RMPF_genb->Write();

  // btagging
  // Jet_btagCMVA
  // Jet_btagCSVV2
  // Jet_btagDeepB
  // Jet_btagDeepC
  // Jet_btagDeepFlavB
  ////if(!Jet_btagCMVA) return;
  
  h_alpha_RpT_btagCSVV2loose->Write();
  h_alpha_RMPF_btagCSVV2loose->Write();
  h_alpha_RpT_btagCSVV2medium->Write();
  h_alpha_RMPF_btagCSVV2medium->Write();
  h_alpha_RpT_btagCSVV2tight->Write();
  h_alpha_RMPF_btagCSVV2tight->Write();

  h_alpha_RpT_btagCSVV2loose_genb->Write();
  h_alpha_RMPF_btagCSVV2loose_genb->Write();
  h_alpha_RpT_btagCSVV2medium_genb->Write();
  h_alpha_RMPF_btagCSVV2medium_genb->Write();
  h_alpha_RpT_btagCSVV2tight_genb->Write();
  h_alpha_RMPF_btagCSVV2tight_genb->Write();

  h_alpha_RpT_btagCMVAloose->Write();
  h_alpha_RMPF_btagCMVAloose->Write();
  h_alpha_RpT_btagCMVAmedium->Write();
  h_alpha_RMPF_btagCMVAmedium->Write();
  h_alpha_RpT_btagCMVAtight->Write();
  h_alpha_RMPF_btagCMVAtight->Write();

  h_alpha_RpT_btagDeepBloose->Write();
  h_alpha_RMPF_btagDeepBloose->Write();
  h_alpha_RpT_btagDeepBmedium->Write();
  h_alpha_RMPF_btagDeepBmedium->Write();
  h_alpha_RpT_btag->Write();
  h_alpha_RMPF_btag->Write();

  h_alpha_RpT_btagDeepFlavBloose->Write();
  h_alpha_RMPF_btagDeepFlavBloose->Write();
  h_alpha_RpT_btagDeepFlavBmedium->Write();
  h_alpha_RMPF_btagDeepFlavBmedium->Write();
  h_alpha_RpT_btagDeepFlavBtight->Write();
  h_alpha_RMPF_btagDeepFlavBtight->Write();

  for(size_t i = 0; i < cuts.size(); ++i){
    h_alpha_RpT_Zpt[i]->Write();
    h_alpha_RMPF_Zpt[i]->Write();
    h_alpha_RpT_Zpt_genb[i]->Write();
    h_alpha_RMPF_Zpt_genb[i]->Write();

    h_alpha_RpT_btagCSVV2loose_Zpt[i]->Write();
    h_alpha_RMPF_btagCSVV2loose_Zpt[i]->Write();
    h_alpha_RpT_btagCSVV2medium_Zpt[i]->Write();
    h_alpha_RMPF_btagCSVV2medium_Zpt[i]->Write();
    h_alpha_RpT_btagCSVV2tight_Zpt[i]->Write();
    h_alpha_RMPF_btagCSVV2tight_Zpt[i]->Write();
    h_alpha_RpT_btagCSVV2loose_Zpt_genb[i]->Write();
    h_alpha_RMPF_btagCSVV2loose_Zpt_genb[i]->Write();
    h_alpha_RpT_btagCSVV2medium_Zpt_genb[i]->Write();
    h_alpha_RMPF_btagCSVV2medium_Zpt_genb[i]->Write();
    h_alpha_RpT_btagCSVV2tight_Zpt_genb[i]->Write();
    h_alpha_RMPF_btagCSVV2tight_Zpt_genb[i]->Write();
    
    h_alpha_RpT_btagDeepBloose_Zpt[i]->Write();
    h_alpha_RMPF_btagDeepBloose_Zpt[i]->Write();
    h_alpha_RpT_btagDeepBmedium_Zpt[i]->Write();
    h_alpha_RMPF_btagDeepBmedium_Zpt[i]->Write();
    h_alpha_RpT_btag_Zpt[i]->Write();
    h_alpha_RMPF_btag_Zpt[i]->Write();

    h_alpha_RpT_btagDeepFlavBloose_Zpt[i]->Write();
    h_alpha_RMPF_btagDeepFlavBloose_Zpt[i]->Write();
    h_alpha_RpT_btagDeepFlavBmedium_Zpt[i]->Write();
    h_alpha_RMPF_btagDeepFlavBmedium_Zpt[i]->Write();
    h_alpha_RpT_btagDeepFlavBtight_Zpt[i]->Write();
    h_alpha_RMPF_btagDeepFlavBtight_Zpt[i]->Write();

  }
  */
  if(!isData){
  h_pTGenJet_JESRpTGen->Write();
  h_pTGenJet_JESRpTGen_genb->Write();
  h_pTGenJet_JESRpTGen_genc->Write();
  h_pTGenJet_JESRpTGen_geng->Write();
  h_pTGenJet_JESRpTGen_genuds->Write();
  h_pTGenJet_JESRpTGen_unclassified->Write();

  h_pTGenJet_JESRMPFGen->Write();
  h_pTGenJet_JESRMPFGen_genb->Write();
  h_pTGenJet_JESRMPFGen_genc->Write();
  h_pTGenJet_JESRMPFGen_geng->Write();
  h_pTGenJet_JESRMPFGen_genuds->Write();
  h_pTGenJet_JESRMPFGen_unclassified->Write();

  h_pTGenJet_JESRpTGen->Write();
  h_pTGenJet_JESRpTGen_genb->Write();
  h_pTGenJet_JESRpTGen_genc->Write();
  h_pTGenJet_JESRpTGen_geng->Write();
  h_pTGenJet_JESRpTGen_genuds->Write();
  h_pTGenJet_JESRpTGen_unclassified->Write();

  h_pTGenJet_JESRMPFGen->Write();
  h_pTGenJet_JESRMPFGen_genb->Write();
  h_pTGenJet_JESRMPFGen_genc->Write();
  h_pTGenJet_JESRMPFGen_geng->Write();
  h_pTGenJet_JESRMPFGen_genuds->Write();
  h_pTGenJet_JESRMPFGen_unclassified->Write();

  h_pTGenZ_JESRpTGen->Write();
  h_pTGenZ_JESRpTGen_genb->Write();
  h_pTGenZ_JESRpTGen_genc->Write();
  h_pTGenZ_JESRpTGen_geng->Write();
  h_pTGenZ_JESRpTGen_genuds->Write();
  h_pTGenZ_JESRpTGen_unclassified->Write();

  h_pTGenZ_JESRMPFGen->Write();
  h_pTGenZ_JESRMPFGen_genb->Write();
  h_pTGenZ_JESRMPFGen_genc->Write();
  h_pTGenZ_JESRMPFGen_geng->Write();
  h_pTGenZ_JESRMPFGen_genuds->Write();
  h_pTGenZ_JESRMPFGen_unclassified->Write();
  }

  h_Zpt_Rho_1->Write();
  h_Zpt_Rho_2->Write();

  h_Zpt_leptonm_pt->Write();
  h_Zpt_leptonp_pt->Write();

  for(size_t i = 0; i < nbins_pt*bins_eta.size()*npsWeights; ++i){
    size_t ipsw = i/(nbins_pt*bins_eta.size());

    fOUT->cd();
    fOUT->cd("analysis");
    if(ipsw > 0){
      std::string s_psw = "analysis/PSWeight"+std::to_string(ipsw-1);

      if(!fOUT->cd(s_psw.c_str())) {
        fOUT->mkdir(s_psw.c_str());
      }

      fOUT->cd(s_psw.c_str());
    }
    h_Zpt_RMPFj2pt[i]->Write();
    h_Zpt_RMPFj2pt_genb[i]->Write();
    h_Zpt_RMPFj2pt_genc[i]->Write();
    h_Zpt_RMPFj2pt_genuds[i]->Write();
    h_Zpt_RMPFj2pt_geng[i]->Write();
    h_Zpt_RMPFj2pt_unclassified[i]->Write();

    h_Zpt_RMPFjet1j2pt[i]->Write();
    h_Zpt_RMPFjet1j2pt_genb[i]->Write();
    h_Zpt_RMPFjet1j2pt_genc[i]->Write();
    h_Zpt_RMPFjet1j2pt_genuds[i]->Write();
    h_Zpt_RMPFjet1j2pt_geng[i]->Write();
    h_Zpt_RMPFjet1j2pt_unclassified[i]->Write();

    h_Zpt_RMPFj2ptx[i]->Write();
    h_Zpt_RMPFjet1j2ptx[i]->Write();
  }
  for(size_t i = 0; i < nbins_pt*npsWeights; ++i){
    size_t ipsw = i/(nbins_pt);

    fOUT->cd();
    fOUT->cd("analysis");
    if(ipsw > 0){
      std::string s_psw = "analysis/PSWeight"+std::to_string(ipsw-1);

      if(!fOUT->cd(s_psw.c_str())) {
        fOUT->mkdir(s_psw.c_str());
      }

      fOUT->cd(s_psw.c_str());
    }
    h3D_Zpt_RMPFj2pt_mu[i]->Write();
    h3D_Zpt_RMPFj2ptx_mu[i]->Write();
    h3D_Zpt_RMPFj2pt_eta[i]->Write();
    h3D_Zpt_RMPFj2ptx_eta[i]->Write();

    fOUT->cd("analysis");
  }
  //std::cout << "bins " << nbins_alpha << " " << bins_eta.size() << " " << npsWeights << std::endl;
  for(size_t i = 0; i < nbins_alpha*bins_eta.size()*npsWeights; ++i){
    size_t ipsw = i/(nbins_alpha*bins_eta.size());
    
    //std::cout << "check ipsw " << ipsw << std::endl;
    fOUT->cd();
    fOUT->cd("analysis");
    if(ipsw > 0){
      std::string s_psw = "analysis/PSWeight"+std::to_string(ipsw-1);
      //std::cout << "check s_psw " << s_psw << " " << ipsw << std::endl;

      //std::cout << "check PSWeight-dir 1 " << std::endl;
      if(!fOUT->cd(s_psw.c_str())) {
        fOUT->mkdir(s_psw.c_str());
        //std::cout << "check mkdir " << s_psw.c_str() << std::endl;
      }

      //std::cout << "check PSWeight-dir 2" << std::endl;
      fOUT->cd(s_psw.c_str());
      //std::cout << "check PSWeight-dir 3" << std::endl;
    }

    h_Zpt_mZ[i]->Write();
    if(!isData) h_Zpt_mZgen[i]->Write();

    h_Zpt_Rho[i]->Write();
    h_JetPt_Rho[i]->Write();
    h_PtAve_Rho[i]->Write();

    h_Zpt_chHEF[i]->Write();
    h_Zpt_neHEF[i]->Write();
    h_Zpt_chEmEF[i]->Write();
    h_Zpt_neEmEF[i]->Write();
    h_Zpt_muEF[i]->Write();

    h_Zpt_JNPF[i]->Write();

    h_Zpt_RpT[i]->Write();
    h_Zpt_RMPF[i]->Write();
    h_Zpt_RMPFx[i]->Write();
    h_Zpt_RMPFjet1[i]->Write();
    h_Zpt_RMPFjet1x[i]->Write();
    h_Zpt_RMPFjetn[i]->Write();
    h_Zpt_RMPFuncl[i]->Write();
    h_Zpt_RMPFfromType1[i]->Write();
    h_Zpt_RMPFPF[i]->Write();

    h_JetPt_QGL[i]->Write();
    if(!isData){
    h_JetPt_QGL_genb[i]->Write();
    h_JetPt_QGL_genc[i]->Write();
    h_JetPt_QGL_genuds[i]->Write();
    h_JetPt_QGL_geng[i]->Write();
    h_JetPt_QGL_unclassified[i]->Write();
    }
    h_Zpt_QGL[i]->Write();
    if(!isData){
    h_Zpt_QGL_genb[i]->Write();
    h_Zpt_QGL_genc[i]->Write();
    h_Zpt_QGL_genuds[i]->Write();
    h_Zpt_QGL_geng[i]->Write();
    h_Zpt_QGL_unclassified[i]->Write();
    }
    h_JetPt_muEF[i]->Write();
    h_JetPt_chEmEF[i]->Write();
    h_JetPt_chHEF[i]->Write();
    h_JetPt_neEmEF[i]->Write();
    h_JetPt_neHEF[i]->Write();

    h_PtAve_QGL[i]->Write();
    h_PtAve_muEF[i]->Write();
    h_PtAve_chEmEF[i]->Write();
    h_PtAve_chHEF[i]->Write();
    h_PtAve_neEmEF[i]->Write();
    h_PtAve_neHEF[i]->Write();

    h_Zpt_Mu_Rho[i]->Write();
    h_Zpt_Mu_NPVgood[i]->Write();
    h_Zpt_Mu_NPVall[i]->Write();
    h_Zpt_Mu_Mu[i]->Write();


    h_ZpT_Mu_RpT[i]->Write();
    h_ZpT_Mu_RMPF[i]->Write();
    h_ZpT_Mu_RMPFjet1[i]->Write();
    h_ZpT_Mu_RMPFjetn[i]->Write();
    h_ZpT_Mu_RMPFuncl[i]->Write();

    h_ZpT_Mu_offset[i]->Write();
    h_ZpT_Rho_offset[i]->Write();
    h_ZpT_NPVgood_offset[i]->Write();
    h_ZpT_NPVall_offset[i]->Write();

    h_ZpT_NPVg_RpT[i]->Write();
    h_ZpT_NPVg_RMPF[i]->Write();
    h_ZpT_NPVg_RMPFjet1[i]->Write();
    h_ZpT_NPVg_RMPFjetn[i]->Write();
    h_ZpT_NPVg_RMPFuncl[i]->Write();

    h_JetPt_Mu_RpT[i]->Write();
    h_JetPt_Mu_RMPF[i]->Write();
    h_JetPt_Mu_RMPFjet1[i]->Write();
    h_JetPt_Mu_RMPFjetn[i]->Write();
    h_JetPt_Mu_RMPFuncl[i]->Write();

    h_JetPt_NPVg_RpT[i]->Write();
    h_JetPt_NPVg_RMPF[i]->Write();
    h_JetPt_NPVg_RMPFjet1[i]->Write();
    h_JetPt_NPVg_RMPFjetn[i]->Write();
    h_JetPt_NPVg_RMPFuncl[i]->Write();

    h_PtAve_Mu_RpT[i]->Write();
    h_PtAve_Mu_RMPF[i]->Write();
    h_PtAve_Mu_RMPFjet1[i]->Write();
    h_PtAve_Mu_RMPFjetn[i]->Write();
    h_PtAve_Mu_RMPFuncl[i]->Write();

    h_PtAve_NPVg_RpT[i]->Write();
    h_PtAve_NPVg_RMPF[i]->Write();
    h_PtAve_NPVg_RMPFjet1[i]->Write();
    h_PtAve_NPVg_RMPFjetn[i]->Write();
    h_PtAve_NPVg_RMPFuncl[i]->Write();


    h_Mu_RpT[i]->Write();
    h_JetPt_RpT[i]->Write();
    h_PtAve_RpT[i]->Write();
    h_Mu_RMPF[i]->Write();
    h_JetPt_RMPF[i]->Write();
    h_PtAve_RMPF[i]->Write();
    h_Mu_RMPFjet1[i]->Write();
    h_JetPt_RMPFjet1[i]->Write();
    h_PtAve_RMPFjet1[i]->Write();
    h_Mu_RMPFjetn[i]->Write();
    h_JetPt_RMPFjetn[i]->Write();
    h_PtAve_RMPFjetn[i]->Write();
    h_Mu_RMPFuncl[i]->Write();
    h_JetPt_RMPFuncl[i]->Write();
    h_PtAve_RMPFuncl[i]->Write();

    h_Mu_ptdiff[i]->Write();

//h_Zpt_RpT_btagCSVV2loose[i]->Write();
//h_Zpt_RMPF_btagCSVV2loose[i]->Write();
//h_Zpt_RpT_btagCSVV2medium[i]->Write();
//h_Zpt_RMPF_btagCSVV2medium[i]->Write();
//h_Zpt_RpT_btagCSVV2tight[i]->Write();
//h_Zpt_RMPF_btagCSVV2tight[i]->Write();

    if(!isData){
    h_Zpt_RpT_genb[i]->Write();
    h_Zpt_RMPF_genb[i]->Write();
    h_Zpt_RMPFjet1_genb[i]->Write();
    h_Zpt_RMPFjetn_genb[i]->Write();
    h_Zpt_RMPFuncl_genb[i]->Write();

    //h_Zpt_RpT_btagCSVV2loose_genb[i]->Write();
    //h_Zpt_RMPF_btagCSVV2loose_genb[i]->Write();
    //h_Zpt_RpT_btagCSVV2medium_genb[i]->Write();
    //h_Zpt_RMPF_btagCSVV2medium_genb[i]->Write();
    //h_Zpt_RpT_btagCSVV2tight_genb[i]->Write();
    //h_Zpt_RMPF_btagCSVV2tight_genb[i]->Write();

    h_Zpt_RpT_genc[i]->Write();
    h_Zpt_RMPF_genc[i]->Write();
    h_Zpt_RMPFjet1_genc[i]->Write();
    h_Zpt_RMPFjetn_genc[i]->Write();
    h_Zpt_RMPFuncl_genc[i]->Write();

    //h_Zpt_RpT_btagCSVV2loose_genc[i]->Write();
    //h_Zpt_RMPF_btagCSVV2loose_genc[i]->Write();
    //h_Zpt_RpT_btagCSVV2medium_genc[i]->Write();
    //h_Zpt_RMPF_btagCSVV2medium_genc[i]->Write();
    //h_Zpt_RpT_btagCSVV2tight_genc[i]->Write();
    //h_Zpt_RMPF_btagCSVV2tight_genc[i]->Write();

    h_Zpt_RpT_genuds[i]->Write();
    h_Zpt_RMPF_genuds[i]->Write();
    h_Zpt_RMPFjet1_genuds[i]->Write();
    h_Zpt_RMPFjetn_genuds[i]->Write();
    h_Zpt_RMPFuncl_genuds[i]->Write();

    h_Zpt_RpT_gens[i]->Write();
    h_Zpt_RMPF_gens[i]->Write();
    h_Zpt_RMPFjet1_gens[i]->Write();
    h_Zpt_RMPFjetn_gens[i]->Write();
    h_Zpt_RMPFuncl_gens[i]->Write();

    h_Zpt_RpT_genud[i]->Write();
    h_Zpt_RMPF_genud[i]->Write();
    h_Zpt_RMPFjet1_genud[i]->Write();
    h_Zpt_RMPFjetn_genud[i]->Write();
    h_Zpt_RMPFuncl_genud[i]->Write();

    /*
    h_Zpt_RpT_btagCSVV2loose_genuds[i]->Write();
    h_Zpt_RMPF_btagCSVV2loose_genuds[i]->Write();
    h_Zpt_RpT_btagCSVV2medium_genuds[i]->Write();
    h_Zpt_RMPF_btagCSVV2medium_genuds[i]->Write();
    h_Zpt_RpT_btagCSVV2tight_genuds[i]->Write();
    h_Zpt_RMPF_btagCSVV2tight_genuds[i]->Write();
    */

    h_Zpt_RpT_geng[i]->Write();
    h_Zpt_RMPF_geng[i]->Write();
    h_Zpt_RMPFjet1_geng[i]->Write();
    h_Zpt_RMPFjetn_geng[i]->Write();
    h_Zpt_RMPFuncl_geng[i]->Write();

    h_Zpt_RpT_unclassified[i]->Write();
    h_Zpt_RMPF_unclassified[i]->Write();
    h_Zpt_RMPFjet1_unclassified[i]->Write();
    h_Zpt_RMPFjetn_unclassified[i]->Write();
    h_Zpt_RMPFuncl_unclassified[i]->Write();

    /*
    h_Zpt_RpT_btagCSVV2loose_geng[i]->Write();
    h_Zpt_RMPF_btagCSVV2loose_geng[i]->Write();
    h_Zpt_RpT_btagCSVV2medium_geng[i]->Write();
    h_Zpt_RMPF_btagCSVV2medium_geng[i]->Write();
    h_Zpt_RpT_btagCSVV2tight_geng[i]->Write();
    h_Zpt_RMPF_btagCSVV2tight_geng[i]->Write();
    */
    //h_Zpt_RpT_btagDeepBloose[i]->Write();
    //h_Zpt_RMPF_btagDeepBloose[i]->Write();
    //h_Zpt_RpT_btagDeepBmedium[i]->Write();
    //h_Zpt_RMPF_btagDeepBmedium[i]->Write();
    }
    h_Zpt_RpT_btag[i]->Write();
    h_Zpt_RMPF_btag[i]->Write();
    h_Zpt_RMPFjet1_btag[i]->Write();
    h_Zpt_RMPFjetn_btag[i]->Write();
    h_Zpt_RMPFuncl_btag[i]->Write();
    h_JetPt_QGL_btag[i]->Write();
    if(!isData){
    h_JetPt_QGL_btag_genb[i]->Write();
    h_JetPt_QGL_btag_genc[i]->Write();
    h_JetPt_QGL_btag_genuds[i]->Write();
    h_JetPt_QGL_btag_geng[i]->Write();
    h_JetPt_QGL_btag_unclassified[i]->Write();
    }
    h_Zpt_QGL_btag[i]->Write();
    if(!isData){
    h_Zpt_QGL_btag_genb[i]->Write();
    h_Zpt_QGL_btag_genc[i]->Write();
    h_Zpt_QGL_btag_genuds[i]->Write();
    h_Zpt_QGL_btag_geng[i]->Write();
    h_Zpt_QGL_btag_unclassified[i]->Write();
    }
    h_JetPt_muEF_btag[i]->Write();
    h_JetPt_chEmEF_btag[i]->Write();
    h_JetPt_chHEF_btag[i]->Write();
    h_JetPt_neEmEF_btag[i]->Write();
    h_JetPt_neHEF_btag[i]->Write();

    h_PtAve_QGL_btag[i]->Write();
    h_PtAve_muEF_btag[i]->Write();
    h_PtAve_chEmEF_btag[i]->Write();
    h_PtAve_chHEF_btag[i]->Write();
    h_PtAve_neEmEF_btag[i]->Write();
    h_PtAve_neHEF_btag[i]->Write();

    h_Zpt_Mu_Rho_btag[i]->Write();
    h_Zpt_Mu_NPVgood_btag[i]->Write();
    h_Zpt_Mu_NPVall_btag[i]->Write();
    h_Zpt_Mu_Mu_btag[i]->Write();
    h_Mu_RpT_btag[i]->Write();
    h_JetPt_RpT_btag[i]->Write();
    h_PtAve_RpT_btag[i]->Write();
    h_Mu_RMPF_btag[i]->Write();
    h_JetPt_RMPF_btag[i]->Write();
    h_PtAve_RMPF_btag[i]->Write();
    h_Mu_RMPFjet1_btag[i]->Write();
    h_JetPt_RMPFjet1_btag[i]->Write();
    h_PtAve_RMPFjet1_btag[i]->Write();
    h_Mu_RMPFjetn_btag[i]->Write();
    h_JetPt_RMPFjetn_btag[i]->Write();
    h_PtAve_RMPFjetn_btag[i]->Write();
    h_Mu_RMPFuncl_btag[i]->Write();
    h_JetPt_RMPFuncl_btag[i]->Write();
    h_PtAve_RMPFuncl_btag[i]->Write();

    //h_Zpt_RpT_btagDeepBloose_genb[i]->Write();
    //h_Zpt_RMPF_btagDeepBloose_genb[i]->Write();
    //h_Zpt_RpT_btagDeepBmedium_genb[i]->Write();
    //h_Zpt_RMPF_btagDeepBmedium_genb[i]->Write();

    if(!isData){
    h_Zpt_RpT_btag_genb[i]->Write();
    h_Zpt_RMPF_btag_genb[i]->Write();
    h_Zpt_RMPFjet1_btag_genb[i]->Write();
    h_Zpt_RMPFjetn_btag_genb[i]->Write();
    h_Zpt_RMPFuncl_btag_genb[i]->Write();

    //h_Zpt_RpT_btagDeepBloose_genc[i]->Write();
    //h_Zpt_RMPF_btagDeepBloose_genc[i]->Write();
    //h_Zpt_RpT_btagDeepBmedium_genc[i]->Write();
    //h_Zpt_RMPF_btagDeepBmedium_genc[i]->Write();
    h_Zpt_RpT_btag_genc[i]->Write();
    h_Zpt_RMPF_btag_genc[i]->Write();
    h_Zpt_RMPFjet1_btag_genc[i]->Write();
    h_Zpt_RMPFjetn_btag_genc[i]->Write();
    h_Zpt_RMPFuncl_btag_genc[i]->Write();

    //h_Zpt_RpT_btagDeepBloose_genuds[i]->Write();
    //h_Zpt_RMPF_btagDeepBloose_genuds[i]->Write();
    //h_Zpt_RpT_btagDeepBmedium_genuds[i]->Write();
    //h_Zpt_RMPF_btagDeepBmedium_genuds[i]->Write();
    h_Zpt_RpT_btag_genuds[i]->Write();
    h_Zpt_RMPF_btag_genuds[i]->Write();
    h_Zpt_RMPFjet1_btag_genuds[i]->Write();
    h_Zpt_RMPFjetn_btag_genuds[i]->Write();
    h_Zpt_RMPFuncl_btag_genuds[i]->Write();

    h_Zpt_RpT_btag_gens[i]->Write();
    h_Zpt_RMPF_btag_gens[i]->Write();
    h_Zpt_RMPFjet1_btag_gens[i]->Write();
    h_Zpt_RMPFjetn_btag_gens[i]->Write();
    h_Zpt_RMPFuncl_btag_gens[i]->Write();

    h_Zpt_RpT_btag_genud[i]->Write();
    h_Zpt_RMPF_btag_genud[i]->Write();
    h_Zpt_RMPFjet1_btag_genud[i]->Write();
    h_Zpt_RMPFjetn_btag_genud[i]->Write();
    h_Zpt_RMPFuncl_btag_genud[i]->Write();

    //h_Zpt_RpT_btagDeepBloose_geng[i]->Write();
    //h_Zpt_RMPF_btagDeepBloose_geng[i]->Write();
    //h_Zpt_RpT_btagDeepBmedium_geng[i]->Write();
    //h_Zpt_RMPF_btagDeepBmedium_geng[i]->Write();
    h_Zpt_RpT_btag_geng[i]->Write();
    h_Zpt_RMPF_btag_geng[i]->Write();
    h_Zpt_RMPFjet1_btag_geng[i]->Write();
    h_Zpt_RMPFjetn_btag_geng[i]->Write();
    h_Zpt_RMPFuncl_btag_geng[i]->Write();

    h_Zpt_RpT_btag_unclassified[i]->Write();
    h_Zpt_RMPF_btag_unclassified[i]->Write();
    h_Zpt_RMPFjet1_btag_unclassified[i]->Write();
    h_Zpt_RMPFjetn_btag_unclassified[i]->Write();
    h_Zpt_RMPFuncl_btag_unclassified[i]->Write();
    }

    //h_Zpt_RpT_btagDeepCloose[i]->Write();
    //h_Zpt_RMPF_btagDeepCloose[i]->Write();
    //h_Zpt_RpT_btagDeepCmedium[i]->Write();
    //h_Zpt_RMPF_btagDeepCmedium[i]->Write();
    h_Zpt_RpT_ctag[i]->Write();
    h_Zpt_RMPF_ctag[i]->Write();
    h_Zpt_RMPFjet1_ctag[i]->Write();
    h_Zpt_RMPFjetn_ctag[i]->Write();
    h_Zpt_RMPFuncl_ctag[i]->Write();
    h_JetPt_QGL_ctag[i]->Write();
    if(!isData){
    h_JetPt_QGL_ctag_genb[i]->Write();
    h_JetPt_QGL_ctag_genc[i]->Write();
    h_JetPt_QGL_ctag_genuds[i]->Write();
    h_JetPt_QGL_ctag_geng[i]->Write();
    h_JetPt_QGL_ctag_unclassified[i]->Write();
    }
    h_Zpt_QGL_ctag[i]->Write();
    if(!isData){
    h_Zpt_QGL_ctag_genb[i]->Write();
    h_Zpt_QGL_ctag_genc[i]->Write();
    h_Zpt_QGL_ctag_genuds[i]->Write();
    h_Zpt_QGL_ctag_geng[i]->Write();
    h_Zpt_QGL_ctag_unclassified[i]->Write();
    }
    h_JetPt_muEF_ctag[i]->Write();
    h_JetPt_chEmEF_ctag[i]->Write();
    h_JetPt_chHEF_ctag[i]->Write();
    h_JetPt_neEmEF_ctag[i]->Write();
    h_JetPt_neHEF_ctag[i]->Write();

    h_PtAve_QGL_ctag[i]->Write();
    h_PtAve_muEF_ctag[i]->Write();
    h_PtAve_chEmEF_ctag[i]->Write();
    h_PtAve_chHEF_ctag[i]->Write();
    h_PtAve_neEmEF_ctag[i]->Write();
    h_PtAve_neHEF_ctag[i]->Write();

    h_Zpt_Mu_Rho_ctag[i]->Write();
    h_Zpt_Mu_NPVgood_ctag[i]->Write();
    h_Zpt_Mu_NPVall_ctag[i]->Write();
    h_Zpt_Mu_Mu_ctag[i]->Write();
    h_Mu_RpT_ctag[i]->Write();
    h_JetPt_RpT_ctag[i]->Write();
    h_PtAve_RpT_ctag[i]->Write();
    h_Mu_RMPF_ctag[i]->Write();
    h_JetPt_RMPF_ctag[i]->Write();
    h_PtAve_RMPF_ctag[i]->Write();
    h_Mu_RMPFjet1_ctag[i]->Write();
    h_JetPt_RMPFjet1_ctag[i]->Write();
    h_PtAve_RMPFjet1_ctag[i]->Write();
    h_Mu_RMPFjetn_ctag[i]->Write();
    h_JetPt_RMPFjetn_ctag[i]->Write();
    h_PtAve_RMPFjetn_ctag[i]->Write();
    h_Mu_RMPFuncl_ctag[i]->Write();
    h_JetPt_RMPFuncl_ctag[i]->Write();
    h_PtAve_RMPFuncl_ctag[i]->Write();

    if(!isData){
    //h_Zpt_RpT_btagDeepCloose_genb[i]->Write();
    //h_Zpt_RMPF_btagDeepCloose_genb[i]->Write();
    //h_Zpt_RpT_btagDeepCmedium_genb[i]->Write();
    //h_Zpt_RMPF_btagDeepCmedium_genb[i]->Write();
    h_Zpt_RpT_ctag_genb[i]->Write();
    h_Zpt_RMPF_ctag_genb[i]->Write();
    h_Zpt_RMPFjet1_ctag_genb[i]->Write();
    h_Zpt_RMPFjetn_ctag_genb[i]->Write();
    h_Zpt_RMPFuncl_ctag_genb[i]->Write();

    //h_Zpt_RpT_btagDeepCloose_genc[i]->Write();
    //h_Zpt_RMPF_btagDeepCloose_genc[i]->Write();
    //h_Zpt_RpT_btagDeepCmedium_genc[i]->Write();
    //h_Zpt_RMPF_btagDeepCmedium_genc[i]->Write();
    h_Zpt_RpT_ctag_genc[i]->Write();
    h_Zpt_RMPF_ctag_genc[i]->Write();
    h_Zpt_RMPFjet1_ctag_genc[i]->Write();
    h_Zpt_RMPFjetn_ctag_genc[i]->Write();
    h_Zpt_RMPFuncl_ctag_genc[i]->Write();

    //h_Zpt_RpT_btagDeepCloose_genuds[i]->Write();
    //h_Zpt_RMPF_btagDeepCloose_genuds[i]->Write();
    //h_Zpt_RpT_btagDeepCmedium_genuds[i]->Write();
    //h_Zpt_RMPF_btagDeepCmedium_genuds[i]->Write();
    h_Zpt_RpT_ctag_genuds[i]->Write();
    h_Zpt_RMPF_ctag_genuds[i]->Write();
    h_Zpt_RMPFjet1_ctag_genuds[i]->Write();
    h_Zpt_RMPFjetn_ctag_genuds[i]->Write();
    h_Zpt_RMPFuncl_ctag_genuds[i]->Write();

    h_Zpt_RpT_ctag_gens[i]->Write();
    h_Zpt_RMPF_ctag_gens[i]->Write();
    h_Zpt_RMPFjet1_ctag_gens[i]->Write();
    h_Zpt_RMPFjetn_ctag_gens[i]->Write();
    h_Zpt_RMPFuncl_ctag_gens[i]->Write();

    h_Zpt_RpT_ctag_genud[i]->Write();
    h_Zpt_RMPF_ctag_genud[i]->Write();
    h_Zpt_RMPFjet1_ctag_genud[i]->Write();
    h_Zpt_RMPFjetn_ctag_genud[i]->Write();
    h_Zpt_RMPFuncl_ctag_genud[i]->Write();

    //h_Zpt_RpT_btagDeepCloose_geng[i]->Write();
    //h_Zpt_RMPF_btagDeepCloose_geng[i]->Write();
    //h_Zpt_RpT_btagDeepCmedium_geng[i]->Write();
    //h_Zpt_RMPF_btagDeepCmedium_geng[i]->Write();
    h_Zpt_RpT_ctag_geng[i]->Write();
    h_Zpt_RMPF_ctag_geng[i]->Write();
    h_Zpt_RMPFjet1_ctag_geng[i]->Write();
    h_Zpt_RMPFjetn_ctag_geng[i]->Write();
    h_Zpt_RMPFuncl_ctag_geng[i]->Write();

    h_Zpt_RpT_ctag_unclassified[i]->Write();
    h_Zpt_RMPF_ctag_unclassified[i]->Write();
    h_Zpt_RMPFjet1_ctag_unclassified[i]->Write();
    h_Zpt_RMPFjetn_ctag_unclassified[i]->Write();
    h_Zpt_RMPFuncl_ctag_unclassified[i]->Write();
    }
    /*
    h_Zpt_RpT_btagDeepFlavBloose[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBloose[i]->Write();
    h_Zpt_RpT_btagDeepFlavBmedium[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBmedium[i]->Write();
    h_Zpt_RpT_btagDeepFlavBtight[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBtight[i]->Write();

    h_Zpt_RpT_btagDeepFlavBloose_genb[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBloose_genb[i]->Write();
    h_Zpt_RpT_btagDeepFlavBmedium_genb[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBmedium_genb[i]->Write();
    h_Zpt_RpT_btagDeepFlavBtight_genb[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBtight_genb[i]->Write();

    h_Zpt_RpT_btagDeepFlavBloose_genc[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBloose_genc[i]->Write();
    h_Zpt_RpT_btagDeepFlavBmedium_genc[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBmedium_genc[i]->Write();
    h_Zpt_RpT_btagDeepFlavBtight_genc[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBtight_genc[i]->Write();

    h_Zpt_RpT_btagDeepFlavBloose_genuds[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBloose_genuds[i]->Write();
    h_Zpt_RpT_btagDeepFlavBmedium_genuds[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBmedium_genuds[i]->Write();
    h_Zpt_RpT_btagDeepFlavBtight_genuds[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBtight_genuds[i]->Write();

    h_Zpt_RpT_btagDeepFlavBloose_geng[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBloose_geng[i]->Write();
    h_Zpt_RpT_btagDeepFlavBmedium_geng[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBmedium_geng[i]->Write();
    h_Zpt_RpT_btagDeepFlavBtight_geng[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBtight_geng[i]->Write();
*/
    h_Zpt_RpT_gluontag[i]->Write();
    h_Zpt_RMPF_gluontag[i]->Write();
    h_Zpt_RMPFjet1_gluontag[i]->Write();
    h_Zpt_RMPFjetn_gluontag[i]->Write();
    h_Zpt_RMPFuncl_gluontag[i]->Write();
    h_JetPt_QGL_gluontag[i]->Write();
    if(!isData){
    h_JetPt_QGL_gluontag_genb[i]->Write();
    h_JetPt_QGL_gluontag_genc[i]->Write();
    h_JetPt_QGL_gluontag_genuds[i]->Write();
    h_JetPt_QGL_gluontag_geng[i]->Write();
    h_JetPt_QGL_gluontag_unclassified[i]->Write();
    }
    h_Zpt_QGL_gluontag[i]->Write();
    if(!isData){
    h_Zpt_QGL_gluontag_genb[i]->Write();
    h_Zpt_QGL_gluontag_genc[i]->Write();
    h_Zpt_QGL_gluontag_genuds[i]->Write();
    h_Zpt_QGL_gluontag_geng[i]->Write();
    h_Zpt_QGL_gluontag_unclassified[i]->Write();
    }
    h_JetPt_muEF_gluontag[i]->Write();
    h_JetPt_chEmEF_gluontag[i]->Write();
    h_JetPt_chHEF_gluontag[i]->Write();
    h_JetPt_neEmEF_gluontag[i]->Write();
    h_JetPt_neHEF_gluontag[i]->Write();

    h_PtAve_QGL_gluontag[i]->Write();
    h_PtAve_muEF_gluontag[i]->Write();
    h_PtAve_chEmEF_gluontag[i]->Write();
    h_PtAve_chHEF_gluontag[i]->Write();
    h_PtAve_neEmEF_gluontag[i]->Write();
    h_PtAve_neHEF_gluontag[i]->Write();

    h_Zpt_Mu_Rho_gluontag[i]->Write();
    h_Zpt_Mu_NPVgood_gluontag[i]->Write();
    h_Zpt_Mu_NPVall_gluontag[i]->Write();
    h_Zpt_Mu_Mu_gluontag[i]->Write();
    h_Mu_RpT_gluontag[i]->Write();
    h_JetPt_RpT_gluontag[i]->Write();
    h_PtAve_RpT_gluontag[i]->Write();
    h_Mu_RMPF_gluontag[i]->Write();
    h_JetPt_RMPF_gluontag[i]->Write();
    h_PtAve_RMPF_gluontag[i]->Write();
    h_Mu_RMPFjet1_gluontag[i]->Write();
    h_JetPt_RMPFjet1_gluontag[i]->Write();
    h_PtAve_RMPFjet1_gluontag[i]->Write();
    h_Mu_RMPFjetn_gluontag[i]->Write();
    h_JetPt_RMPFjetn_gluontag[i]->Write();
    h_PtAve_RMPFjetn_gluontag[i]->Write();
    h_Mu_RMPFuncl_gluontag[i]->Write();
    h_JetPt_RMPFuncl_gluontag[i]->Write();
    h_PtAve_RMPFuncl_gluontag[i]->Write();

    if(!isData){
    h_Zpt_RpT_gluontag_genb[i]->Write();
    h_Zpt_RMPF_gluontag_genb[i]->Write();
    h_Zpt_RMPFjet1_gluontag_genb[i]->Write();
    h_Zpt_RMPFjetn_gluontag_genb[i]->Write();
    h_Zpt_RMPFuncl_gluontag_genb[i]->Write();
    h_Zpt_RpT_gluontag_genc[i]->Write();
    h_Zpt_RMPF_gluontag_genc[i]->Write();
    h_Zpt_RMPFjet1_gluontag_genc[i]->Write();
    h_Zpt_RMPFjetn_gluontag_genc[i]->Write();
    h_Zpt_RMPFuncl_gluontag_genc[i]->Write();
    h_Zpt_RpT_gluontag_genuds[i]->Write();
    h_Zpt_RMPF_gluontag_genuds[i]->Write();
    h_Zpt_RMPFjet1_gluontag_genuds[i]->Write();
    h_Zpt_RMPFjetn_gluontag_genuds[i]->Write();
    h_Zpt_RMPFuncl_gluontag_genuds[i]->Write();
    h_Zpt_RpT_gluontag_geng[i]->Write();
    h_Zpt_RMPF_gluontag_geng[i]->Write();
    h_Zpt_RMPFjet1_gluontag_geng[i]->Write();
    h_Zpt_RMPFjetn_gluontag_geng[i]->Write();
    h_Zpt_RMPFuncl_gluontag_geng[i]->Write();
    h_Zpt_RpT_gluontag_unclassified[i]->Write();
    h_Zpt_RMPF_gluontag_unclassified[i]->Write();
    h_Zpt_RMPFjet1_gluontag_unclassified[i]->Write();
    h_Zpt_RMPFjetn_gluontag_unclassified[i]->Write();
    h_Zpt_RMPFuncl_gluontag_unclassified[i]->Write();

    h_Zpt_RpT_gluontag_gens[i]->Write();
    h_Zpt_RMPF_gluontag_gens[i]->Write();
    h_Zpt_RMPFjet1_gluontag_gens[i]->Write();
    h_Zpt_RMPFjetn_gluontag_gens[i]->Write();
    h_Zpt_RMPFuncl_gluontag_gens[i]->Write();

    h_Zpt_RpT_gluontag_genud[i]->Write();
    h_Zpt_RMPF_gluontag_genud[i]->Write();
    h_Zpt_RMPFjet1_gluontag_genud[i]->Write();
    h_Zpt_RMPFjetn_gluontag_genud[i]->Write();
    h_Zpt_RMPFuncl_gluontag_genud[i]->Write();
    }

    h_Zpt_RpT_quarktag[i]->Write();
    h_Zpt_RMPF_quarktag[i]->Write();
    h_Zpt_RMPFjet1_quarktag[i]->Write();
    h_Zpt_RMPFjetn_quarktag[i]->Write();
    h_Zpt_RMPFuncl_quarktag[i]->Write();
    h_JetPt_QGL_quarktag[i]->Write();
    if(!isData){
    h_JetPt_QGL_quarktag_genb[i]->Write();
    h_JetPt_QGL_quarktag_genc[i]->Write();
    h_JetPt_QGL_quarktag_genuds[i]->Write();
    h_JetPt_QGL_quarktag_geng[i]->Write();
    h_JetPt_QGL_quarktag_unclassified[i]->Write();
    }
    h_Zpt_QGL_quarktag[i]->Write();
    if(!isData){
    h_Zpt_QGL_quarktag_genb[i]->Write();
    h_Zpt_QGL_quarktag_genc[i]->Write();
    h_Zpt_QGL_quarktag_genuds[i]->Write();
    h_Zpt_QGL_quarktag_geng[i]->Write();
    h_Zpt_QGL_quarktag_unclassified[i]->Write();
    }
    h_JetPt_muEF_quarktag[i]->Write();
    h_JetPt_chEmEF_quarktag[i]->Write();
    h_JetPt_chHEF_quarktag[i]->Write();
    h_JetPt_neEmEF_quarktag[i]->Write();
    h_JetPt_neHEF_quarktag[i]->Write();

    h_PtAve_QGL_quarktag[i]->Write();
    h_PtAve_muEF_quarktag[i]->Write();
    h_PtAve_chEmEF_quarktag[i]->Write();
    h_PtAve_chHEF_quarktag[i]->Write();
    h_PtAve_neEmEF_quarktag[i]->Write();
    h_PtAve_neHEF_quarktag[i]->Write();

    h_Zpt_Mu_Rho_quarktag[i]->Write();
    h_Zpt_Mu_NPVgood_quarktag[i]->Write();
    h_Zpt_Mu_NPVall_quarktag[i]->Write();
    h_Zpt_Mu_Mu_quarktag[i]->Write();
    h_Mu_RpT_quarktag[i]->Write();
    h_JetPt_RpT_quarktag[i]->Write();
    h_PtAve_RpT_quarktag[i]->Write();
    h_Mu_RMPF_quarktag[i]->Write();
    h_JetPt_RMPF_quarktag[i]->Write();
    h_PtAve_RMPF_quarktag[i]->Write();
    h_Mu_RMPFjet1_quarktag[i]->Write();
    h_JetPt_RMPFjet1_quarktag[i]->Write();
    h_PtAve_RMPFjet1_quarktag[i]->Write();
    h_Mu_RMPFjetn_quarktag[i]->Write();
    h_JetPt_RMPFjetn_quarktag[i]->Write();
    h_PtAve_RMPFjetn_quarktag[i]->Write();
    h_Mu_RMPFuncl_quarktag[i]->Write();
    h_JetPt_RMPFuncl_quarktag[i]->Write();
    h_PtAve_RMPFuncl_quarktag[i]->Write();

    if(!isData){
    h_Zpt_RpT_quarktag_genb[i]->Write();
    h_Zpt_RMPF_quarktag_genb[i]->Write();
    h_Zpt_RMPFjet1_quarktag_genb[i]->Write();
    h_Zpt_RMPFjetn_quarktag_genb[i]->Write();
    h_Zpt_RMPFuncl_quarktag_genb[i]->Write();
    h_Zpt_RpT_quarktag_genc[i]->Write();
    h_Zpt_RMPF_quarktag_genc[i]->Write();
    h_Zpt_RMPFjet1_quarktag_genc[i]->Write();
    h_Zpt_RMPFjetn_quarktag_genc[i]->Write();
    h_Zpt_RMPFuncl_quarktag_genc[i]->Write();
    h_Zpt_RpT_quarktag_genuds[i]->Write();
    h_Zpt_RMPF_quarktag_genuds[i]->Write();
    h_Zpt_RMPFjet1_quarktag_genuds[i]->Write();
    h_Zpt_RMPFjetn_quarktag_genuds[i]->Write();
    h_Zpt_RMPFuncl_quarktag_genuds[i]->Write();
    h_Zpt_RpT_quarktag_geng[i]->Write();
    h_Zpt_RMPF_quarktag_geng[i]->Write();
    h_Zpt_RMPFjet1_quarktag_geng[i]->Write();
    h_Zpt_RMPFjetn_quarktag_geng[i]->Write();
    h_Zpt_RMPFuncl_quarktag_geng[i]->Write();

    h_Zpt_RpT_quarktag_unclassified[i]->Write();
    h_Zpt_RMPF_quarktag_unclassified[i]->Write();
    h_Zpt_RMPFjet1_quarktag_unclassified[i]->Write();
    h_Zpt_RMPFjetn_quarktag_unclassified[i]->Write();
    h_Zpt_RMPFuncl_quarktag_unclassified[i]->Write();
    
    h_Zpt_RpT_quarktag_gens[i]->Write();
    h_Zpt_RMPF_quarktag_gens[i]->Write();
    h_Zpt_RMPFjet1_quarktag_gens[i]->Write();
    h_Zpt_RMPFjetn_quarktag_gens[i]->Write();
    h_Zpt_RMPFuncl_quarktag_gens[i]->Write();
    
    h_Zpt_RpT_quarktag_genud[i]->Write();
    h_Zpt_RMPF_quarktag_genud[i]->Write();
    h_Zpt_RMPFjet1_quarktag_genud[i]->Write();
    h_Zpt_RMPFjetn_quarktag_genud[i]->Write();
    h_Zpt_RMPFuncl_quarktag_genud[i]->Write();
    }
    
    h_Zpt_RpT_notag[i]->Write();
    h_Zpt_RMPF_notag[i]->Write();
    h_Zpt_RMPFjet1_notag[i]->Write();
    h_Zpt_RMPFjetn_notag[i]->Write();
    h_Zpt_RMPFuncl_notag[i]->Write();
    h_JetPt_QGL_notag[i]->Write();
    if(!isData){
    h_JetPt_QGL_notag_genb[i]->Write();
    h_JetPt_QGL_notag_genc[i]->Write();
    h_JetPt_QGL_notag_genuds[i]->Write();
    h_JetPt_QGL_notag_geng[i]->Write();
    h_JetPt_QGL_notag_unclassified[i]->Write();
    }
    h_Zpt_QGL_notag[i]->Write();
    if(!isData){
    h_Zpt_QGL_notag_genb[i]->Write();
    h_Zpt_QGL_notag_genc[i]->Write();
    h_Zpt_QGL_notag_genuds[i]->Write();
    h_Zpt_QGL_notag_geng[i]->Write();
    h_Zpt_QGL_notag_unclassified[i]->Write();
    }
    h_JetPt_muEF_notag[i]->Write();
    h_JetPt_chEmEF_notag[i]->Write();
    h_JetPt_chHEF_notag[i]->Write();
    h_JetPt_neEmEF_notag[i]->Write();
    h_JetPt_neHEF_notag[i]->Write();

    h_PtAve_QGL_notag[i]->Write();
    h_PtAve_muEF_notag[i]->Write();
    h_PtAve_chEmEF_notag[i]->Write();
    h_PtAve_chHEF_notag[i]->Write();
    h_PtAve_neEmEF_notag[i]->Write();
    h_PtAve_neHEF_notag[i]->Write();

    h_Zpt_Mu_Rho_notag[i]->Write();
    h_Zpt_Mu_NPVgood_notag[i]->Write();
    h_Zpt_Mu_NPVall_notag[i]->Write();
    h_Zpt_Mu_Mu_notag[i]->Write();
    h_Mu_RpT_notag[i]->Write();
    h_JetPt_RpT_notag[i]->Write();
    h_PtAve_RpT_notag[i]->Write();
    h_Mu_RMPF_notag[i]->Write();
    h_JetPt_RMPF_notag[i]->Write();
    h_PtAve_RMPF_notag[i]->Write();
    h_Mu_RMPFjet1_notag[i]->Write();
    h_JetPt_RMPFjet1_notag[i]->Write();
    h_PtAve_RMPFjet1_notag[i]->Write();
    h_Mu_RMPFjetn_notag[i]->Write();
    h_JetPt_RMPFjetn_notag[i]->Write();
    h_PtAve_RMPFjetn_notag[i]->Write();
    h_Mu_RMPFuncl_notag[i]->Write();
    h_JetPt_RMPFuncl_notag[i]->Write();
    h_PtAve_RMPFuncl_notag[i]->Write();

    if(!isData){
    h_Zpt_RpT_notag_genb[i]->Write();
    h_Zpt_RMPF_notag_genb[i]->Write();
    h_Zpt_RMPFjet1_notag_genb[i]->Write();
    h_Zpt_RMPFjetn_notag_genb[i]->Write();
    h_Zpt_RMPFuncl_notag_genb[i]->Write();
    h_Zpt_RpT_notag_genc[i]->Write();
    h_Zpt_RMPF_notag_genc[i]->Write();
    h_Zpt_RMPFjet1_notag_genc[i]->Write();
    h_Zpt_RMPFjetn_notag_genc[i]->Write();
    h_Zpt_RMPFuncl_notag_genc[i]->Write();
    h_Zpt_RpT_notag_genuds[i]->Write();
    h_Zpt_RMPF_notag_genuds[i]->Write();
    h_Zpt_RMPFjet1_notag_genuds[i]->Write();
    h_Zpt_RMPFjetn_notag_genuds[i]->Write();
    h_Zpt_RMPFuncl_notag_genuds[i]->Write();

    h_Zpt_RpT_notag_gens[i]->Write();
    h_Zpt_RMPF_notag_gens[i]->Write();
    h_Zpt_RMPFjet1_notag_gens[i]->Write();
    h_Zpt_RMPFjetn_notag_gens[i]->Write();
    h_Zpt_RMPFuncl_notag_gens[i]->Write();

    h_Zpt_RpT_notag_genud[i]->Write();
    h_Zpt_RMPF_notag_genud[i]->Write();
    h_Zpt_RMPFjet1_notag_genud[i]->Write();
    h_Zpt_RMPFjetn_notag_genud[i]->Write();
    h_Zpt_RMPFuncl_notag_genud[i]->Write();
    
    h_Zpt_RpT_notag_geng[i]->Write();
    h_Zpt_RMPF_notag_geng[i]->Write();
    h_Zpt_RMPFjet1_notag_geng[i]->Write();
    h_Zpt_RMPFjetn_notag_geng[i]->Write();
    h_Zpt_RMPFuncl_notag_geng[i]->Write();
    h_Zpt_RpT_notag_unclassified[i]->Write();
    h_Zpt_RMPF_notag_unclassified[i]->Write();
    h_Zpt_RMPFjet1_notag_unclassified[i]->Write();
    h_Zpt_RMPFjetn_notag_unclassified[i]->Write();
    h_Zpt_RMPFuncl_notag_unclassified[i]->Write();
    }
    
    if(!isData){
      h_Zpt_RZ[i]->Write();

      h_Zpt_RBal[i]->Write();
      h_Zpt_Resp[i]->Write();
      h_Zpt_RGMPF[i]->Write();
      h_Zpt_RGenjet1[i]->Write();
      h_Zpt_RGenjetn[i]->Write();
      h_Zpt_RGenuncl[i]->Write();

      h_JetPt_RBal[i]->Write();
      h_JetPt_RGenjet1[i]->Write();
      h_JetPt_RGenjetn[i]->Write();
      h_JetPt_RGenuncl[i]->Write();

      h_PtAve_RBal[i]->Write();
      h_PtAve_RGenjet1[i]->Write();
      h_PtAve_RGenjetn[i]->Write();
      h_PtAve_RGenuncl[i]->Write();

      h_Zpt_RBal_btag[i]->Write();
      h_Zpt_Resp_btag[i]->Write();
      h_Zpt_RGMPF_btag[i]->Write();
      h_Zpt_RGenjet1_btag[i]->Write();
      h_Zpt_RGenjetn_btag[i]->Write();
      h_Zpt_RGenuncl_btag[i]->Write();

      h_Zpt_RBal_ctag[i]->Write();
      h_Zpt_Resp_ctag[i]->Write();
      h_Zpt_RGMPF_ctag[i]->Write();
      h_Zpt_RGenjet1_ctag[i]->Write();
      h_Zpt_RGenjetn_ctag[i]->Write();
      h_Zpt_RGenuncl_ctag[i]->Write();

      h_Zpt_RBal_gluontag[i]->Write();
      h_Zpt_Resp_gluontag[i]->Write();
      h_Zpt_RGMPF_gluontag[i]->Write();
      h_Zpt_RGenjet1_gluontag[i]->Write();
      h_Zpt_RGenjetn_gluontag[i]->Write();
      h_Zpt_RGenuncl_gluontag[i]->Write();

      h_Zpt_RBal_quarktag[i]->Write();
      h_Zpt_Resp_quarktag[i]->Write();
      h_Zpt_RGMPF_quarktag[i]->Write();
      h_Zpt_RGenjet1_quarktag[i]->Write();
      h_Zpt_RGenjetn_quarktag[i]->Write();
      h_Zpt_RGenuncl_quarktag[i]->Write();

      h_Zpt_RBal_notag[i]->Write();
      h_Zpt_Resp_notag[i]->Write();
      h_Zpt_RGMPF_notag[i]->Write();
      h_Zpt_RGenjet1_notag[i]->Write();
      h_Zpt_RGenjetn_notag[i]->Write();
      h_Zpt_RGenuncl_notag[i]->Write();

      
      h_Zpt_RBal_genb[i]->Write();
      h_Zpt_Resp_genb[i]->Write();
      h_Zpt_RGMPF_genb[i]->Write();
      h_Zpt_RGenjet1_genb[i]->Write();
      h_Zpt_RGenjetn_genb[i]->Write();
      h_Zpt_RGenuncl_genb[i]->Write();

      h_Zpt_RBal_btag_genb[i]->Write();
      h_Zpt_Resp_btag_genb[i]->Write();
      h_Zpt_RGMPF_btag_genb[i]->Write();
      h_Zpt_RGenjet1_btag_genb[i]->Write();
      h_Zpt_RGenjetn_btag_genb[i]->Write();
      h_Zpt_RGenuncl_btag_genb[i]->Write();

      h_Zpt_RBal_ctag_genb[i]->Write();
      h_Zpt_Resp_ctag_genb[i]->Write();
      h_Zpt_RGMPF_ctag_genb[i]->Write();
      h_Zpt_RGenjet1_ctag_genb[i]->Write();
      h_Zpt_RGenjetn_ctag_genb[i]->Write();
      h_Zpt_RGenuncl_ctag_genb[i]->Write();

      h_Zpt_RBal_gluontag_genb[i]->Write();
      h_Zpt_Resp_gluontag_genb[i]->Write();
      h_Zpt_RGMPF_gluontag_genb[i]->Write(); 
      h_Zpt_RGenjet1_gluontag_genb[i]->Write();
      h_Zpt_RGenjetn_gluontag_genb[i]->Write();
      h_Zpt_RGenuncl_gluontag_genb[i]->Write();

      h_Zpt_RBal_quarktag_genb[i]->Write();
      h_Zpt_Resp_quarktag_genb[i]->Write();
      h_Zpt_RGMPF_quarktag_genb[i]->Write(); 
      h_Zpt_RGenjet1_quarktag_genb[i]->Write();
      h_Zpt_RGenjetn_quarktag_genb[i]->Write();
      h_Zpt_RGenuncl_quarktag_genb[i]->Write();

      h_Zpt_RBal_notag_genb[i]->Write();
      h_Zpt_Resp_notag_genb[i]->Write();
      h_Zpt_RGMPF_notag_genb[i]->Write();
      h_Zpt_RGenjet1_notag_genb[i]->Write();
      h_Zpt_RGenjetn_notag_genb[i]->Write();
      h_Zpt_RGenuncl_notag_genb[i]->Write();

      h_Zpt_RBal_genc[i]->Write();
      h_Zpt_Resp_genc[i]->Write();
      h_Zpt_RGMPF_genc[i]->Write();
      h_Zpt_RGenjet1_genc[i]->Write();
      h_Zpt_RGenjetn_genc[i]->Write();
      h_Zpt_RGenuncl_genc[i]->Write();

      h_Zpt_RBal_btag_genc[i]->Write();
      h_Zpt_Resp_btag_genc[i]->Write();
      h_Zpt_RGMPF_btag_genc[i]->Write();
      h_Zpt_RGenjet1_btag_genc[i]->Write();
      h_Zpt_RGenjetn_btag_genc[i]->Write();
      h_Zpt_RGenuncl_btag_genc[i]->Write();

      h_Zpt_RBal_ctag_genc[i]->Write();
      h_Zpt_Resp_ctag_genc[i]->Write();
      h_Zpt_RGMPF_ctag_genc[i]->Write();
      h_Zpt_RGenjet1_ctag_genc[i]->Write();
      h_Zpt_RGenjetn_ctag_genc[i]->Write();
      h_Zpt_RGenuncl_ctag_genc[i]->Write();

      h_Zpt_RBal_gluontag_genc[i]->Write();
      h_Zpt_Resp_gluontag_genc[i]->Write();
      h_Zpt_RGMPF_gluontag_genc[i]->Write();
      h_Zpt_RGenjet1_gluontag_genc[i]->Write();
      h_Zpt_RGenjetn_gluontag_genc[i]->Write();
      h_Zpt_RGenuncl_gluontag_genc[i]->Write();

      h_Zpt_RBal_quarktag_genc[i]->Write();
      h_Zpt_Resp_quarktag_genc[i]->Write();
      h_Zpt_RGMPF_quarktag_genc[i]->Write();
      h_Zpt_RGenjet1_quarktag_genc[i]->Write();
      h_Zpt_RGenjetn_quarktag_genc[i]->Write();
      h_Zpt_RGenuncl_quarktag_genc[i]->Write();

      h_Zpt_RBal_notag_genc[i]->Write();
      h_Zpt_Resp_notag_genc[i]->Write();
      h_Zpt_RGMPF_notag_genc[i]->Write();
      h_Zpt_RGenjet1_notag_genc[i]->Write();
      h_Zpt_RGenjetn_notag_genc[i]->Write();
      h_Zpt_RGenuncl_notag_genc[i]->Write();


      h_Zpt_RBal_genuds[i]->Write();
      h_Zpt_Resp_genuds[i]->Write();
      h_Zpt_RGMPF_genuds[i]->Write();
      h_Zpt_RGenjet1_genuds[i]->Write();
      h_Zpt_RGenjetn_genuds[i]->Write();
      h_Zpt_RGenuncl_genuds[i]->Write();

      h_Zpt_RBal_btag_genuds[i]->Write();
      h_Zpt_Resp_btag_genuds[i]->Write();
      h_Zpt_RGMPF_btag_genuds[i]->Write();
      h_Zpt_RGenjet1_btag_genuds[i]->Write();
      h_Zpt_RGenjetn_btag_genuds[i]->Write();
      h_Zpt_RGenuncl_btag_genuds[i]->Write();

      h_Zpt_RBal_ctag_genuds[i]->Write();
      h_Zpt_Resp_ctag_genuds[i]->Write();
      h_Zpt_RGMPF_ctag_genuds[i]->Write();
      h_Zpt_RGenjet1_ctag_genuds[i]->Write();
      h_Zpt_RGenjetn_ctag_genuds[i]->Write();
      h_Zpt_RGenuncl_ctag_genuds[i]->Write();

      h_Zpt_RBal_gluontag_genuds[i]->Write();
      h_Zpt_Resp_gluontag_genuds[i]->Write();
      h_Zpt_RGMPF_gluontag_genuds[i]->Write();
      h_Zpt_RGenjet1_gluontag_genuds[i]->Write();
      h_Zpt_RGenjetn_gluontag_genuds[i]->Write();
      h_Zpt_RGenuncl_gluontag_genuds[i]->Write();

      h_Zpt_RBal_quarktag_genuds[i]->Write();
      h_Zpt_Resp_quarktag_genuds[i]->Write();
      h_Zpt_RGMPF_quarktag_genuds[i]->Write();
      h_Zpt_RGenjet1_quarktag_genuds[i]->Write();
      h_Zpt_RGenjetn_quarktag_genuds[i]->Write();
      h_Zpt_RGenuncl_quarktag_genuds[i]->Write();

      h_Zpt_RBal_notag_genuds[i]->Write();
      h_Zpt_Resp_notag_genuds[i]->Write();
      h_Zpt_RGMPF_notag_genuds[i]->Write();
      h_Zpt_RGenjet1_notag_genuds[i]->Write();
      h_Zpt_RGenjetn_notag_genuds[i]->Write();
      h_Zpt_RGenuncl_notag_genuds[i]->Write();
      
      h_Zpt_RBal_geng[i]->Write();
      h_Zpt_Resp_geng[i]->Write();
      h_Zpt_RGMPF_geng[i]->Write();
      h_Zpt_RGenjet1_geng[i]->Write();
      h_Zpt_RGenjetn_geng[i]->Write();
      h_Zpt_RGenuncl_geng[i]->Write();

      h_Zpt_RBal_btag_geng[i]->Write();
      h_Zpt_Resp_btag_geng[i]->Write();
      h_Zpt_RGMPF_btag_geng[i]->Write();
      h_Zpt_RGenjet1_btag_geng[i]->Write();
      h_Zpt_RGenjetn_btag_geng[i]->Write();
      h_Zpt_RGenuncl_btag_geng[i]->Write();

      h_Zpt_RBal_ctag_geng[i]->Write();
      h_Zpt_Resp_ctag_geng[i]->Write();
      h_Zpt_RGMPF_ctag_geng[i]->Write();
      h_Zpt_RGenjet1_ctag_geng[i]->Write();
      h_Zpt_RGenjetn_ctag_geng[i]->Write();
      h_Zpt_RGenuncl_ctag_geng[i]->Write();

      h_Zpt_RBal_gluontag_geng[i]->Write();
      h_Zpt_Resp_gluontag_geng[i]->Write();
      h_Zpt_RGMPF_gluontag_geng[i]->Write();
      h_Zpt_RGenjet1_gluontag_geng[i]->Write();
      h_Zpt_RGenjetn_gluontag_geng[i]->Write();
      h_Zpt_RGenuncl_gluontag_geng[i]->Write();

      h_Zpt_RBal_quarktag_geng[i]->Write();
      h_Zpt_Resp_quarktag_geng[i]->Write();
      h_Zpt_RGMPF_quarktag_geng[i]->Write();
      h_Zpt_RGenjet1_quarktag_geng[i]->Write();
      h_Zpt_RGenjetn_quarktag_geng[i]->Write();
      h_Zpt_RGenuncl_quarktag_geng[i]->Write();

      h_Zpt_RBal_notag_geng[i]->Write();
      h_Zpt_Resp_notag_geng[i]->Write();
      h_Zpt_RGMPF_notag_geng[i]->Write();
      h_Zpt_RGenjet1_notag_geng[i]->Write();
      h_Zpt_RGenjetn_notag_geng[i]->Write();
      h_Zpt_RGenuncl_notag_geng[i]->Write();


      h_Zpt_RBal_unclassified[i]->Write();
      h_Zpt_Resp_unclassified[i]->Write();
      h_Zpt_RGMPF_unclassified[i]->Write();
      h_Zpt_RGenjet1_unclassified[i]->Write();
      h_Zpt_RGenjetn_unclassified[i]->Write();
      h_Zpt_RGenuncl_unclassified[i]->Write();

      h_Zpt_RBal_btag_unclassified[i]->Write();
      h_Zpt_Resp_btag_unclassified[i]->Write();
      h_Zpt_RGMPF_btag_unclassified[i]->Write();
      h_Zpt_RGenjet1_btag_unclassified[i]->Write();
      h_Zpt_RGenjetn_btag_unclassified[i]->Write();
      h_Zpt_RGenuncl_btag_unclassified[i]->Write();

      h_Zpt_RBal_ctag_unclassified[i]->Write();
      h_Zpt_Resp_ctag_unclassified[i]->Write();
      h_Zpt_RGMPF_ctag_unclassified[i]->Write();
      h_Zpt_RGenjet1_ctag_unclassified[i]->Write();
      h_Zpt_RGenjetn_ctag_unclassified[i]->Write();
      h_Zpt_RGenuncl_ctag_unclassified[i]->Write();

      h_Zpt_RBal_gluontag_unclassified[i]->Write();
      h_Zpt_Resp_gluontag_unclassified[i]->Write();
      h_Zpt_RGMPF_gluontag_unclassified[i]->Write();
      h_Zpt_RGenjet1_gluontag_unclassified[i]->Write();
      h_Zpt_RGenjetn_gluontag_unclassified[i]->Write();
      h_Zpt_RGenuncl_gluontag_unclassified[i]->Write();

      h_Zpt_RBal_quarktag_unclassified[i]->Write();
      h_Zpt_Resp_quarktag_unclassified[i]->Write();
      h_Zpt_RGMPF_quarktag_unclassified[i]->Write();
      h_Zpt_RGenjet1_quarktag_unclassified[i]->Write();
      h_Zpt_RGenjetn_quarktag_unclassified[i]->Write();
      h_Zpt_RGenuncl_quarktag_unclassified[i]->Write();
      
      h_Zpt_RBal_notag_unclassified[i]->Write();
      h_Zpt_Resp_notag_unclassified[i]->Write();
      h_Zpt_RGMPF_notag_unclassified[i]->Write();
      h_Zpt_RGenjet1_notag_unclassified[i]->Write();
      h_Zpt_RGenjetn_notag_unclassified[i]->Write();
      h_Zpt_RGenuncl_notag_unclassified[i]->Write();
      /*      
      h_Zpt_RGMPF_genc[i]->Write();
      h_Zpt_RGenjet1_genc[i]->Write();
      h_Zpt_RGenjetn_genc[i]->Write();
      h_Zpt_RGenuncl_genc[i]->Write();
      h_Zpt_RJet1_genc[i]->Write();
      h_Zpt_RZ_genc[i]->Write();

      h_Zpt_RGMPF_genc[i]->Write();
      h_Zpt_RGenjet1_genc[i]->Write();
      h_Zpt_RGenjetn_genc[i]->Write();
      h_Zpt_RGenuncl_genc[i]->Write();
      h_Zpt_RJet1_genc[i]->Write();
      h_Zpt_RZ_genc[i]->Write();

      h_Zpt_RGMPF_genuds[i]->Write();
      h_Zpt_RGenjet1_genuds[i]->Write();
      h_Zpt_RGenjetn_genuds[i]->Write();
      h_Zpt_RGenuncl_genuds[i]->Write();
      h_Zpt_RJet1_genuds[i]->Write();
      h_Zpt_RZ_genuds[i]->Write();

      h_Zpt_RGMPF_geng[i]->Write();
      h_Zpt_RGenjet1_geng[i]->Write();
      h_Zpt_RGenjetn_geng[i]->Write();
      h_Zpt_RGenuncl_geng[i]->Write();
      h_Zpt_RJet1_geng[i]->Write();
      h_Zpt_RZ_geng[i]->Write();
      */

      h_pTgen_Resp[i]->Write();
      h_pTgen_Resp_btag[i]->Write();
      h_pTgen_Resp_ctag[i]->Write();
      h_pTgen_Resp_gluontag[i]->Write();
      h_pTgen_Resp_quarktag[i]->Write();
      h_pTgen_Resp_notag[i]->Write();

      h_pTgen_Resp_genb[i]->Write();
      h_pTgen_Resp_btag_genb[i]->Write();
      h_pTgen_Resp_ctag_genb[i]->Write();
      h_pTgen_Resp_gluontag_genb[i]->Write();
      h_pTgen_Resp_quarktag_genb[i]->Write();
      h_pTgen_Resp_notag_genb[i]->Write();
      
      h_pTgen_Resp_genc[i]->Write();
      h_pTgen_Resp_btag_genc[i]->Write();
      h_pTgen_Resp_ctag_genc[i]->Write();
      h_pTgen_Resp_gluontag_genc[i]->Write();
      h_pTgen_Resp_quarktag_genc[i]->Write();
      h_pTgen_Resp_notag_genc[i]->Write();

      h_pTgen_Resp_genuds[i]->Write();
      h_pTgen_Resp_btag_genuds[i]->Write();
      h_pTgen_Resp_ctag_genuds[i]->Write();
      h_pTgen_Resp_gluontag_genuds[i]->Write();
      h_pTgen_Resp_quarktag_genuds[i]->Write();
      h_pTgen_Resp_notag_genuds[i]->Write();

      h_pTgen_Resp_geng[i]->Write();
      h_pTgen_Resp_btag_geng[i]->Write();
      h_pTgen_Resp_ctag_geng[i]->Write();
      h_pTgen_Resp_gluontag_geng[i]->Write();
      h_pTgen_Resp_quarktag_geng[i]->Write();
      h_pTgen_Resp_notag_geng[i]->Write();

      h_pTgen_Resp_unclassified[i]->Write();
      h_pTgen_Resp_btag_unclassified[i]->Write();
      h_pTgen_Resp_ctag_unclassified[i]->Write();
      h_pTgen_Resp_gluontag_unclassified[i]->Write();
      h_pTgen_Resp_quarktag_unclassified[i]->Write();
      h_pTgen_Resp_notag_unclassified[i]->Write();
    }

    /*
    std::cout << "Jet flavor fraction check incl. vs sum " << h_Zpt_RpT_btagDeepFlavBtight[i]->GetName() << " " << h_Zpt_RpT_btagDeepFlavBtight[i]->GetEntries() << " " 
              << h_Zpt_RpT_btagDeepFlavBtight_genb[i]->GetEntries()+
                 h_Zpt_RpT_btagDeepFlavBtight_genc[i]->GetEntries()+
                 h_Zpt_RpT_btagDeepFlavBtight_geng[i]->GetEntries()+
                 h_Zpt_RpT_btagDeepFlavBtight_genuds[i]->GetEntries() << std::endl;
    */
    fOUT->cd("analysis");
  }
  for(size_t i = 0; i < nbins_alpha*npsWeights; ++i){
    size_t ipsw = i/(nbins_alpha);

    fOUT->cd();
    fOUT->cd("analysis");
    /*
    if(ipsw > 0){
      std::string s_psw = "analysis/PSWeight"+std::to_string(ipsw-1);
      if(!fOUT->cd(s_psw.c_str())) {
        fOUT->mkdir(s_psw.c_str());
      }
      fOUT->cd(s_psw.c_str());
    }
    */
    h3D_Zpt_RMPF_mu[i]->Write();
    h3D_Zpt_RMPFx_mu[i]->Write();
    h3D_Zpt_RMPF_eta[i]->Write();
    h3D_Zpt_RMPFx_eta[i]->Write();

    tprof2D_eta_Zpt_RMPF[i]->Write();
    tprof2D_eta_Zpt_RMPFx[i]->Write();
    fOUT->cd("analysis");
  }

  fOUT->mkdir("l2res");
  fOUT->cd("l2res");
  l2resHistograms->write();
  fOUT->Close();


  std::string resultDir = std::string(this->GetParameter<TNamed*>("outputdir")->GetTitle());
  pickEvents->Write(resultDir+"/pickevents.txt");
}

void Analysis::Init(TTree *tree){
  //std::cout << "check Init begin" << std::endl;
  if (!tree) std::cout << " no tree" << std::endl;
  if (!tree) return;
  fChain = tree;
  fChain->SetMakeClass(1);

  setupTriggerBranches(fChain);
  if(isData)
    setupMETCleaningBranches(fChain);
  else
    setupGenLevelBranches(fChain);
  setupBranches(fChain);

  std::string runPeriod(year);
  std::regex run_re("_Run(\\d+\\S)_");
  std::regex runs_re("_Run(\\d+\\S+?)_");
  std::regex year_re("_Run(\\d+)\\S_");
  std::smatch match;
  std::string dsetName = std::string(datasetName);
  if (std::regex_search(dsetName, match, run_re) && match.size() > 0) {
    runPeriod = match.str(1);
      if(runPeriod == "2023C"){
	std::regex run_re("_Run2023C_22Sep2023_v(\\d)");
	if (std::regex_search(dsetName, match, run_re) && match.size() > 0) {
	  runPeriod += match.str(1);
	}
      }
  }
  std::string runs = "";
  std::string resultDir = std::string(this->GetParameter<TNamed*>("outputdir")->GetTitle());
  if (std::regex_search(resultDir, match, runs_re) && match.size() > 0) {
    runs = match.str(1);
    runs = runs.replace(0,4,"");
  }

  std::string prefix = "";
  std::string jrprefix = "";
  std::string version = "";
  std::string data = "DATA";
  std::string jrversion = "JRVx";

  // Jet corr txt files at https://github.com/cms-jet/JECDatabase/tree/master/textFiles
  const char* JEC_L1RC = 0;
  const char* JEC_L1FastJet = 0;
  const char* JEC_L2Relative = 0;
  const char* JEC_L3Absolute = 0;
  const char* JEC_L2L3Residual = 0;
  const char* JR_JetResolution = 0;
  const char* JR_JetResolutionScaleFactor = 0;

  const char *sd = "src/CondFormats/JetMETObjects/data";

  if(JECs.size() > 0){
    for(std::vector<std::string>::const_iterator i = JECs.begin(); i != JECs.end(); ++i){
      if(i->find("L1RC") != std::string::npos) JEC_L1RC = i->c_str();
      if(i->find("L1FastJet") != std::string::npos) JEC_L1FastJet = i->c_str();
      if(i->find("L2Relative") != std::string::npos) JEC_L2Relative = i->c_str();
      if(i->find("L3Absolute") != std::string::npos) JEC_L3Absolute = i->c_str();
      if(i->find("L2L3Residual") != std::string::npos) JEC_L2L3Residual = i->c_str();
      if(i->find("PtResolution") != std::string::npos) JR_JetResolution = i->c_str();
      if(i->find("SF") != std::string::npos) JR_JetResolutionScaleFactor = i->c_str();
    }
  }else{
    std::cout << "\033[1;31mWARNING, " << datasetName << " no JECs used!\033[0m" << std::endl; 
  }
  /*
  //std::cout << "check year " << year << " " << runPeriod << std::endl;
  if(std::string(year).compare("2016") == 0){
    if(std::string("BCDEF").find(runs) < 5 ){
      if(std::string(runPeriod).compare("2016B") == 0 ||
         std::string(runPeriod).compare("2016C") == 0 ||
         std::string(runPeriod).compare("2016D") == 0){
	  JEC_L1RC         = "Summer19UL16APV_RunBCD_V7_DATA_L1RC_AK4PFchs.txt";
	  JEC_L1FastJet    = "Summer19UL16APV_RunBCD_V7_DATA_L1FastJet_AK4PFchs.txt";
	  JEC_L2Relative   = "Summer19UL16APV_RunBCD_V7_DATA_L2Relative_AK4PFchs.txt";
	  JEC_L3Absolute   = "Summer19UL16APV_RunBCD_V7_DATA_L3Absolute_AK4PFchs.txt";
	  JEC_L2L3Residual = "Summer19UL16APV_RunBCD_V7_DATA_L2L3Residual_AK4PFchs.txt";
	  JR_JetResolution            = "Summer20UL16APV_JRV3_DATA_PtResolution_AK4PFchs.txt";
	  JR_JetResolutionScaleFactor = "Summer20UL16APV_JRV3_DATA_SF_AK4PFchs.txt";
      }
      if(std::string(runPeriod).compare("2016E") == 0 ||
         std::string(runPeriod).compare("2016F") == 0){
	JEC_L1RC         = "Summer19UL16APV_RunEF_V7_DATA_L1RC_AK4PFchs.txt";
	JEC_L1FastJet    = "Summer19UL16APV_RunEF_V7_DATA_L1FastJet_AK4PFchs.txt";
	JEC_L2Relative   = "Summer19UL16APV_RunEF_V7_DATA_L2Relative_AK4PFchs.txt";
	JEC_L3Absolute   = "Summer19UL16APV_RunEF_V7_DATA_L3Absolute_AK4PFchs.txt";
	JEC_L2L3Residual = "Summer19UL16APV_RunEF_V7_DATA_L2L3Residual_AK4PFchs.txt";
	JR_JetResolution            = "Summer20UL16APV_JRV3_DATA_PtResolution_AK4PFchs.txt";
	JR_JetResolutionScaleFactor = "Summer20UL16APV_JRV3_DATA_SF_AK4PFchs.txt";
      }
      if(!isData) {
	JEC_L1RC         = "Summer19UL16APV_V7_MC_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL16APV_V7_MC_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL16APV_V7_MC_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL16APV_V7_MC_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL16APV_V7_MC_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer20UL16APV_JRV3_MC_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer20UL16APV_JRV3_MC_SF_AK4PFchs.txt";
      }
    }
    if(std::string("FGH").find(runs) < 3){
      if(isData) {
	JEC_L1RC         = "Summer19UL16_RunFGH_V7_DATA_L1RC_AK4PFchs.txt";
	JEC_L1FastJet    = "Summer19UL16_RunFGH_V7_DATA_L1FastJet_AK4PFchs.txt";
	JEC_L2Relative   = "Summer19UL16_RunFGH_V7_DATA_L2Relative_AK4PFchs.txt";
	JEC_L3Absolute   = "Summer19UL16_RunFGH_V7_DATA_L3Absolute_AK4PFchs.txt";
	JEC_L2L3Residual = "Summer19UL16_RunFGH_V7_DATA_L2L3Residual_AK4PFchs.txt";
	JR_JetResolution            = "Summer20UL16_JRV3_DATA_PtResolution_AK4PFchs.txt";
	JR_JetResolutionScaleFactor = "Summer20UL16_JRV3_DATA_SF_AK4PFchs.txt";
      }else{
	JEC_L1RC         = "Summer19UL16_V7_MC_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL16_V7_MC_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL16_V7_MC_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL16_V7_MC_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL16_V7_MC_L2L3Residual_AK4PFchs.txt";
	JR_JetResolution            = "Summer20UL16_JRV3_MC_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer20UL16_JRV3_MC_SF_AK4PFchs.txt";
      }
    }
  }

  if(std::string(year).compare("2017") == 0){
    if(std::string(runs).find("B") < runs.length()){
      if(isData) {
        JEC_L1RC         = "Summer19UL17_RunB_V6_DATA_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL17_RunB_V6_DATA_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL17_RunB_V6_DATA_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL17_RunB_V6_DATA_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL17_RunB_V6_DATA_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL17_JRV2_DATA_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL17_JRV2_DATA_SF_AK4PFchs.txt";
      }else{
        JEC_L1RC         = "Summer19UL17_V6_MC_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL17_V6_MC_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL17_V6_MC_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL17_V6_MC_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL17_V6_MC_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL17_JRV2_MC_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL17_JRV2_MC_SF_AK4PFchs.txt";
      }
    }
    if(std::string(runs).find("C") < runs.length()){
      if(isData) {
        JEC_L1RC         = "Summer19UL17_RunC_V6_DATA_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL17_RunC_V6_DATA_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL17_RunC_V6_DATA_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL17_RunC_V6_DATA_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL17_RunC_V6_DATA_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL17_JRV2_DATA_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL17_JRV2_DATA_SF_AK4PFchs.txt";
      }else{
        JEC_L1RC         = "Summer19UL17_V6_MC_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL17_V6_MC_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL17_V6_MC_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL17_V6_MC_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL17_V6_MC_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL17_JRV2_MC_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL17_JRV2_MC_SF_AK4PFchs.txt";
      }
    }
    if(std::string(runs).find("D") < runs.length()){
      if(isData) {
        JEC_L1RC         = "Summer19UL17_RunD_V6_DATA_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL17_RunD_V6_DATA_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL17_RunD_V6_DATA_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL17_RunD_V6_DATA_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL17_RunD_V6_DATA_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL17_JRV2_DATA_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL17_JRV2_DATA_SF_AK4PFchs.txt";
      }else{
        JEC_L1RC         = "Summer19UL17_V6_MC_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL17_V6_MC_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL17_V6_MC_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL17_V6_MC_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL17_V6_MC_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL17_JRV2_MC_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL17_JRV2_MC_SF_AK4PFchs.txt";
      }
    }
    if(std::string(runs).find("E") < runs.length()){
      if(isData) {
        JEC_L1RC         = "Summer19UL17_RunE_V6_DATA_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL17_RunE_V6_DATA_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL17_RunE_V6_DATA_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL17_RunE_V6_DATA_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL17_RunE_V6_DATA_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL17_JRV2_DATA_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL17_JRV2_DATA_SF_AK4PFchs.txt";
      }else{
        JEC_L1RC         = "Summer19UL17_V6_MC_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL17_V6_MC_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL17_V6_MC_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL17_V6_MC_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL17_V6_MC_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL17_JRV2_MC_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL17_JRV2_MC_SF_AK4PFchs.txt";
      }
    }
    if(std::string(runs).find("F") < runs.length()){
      if(isData) {
        JEC_L1RC         = "Summer19UL17_RunF_V6_DATA_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL17_RunF_V6_DATA_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL17_RunF_V6_DATA_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL17_RunF_V6_DATA_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL17_RunF_V6_DATA_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL17_JRV2_DATA_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL17_JRV2_DATA_SF_AK4PFchs.txt";
      }else{
        JEC_L1RC         = "Summer19UL17_V6_MC_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL17_V6_MC_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL17_V6_MC_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL17_V6_MC_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL17_V6_MC_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL17_JRV2_MC_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL17_JRV2_MC_SF_AK4PFchs.txt";
      }
    }
  }
  //std::cout << "Run period " << runPeriod << " " << runs << " " << year << " " << resultDir << std::endl;
  if(std::string(year).compare("2018") == 0){
    if(std::string(runs).find("A") < runs.length()){
      if(isData) {
        JEC_L1RC         = "Summer19UL18_RunA_V5_DATA_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL18_RunA_V5_DATA_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL18_RunA_V5_DATA_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL18_RunA_V5_DATA_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL18_RunA_V5_DATA_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL18_JRV2_DATA_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL18_JRV2_DATA_SF_AK4PFchs.txt";
      }else{
        JEC_L1RC         = "Summer19UL18_V5_MC_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL18_V5_MC_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL18_V5_MC_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL18_V5_MC_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL18_V5_MC_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL18_JRV2_MC_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL18_JRV2_MC_SF_AK4PFchs.txt";
      }
    }
    if(std::string(runs).find("B") < runs.length()){
      if(isData) {
        JEC_L1RC         = "Summer19UL18_RunB_V5_DATA_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL18_RunB_V5_DATA_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL18_RunB_V5_DATA_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL18_RunB_V5_DATA_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL18_RunB_V5_DATA_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL18_JRV2_DATA_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL18_JRV2_DATA_SF_AK4PFchs.txt";
      }else{
        JEC_L1RC         = "Summer19UL18_V5_MC_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL18_V5_MC_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL18_V5_MC_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL18_V5_MC_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL18_V5_MC_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL18_JRV2_MC_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL18_JRV2_MC_SF_AK4PFchs.txt";
      } 
    }
    if(std::string(runs).find("C") < runs.length()){
      if(isData) {
        JEC_L1RC         = "Summer19UL18_RunC_V5_DATA_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL18_RunC_V5_DATA_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL18_RunC_V5_DATA_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL18_RunC_V5_DATA_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL18_RunC_V5_DATA_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL18_JRV2_DATA_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL18_JRV2_DATA_SF_AK4PFchs.txt";
      }else{
        JEC_L1RC         = "Summer19UL18_V5_MC_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL18_V5_MC_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL18_V5_MC_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL18_V5_MC_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL18_V5_MC_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL18_JRV2_MC_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL18_JRV2_MC_SF_AK4PFchs.txt";
      } 
    }
    if(std::string(runs).find("D") < runs.length()){
      if(isData) {
        JEC_L1RC         = "Summer19UL18_RunD_V5_DATA_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL18_RunD_V5_DATA_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL18_RunD_V5_DATA_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL18_RunD_V5_DATA_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL18_RunD_V5_DATA_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL18_JRV2_DATA_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL18_JRV2_DATA_SF_AK4PFchs.txt";
      }else{
        JEC_L1RC         = "Summer19UL18_V5_MC_L1RC_AK4PFchs.txt";
        JEC_L1FastJet    = "Summer19UL18_V5_MC_L1FastJet_AK4PFchs.txt";
        JEC_L2Relative   = "Summer19UL18_V5_MC_L2Relative_AK4PFchs.txt";
        JEC_L3Absolute   = "Summer19UL18_V5_MC_L3Absolute_AK4PFchs.txt";
        JEC_L2L3Residual = "Summer19UL18_V5_MC_L2L3Residual_AK4PFchs.txt";
        JR_JetResolution            = "Summer19UL18_JRV2_MC_PtResolution_AK4PFchs.txt";
        JR_JetResolutionScaleFactor = "Summer19UL18_JRV2_MC_SF_AK4PFchs.txt";
      } 
    }
  }

  if(std::string(year).compare("2022") == 0){
    if(std::string(runs).find("CD") < runs.length()){
      if(isData) {
	JEC_L2Relative   = "Summer22Run3_V1_MC_L2Relative_AK4PUPPI.txt";
	JEC_L2L3Residual = "Summer22-22Sep2023_Run2022CD_V3_DATA_L2L3Residual_AK4PFPuppi.txt";
	//JEC_L1FastJet    = "Winter22Run3_RunC_V2_DATA_L1FastJet_AK4PFPuppi.txt";
	//JEC_L2Relative   = "Summer22Run3_V1_MC_L2Relative_AK4PUPPI.txt";
	//JEC_L2Relative   = "Winter22Run3_RunC_V2_DATA_L2Relative_AK4PFPuppi.txt";
	//JEC_L3Absolute   = "Winter22Run3_RunC_V2_DATA_L3Absolute_AK4PFPuppi.txt";
	//JEC_L2L3Residual = "Run22CD-22Sep2023_DATA_L2L3Residual_AK4PFPuppi.txt";
	//JEC_L2L3Residual = "Winter22Run3_RunC_V2_DATA_L2L3Residual_AK4PFPuppi.txt";
	//JR_JetResolution            = "JR_Winter22Run3_V1_DATA_PtResolution_AK4PFPuppi.txt";
	//JR_JetResolutionScaleFactor = "JR_Winter22Run3_V1_DATA_SF_AK4PFPuppi.txt";
      }else{
	JEC_L2Relative   = "Summer22Run3_V1_MC_L2Relative_AK4PUPPI.txt";
	//JEC_L2Relative   = "Winter22Run3_V2_MC_L2Relative_AK4PFPuppi.txt";
	//JEC_L3Absolute   = "Winter22Run3_V2_MC_L3Absolute_AK4PFPuppi.txt";
	//JEC_L2L3Residual = "Winter22Run3_V2_MC_L2L3Residual_AK4PFPuppi.txt";
	JR_JetResolution            = "JR_Winter22Run3_V1_MC_PtResolution_AK4PFPuppi.txt";
	JR_JetResolutionScaleFactor = "JR_Winter22Run3_V1_MC_SF_AK4PFPuppi.txt";
      }
    }
    if(std::string(runs).find("EF") < runs.length() || std::string(runs).find("FG") < runs.length()){
      if(isData) {
	if(std::string(runPeriod).find("E") < runPeriod.length()){
	  JEC_L2Relative   = "Summer22EEVetoRun3_V1_MC_L2Relative_AK4PUPPI.txt";
	  JEC_L2L3Residual = "Summer22EE-22Sep2023_Run2022E_V3_DATA_L2L3Residual_AK4PFPuppi.txt";
	  //JEC_L2Relative   = "Winter23Prompt23_RunA_V1_DATA_L2Relative_AK4PFPuppi.txt";
	  //JEC_L3Absolute   = "Winter23Prompt23_RunA_V1_DATA_L3Absolute_AK4PFPuppi.txt";
	  //JEC_L2L3Residual = "Run22E-22Sep2023_DATA_L2L3Residual_AK4PFPuppi.txt";
	  //JEC_L2L3Residual = "Winter23Prompt23_RunA_V1_DATA_L2L3Residual_AK4PFPuppi.txt";
	  //JR_JetResolution            = "JR_Winter22Run3_V1_DATA_PtResolution_AK4PFPuppi.txt";
	  //JR_JetResolutionScaleFactor = "JR_Winter22Run3_V1_DATA_SF_AK4PFPuppi.txt";
	}
	if(std::string(runPeriod).find("F") < runPeriod.length()){
          JEC_L2Relative   = "Summer22EEVetoRun3_V1_MC_L2Relative_AK4PUPPI.txt";
	  JEC_L2L3Residual = "Summer22EEPrompt22_Run2022F_V3_DATA_L2L3Residual_AK4PFPuppi.txt";
	  //JEC_L2Relative   = "Winter23Prompt23_RunA_V1_DATA_L2Relative_AK4PFPuppi.txt";
	  //JEC_L3Absolute   = "Winter23Prompt23_RunA_V1_DATA_L3Absolute_AK4PFPuppi.txt";
	  //JEC_L2L3Residual = "Run22F-Prompt_DATA_L2L3Residual_AK4PFPuppi.txt";
	  //JEC_L2L3Residual = "Winter23Prompt23_RunA_V1_DATA_L2L3Residual_AK4PFPuppi.txt";
	  //JR_JetResolution            = "JR_Winter22Run3_V1_DATA_PtResolution_AK4PFPuppi.txt";
	  //JR_JetResolutionScaleFactor = "JR_Winter22Run3_V1_DATA_SF_AK4PFPuppi.txt";
	}
	if(std::string(runPeriod).find("G") < runPeriod.length()){
          JEC_L2Relative   = "Summer22EEVetoRun3_V1_MC_L2Relative_AK4PUPPI.txt";
	  JEC_L2L3Residual = "Summer22EEPrompt22_Run2022G_V3_DATA_L2L3Residual_AK4PFPuppi.txt";
	  //JEC_L2Relative   = "Winter23Prompt23_RunA_V1_DATA_L2Relative_AK4PFPuppi.txt";
	  //JEC_L2L3Residual = "Run22G-Prompt_DATA_L2L3Residual_AK4PFPuppi.txt";
	  //JEC_L2L3Residual = "Winter23Prompt23_RunA_V1_DATA_L2L3Residual_AK4PFPuppi.txt";
	  //JR_JetResolution            = "JR_Winter22Run3_V1_DATA_PtResolution_AK4PFPuppi.txt";
	  //JR_JetResolutionScaleFactor = "JR_Winter22Run3_V1_DATA_SF_AK4PFPuppi.txt";
        }
      }else{
        JEC_L2Relative   = "Summer22EEVetoRun3_V1_MC_L2Relative_AK4PUPPI.txt";
        //JEC_L2Relative   = "Winter23Prompt23_V1_MC_L2Relative_AK4PFPuppi.txt";
        //JEC_L3Absolute   = "Winter23Prompt23_V1_MC_L3Absolute_AK4PFPuppi.txt";
        //JEC_L2L3Residual = "Winter23Prompt23_V1_MC_L2L3Residual_AK4PFPuppi.txt";
        JR_JetResolution            = "JR_Winter22Run3_V1_MC_PtResolution_AK4PFPuppi.txt";
        JR_JetResolutionScaleFactor = "JR_Winter22Run3_V1_MC_SF_AK4PFPuppi.txt";
      }
    }
  }
  if(std::string(year).compare("2023") == 0){
    if(isData) {
      if(std::string(runPeriod).find("C1") < runPeriod.length() ||
	 std::string(runPeriod).find("C2") < runPeriod.length() ||
	 std::string(runPeriod).find("C3") < runPeriod.length()){
        JEC_L2Relative   = "Summer23Run3_V1_MC_L2Relative_AK4PUPPI.txt";
        //JEC_L2Relative   = "Winter23Prompt23_V2_MC_L2Relative_AK4PFPuppi.txt";
        //JEC_L2Relative   = "Summer22Run3_V1_MC_L2Relative_AK4PUPPI.txt";
        //JEC_L2L3Residual = "Summer23Prompt23_Run2023Cv123_V1_DATA_L2Residual_AK4PFPuppi.txt";
	JEC_L2L3Residual = "Summer23Prompt23_Run2023Cv123_V2_DATA_L2L3Residual_AK4PFPuppi.txt";
        //JEC_L2L3Residual = "Summer22Prompt23_Run2023Cv123_V3_DATA_L2L3Residual_AK4PFPUPPI.txt";
        //JEC_L2Relative   = "Winter23Prompt23_RunA_V1_DATA_L2Relative_AK4PFPuppi.txt";
        //JEC_L3Absolute   = "Winter23Prompt23_RunA_V1_DATA_L3Absolute_AK4PFPuppi.txt";
        //JEC_L2L3Residual = "Run23C123-Prompt_DATA_L2L3Residual_AK4PFPuppi.txt";
        //JEC_L2L3Residual = "Winter23Prompt23_RunA_V1_DATA_L2L3Residual_AK4PFPuppi.txt";
        //JR_JetResolution            = "JR_Winter22Run3_V1_DATA_PtResolution_AK4PFPuppi.txt";
	//JR_JetResolutionScaleFactor = "JR_Winter22Run3_V1_DATA_SF_AK4PFPuppi.txt";
      }
      if(std::string(runPeriod).find("C4") < runPeriod.length()){
        JEC_L2Relative   = "Summer23Run3_V1_MC_L2Relative_AK4PUPPI.txt";
        //JEC_L2Relative   = "Winter23Prompt23_V2_MC_L2Relative_AK4PFPuppi.txt";
        //JEC_L2Relative   = "Summer22Run3_V1_MC_L2Relative_AK4PUPPI.txt";
        //JEC_L2L3Residual = "Summer22Prompt23_Run2023Cv4_V3_DATA_L2L3Residual_AK4PFPUPPI.txt";
        //JEC_L2L3Residual = "Run23C4-Prompt_DATA_L2L3Residual_AK4PFPuppi.txt";
	//JEC_L2L3Residual = "Summer23Prompt23_Run2023Cv4_V1_DATA_L2Residual_AK4PFPuppi.txt";
	JEC_L2L3Residual = "Summer23Prompt23_Run2023Cv4_V2_DATA_L2L3Residual_AK4PFPuppi.txt";
        //JR_JetResolution            = "JR_Winter22Run3_V1_DATA_PtResolution_AK4PFPuppi.txt";
        //JR_JetResolutionScaleFactor = "JR_Winter22Run3_V1_DATA_SF_AK4PFPuppi.txt";
      }
      if(std::string(runPeriod).find("D") < runPeriod.length()){
	JEC_L2Relative   = "Summer23BPixRun3_V3_MC_L2Relative_AK4PUPPI.txt";
        //JEC_L2Relative   = "Winter23Prompt23_V2_MC_L2Relative_AK4PFPuppi.txt";
        //JEC_L2Relative   = "Summer22Run3_V1_MC_L2Relative_AK4PUPPI.txt";
        //JEC_L2L3Residual = "Summer22Prompt23_Run2023D_V3_DATA_L2L3Residual_AK4PFPUPPI.txt";
        //JEC_L2L3Residual = "Run23D-Prompt_DATA_L2L3Residual_AK4PFPuppi.txt";
	//JEC_L2L3Residual = "Summer23Prompt23_Run2023D_V1_DATA_L2Residual_AK4PFPuppi.txt";
	JEC_L2L3Residual = "Summer23Prompt23_Run2023D_V2_DATA_L2L3Residual_AK4PFPuppi.txt";
        //JR_JetResolution            = "JR_Winter22Run3_V1_DATA_PtResolution_AK4PFPuppi.txt";
        //JR_JetResolutionScaleFactor = "JR_Winter22Run3_V1_DATA_SF_AK4PFPuppi.txt";
      }
    }else{
      //std::cout << "check runs " << runs << std::endl;
      if(std::string(runs).find("C1") < runPeriod.length() ||
         std::string(runs).find("C2") < runPeriod.length() ||
         std::string(runs).find("C3") < runPeriod.length()){
	JEC_L2Relative   = "Summer23Run3_V1_MC_L2Relative_AK4PUPPI.txt";
        //JEC_L2L3Residual = "Summer23Prompt23_Run2023Cv123_V1_DATA_L2Residual_AK4PFPuppi.txt";
	//JEC_L2L3Residual = "Summer23Prompt23_Run2023Cv123_V2_DATA_L2L3Residual_AK4PFPuppi.txt";
        JR_JetResolution            = "JR_Winter22Run3_V1_DATA_PtResolution_AK4PFPuppi.txt";
        JR_JetResolutionScaleFactor = "Summer23_2023Cv123_JRV1_MC_SF_AK4PFPuppi.txt";
      }
      if(std::string(runs).find("C4") < runPeriod.length()){
	JEC_L2Relative   = "Summer23Run3_V1_MC_L2Relative_AK4PUPPI.txt";
        //JEC_L2L3Residual = "Summer23Prompt23_Run2023Cv4_V1_DATA_L2Residual_AK4PFPuppi.txt";
        //JEC_L2L3Residual = "Summer23Prompt23_Run2023Cv4_V2_DATA_L2L3Residual_AK4PFPuppi.txt";
        JR_JetResolution            = "JR_Winter22Run3_V1_DATA_PtResolution_AK4PFPuppi.txt";
        JR_JetResolutionScaleFactor = "Summer23_2023Cv4_JRV1_MC_SF_AK4PFPuppi.txt";
      }
      if(std::string(runs).find("D") < runs.length()){
	JEC_L2Relative   = "Summer23BPixRun3_V3_MC_L2Relative_AK4PUPPI.txt";
	//JEC_L2L3Residual = "Summer23Prompt23_Run2023D_V1_DATA_L2Residual_AK4PFPuppi.txt";
        //JEC_L2L3Residual = "Summer23Prompt23_Run2023D_V2_DATA_L2L3Residual_AK4PFPuppi.txt";
	JR_JetResolution            = "JR_Winter22Run3_V1_MC_PtResolution_AK4PFPuppi.txt";
	JR_JetResolutionScaleFactor = "Summer23_2023D_JRV1_MC_SF_AK4PFPuppi.txt";
      }
      //JR_JetResolution            = "JR_Winter22Run3_V1_MC_PtResolution_AK4PFPuppi.txt";
      //JR_JetResolutionScaleFactor = "JR_Winter22Run3_V1_MC_SF_AK4PFPuppi.txt";
    }
  }
  if(std::string(year).compare("2024") == 0){
    if(isData) {
      JEC_L2Relative   = "Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt";
      JEC_L2L3Residual = "Prompt24_Run2024BCD_V7M_DATA_L2L3Residual_AK4PFPuppi.txt";
      if(std::string(runPeriod).find("E") < runPeriod.length()){
        JEC_L2L3Residual = "Prompt24_Run2024E_V7M_DATA_L2L3Residual_AK4PFPuppi.txt";
      }
      if(std::string(runPeriod).find("F") < runPeriod.length()){
	JEC_L2L3Residual = "Prompt24_Run2024F_V7M_DATA_L2L3Residual_AK4PFPuppi.txt";
      }
      if(std::string(runPeriod).find("G") < runPeriod.length()){
        JEC_L2L3Residual = "Prompt24_Run2024G_V7M_DATA_L2L3Residual_AK4PFPuppi.txt";
      }
      if(std::string(runPeriod).find("H") < runPeriod.length()){
        JEC_L2L3Residual = "Prompt24_Run2024H_V7M_DATA_L2L3Residual_AK4PFPuppi.txt";
      }
      if(std::string(runPeriod).find("I") < runPeriod.length()){
        JEC_L2L3Residual = "Prompt24_Run2024I_V7M_DATA_L2L3Residual_AK4PFPuppi.txt";
      }
    }else{
      //if(std::string(runs).find("B") < runs.length()){
        JEC_L2Relative   = "Winter24Run3_V1_MC_L2Relative_AK4PUPPI.txt";
        JR_JetResolution            = "JR_Winter22Run3_V1_MC_PtResolution_AK4PFPuppi.txt";
        JR_JetResolutionScaleFactor = "Prompt24_2024BC_JRV1M_MC_SF_AK4PFPuppi.txt";
	//}
    }
  }
  */

  if(!isData) data = "MC";
  if(!smearingON){
    JR_JetResolution            = 0;
    JR_JetResolutionScaleFactor = 0;
  }
  //std::cout << "check prefix " << prefix << year << " " << runs << " " << runPeriod << std::endl;
  //std::cout << "check JEC " << JEC_L2Relative << " " << JEC_L2L3Residual << std::endl;
  //std::cout << "check JER " << JR_JetResolution << " " << JR_JetResolutionScaleFactor << std::endl;
  const char *st = Form("%s%s_%s_%s",prefix.c_str(),runPeriod.c_str(),version.c_str(),data.c_str());
  const char *s;

  std::vector<JetCorrectorParameters> v0;
  //s = Form("%s/%s_L1RC_AK4PFchs.txt",sd,st); std::cout << s << std::endl;
  if(JEC_L1RC != 0){
    s = Form("%s/%s",sd,JEC_L1RC); std::cout << "JECs " << datasetName << " " << JEC_L1RC << std::endl;
    if(!exists(std::string(s))) {
      std::cout << "File " << s << " not found" << std::endl;
      exit(1);
    }
    l1rc = new JetCorrectorParameters(s);
    v0.push_back(*l1rc);
  }

  std::vector<JetCorrectorParameters> v1;
  std::vector<JetCorrectorParameters> v2;
  std::vector<JetCorrectorParameters> v3;
  if(JEC_L1FastJet != 0){
    s = Form("%s/%s",sd,JEC_L1FastJet); std::cout << "JECs " << datasetName << " " << JEC_L1FastJet << std::endl;
    if(!exists(std::string(s))) {
      std::cout << "File " << s << " not found" << std::endl;
      exit(1);
    }
    l1 = new JetCorrectorParameters(s);
    v1.push_back(*l1);
    v2.push_back(*l1);
    v3.push_back(*l1);
  }
  if(JEC_L2Relative != 0){
    s = Form("%s/%s",sd,JEC_L2Relative); std::cout << "JECs " << datasetName << " " << JEC_L2Relative << std::endl;
    l2 = new JetCorrectorParameters(s);
    v1.push_back(*l2);
    v2.push_back(*l2);
    v3.push_back(*l2);
  }
  if(JEC_L3Absolute != 0){
    s = Form("%s/%s",sd,JEC_L3Absolute); std::cout << "JECs " << datasetName << " " << JEC_L3Absolute << std::endl;
    l3 = new JetCorrectorParameters(s);
    v1.push_back(*l3);
    v2.push_back(*l3);
    v3.push_back(*l3);
  }
  if(JEC_L2L3Residual != 0){
    s = Form("%s/%s",sd,JEC_L2L3Residual); std::cout << "JECs " << datasetName << " " << JEC_L2L3Residual << std::endl;
    l2l3 = new JetCorrectorParameters(s);
    v1.push_back(*l2l3);
    v2.push_back(*l2l3);
    v3.push_back(*l2l3);
  }

  jec_L1rc = 0;
  jec_L1fj = 0;
  jec_L1fjL2RelL2Abs = 0;
  jec_L1fjL2RelL2AbsL2L3Res = 0;
  if(v0.size() > 0) jec_L1rc = new FactorizedJetCorrector(v0);
  if(v1.size() > 0) jec_L1fj = new FactorizedJetCorrector(v1);
  if(v2.size() > 0) jec_L1fjL2RelL2Abs = new FactorizedJetCorrector(v2);
  if(v3.size() > 0) jec_L1fjL2RelL2AbsL2L3Res = new FactorizedJetCorrector(v3);

  //st = Form("%s_%s_%s",jrprefix.c_str(),jrversion.c_str(),data.c_str());

  if(JR_JetResolution != 0){
    s = Form("%s/%s",sd,JR_JetResolution); std::cout << "JERs " << datasetName << " " << JR_JetResolution << std::endl;
    m_jetResolution = new JME::JetResolution(s);
    s = Form("%s/%s",sd,JR_JetResolutionScaleFactor); std::cout << "JERs " << datasetName << " " << JR_JetResolutionScaleFactor << std::endl;
    //m_jetResolutionScaleFactor.reset(new JME::JetResolutionScaleFactor(s));
    vector<JetCorrectorParameters> v;
    JetCorrectorParameters *pres = new JetCorrectorParameters(s);
    v.push_back(*pres);
    jersf = new FactorizedJetCorrector(v);
  }

  int seed = 92837465;
  m_randomNumberGenerator = std::mt19937(seed);
  m_randomNumberGenerator2 = new TRandom3(seed);
  //std::cout << "check Init end" << std::endl;
}


template <class T>
T Analysis::GetParameter (const char* parameterName) {
  TList* inputList = this->GetInputList();
  T obj = (T)inputList->FindObject(parameterName);
  return obj;
}

std::vector<std::string> Analysis::GetParameters (const char* regexp) {
  std::vector<std::string> parameters;

  std::regex r(regexp);
  TList* inputList = this->GetInputList();
  for(int i = 0; i < inputList->GetEntries(); ++i){
    TNamed* name = (TNamed*)inputList->At(i);
    //std::cout << "check parameters " << name->GetName() << " " << name->GetTitle() << std::endl;
    if(std::regex_match(name->GetName(),r)) parameters.push_back(name->GetTitle());
  }
  //std::cout << "check para len " << parameters.size() << std::endl;
  return parameters;
}

void Analysis::setupTriggerBranches(TTree *tree){
  //triggerNames = this->GetParameters("trigger");
  
  /*
  const char* trg = this->GetParameter<TNamed*>("trigger")->GetTitle();
  //std::cout << "check trg " << trg << std::endl;
  triggerNames.clear();
  std::string s_trg = std::string(trg);
  size_t pos = s_trg.find("||");
  //std::cout << "check pos " << pos << std::endl;
  
  if (pos >= s_trg.size()){
    triggerNames.push_back(s_trg);
  }else{
    while (pos < s_trg.size()){
      pos = s_trg.find("||");
      triggerNames.push_back(s_trg.substr(0,pos));
      s_trg = s_trg.substr(pos+2,s_trg.size());
    }
  }
  */
    
  triggerBit = new Bool_t[triggerNames.size()];
  for(size_t i = 0; i < triggerNames.size(); ++i){
    if(tree->FindBranch(triggerNames[i].c_str()) == 0){ 
      std::cout << "\033[1;31mWarning, trying to access trigger " << triggerNames[i] << ", which does not exist in " << datasetName << "\033[0m" << std::endl;
      //std::cout << "Exiting..\n" << std::endl;
      //exit(1);
    }else tree->SetBranchAddress(triggerNames[i].c_str(),&triggerBit[i]);
  }
}

void Analysis::setupMETCleaningBranches(TTree *tree){
  filterFlags = this->GetParameters("Filters");
  filter = new Bool_t[filterFlags.size()];
  for(size_t i = 0; i < filterFlags.size(); ++i){
    tree->SetBranchAddress(filterFlags[i].c_str(),&filter[i]);
    //std::cout << "check flags " << filterFlags[i] << std::endl;
  }
}

void Analysis::setupGenLevelBranches(TTree *tree){
  
}

void Analysis::setupBranches(TTree *tree){
  tree->SetBranchAddress("run",&run, &b_run);
  tree->SetBranchAddress("luminosityBlock", &luminosityBlock, &b_luminosityBlock);
  tree->SetBranchAddress("event", &event, &b_event);

  if(!isData){
    tree->SetBranchAddress("Pileup_nTrueInt", &Pileup_nTrueInt, &b_Pileup_nTrueInt);
    tree->SetBranchAddress("nPSWeight", &nPSWeight, &b_nPSWeight);
    tree->SetBranchAddress("PSWeight", PSWeight, &b_PSWeight);
    //tree->SetBranchAddress("LHEWeight_originalXWGTUP", &LHEWeight_originalXWGTUP, &b_LHEWeight_originalXWGTUP);
  }
  
  if(atoi(year) < 2020){
    tree->SetBranchAddress("fixedGridRhoFastjetAll", &fixedGridRhoFastjetAll, &b_fixedGridRhoFastjetAll);
    tree->SetBranchAddress("fixedGridRhoFastjetCentral", &fixedGridRhoFastjetCentral, &b_fixedGridRhoFastjetCentral);
    tree->SetBranchAddress("fixedGridRhoFastjetCentralChargedPileUp", &fixedGridRhoFastjetCentralChargedPileUp, &b_fixedGridRhoFastjetCentralChargedPileUp);
  }else{
    tree->SetBranchAddress("Rho_fixedGridRhoFastjetAll", &fixedGridRhoFastjetAll, &b_fixedGridRhoFastjetAll);
    tree->SetBranchAddress("Rho_fixedGridRhoFastjetCentral", &fixedGridRhoFastjetCentral, &b_fixedGridRhoFastjetCentral);
    tree->SetBranchAddress("Rho_fixedGridRhoFastjetCentralChargedPileUp", &fixedGridRhoFastjetCentralChargedPileUp, &b_fixedGridRhoFastjetCentralChargedPileUp);
  }

  tree->SetBranchAddress("PV_npvsGood", &PV_npvsGood, &b_PV_npvsGood);
  tree->SetBranchAddress("PV_npvs", &PV_npvs, &b_PV_npvs);

  tree->SetBranchAddress("nMuon", &nMuon, &b_nMuon);
  tree->SetBranchAddress("Muon_pt", Muon_pt, &b_Muon_pt);
  tree->SetBranchAddress("Muon_eta", Muon_eta, &b_Muon_eta);
  tree->SetBranchAddress("Muon_phi", Muon_phi, &b_Muon_phi);
  tree->SetBranchAddress("Muon_mass", Muon_mass, &b_Muon_mass);
  tree->SetBranchAddress("Muon_charge", Muon_charge, &b_Muon_charge);
  tree->SetBranchAddress("Muon_softId", Muon_softId, &b_Muon_softId);
  tree->SetBranchAddress("Muon_mediumPromptId", Muon_mediumPromptId, &b_Muon_mediumPromptId);
  tree->SetBranchAddress("Muon_miniPFRelIso_all", Muon_miniPFRelIso_all, &b_Muon_miniPFRelIso_all);
  tree->SetBranchAddress("Muon_pfRelIso03_all",  Muon_pfRelIso03_all, &b_Muon_pfRelIso03_all);
  tree->SetBranchAddress("Muon_pfRelIso04_all",  Muon_pfRelIso04_all, &b_Muon_pfRelIso04_all);
  tree->SetBranchAddress("Muon_nTrackerLayers",  Muon_nTrackerLayers, &b_Muon_nTrackerLayers);
  tree->SetBranchAddress("Muon_tightId", Muon_tightId, &b_Muon_tightId);

  tree->SetBranchAddress("nElectron", &nElectron, &b_nElectron);
  tree->SetBranchAddress("Electron_pt", Electron_pt, &b_Electron_pt);
  tree->SetBranchAddress("Electron_eta", Electron_eta, &b_Electron_eta);
  tree->SetBranchAddress("Electron_phi", Electron_phi, &b_Electron_phi);
  tree->SetBranchAddress("Electron_mass", Electron_mass, &b_Electron_mass);
  tree->SetBranchAddress("Electron_charge", Electron_charge, &b_Electron_charge);
  tree->SetBranchAddress("Electron_cutBased", Electron_cutBased, &b_Electron_cutBased);
  //  tree->SetBranchAddress("Electron_mvaFall17Iso_WP90", Electron_mvaFall17Iso_WP90, &b_Electron_mvaFall17Iso_WP90);
  tree->SetBranchAddress("Electron_miniPFRelIso_all", Electron_miniPFRelIso_all, &b_Electron_miniPFRelIso_all);
  tree->SetBranchAddress("Electron_pfRelIso03_all", Electron_pfRelIso03_all, &b_Electron_pfRelIso03_all);
  tree->SetBranchAddress("Electron_dxy", Electron_dxy, &b_Electron_dxy);
  tree->SetBranchAddress("Electron_dz", Electron_dz, &b_Electron_dz);
  //  tree->SetBranchAddress("Electron_mvaFall17V2Iso_WP90", Electron_mvaFall17V2Iso_WP90, &b_Electron_mvaFall17V2Iso_WP90);
  //  tree->SetBranchAddress("Electron_cutBased_Fall17_V1", Electron_cutBased_Fall17_V1, &b_Electron_cutBased_Fall17_V1);
  
  tree->SetBranchAddress("nJet", &nJet, &b_nJet);
  tree->SetBranchAddress("Jet_area", Jet_area, &b_Jet_area);
  tree->SetBranchAddress("Jet_pt", Jet_pt, &b_Jet_pt);
  tree->SetBranchAddress("Jet_eta", Jet_eta, &b_Jet_eta);
  tree->SetBranchAddress("Jet_phi", Jet_phi, &b_Jet_phi);
  tree->SetBranchAddress("Jet_mass", Jet_mass, &b_Jet_mass);

  tree->SetBranchAddress("Jet_rawFactor", Jet_rawFactor, &b_Jet_rawFactor);

  tree->SetBranchAddress("Jet_electronIdx1", Jet_electronIdx1, &b_Jet_electronIdx1);
  tree->SetBranchAddress("Jet_electronIdx2", Jet_electronIdx2, &b_Jet_electronIdx2);
  tree->SetBranchAddress("Jet_muonIdx1", Jet_muonIdx1, &b_Jet_muonIdx1);
  tree->SetBranchAddress("Jet_muonIdx2", Jet_muonIdx2, &b_Jet_muonIdx2);

  tree->SetBranchAddress("Jet_jetId", Jet_jetId, &b_Jet_jetId);
  tree->SetBranchAddress("Jet_nElectrons", Jet_nElectrons, &b_Jet_nElectrons);
  tree->SetBranchAddress("Jet_nMuons", Jet_nMuons, &b_Jet_nMuons);
  //tree->SetBranchAddress("Jet_puId", Jet_puId, &b_Jet_puId);

  tree->SetBranchAddress("Jet_nConstituents", Jet_nConstituents, &b_Jet_nConstituents);
  tree->SetBranchAddress("Jet_neEmEF", Jet_neEmEF, &b_Jet_neEmEF);
  tree->SetBranchAddress("Jet_neHEF", Jet_neHEF, &b_Jet_neHEF);
  tree->SetBranchAddress("Jet_muEF", Jet_muEF, &b_Jet_muEF);
  tree->SetBranchAddress("Jet_chEmEF", Jet_chEmEF, &b_Jet_chEmEF);
  tree->SetBranchAddress("Jet_chHEF", Jet_chHEF, &b_Jet_chHEF);

  
  //  tree->SetBranchAddress("Jet_btagCMVA", Jet_btagCMVA, &b_Jet_btagCMVA);
  //tree->SetBranchAddress("Jet_btagCSVV2", Jet_btagCSVV2, &b_Jet_btagCSVV2);
  tree->SetBranchAddress("Jet_btagDeepFlavB", Jet_btagDeepFlavB, &b_Jet_btagDeepFlavB);
  tree->SetBranchAddress("Jet_btagDeepFlavCvL", Jet_btagDeepFlavCvL, &b_Jet_btagDeepFlavCvL);
  tree->SetBranchAddress("Jet_btagDeepFlavQG", Jet_btagDeepFlavQG, &b_Jet_btagDeepFlavQG);
  if(atoi(year) < 2020){
    tree->SetBranchAddress("Jet_btagDeepB", Jet_btagDeepB, &b_Jet_btagDeepB);
    tree->SetBranchAddress("Jet_btagDeepCvB", Jet_btagDeepCvB, &b_Jet_btagDeepCvB);
    tree->SetBranchAddress("Jet_btagDeepCvL", Jet_btagDeepCvL, &b_Jet_btagDeepCvL);

    tree->SetBranchAddress("Jet_particleNetAK4_B", Jet_particleNetAK4_B, &b_Jet_particleNetAK4_B);
    tree->SetBranchAddress("Jet_particleNetAK4_CvsB", Jet_particleNetAK4_CvsB, &b_Jet_particleNetAK4_CvsB);
    tree->SetBranchAddress("Jet_particleNetAK4_CvsL", Jet_particleNetAK4_CvsL, &b_Jet_particleNetAK4_CvsL);
    tree->SetBranchAddress("Jet_particleNetAK4_QvsG", Jet_particleNetAK4_QvsG, &b_Jet_particleNetAK4_QvsG);
  }
  //tree->SetBranchAddress("Jet_qgl", Jet_qgl, &b_Jet_qgl);

  tree->SetBranchAddress("nTrigObj",    &nTrigObj,    &b_nTrigObj);
  tree->SetBranchAddress("TrigObj_pt",  TrigObj_pt,  &b_TrigObj_pt);
  tree->SetBranchAddress("TrigObj_eta", TrigObj_eta, &b_TrigObj_eta);
  tree->SetBranchAddress("TrigObj_phi", TrigObj_phi, &b_TrigObj_phi);
  tree->SetBranchAddress("TrigObj_id",  TrigObj_id,  &b_TrigObj_id);

  if(!isData){
    tree->SetBranchAddress("Jet_hadronFlavour", Jet_hadronFlavour, &b_Jet_hadronFlavour);
    tree->SetBranchAddress("Jet_partonFlavour", Jet_partonFlavour, &b_Jet_partonFlavour);

    tree->SetBranchAddress("nGenJet", &nGenJet, &b_nGenJet);
    tree->SetBranchAddress("GenJet_pt", GenJet_pt, &b_GenJet_pt);
    tree->SetBranchAddress("GenJet_eta", GenJet_eta, &b_GenJet_eta);
    tree->SetBranchAddress("GenJet_phi", GenJet_phi, &b_GenJet_phi);
    tree->SetBranchAddress("GenJet_mass", GenJet_mass, &b_GenJet_mass);
  }  
  //tree->SetBranchAddress("MET_pt", &MET_pt, &b_MET_pt);
  //tree->SetBranchAddress("MET_phi", &MET_phi, &b_MET_phi);
  //tree->SetBranchAddress("ChsMET_pt", &ChsMET_pt, &b_ChsMET_pt);
  //tree->SetBranchAddress("ChsMET_phi", &ChsMET_phi, &b_ChsMET_phi);

  tree->SetBranchAddress("PuppiMET_phi", &PuppiMET_phi, &b_PuppiMET_phi);
  tree->SetBranchAddress("PuppiMET_pt", &PuppiMET_pt, &b_PuppiMET_pt);

  //tree->SetBranchAddress("RawMET_pt", &RawMET_pt, &b_RawMET_pt);
  //tree->SetBranchAddress("RawMET_phi", &RawMET_phi, &b_RawMET_phi);

  tree->SetBranchAddress("RawPuppiMET_phi", &RawPuppiMET_phi, &b_RawPuppiMET_phi);
  tree->SetBranchAddress("RawPuppiMET_pt", &RawPuppiMET_pt, &b_RawPuppiMET_pt);

  //tree->SetBranchAddress("MET_MetUnclustEnUpDeltaX", &MET_MetUnclustEnUpDeltaX, &b_MET_MetUnclustEnUpDeltaX);
  //tree->SetBranchAddress("MET_MetUnclustEnUpDeltaY", &MET_MetUnclustEnUpDeltaY, &b_MET_MetUnclustEnUpDeltaY);

  if(!isData){
    tree->SetBranchAddress("GenMET_pt", &GenMET_pt, &b_GenMET_pt);
    tree->SetBranchAddress("GenMET_phi", &GenMET_phi, &b_GenMET_phi);
  }

  if(!isData){
    tree->SetBranchAddress("nGenPart", &nGenPart, &b_nGenPart);
    tree->SetBranchAddress("GenPart_pt", &GenPart_pt, &b_GenPart_pt);
    tree->SetBranchAddress("GenPart_eta", &GenPart_eta, &b_GenPart_eta);
    tree->SetBranchAddress("GenPart_phi", &GenPart_phi, &b_GenPart_phi);
    tree->SetBranchAddress("GenPart_mass", &GenPart_mass, &b_GenPart_mass);
    tree->SetBranchAddress("GenPart_pdgId", &GenPart_pdgId, &b_GenPart_pdgId);
    tree->SetBranchAddress("GenPart_genPartIdxMother", &GenPart_genPartIdxMother, &b_GenPart_genPartIdxMother);
    tree->SetBranchAddress("GenPart_status", &GenPart_status, &b_GenPart_status);
  }
}

bool Analysis::getTriggerDecision(){
  //std::cout << "check Analysis::getTriggerDecision " << triggerNames.size() << std::endl;
  bool decision = false;
  for(size_t i = 0; i < triggerNames.size(); ++i){
    //    if(triggerBit[i]) triggerBit[i] = 0;
    //std::cout << triggerNames[i] << " " << triggerBit[i] << std::endl;
    decision = decision || triggerBit[i];
  }
  //std::cout << "trg decision " << decision << std::endl;
  return decision;
}

bool Analysis::triggeredBy(std::string regexp){
  std::regex word_regex(regexp);
  bool decision = false;
  for(size_t i = 0; i < triggerNames.size(); ++i){
    //std::cout << "check triggeredBy " << triggerNames[i] << " " << regexp << " " << std::regex_search(triggerNames[i], word_regex) << std::endl;
    if (std::regex_search(triggerNames[i], word_regex) && triggerBit[i]) decision = true;
  }
  return decision;
}

bool Analysis::METCleaning(){
  if(!isData) return true;
  
  //std::cout << "check Analysis::getTriggerDecision " << triggerNames.size() << "  " << triggerBit[0] << std::endl;
  bool decision = true;
  for(size_t i = 0; i < filterFlags.size(); ++i){
    //    if(filter[i]) filter[i] = 0;
    //std::cout << filterFlags[i] << " " << filter[i] << std::endl;
    decision = decision && filter[i];
  }
  return decision;
}

//void Analysis::setupBranches(BranchManager& branchManager) {}

void Analysis::collectionSizeCheck(const char* name, uint dim, uint maxdim){
  //std::cout << "check dim " << dim << " " << maxdim << std::endl;
  //std::cout << "check Analysis::collectionSizeCheck Jet" << sizeof(Jet_pt)/sizeof(float) << std::endl;
  if(dim > maxdim) {
    std::cout << "\033[1;31mFAILED Analysis::collectionSizeCheck " << name << " " << dim << " " << maxdim << "\033[0m" << std::endl;
    //    exit(EXIT_FAILURE);
  }
  return;
}

bool Analysis::passLeptonPtCut(std::vector<TLorentzVector> particles, int iLepton){
  if(particles.size() < 2) return false;

  double cut1 = 20;
  double cut2 = 10;
  double etamax = 2.3;
  if(abs(iLepton) == 11){
    cut1 = 25;
    cut2 = 15;
    etamax = 2.4;
  }

  int passedLeading = 0;
  int passedSubLeading = 0;
  for(std::vector<TLorentzVector>::const_iterator i = particles.begin(); i != particles.end(); ++i){
    //std::cout << "check lepton pt " << i->Pt() << " " << i->Eta() << std::endl;
    if(fabs(i->Eta()) > etamax) continue;
    if(i->Pt() > cut1) passedLeading++;
    if(i->Pt() > cut2) passedSubLeading++;
  }
  //std::cout << "check pass leptons " << passedLeading << " " << passedSubLeading << std::endl;
  if(passedLeading > 0 && passedSubLeading > 1) return true;
  return false;
}

TLorentzVector Analysis::triggerMatching(TLorentzVector lepton){
  double trgMatchingCone = 0.3;

  TLorentzVector hltobject(0,0,0,0);

  for(int i = 0; i < nTrigObj; ++i){
    if(TrigObj_id[i] != abs(leptonflavor)) continue;

    double hlt_pt = TrigObj_pt[i];
    double hlt_eta = TrigObj_eta[i];
    double hlt_phi = TrigObj_phi[i];
    double hlt_m   = 0;
    TLorentzVector hlt_lepton;
    hlt_lepton.SetPtEtaPhiM(hlt_pt,hlt_eta,hlt_phi,hlt_m);

    double DR = ROOT::Math::VectorUtil::DeltaR(lepton,hlt_lepton);
    if(DR < trgMatchingCone){
      hltobject = hlt_lepton;
      break;
    }
  }
  return hltobject;
}

std::vector<TLorentzVector> Analysis::getMuons(int charge=0){
  std::vector<TLorentzVector> muons;

  double ptmin  = 8; // all muons
  double ptmin1 = 20;
  double ptmin2 = 10;
  double etamax = 2.3;
  //double etamax = 2.4;
  double pfRelIsoMax = 0.15;

  bool passedCut1 = false;
  int n = nMuon;
  collectionSizeCheck("Muon",nMuon,sizeof(Muon_pt)/sizeof(float));

  //std::cout << "check nMuons " << n << std::endl;
  for(int i = 0; i < n; ++i){
    //std::cout << "  check muon pt " << Muon_pt[i] << std::endl;
    if(Muon_pt[i] > ptmin &&
       //fabs(Muon_eta[i]) < etamax &&
       Muon_tightId[i] &&
       //Muon_pfRelIso03_all[i] < pfRelIsoMax){
       Muon_pfRelIso04_all[i] < pfRelIsoMax){
      //if(Muon_pt[i] > ptmin1) passedCut1 = true;
      double pt = Muon_pt[i];
      double eta = Muon_eta[i];
      double phi = Muon_phi[i];
      double m = Muon_mass[i];
      double q = Muon_charge[i];
      
      TLorentzVector mu;
      mu.SetPtEtaPhiM(pt,eta,phi,m);

      TLorentzVector hlt_obj = triggerMatching(mu);
      if(hlt_obj.Pt() == 0) continue; // Trg matching cut

      double SF = 1;
      if(rcFound){
        if(isData) SF = rc.kScaleDT(q, pt, eta, phi, 0, 0);
        else{
          double genPt = 0;
          for(int j = 0; j < n; ++j){
          }
          if(genPt > 0){
            SF = rc.kSpreadMC(q, pt, eta, phi, genPt, 0, 0);
          }else{
            float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
            SF = rc.kSmearMC(q, pt, eta, phi, Muon_nTrackerLayers[i], r, 0, 0);
          }
        }
      }

      //std::cout << "  check muon pt " << pt << " " << Muon_charge[i] << " " << q << std::endl;
      //std::cout << "  check muon corr " << pt << " " << SF << std::endl;
      mu = SF*mu;
      if(charge == 0){
        muons.push_back(mu);
      }else{
        if(fabs(q-charge) < 0.001) muons.push_back(mu);
      }
    }
  }

  return muons;
}


bool Analysis::passElectronID(int iElectron){
  bool pass = true;

  //  if(Electron_mvaFall17V2Iso_WP80[iElectron] < 0.5) return false;
  //std::cout << "  check electron Analysis::passElectronID >" << Electron_cutBased[iElectron] << "<" << int(Electron_cutBased[iElectron]) <<std::endl;
  if(!(int(Electron_cutBased[iElectron]) == 4)) return false; //0:fail, 1:veto, 2:loose, 3:medium, 4:tight
  //std::cout << "check electron id " << nElectron << " " << iElectron << " " << Electron_cutBased_Fall17_V1[iElectron] << " " << Electron_cutBased_Fall17_V1[0] << std::endl;
  // electron IP
  /*
  if(fabs(Electron_eta[iElectron]) <= 1.479){   
    if(Electron_dxy[iElectron] > 0.05) return false;
    if(Electron_dz[iElectron] > 0.10) return false;
  }else{
    if(Electron_dxy[iElectron] > 0.10) return false;
    if(Electron_dz[iElectron] > 0.20) return false;
  }
  */
  return pass;
}

std::vector<TLorentzVector> Analysis::getElectrons(int charge=0){
  std::vector<TLorentzVector> cands;
  
  //double ptmin1 = 25;
  double ptmin = 15;
  double etamax = 2.4;
  double pfRelIsoMax = 1.0;//0.4;//0.15;

  bool passedCut1 = false;
  int n = nElectron;
  collectionSizeCheck("Electron",nElectron,sizeof(Electron_pt)/sizeof(float));

  //std::cout << "check nElectrons " << n << std::endl;
  for(int i = 0; i < n; ++i){
    //std::cout << "  check electron pt " << Electron_pt[i] << " " << Electron_eta[i] << " " << Electron_phi[i] << " " << Electron_mass[i] << std::endl;
    //std::cout << "  check electron iso " << Electron_pfRelIso03_all[i] << " < " << pfRelIsoMax << std::endl;
    if(Electron_pt[i] > ptmin &&
       //fabs(Electron_eta[i]) < etamax
       //       int(Electron_cutBased[i]) == 3
       //       Electron_mvaSpring16GP_WP90[i] &&
       //Electron_miniPFRelIso_all[i] < pfRelIsoMax
       Electron_pfRelIso03_all[i] < pfRelIsoMax
       ){

      if(!passElectronID(i)) continue;
      //std::cout << "  check passElectronID" << std::endl;
      //      if(Electron_pt[i] > ptmin1) passedCut1 = true;

      Double_t pt = Electron_pt[i];
      Double_t eta = Electron_eta[i];
      Double_t phi = Electron_phi[i];
      TVector3 tv3;
      tv3.SetPtEtaPhi(pt,eta,phi);
      //std::cout << "check tv3 " << tv3.Mag()  << std::endl;
      //Double_t m = 0.0; //Electron_mass[i];
      Int_t q = Electron_charge[i];
      //std::cout << "check m " << Electron_mass[i] << " " << m << " " << Double_t(m) << std::endl;
      //std::cout << "check q " << Electron_charge[i] << " " << charge << " " << std::endl;
      TLorentzVector cand;
      //      cand.SetPtEtaPhiM(pt,eta,phi,m);
      cand.SetPtEtaPhiE(pt,eta,phi,tv3.Mag());

      TLorentzVector hlt_obj = triggerMatching(cand);
      if(hlt_obj.Pt() == 0) continue; // Trg matching cut

      //std::cout << "check charge" << std::endl;
      if(charge == 0){
        cands.push_back(cand);
      }else{
        //std::cout << "check q " << Electron_charge[i] << " " << charge << " " << q-charge << std::endl;
        if(fabs(q-charge) < 0.001) cands.push_back(cand);
        //std::cout << "check ele " << cand.Pt() << " " << cand.Eta() << " " << cand.M() << std::endl;
      }
    }
  }
  //std::cout << "check selected ele cands " << cands.size() << std::endl;
  //if(passedCut1)  return cands;
  //else return std::vector<TLorentzVector>();
  return cands;
}

std::vector<TLorentzVector> Analysis::getGenParticles(int pid){
  std::vector<TLorentzVector> cands;
  for(size_t i = 0; i < nGenPart; ++i){
    if(GenPart_pdgId[i] != pid) continue;
    if(abs(GenPart_pdgId[i]) == GenPart_genPartIdxMother[i]) continue;
    //std::cout << "check mother " << GenPart_pdgId[i] << " " << GenPart_genPartIdxMother[i] << std::endl;
    //std::cout << "check genparticles " << i << " " << GenPart_status[i] << std::endl;
    double pt = GenPart_pt[i];
    double eta = GenPart_eta[i];
    double phi = GenPart_phi[i];
    double m = GenPart_mass[i];

    TLorentzVector cand;
    cand.SetPtEtaPhiM(pt,eta,phi,m);
    cands.push_back(cand);
  }
  return cands;
}

/*
std::vector<TLorentzVector> Analysis::getJets() {
  std::vector<TLorentzVector> cands;

  double ptmin = 15;
  double etamax = 2.5;

  int n = nJet;
  h_njets->Fill(nJet);
  //std::cout << "check nJet " << n << std::endl;
  TLorentzVector leadingJet(0,0,0,0);
  for(int i = 0; i < n; ++i){
    if(Jet_pt[i] > ptmin &&
       fabs(Jet_eta[i]) < etamax){
      double pt  = Jet_pt[i];
      double eta = Jet_eta[i];
      double phi = Jet_phi[i];
      double m   = Jet_mass[i];

      if(pt > leadingJet.Pt()) leadingJet.SetPtEtaPhiE(pt,eta,phi,m);
    }
  }
  
      
      
      h_Jet_jetId->Fill(Jet_jetId[i]);
      h_Jet_electronIdx1->Fill(Jet_electronIdx1[i]);
      h_Jet_electronIdx2->Fill(Jet_electronIdx2[i]);
      h_Jet_muonIdx1->Fill(Jet_muonIdx1[i]);
      h_Jet_muonIdx2->Fill(Jet_muonIdx2[i]);
      h_Jet_nElectrons->Fill(Jet_nElectrons[i]);
      h_Jet_nMuons->Fill(Jet_nMuons[i]);
      h_Jet_puId->Fill(Jet_puId[i]);
      //std::cout << "check jet" << pt << " " << eta << " " << phi << " " << Jet_jetId[i] << " " << Jet_electronIdx1[i] << " " << Jet_electronIdx2[i] << " " << Jet_muonIdx1[i] << " " << Jet_muonIdx2[i] << " " << Jet_nElectrons[i] << " " << Jet_nMuons[i] << " " << Jet_puId[i] << std::endl;

//      if(Jet_electronIdx1[i] >= 0) continue;
//      if(Jet_electronIdx2[i] >= 0) continue;
//      if(Jet_muonIdx1[i] >= 0) continue;
//      if(Jet_muonIdx2[i] >= 0) continue;
//      if(Jet_nElectrons[i] > 0) continue;
//      if(Jet_nMuons[i] > 0) continue;

      TLorentzVector cand;
      cand.SetPtEtaPhiE(pt,eta,phi,m);

      cands.push_back(cand);
    }
  }
  return cands;
}
*/

std::vector<int> Analysis::getJetIndexList(){
  std::vector<int> jetIndices;
  size_t n = nJet;
  collectionSizeCheck("Jet",nJet,100);//sizeof(Jet_pt)/sizeof(float));

  for(size_t i = 0; i < n; ++i){
    jetIndices.push_back(i);
  }
  return jetIndices;
}

void Analysis::applyJEC(std::vector<int> jetIndices){

  if(jec_L1fjL2RelL2AbsL2L3Res == 0) return;

  size_t smearNMax = 3;
  for(size_t i = 0; i < jetIndices.size(); ++i){
    size_t index = jetIndices[i];
    //std::cout << "check orig pt " << index << " " << Jet_pt[index] << std::endl;
    Jet_ptorig[index] = Jet_pt[index];
    Jet_massorig[index] = Jet_mass[index];
    Jet_ptraw[index] = Jet_pt[index] * (1 - Jet_rawFactor[index]);

    jec_L1fjL2RelL2AbsL2L3Res->setJetPt(Jet_ptraw[index]);
    jec_L1fjL2RelL2AbsL2L3Res->setJetEta(Jet_eta[index]);
    jec_L1fjL2RelL2AbsL2L3Res->setJetPhi(Jet_phi[index]);
    jec_L1fjL2RelL2AbsL2L3Res->setJetA(Jet_area[index]);
    jec_L1fjL2RelL2AbsL2L3Res->setRho(fixedGridRhoFastjetAll);
    //float corr = jec_L1fjL2RelL2AbsL2L3Res->getCorrection();

    vector<float> v = jec_L1fjL2RelL2AbsL2L3Res->getSubCorrections();
    float corr = v.back();
    //std::cout << "check corr " << corr << std::endl;
    double res = (v.size()>1 ? v[v.size()-1]/v[v.size()-2] : 1.);
    Jet_RES[index] = res;
    //std::cout << "check RES " << res << std::endl;

    float smear = 1.0;

    Jet_pt[index] = Jet_ptraw[index]*corr;
    //std::cout << "check jec ptcor,ptraw " << Jet_pt[index] << " " << Jet_ptraw[index] << " " << corr << std::endl;
    Jet_mass[index] = Jet_massorig[index] * (1 - Jet_rawFactor[index]) * corr;

    //Jet_CF[index] = smearingKIT(Jet_pt[index],Jet_eta[index],fixedGridRhoFastjetAll);
    //if(atoi(year) < 2020)
    //if(!isData && i < smearNMax) smear = smearing(index,fixedGridRhoFastjetAll);
    if(smearingON && !isData&& i < smearNMax) smear = smearing(index,fixedGridRhoFastjetAll);
    Jet_CF[index] = smear;
    //std::cout << std::endl << "check smear,pt,eta,rho " << smear << " " << Jet_pt[index] << " " << Jet_eta[index] << " " << fixedGridRhoFastjetAll << std::endl;
    h_jet_smearOff->Fill(Jet_pt[index]);
    Jet_pt[index] = Jet_pt[index] * smear;
    Jet_mass[index] = Jet_mass[index] * smear;
    h_jet_smearOn->Fill(Jet_pt[index]);
    h_jet_smearFactor->Fill(smear);
    //std::cout << "check jet.pt smeared " << Jet_pt[index] << std::endl;

    //std::cout << "check jec_L1rc ptraw,eta,area,rho "<<Jet_ptraw[index]<<" " << Jet_eta[index] <<" " << Jet_area[index]<<" " << fixedGridRhoFastjetAll << std::endl;
    if(atoi(year) < 2020){
      jec_L1rc->setJetPt(Jet_ptraw[index]);
      jec_L1rc->setJetEta(Jet_eta[index]);
      jec_L1rc->setJetA(Jet_area[index]);
      jec_L1rc->setRho(fixedGridRhoFastjetAll);
      float corr_L1rc = jec_L1rc->getCorrection();
      Jet_pt_corr_L1rc[index] = Jet_ptraw[index]*corr_L1rc;
    }
    //std::cout << "check jec L1rc pt,raw,corr " << Jet_pt_corr_L1rc[index] << " " << Jet_ptraw[index] << " " << corr_L1rc << std::endl;
    //Jet_mass_corr_L1rc[index] = Jet_massorig[index] * (1 - Jet_rawFactor[index]) * corr_L1rc;
  }
}
double Analysis::smearing(int index, float rho){
  if(isData) return 1.0;

  // Retrieve genJet and calculate dR
  double dR(999);
  TLorentzVector p4, p4g;
  p4.SetPtEtaPhiM(Jet_pt[index], Jet_eta[index], Jet_phi[index], Jet_mass[index]);
  if (Jet_genJetIdx[index] >= 0 ) {
    int j = Jet_genJetIdx[index];
    p4g.SetPtEtaPhiM(GenJet_pt[j], GenJet_eta[j], GenJet_phi[j], GenJet_mass[j]);
    dR = p4g.DeltaR(p4);
  }else
    p4g.SetPtEtaPhiM(0, 0, 0, 0);

  double jPt = Jet_pt[index];
  double jEta = Jet_eta[index];
  double jE = p4.E();
  double jPtGen = p4g.Pt();
  double MIN_JET_ENERGY = 0.01;

  //  std::cout << "check smearing input " << jPt << " " << jEta << " " << rho << std::endl;
  
  double Reso = m_jetResolution->getResolution({{JME::Binning::JetPt, jPt}, {JME::Binning::JetEta, jEta}, {JME::Binning::Rho, rho}});

  jersf->setJetPt(jPt);
  jersf->setJetEta(jEta);
  jersf->setRho(rho);
  double SF = jersf->getCorrection();
  //  std::cout << "check Reso,SF " << Reso << " " << SF << std::endl;
  double CF = 1.0;

  // We see if the gen jet meets our requirements
  bool condPt = (jPtGen > MIN_JET_ENERGY && dR < 0.2);
  double relDPt = condPt ? (jPt - jPtGen) / jPt : 0.0;
  bool condPtReso = fabs(relDPt) < 3 * Reso;
  if (condPt and condPtReso) {
    // Case 1: we have a "good" gen jet matched to the reco jet (indicated by positive gen jet pt)
    CF += (SF - 1.) * relDPt;
    //std::cout << "check Case 1: CF relDPt " << CF << " " << relDPt << std::endl;
  } else if (SF > 1) {
    // Case 2: we don't have a gen jet. Smear jet pt using a random gaussian variation
    double sigma = Reso * std::sqrt(SF * SF - 1);
    std::normal_distribution<> d(0, sigma);
    double tmp = d(m_randomNumberGenerator);
    CF += tmp;//d(m_randomNumberGenerator);
    //std::cout << "check Case 2: CF sigma rnd " << CF << " " << sigma << " " << tmp << std::endl;
  }
  // Negative or too small smearFactor. Safety precautions.
  double CFLimit = MIN_JET_ENERGY / jE;
  if (CF < CFLimit) CF = CFLimit;

  return (CF < 0) ? 0.0 : CF;
}

double Analysis::smearingKIT(float pt, float eta, float rho){
  //KIT: jecSmearFactor = 1 + std::normal_distribution<>(0, jetResolution)(m_randomNumberGenerator) * std::sqrt(std::max(jetResolutionScaleFactor * jetResolutionScaleFactor - 1, 0.0));
  //KIT: // apply factor (prevent negative values)
  //KIT: recoJets[iJet]->p4 *= (jecSmearFactor < 0) ? 0.0 : jecSmearFactor;

  // Following the example by KIT
  double jetResolution = m_jetResolution->getResolution({
                {JME::Binning::JetPt, pt},
                {JME::Binning::JetEta, eta},
                {JME::Binning::Rho, rho}}
    );
  //std::cout << "check reso,pt,eta,rho " << jetResolution << "  " << pt << "  " << eta << "  " << rho << std::endl;
  double jetResolutionScaleFactor = m_jetResolutionScaleFactor->getScaleFactor({
      {JME::Binning::JetPt, pt},
      {JME::Binning::JetEta, eta}
    }, Variation::NOMINAL);
  //std::cout << "check resoSF,pt,eta,rho " << jetResolutionScaleFactor << "  " << pt << "  " << eta << "  " << rho << std::endl;
  double jecSmearFactor = 1.0;
  //double rnd = std::normal_distribution<>(0.0, 1.0)(m_randomNumberGenerator);
  double rnd = m_randomNumberGenerator2->Gaus(0.0,1.0); 
  //std::cout << "check rnd " << rnd << std::endl;
  //std::cout << "check rnd2 " << rnd2 << std::endl;
  rnd *= jetResolution;
  //jecSmearFactor = 1 + std::normal_distribution<>(0, jetResolution)(m_randomNumberGenerator) * std::sqrt(std::max(jetResolutionScaleFactor * jetResolutionScaleFactor - 1, 0.0));
  jecSmearFactor = 1 + rnd * std::sqrt(std::max(jetResolutionScaleFactor * jetResolutionScaleFactor - 1, 0.0));

  return (jecSmearFactor < 0) ? 0.0 : jecSmearFactor;
}

std::vector<int> Analysis::ptOrder(std::vector<int> jetIndices){
  std::map<double,int> ordered;
  for(std::vector<int>::const_iterator i = jetIndices.begin(); i != jetIndices.end(); ++i){
    //std::cout << "check order1 " << getJet(*i).Pt() << std::endl;
    ordered[getJet(*i).Pt()] = *i;
  }

  std::vector<int> retOrder;
  for(std::map<double,int>::reverse_iterator i = ordered.rbegin(); i != ordered.rend(); ++i){
    //std::cout << "check ordered " << i->first << " " << i->second << std::endl;
    retOrder.push_back(i->second);
  }
  return retOrder;
}

TLorentzVector Analysis::getJet(int i){
  if(i < 0) return TLorentzVector(0,0,0,0);

  collectionSizeCheck("Jet",nJet,100);//sizeof(Jet_pt)/sizeof(float));

  double pt  = Jet_pt[i];
  double eta = Jet_eta[i];
  double phi = Jet_phi[i];
  double m   = Jet_mass[i];
  TLorentzVector jet;
  jet.SetPtEtaPhiM(pt,eta,phi,m);
  return jet;
}
TLorentzVector Analysis::getGenJet(int i){
  if(i < 0) return TLorentzVector(0,0,0,0);

  collectionSizeCheck("GenJet",nGenJet,sizeof(GenJet_pt)/sizeof(float));
  
  double pt  = GenJet_pt[i];
  double eta = GenJet_eta[i];
  double phi = GenJet_phi[i];
  double m   = GenJet_mass[i];
  TLorentzVector jet;
  jet.SetPtEtaPhiM(pt,eta,phi,m);
  return jet;
}

std::vector<int> Analysis::drClean(std::vector<int> jetIndices,std::vector<TLorentzVector> compare, double cone){
  std::vector<int> cleaned;
  for(std::vector<int>::const_iterator i = jetIndices.begin(); i != jetIndices.end(); ++i){
    TLorentzVector jet = getJet(*i);
    bool passed = true;
    std::vector<TLorentzVector>::const_iterator m;
    for(m = compare.begin(); m != compare.end(); ++m){
      double DR = ROOT::Math::VectorUtil::DeltaR(jet,*m);
      if(DR < cone) passed = false;
    }
    if(passed) cleaned.push_back(*i);
  }
  return cleaned;
}

std::vector<int> Analysis::jetPt(std::vector<int> jetIndices, double ptcut){
  std::vector<int> cleaned;
  for(std::vector<int>::const_iterator i = jetIndices.begin(); i != jetIndices.end(); ++i){
    if(Jet_pt[*i] >= ptcut) cleaned.push_back(*i);
  }
  return cleaned;
}

std::vector<int> Analysis::jetId(std::vector<int> jetIndices){
  std::vector<int> cleaned;
  for(std::vector<int>::const_iterator i = jetIndices.begin(); i != jetIndices.end(); ++i){
    // Int_t Jet ID flags bit1 is loose, bit2 is tight, bit3 is tightLepVeto
    if(Jet_jetId[*i] >= 4) cleaned.push_back(*i);
  }
  return cleaned;
}

bool Analysis::jetId(int jetIndex){
  bool passed = false;
  // Int_t Jet ID flags bit1 is loose, bit2 is tight, bit3 is tightLepVeto
  if(Jet_jetId[jetIndex] >= 4) passed = true;
  return passed;
}

bool Analysis::matchGenJetFound(int jetIndex){
  double drMax = 0.4;
  double ptgenmax = 10;
  bool found = false;
  TLorentzVector refjet = getJet(jetIndex);
  size_t n = nGenJet;
  for(size_t i = 0; i < n; ++i){
    TLorentzVector genjet = getGenJet(i);

    if(genjet.Pt() < ptgenmax) continue;

    double DR = ROOT::Math::VectorUtil::DeltaR(refjet,genjet);
    if(DR < drMax) {
      found = true;
      break;
    }
  }
  return found;
}

int Analysis::getLeadingJetIndex(std::vector<int> jetIndices, size_t nth){
  if(jetIndices.size() < nth+1) return -1;
  double ptmin = 12;
  double etamax = 5;//2.5;//1.3; divided in eta bins 0-1.3, 1.3-1.9, 1.9-2.5

  int leadingJetIndex = -1;
  for(std::vector<int>::const_iterator i = jetIndices.begin(); i != jetIndices.end(); ++i){
    if(Jet_pt[*i] < ptmin) continue;
    if(fabs(Jet_eta[*i]) > etamax) continue;
    
    if(leadingJetIndex < 0) leadingJetIndex = *i;
    else{
      if(Jet_pt[*i] > Jet_pt[leadingJetIndex]) leadingJetIndex = *i;
    }
  }
  if(leadingJetIndex < 0) return -1;

  std::vector<int> reordered;
  reordered.push_back(leadingJetIndex);
  // assuming the jets are pt ordered
  for(std::vector<int>::const_iterator i = jetIndices.begin(); i != jetIndices.end(); ++i){
    if( *i != leadingJetIndex) reordered.push_back(*i);
  }
  //std::cout << "check Analysis::JetSelection::leadingJet " << jetIndices.size() << std::endl;
  return reordered[nth];
}
/*
std::vector<TLorentzVector> Analysis::drClean(std::vector<TLorentzVector> orig, std::vector<TLorentzVector> compare, double cone){
  std::vector<TLorentzVector> selected;
  for(size_t i = 0; i < orig.size(); ++i){
    for(size_t j = 0; j < compare.size(); ++j){
      double DR = ROOT::Math::VectorUtil::DeltaR(orig[i],compare[j]);
      if(DR < cone) continue;
      selected.push_back(orig[i]);
    }
  }
  return selected;
}
*/

TLorentzVector Analysis::getZboson(std::vector<TLorentzVector> leptPlus,std::vector<TLorentzVector> leptMinus) {
  double mZ = 90; //91.1876;
  TLorentzVector Zboson(0,0,0,0);
  std::vector<TLorentzVector> lp;
  std::vector<TLorentzVector> lm;
  for(size_t i=0; i < leptPlus.size(); ++i){
    for(size_t j=0; j < leptMinus.size(); ++j){
      //std::cout << "check lepton1: " << leptPlus[i].Pt() << " " << leptPlus[i].Eta() << " " << leptPlus[i].Phi() << " " << leptPlus[i].M() << std::endl;
      //std::cout << "check lepton2: " << leptMinus[j].Pt() << " " << leptMinus[j].Eta() << " " << leptMinus[j].Phi() << " " << leptMinus[j].M() << std::endl;
      TLorentzVector Zbosoncand = leptPlus[i]+leptMinus[j];
      //std::cout << "check Z " << Zbosoncand.Pt() << " " << Zbosoncand.M() << std::endl;
      //if(Zbosoncand.Pt() < 15) continue;
      //if(fabs(Zbosoncand.M()-mZ) > 20 ) continue;
      if(fabs(Zbosoncand.M()-mZ) < fabs(Zboson.M()-mZ) ) {
        Zboson = Zbosoncand;
        lp.clear(); lp.push_back(leptPlus[i]);
        lm.clear(); lm.push_back(leptMinus[j]);
      }
    }
  }
  leptonsPlus = lp;
  leptonsMinus = lm;
  
  if(Zboson.Pt() < 12) return TLorentzVector(0,0,0,0);

  if(fabs(Zboson.M()-mZ) > 20 ) return TLorentzVector(0,0,0,0);

  return Zboson;
}
/*
TLorentzVector Analysis::selectLeadingJet(std::vector<TLorentzVector> jets, size_t nth){
  if(jets.size() >= nth) return jets[nth];
  else return TLorentzVector(0,0,0,0);
}
*/
double Analysis::alphaCut(double zpt){
  double theCut = 0.3;
  if(zpt>50 && zpt < 100) theCut = 0.6;
  if(zpt < 50) theCut = 1.0;
  theCut = 1.0; // cut disabled
  return theCut;
}

#include <sys/stat.h>
bool Analysis::exists(const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

#include <regex>
TVector3 Analysis::recalculateMET(std::vector<int> jets, int label){
  //std::cout << "check Analysis::recalculateMET " << label << " " << year << std::endl;
  // Type1 from Raw MET
  TVector3 met;
  //if(label==1) met.SetPtEtaPhi(ChsMET_pt,0,ChsMET_phi);
  //met.SetPtEtaPhi(RawMET_pt,0,RawMET_phi);
  //if(label==2) met.SetPtEtaPhi(MET_pt,0,MET_phi);
  //if(label==3) met.SetPtEtaPhi(RawMET_pt,0,RawMET_phi);
  if(label==4) met.SetPtEtaPhi(PuppiMET_pt,0,PuppiMET_phi);
  if(label==5) met.SetPtEtaPhi(RawPuppiMET_pt,0,RawPuppiMET_phi);

  /*
  std::cout << "check MET " << met.Pt() << " " << met.Phi() << std::endl;
  std::cout << "check RawMET_pt " << RawMET_pt << std::endl;
  std::cout << "check ChsMET_pt " << ChsMET_pt << std::endl;
  std::cout << "check PuppiMET " << PuppiMET_pt << std::endl;
  std::cout << "check RawPuppiMET " << RawPuppiMET_pt << std::endl;
  std::cout << "check begin Type-1 loop" << std::endl;
  std::cout << "check MET x,y " << met.Px() << " " << met.Py() << std::endl;
  */
  for(size_t i = 0; i < jets.size(); ++i){

    TVector3 jet_orig;
    jet_orig.SetPtEtaPhi(Jet_ptorig[jets[i]],Jet_eta[jets[i]],Jet_phi[jets[i]]);
    /*    
    float ptraw = Jet_pt[i] * (1 - Jet_rawFactor[i]);

    jec_L1rc->setJetPt(ptraw);
    jec_L1rc->setJetEta(Jet_eta[i]);
    jec_L1rc->setJetA(Jet_area[i]);
    jec_L1rc->setRho(rho);
    float corr_L1rc = jec_L1rc->getCorrection();

    jec_L1fjL2RelL2AbsL2L3Res->setJetPt(ptraw);
    jec_L1fjL2RelL2AbsL2L3Res->setJetEta(Jet_eta[i]);
    jec_L1fjL2RelL2AbsL2L3Res->setJetA(Jet_area[i]);
    jec_L1fjL2RelL2AbsL2L3Res->setRho(rho);
    float corr_withL2L3 = jec_L1fjL2RelL2AbsL2L3Res->getCorrection();
    */
    TVector3 jet_raw;
    jet_raw.SetPtEtaPhi(Jet_ptraw[jets[i]],Jet_eta[jets[i]],Jet_phi[jets[i]]);
    TVector3 jet_corr;
    jet_corr.SetPtEtaPhi(Jet_pt[jets[i]],Jet_eta[jets[i]],Jet_phi[jets[i]]);
    //std::cout << "    check jet orig,corr,raw" << jet_orig.Pt() << " " << jet_corr.Pt() << " " << jet_raw.Pt() << std::endl;
    TVector3 jet_corrL1rc;
    if(atoi(year) < 2020) jet_corrL1rc.SetPtEtaPhi(Jet_pt_corr_L1rc[jets[i]],Jet_eta[jets[i]],Jet_phi[jets[i]]);

    //if(jet_orig.Pt() < 15) continue; //Hannu's email Date: Mon, 25 May 2020 14:08:49: old jet pt
    //if(fabs(jet_orig.Eta()) > 4.7) continue;
    if(jet_corr.Pt() < 15) continue; //Minsuk 16.6.:  We need to apply a cut on corrected jet pT, and remove the eta cut

    //if(jet.Pt() < ) continue;
    //    if(2.5 < fabs(Jet_eta[i]) && fabs(Jet_eta[i]) < 3.0 && Jet_pt[i] < 70) continue; 
    //std::cout << "    check jet " << Jet_pt[i] << " " << jet_raw.Pt() << " " << jet_corr.Pt() << " " << Jet_pt[i]-jet_corr.Pt() << " " << (jet - jet_corr).Px() << std::endl;
    //std::cout << "    check met1 " << met.Pt() << " " << met.Px() << std::endl;
    /*
    std::cout << "    check jet pt,eta,phi " << jet_orig.Pt() << " " << jet_orig.Eta() << " " << jet_orig.Phi() << std::endl;
    std::cout << "    check jet orig,corr,raw,l1rc" << jet_orig.Pt() << " " << jet_corr.Pt() << " " << jet_raw.Pt() << " " << jet_corrL1rc.Pt() << std::endl;
    std::cout << "    check jet_corrL1rc x " << jet_corrL1rc.Px() << " " << jet_corrL1rc.Py() << " " << jet_corrL1rc.Pt() << std::endl;
    std::cout << "    check jet_corr x " << jet_corr.Px() << " " << jet_corr.Py() << " " << jet_corr.Pt() <<std::endl;
    std::cout << "    check jet_raw x " << jet_raw.Px() << " " << jet_raw.Py() << " " << jet_raw.Pt() << std::endl;
    */
    if(label==1 || label==3) met += jet_corrL1rc - jet_corr;
    if(label==2) met += jet_orig - jet_corr - (jet_raw - jet_corrL1rc);
    if(label==4) met += jet_orig - jet_corr;
    if(label==5) met += jet_raw - jet_corr; // rawPuppiMET += jet_raw - jet_corr
    //met += (1-corr_withL2L3)*jet_raw;
    //std::cout << "    check met building " << met.Pt() << " " << met.Phi() << std::endl;
  }
  /*
  std::cout << "check end Type-1 loop" << std::endl;
  std::cout << "check MET corr,phi " << met.Pt() << " " << met.Phi() << std::endl;
  std::cout << "check MET x,y " << met.Px() << " " << met.Py() << std::endl;
  */
//  std::vector<TLorentzVector> leptons = leptonsPlus;
//  leptons.insert(std::end(leptons), std::begin(leptonsMinus), std::end(leptonsMinus));
//  for(size_t i = 0; i < leptons.size(); ++i){
//    met += -leptons[i].Vect();
//  }

  return met;

    
  //  TVector3 met;
  //  met.SetPtEtaPhi(MET_pt,0,MET_phi);
  //std::cout << "check met " << MET_pt << " " << MET_phi << std::endl;
  /*
  for(size_t i = 0; i < jets.size(); ++i){
    jec_L1fjL2RelL2Abs->setJetPt(Jet_pt[i]);
    jec_L1fjL2RelL2Abs->setJetEta(Jet_eta[i]);
    jec_L1fjL2RelL2Abs->setJetA(Jet_area[i]);
    jec_L1fjL2RelL2Abs->setRho(rho);
    float corr_noL2L3 = jec_L1fjL2RelL2Abs->getCorrection();

    jec_L1fjL2RelL2AbsL2L3Res->setJetPt(Jet_pt[i]);
    jec_L1fjL2RelL2AbsL2L3Res->setJetEta(Jet_eta[i]);
    jec_L1fjL2RelL2AbsL2L3Res->setJetA(Jet_area[i]);
    jec_L1fjL2RelL2AbsL2L3Res->setRho(rho);
    float corr_withL2L3 = jec_L1fjL2RelL2AbsL2L3Res->getCorrection();
    //    std::cout << "check jet " << Jet_pt[i] << " " << corr1 << " " << corr2 << std::endl;

    TVector3 jet;
    jet.SetPtEtaPhi(Jet_pt[i],0,Jet_phi[i]);
    ////    met += (corr_noL2L3 - corr_withL2L3)*jet;
  }
  */
  return met;
}

bool Analysis::passing(double value, std::string cut){
  //  cuts = {"0_30","30_50","50_70","70_100","100_140","140_200","200_Inf"}
  std::regex cut_re("(\\S+)_(\\S+)");
  std::smatch match;
  if (std::regex_search(cut, match, cut_re)){
    //std::cout << "check Analysis::passing re " << match.size() << " " << match[1] << " " << match[2] << std::endl;
    float lowEdge = atof(match.str(1).c_str());
    float highEdge = atof(match.str(2).c_str());
    //std::cout << "check Analysis::passing " << value << " " << lowEdge  << " " << highEdge << std::endl;
    if(lowEdge < fabs(value) && fabs(value) < highEdge) return true;
    if(highEdge == 0 && lowEdge < fabs(value)) return true;
    //    std::cout << "check atoi " << lowEdge << " " << highEdge << std::endl; 
  }
  //  exit(0);
  return false;
}

int Analysis::getJetFlavour(int jetIndex){
  if(isData) return -1;
  //std::cout << "check jet flavor " << Jet_hadronFlavour[jetIndex] << " " << Jet_partonFlavour[jetIndex] << std::endl;
  return abs(Jet_partonFlavour[jetIndex]);
}
/*
size_t Analysis::getEtaBin(double eta){
  for(size_t i = 0; i < nbins_eta-1; ++i){
    if(fabs(eta) > bins_eta[i] && fabs(eta) < bins_eta[i+1]) return i;
  }
  return 999;
}
*/
bool Analysis::Process(Long64_t entry) {
  printStatus();
  //std::cout << "check Analysis::Process " << std::endl;
  //if(entry%100000 == 0) std::cout << datasetName << " entry " << entry << "/" << fChain->GetEntries() << std::endl;
  //else std::cout << entry << std::endl;
  //std::streamsize sss = std::cout.precision();
  //if(entry%100000 == 0) std::cout << "  Processed " << std::setprecision(2) << std::setw(3) << cMain->getCount("all events")/fChain->GetEntries() << std::setprecision(sss) << std::setw(6) << "    \r";
  
  //  fChain->GetEntry(entry);
  fChain->GetTree()->GetEntry(entry);

  //std::cout << "check Analysis::Process run:lumi:event " << run << ":" << luminosityBlock << ":" << event << std::endl;
  if(!pickEvents->pick(run,luminosityBlock,event)) return false;

  //  if(!isData) std::cout << "eventWeight " << eventWeight << ",  pileupWeight " << Pileup_nTrueInt << " " << pileupWeight->getWeight(Pileup_nTrueInt) << std::endl;
  double totEventWeight = 1.0;
  if(isData) hprof_nz_run_incl->Fill(run,totEventWeight);
  //if(!isData) totEventWeight = eventWeight*pileupWeight->getWeight(Pileup_nTrueInt);
  if(pileupreweightingON && !isData) {
    totEventWeight = pileupWeight->getWeight(Pileup_nTrueInt);
    pu_mc_reweighted->Fill(Pileup_nTrueInt,totEventWeight);
  }
  //if(!isData && Pileup_nTrueInt < 10) return false; // REMOVEME 05112024/SLehti
  //if(!isData) std::cout << "eventWeight " << totEventWeight << " " << Pileup_nTrueInt << " " << pileupWeight->getWeight(Pileup_nTrueInt) << std::endl;
  if(totEventWeight <= 0) return false;

  float mu = 0;
  if(isData) mu = getAvgPU(run,luminosityBlock);
  else mu = Pileup_nTrueInt;

  if(!isData && mu > 100) return false;
  cMain->increment("pileup<=100",totEventWeight);

  if(totEventWeight > 1000) {
    std::cout << "Warning, totEventWeight > 1000! run:lumi:event " << run << ":" << luminosityBlock << ":" << event << std::endl;
    return false;
  }
  cMain->increment("all events",totEventWeight);
  //  if(!isData) std::cout << "all events " << cMain->getCount("all events") << std::endl;

  if(isData) {
    if(!lumimask->filter(run,luminosityBlock)) return false;
    cMain->increment("lumimask",totEventWeight);
  }

  // PS weight
/*
  psWeights[0] = 1.0;
  std::cout << "check 4.1" << std::endl;
  if(!isData){
    //one should use genWeight * PSWeight[i] / PSWeight[1]
    //in nAODv5 and nAODv6 instead of the normally expected
    //genWeight * PSWeight[i] which will be the default for nAODv7.
    //PSWeight[i] / PSWeight[1]
    //    for(size_t i = 1; i < npsWeights; ++i) psWeights[i] = PSWeight[i-1]/PSWeight[1];
    for(size_t i = 1; i < npsWeights; ++i) psWeights[i] = PSWeight[i-1];

    h_psweight0->Fill(PSWeight[0]);
    h_psweight1->Fill(PSWeight[1]);
    h_psweight2->Fill(PSWeight[2]);
    h_psweight3->Fill(PSWeight[3]);
  }

  if(!isData)
  for(size_t iPS = 0; iPS < npsWeights; ++iPS){
    std::cout << "check ps weights " << psWeights[iPS] << std::endl;
    //    totEventWeight = totEventWeight*psWeights[iPS];
  }  
  */

  // trigger
  bool trgDecision = this->getTriggerDecision();
  if(! trgDecision) return false;
  cMain->increment("trigger",totEventWeight);
  //std::cout << "check pass trigger " << std::endl;

  bool metCleaningDecision = this->METCleaning();
  if(! metCleaningDecision) return false;
  cMain->increment("MET cleaning",totEventWeight);


  // leptons
  //std::cout << "check leptonflavor " << leptonflavor << std::endl;

  std::vector<TLorentzVector> allLeptons;
  std::vector<TLorentzVector> muonsp = getMuons(1);
  std::vector<TLorentzVector> muonsn = getMuons(-1);
  std::vector<TLorentzVector> electronsp = getElectrons(1);
  std::vector<TLorentzVector> electronsn = getElectrons(-1);
  //std::cout << "check muon q " << muonsp.size() << " " << muonsn.size() << std::endl;
  //std::cout << "check electron q " << electronsp.size() << " " << electronsn.size() << std::endl; 
  //  allLeptons.insert(std::end(allLeptons), std::begin(muonsp), std::end(muonsp));
  //  allLeptons.insert(std::end(allLeptons), std::begin(muonsn), std::end(muonsn));
  //  allLeptons.insert(std::end(allLeptons), std::begin(electronsp), std::end(electronsp));
  //  allLeptons.insert(std::end(allLeptons), std::begin(electronsn), std::end(electronsn));

  if(leptonflavor == 13){
    leptonsPlus  = muonsp;
    leptonsMinus = muonsn;
  }
  if(leptonflavor == 11){
    leptonsPlus  = electronsp;
    leptonsMinus = electronsn;
  }
  if(leptonflavor == -13){
    if(muonsp.size() > 0 && electronsn.size() > 0){
      leptonsPlus = muonsp;
      leptonsMinus = electronsn;
    }
    if(muonsn.size() > 0 && electronsp.size() > 0){
      leptonsPlus = electronsp;
      leptonsMinus = muonsn;
    }
  }
  allLeptons.insert(std::end(allLeptons), std::begin(leptonsPlus), std::end(leptonsPlus));
  allLeptons.insert(std::end(allLeptons), std::begin(leptonsMinus), std::end(leptonsMinus));

  int nLepton = allLeptons.size();
  if(nLepton < 2 || nLepton > 3) return false;
  cMain->increment("2-3 leptons",totEventWeight);
  /*
  std::cout << "leptons " << leptonsPlus.size() << " " << leptonsMinus.size() << std::endl;
  for(int il = 0; il < allLeptons.size(); ++il){
    std::cout << "check leptons " << allLeptons[il].Pt() << " " << allLeptons[il].Eta() << std::endl;
  }
  */
  int nMuEle = leptonsPlus.size() + leptonsMinus.size();
  if(nMuEle < 2 || nMuEle > 3) return false;
  //if(nMuEle !=  3) return false;
  cMain->increment("2-3 e or 2-3 mu",totEventWeight);
  if(leptonsPlus.size() * leptonsMinus.size() == 0) return false;
  cMain->increment("lepton charge",totEventWeight);

  //if(nLepton == 2) cMain->increment("2 leptons",totEventWeight);
  //  std::cout << "check run:lumi:event " << run << ":" << luminosityBlock << ":" << event << std::endl;

  // Z boson
  TLorentzVector Zboson = getZboson(leptonsPlus,leptonsMinus);
  if(leptonsMinus.size() > 0) h_Zpt_leptonm_pt->Fill(Zboson.Pt(),leptonsMinus[0].Pt());
  if(leptonsPlus.size() > 0) h_Zpt_leptonp_pt->Fill(Zboson.Pt(),leptonsPlus[0].Pt());

  std::vector<TLorentzVector> leptonsFromZ;
  leptonsFromZ.insert(std::end(leptonsFromZ), std::begin(leptonsPlus), std::end(leptonsPlus));
  leptonsFromZ.insert(std::end(leptonsFromZ), std::begin(leptonsMinus), std::end(leptonsMinus));
  
  for(int iLep = 0; iLep < nLepton; iLep++){
    h_leptonpt->Fill(allLeptons[iLep].Pt());
    h_leptoneta->Fill(allLeptons[iLep].Eta());
  }

  /*
  std::cout << "check leptons " << std::endl;
  for(size_t i = 0; i < leptonsPlus.size(); ++i){
    std::cout << "check l+ " << leptonsPlus[i].Pt() << " " << leptonsPlus[i].Eta() << " " << leptonsPlus[i].Phi() << " " << std::endl;
  }
  for(size_t i = 0; i < leptonsMinus.size(); ++i){
    std::cout << "check l- " << leptonsMinus[i].Pt() << " " << leptonsMinus[i].Eta() << " " << leptonsMinus[i].Phi() << " " << std::endl;
  }
  */
  TLorentzVector genZboson;
  if(!isData){
    std::vector<TLorentzVector> genleptonsPlus = getGenParticles(-leptonflavor);
    std::vector<TLorentzVector> genleptonsMinus = getGenParticles(leptonflavor);
    /*
    for(size_t i = 0; i < genleptonsPlus.size(); ++i){
      std::cout << "check gen l+ " << genleptonsPlus[i].Pt() << " " << genleptonsPlus[i].Eta() << " " << genleptonsPlus[i].Phi() << " " << std::endl;
    }
    for(size_t i = 0; i < genleptonsMinus.size(); ++i){
      std::cout << "check gen l- " << genleptonsMinus[i].Pt() << " " << genleptonsMinus[i].Eta() << " " << genleptonsMinus[i].Phi() << " " << std::endl;
    }
    */
    genZboson = getZboson(genleptonsPlus,genleptonsMinus);
  }

  //h_Zpt->Fill(Zboson.Pt());
  //h_Zeta->Fill(Zboson.Eta());
  //h_Zmass->Fill(Zboson.M());
  
  //if(Zboson.Pt() < 30) return false;
  //if(Zboson.Pt() < 50 || Zboson.Pt() > 70) return false;
  //if(Zboson.Pt() < 100 || Zboson.Pt() > 140) return false;
  //if(Zboson.Pt() < 70 || Zboson.Pt() > 100) return false;
  //if(Zboson.Pt() < 140 || Zboson.Pt() > 200) return false;
  //if(! (Zboson.Pt() > 0) ) return false;
  if(Zboson.Pt() == 0) return false;
  //std::cout << "check Zboson.Pt " << Zboson.Pt() << std::endl;
  cMain->increment("Z boson",totEventWeight);

  h_Zpt_Rho_1->Fill(Zboson.Pt(),fixedGridRhoFastjetAll,totEventWeight);

  if(!passLeptonPtCut(leptonsFromZ,leptonflavor)) return false;
  cMain->increment("lepton pt cuts");

  if(isData){
    hprof_mz_run_zpt0->Fill(run,Zboson.M(),totEventWeight);
    hprof_nz_run_zpt0->Fill(run,totEventWeight);
  }
  std::vector<int> jetSelection = getJetIndexList();
  /*
  for(size_t i = 0; i < jetSelection.size(); ++i){
    TLorentzVector j = getJet(jetSelection[i]);
    //std::cout << "check all jets " << j.Pt() << " " << j.Eta() << " " << j.Phi() << std::endl;
  }
  */
  h_njets->Fill(nJet);
  jetSelection = drClean(jetSelection,allLeptons,0.3);
  //jetSelection = drClean(jetSelection,leptonsPlus);
  //jetSelection = drClean(jetSelection,leptonsMinus);
  /*
  std::cout << "check njets " << jetSelection.size() << std::endl;
  for(size_t i = 0; i < jetSelection.size(); ++i){
    TLorentzVector j = getJet(jetSelection[i]);
    std::cout << "check jets1 " << j.Pt() << " " << j.Eta() << " " << j.Phi() << std::endl;
  }
  */

  applyJEC(jetSelection);
  /*
  for(size_t i = 0; i < jetSelection.size(); ++i){
    TLorentzVector j = getJet(jetSelection[i]);
    std::cout << "check jets2 (after applyJEC) " << j.Pt() << " " << j.Eta() << " " << j.Phi() << std::endl;
  }
  */
  jetSelection = ptOrder(jetSelection);
  for(size_t i = 0; i < jetSelection.size(); ++i){
    TLorentzVector j = getJet(jetSelection[i]);
    //std::cout << "check jets3 (after ptorder) " << j.Pt() << " " << j.Eta() << " " << j.Phi() << std::endl;
  }
  //  jetSelection = jetId(jetSelection);
  //std::cout << "check jetSelection " << getJetIndexList().size() << " " << jetSelection.size() << std::endl;
  h_njets_sele->Fill(jetSelection.size());
  if(jetSelection.size() == 0) return false;
  for(size_t i = 0; i < jetSelection.size(); ++i){
    TLorentzVector j = getJet(jetSelection[i]);
    //std::cout << "check sele jets " << j.Pt() << " " << j.Eta() << " " << j.Phi() << std::endl;
  }

  /*
  std::vector<TLorentzVector> jets = getJets();
  //size_t nj = jets.size();
  jets = drClean(jets,muonsPlus);
  jets = drClean(jets,muonsMinus);
  //std::cout << "check jet cleaning " << nj << " " << jets.size() << " " << nj - jets.size() << std::endl;
  h_njets_sele->Fill(jets.size());
  if(jets.size() == 0) return false;
  */
  cMain->increment("jets",totEventWeight);

  int leadingJetIndex = getLeadingJetIndex(jetSelection);
  //std::cout << "------------------>check leadingJetIndex " << leadingJetIndex << std::endl;
  //std::cout << "check jet id " << Jet_jetId[leadingJetIndex] << std::endl;
  /*
  if(!isData){
    int jetGenFlavor = getJetFlavour(leadingJetIndex);
    cJetLabel->increment("all jets");
    switch(jetGenFlavor){
    case 5: cJetLabel->increment("b jets");
      break;
    case 4: cJetLabel->increment("c jets");
      break;
    case 21: cJetLabel->increment("gluon jets");
      break;
    default: cJetLabel->increment("uds jets");
      break;
    }
  }
  */
  TLorentzVector leadingJet = getJet(leadingJetIndex);
  //h_jet1pt->Fill(leadingJet.Pt());
  //h_jet1eta->Fill(leadingJet.Eta());

  //std::cout << "check leading jet " << run << ":" << luminosityBlock << ":" << event << " " << Jet_qgl[leadingJetIndex] << std::endl;
  /*
  h_JetPt_muEF->Fill(leadingJet.Pt(),Jet_muEF[leadingJetIndex]);
  h_PtAve_muEF->Fill(leadingJet.Pt(),Jet_muEF[leadingJetIndex]);
  if(Jet_muEF[leadingJetIndex] < 0.90){
    h_JetPt_chHEF->Fill(leadingJet.Pt(),Jet_chHEF[leadingJetIndex]);
    h_JetPt_neHEF->Fill(leadingJet.Pt(),Jet_neHEF[leadingJetIndex]);
    h_JetPt_neEmEF->Fill(leadingJet.Pt(),Jet_neEmEF[leadingJetIndex]);
    h_JetPt_chEmEF->Fill(leadingJet.Pt(),Jet_chEmEF[leadingJetIndex]);
  }
  */
  double DRmin = 999;
  int leadingGenJetIndex = -1;
  if(!isData){
    size_t n = nGenJet;
    for(size_t i = 0; i < n; ++i){
      TLorentzVector gjet = getGenJet(i);
      double DR = ROOT::Math::VectorUtil::DeltaR(leadingJet,gjet);
      if(DR < DRmin){
        DRmin = DR;
        leadingGenJetIndex = i;
      }
    }
  }

  TLorentzVector leadingGenJet;
  if(leadingGenJetIndex >= 0){
    leadingGenJet = getGenJet(leadingGenJetIndex);
    //std::cout << "check leading jet1 " << leadingJet.Pt() << " " << leadingGenJet.Pt() << std::endl;
  }

  //  size_t etaBin = getEtaBin(leadingJet.Eta());
  //std::cout << "check leading jet " << leadingJet.Pt() << std::endl;
  //if(leadingJet.Pt() < 12 || etaBin > nbins_eta) return false;
  //  if(leadingJet.Pt() < 12 || fabs(leadingJet.Eta()) > 2.5) return false;
  if(leadingJet.Pt() < 12) return false;
  //std::cout << "check leading jet1 " << cMain->getCount("leading jet") << " " << totEventWeight <<std::endl;
  cMain->increment("leading jet",totEventWeight);
  if(jetSelection[0] != 0) cMain->increment("  0-jet == muon",totEventWeight);
  if(leadingJetIndex == 0) cMain->increment("  leading jet == 0-jet",totEventWeight);

  //std::cout << "check leading jet2 " << cMain->getCount("leading jet") << std::endl;
  //if(cMain->getCount("leading jet") < 0) exit(0);
  //std::cout << "check run:lumi:event " << run << ":" << luminosityBlock << ":" << event << std::endl;
//std::cout << "check event,ljet(pt,eta,phi) " << event << " " << leadingJet.Pt() << " " << leadingJet.Eta() << " " << leadingJet.Phi() << std::endl;
  bool passed = jetvetoMap->pass(leadingJet);
  //std::cout << "check jetvetoMap " << passed << std::endl;
  if(!passed) return false;
  cMain->increment("jetvetomap",totEventWeight);

  double ptave = -1;
  if(leadingJet.Pt() > 12 && Zboson.Pt() > 12) ptave = 0.5*(leadingJet.Pt() + Zboson.Pt());
  double ptdiff = (leadingJet.Pt() - Zboson.Pt())/Zboson.Pt();

  double offset = Zboson.Pt() - leadingJet.Pt();

  //std::cout << "check phiBB pt " << Zboson.Pt() << " " << leadingJet.Pt() << std::endl;
  //std::cout << "check phiBB phi " << Zboson.Phi() << " " << leadingJet.Phi() << std::endl;
  double phiBB = fabs(ROOT::Math::VectorUtil::DeltaPhi(leadingJet,Zboson)-TMath::Pi());
  h_phibb->Fill(phiBB);
  double phiBB_cut = 0.44; // 0.34;
  //std::cout << "check phiBB " << phiBB << std::endl;
  if(phiBB > phiBB_cut && phiBB < 2*TMath::Pi()-phiBB_cut) return false;
  cMain->increment("phiBB",totEventWeight);

  if(!jetId(leadingJetIndex)) return false;
  cMain->increment("leading jet jetId",totEventWeight);
  //std::cout << "check run:lumi:event " << run << ":" << luminosityBlock << ":" << event << std::endl;

  //  if(!isData){
  //    if(!matchGenJetFound(leadingJetIndex)) return false;
  //    cMain->increment("leading jet matched to genjet",totEventWeight);
  //  }

  pickEvents->fill(run,luminosityBlock,event);

  TLorentzVector hlt_lep1 = triggerMatching(leptonsMinus[0]);
  TLorentzVector hlt_lep2 = triggerMatching(leptonsPlus[0]);
  if(hlt_lep1.Pt() > 0) h_lep1pt_hltoveroffline->Fill(hlt_lep1.Pt()/leptonsMinus[0].Pt());
  if(hlt_lep2.Pt() > 0) h_lep2pt_hltoveroffline->Fill(hlt_lep2.Pt()/leptonsPlus[0].Pt());

  h_lep1pt->Fill(leptonsMinus[0].Pt());
  h_lep1eta->Fill(leptonsMinus[0].Eta());
  h_lep2pt->Fill(leptonsPlus[0].Pt());
  h_lep2eta->Fill(leptonsPlus[0].Eta());

  h_Zpt->Fill(Zboson.Pt());
  h_Zeta->Fill(Zboson.Eta());
  h_Zmass->Fill(Zboson.M());
  h_Mmumu->Fill(Zboson.M());
  h_jet1pt->Fill(leadingJet.Pt());
  h_jet1eta->Fill(leadingJet.Eta());
  h_njets_aas->Fill(jetSelection.size());

  for(size_t i = 0; i < jetSelection.size(); ++i){
    TLorentzVector j = getJet(jetSelection[i]);
    if(j.Pt() > 20) h_jetseta_pt20->Fill(j.Eta());
    if(j.Pt() > 30) h_jetseta_pt30->Fill(j.Eta());
    if(j.Pt() > 40) h_jetseta_pt40->Fill(j.Eta());
    //std::cout << "check before jetPt jets " << j.Pt() << " " << j.Eta() << " " << j.Phi() << std::endl;
  }
  //for(size_t i = 0; i < jetSelection.size(); ++i){
  //  TLorentzVector j = getJet(jetSelection[i]);
  //  //std::cout << "check before jetPt jets " << j.Pt() << " " << j.Eta() << " " << j.Phi() << std::endl;
  //}
  jetSelection = jetPt(jetSelection,10);
  h_njets_pt->Fill(jetSelection.size());
  for(size_t i = 0; i < jetSelection.size(); ++i){
    TLorentzVector j = getJet(jetSelection[i]);
    if(j.Pt() > 20) h_jetseta_pt20->Fill(j.Eta());
    if(j.Pt() > 30) h_jetseta_pt30->Fill(j.Eta());
    if(j.Pt() > 40) h_jetseta_pt40->Fill(j.Eta());
    //std::cout << "check before jetId jets " << j.Pt() << " " << j.Eta() << " " << j.Phi() << std::endl;
  }
  //jetSelection = jetId(jetSelection);
  h_njets_id->Fill(jetSelection.size());

  TLorentzVector subLeadingJet = getJet(getLeadingJetIndex(jetSelection,1));
  if(subLeadingJet.Pt() > 0){ 
    h_jet2pt->Fill(subLeadingJet.Pt());
    h_jet2eta->Fill(subLeadingJet.Eta());
  }

  TLorentzVector jets_all(0,0,0,0);
  /*
  for(size_t i = 0; i < jetSelection.size(); ++i) jets_all += getJet(i);
  for(size_t i = 0; i < leptonsPlus.size(); ++i) jets_all += leptonsPlus[i];
  for(size_t i = 0; i < leptonsMinus.size(); ++i) jets_all += leptonsMinus[i];
  */
  std::vector<int> jetIndex_all = jetSelection; //getJetIndexList();
  /*
  std::vector<TLorentzVector> allMus;
  for(size_t i = 0; i < nMuon; ++i){
    double pt = Muon_pt[i];
    double eta = Muon_eta[i];
    double phi = Muon_phi[i];
    double m = Muon_mass[i];

    TLorentzVector mu;
    mu.SetPtEtaPhiM(pt,eta,phi,m);
    allMus.push_back(mu);
    //jets_all+=mu;
  }
  */
  //  jetIndex_all = drClean(jetIndex_all,allMus);
  for(size_t i = 0; i < jetIndex_all.size(); ++i){
    //TLorentzVector j = getJet(i);
    TLorentzVector j = getJet(jetIndex_all[i]);
    if(j.Pt() > 15) jets_all+=j;
  }
  TLorentzVector jets_notLeadingJet = jets_all - leadingJet;

  //std::cout << "check jets " << jets_all.Pt() << " " << (jets_notLeadingJet+leadingJet).Pt() << std::endl;
  //cMain->increment("subleading jet",totEventWeight);
  /*
  // Z boson
  std::cout << "check1 " << muonsPlus.size() << " " << muonsMinus.size() << std::endl;
  TLorentzVector Zboson = getZboson(muonsPlus,muonsMinus);
  std::cout << "check2 " << muonsPlus.size() << " " << muonsMinus.size() << std::endl;
  
  h_Zpt->Fill(Zboson.Pt());

  if(Zboson.Pt() < 30) return false;
  //  if(Zboson.Pt() < 70 || Zboson.Pt() > 100) return false;
  //if(Zboson.Pt() < 140 || Zboson.Pt() > 200) return false;
  cMain->increment("Z boson",totEventWeight);
  */
  //for (size_t i = 0; i < jets.size(); ++i){
  //  std::cout << "check jet " << i << " " << jets[i].Pt() << std::endl;
  //}
  //std::cout << "check leading " << leadingJet.Pt() << std::endl;
  //std::cout << "check subleading " << subLeadingJet.Pt() << " " << Zboson.Pt() << std::endl;
  //std::cout << "check 1 " << Zboson.Pt() << std::endl;
  double alpha = subLeadingJet.Pt()/Zboson.Pt();
  //std::cout << "check 2" << std::endl;
  //  double alpha = subLeadingJet.Pt()/Zboson.Pt();
  if(subLeadingJet.Pt() < 15) alpha = 0;
  // if(subLeadingJet.Pt() < 20) alpha = 0;

  //h_alpha->Fill(alpha);

  if(Zboson.Pt() * alpha > 15){
  if(alpha < 0.10) h_Zpt_a10->Fill(Zboson.Pt());
  if(alpha < 0.15) h_Zpt_a15->Fill(Zboson.Pt());
  if(alpha < 0.20) h_Zpt_a20->Fill(Zboson.Pt());
  if(alpha < 0.30) h_Zpt_a30->Fill(Zboson.Pt());
  if(alpha < 0.40) h_Zpt_a40->Fill(Zboson.Pt());
  if(alpha < 0.50) h_Zpt_a50->Fill(Zboson.Pt());
  if(alpha < 0.60) h_Zpt_a60->Fill(Zboson.Pt());
  if(alpha < 0.80) h_Zpt_a80->Fill(Zboson.Pt());
  if(alpha < 1.0)  h_Zpt_a100->Fill(Zboson.Pt());
  }
  //for(size_t i = 0; i < cuts.size(); ++i){
  //  if(passing(Zboson.Pt(),cuts[i])) h_alphaZpt[i]->Fill(alpha);
  //}

  if(alpha < 0.3) cMain->increment("  alpha0.3",totEventWeight);
  if(alpha < 1.0) cMain->increment("  alpha1.0",totEventWeight);

  for(size_t i = 0; i < jetSelection.size(); ++i){
    TLorentzVector j = getJet(jetSelection[i]);
    //std::cout << "check type-1 jets " << j.Pt() << " " << j.Eta() << " " << j.Phi() << std::endl;
  }
  //  h_METorig->Fill(ChsMET_pt);
  TVector3 MET;
  if(atoi(year) > 2020){
    MET = recalculateMET(jetSelection,5); // PUPPI
  }else{
    MET = recalculateMET(jetSelection,1); //ChsMET
  }
  h_MET->Fill(MET.Pt());

  TVector3 METuncl = -MET - jets_all.Vect() - Zboson.Vect();

  //TVector3 METuncl = jets_all.Vect();
  //std::cout << "check MET uncl " << METuncl.Pt() << " " << TVector3(MET_MetUnclustEnUpDeltaX,MET_MetUnclustEnUpDeltaY,0).Pt() << std::endl;  
  float R_pT = leadingJet.Pt()/Zboson.Pt();
  //std::cout << "check run:lumi:event " << run << ":" << luminosityBlock << ":" << event << " check R_pT " << R_pT << " " << leadingJet.Pt() << " " << Zboson.Pt() << std::endl;

  //std::cout << "check R_pT " << R_pT << " " << leadingJet.Pt() << " " << Zboson.Pt() << std::endl;
  //std::cout << "check h_RpT " << h_RpT->GetEntries() << std::endl;
  float R_MPF = 1 + MET.Pt()*(cos(MET.Phi())*Zboson.Px() + sin(MET.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
  //  if(alpha < 1.0) std::cout << "check run:lumi:event " << run << ":" << luminosityBlock << ":" << event << " check RMPF " << R_MPF << " " << Zboson.Pt() << std::endl;
  float R_MPFjet1 = -leadingJet.Pt()*(cos(leadingJet.Phi())*Zboson.Px() + sin(leadingJet.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
  float R_MPFjetn = -jets_notLeadingJet.Pt()*(cos(jets_notLeadingJet.Phi())*Zboson.Px() + sin(jets_notLeadingJet.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
  float R_MPFuncl = -METuncl.Pt()*(cos(METuncl.Phi())*Zboson.Px() + sin(METuncl.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
  //std::cout << "check MPF " << R_MPF << " " << R_MPFjet1 << " " << R_MPFjetn << " " << R_MPFuncl << " " << R_MPFjet1+R_MPFjetn+R_MPFuncl << std::endl;

  double jreso = 1.;
  if (Jet_RES[leadingJetIndex] != 0) jreso = 1./Jet_RES[leadingJetIndex];
  double jes = (1. - Jet_rawFactor[leadingJetIndex]);
  double jsf = Jet_CF[leadingJetIndex];
  TLorentzVector p4m0, p4m2, p4mn, p4mu;
  p4m0.SetPtEtaPhiM(MET.Pt(),0,MET.Phi(),0); // type-I MET
  TLorentzVector zljet = -(leadingJet+Zboson);
  p4m2.SetPtEtaPhiM(zljet.Pt(),0,zljet.Phi(),0);
  TLorentzVector njet = -jets_notLeadingJet;
  p4mn.SetPtEtaPhiM(njet.Pt(),0,njet.Phi(),0);
  //std::cout << "check p4mn " << jets_notLeadingJet.Pt() << " " << jets_notLeadingJet.Phi() << " " << njet.Phi() << std::endl;
  p4mu = p4m0 -p4m2 -p4mn;
  l2resHistograms->fill(Zboson,leadingJet,jreso,jes,jsf,p4m0,p4m2,p4mn,p4mu,totEventWeight);
  /*
  std::cout << "check run:lumi:event " << run << ":" << luminosityBlock << ":" << event << std::endl;
  std::cout << "check leadingjet pt " << leadingJet.Pt() << std::endl;
  std::cout << "check Z pt " << Zboson.Pt() << std::endl;
  std::cout << "check R_pT " << R_pT << std::endl;
  std::cout << "check MET " << MET.Pt() << std::endl;
  std::cout << "check METuncl " << METuncl.Pt() << std::endl;
  std::cout << "check R_MPF " << R_MPF << std::endl;
  std::cout << "check R_MPF1 " << R_MPFjet1 << std::endl;
  std::cout << "check R_MPFn " << R_MPFjetn << std::endl;
  */
  double pi2 = acos(0.0); // pi/2
  float R_MPFx = MET.Pt()*(cos(MET.Phi()+pi2)*Zboson.Px() + sin(MET.Phi()+pi2)*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
  float R_MPFjet1x = -leadingJet.Pt()*(cos(leadingJet.Phi()+pi2)*Zboson.Px() + sin(leadingJet.Phi()+pi2)*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());

  //TVector3 METfromType1 = recalculateMET(jetSelection,2);
  //float R_MPFfromType1 = 1 + METfromType1.Pt()*(cos(METfromType1.Phi())*Zboson.Px() + sin(METfromType1.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());

  //TVector3 METPF = recalculateMET(jetSelection,3);
  //float R_MPFPF = 1 + METPF.Pt()*(cos(METPF.Phi())*Zboson.Px() + sin(METPF.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());

  if(isData && alpha < 1.0 && fabs(leadingJet.Eta()) < 1.3){
    if(Zboson.Pt() > 15){
      hprof_db_run_zpt15->Fill(run,R_pT,totEventWeight);
      hprof_mpf_run_zpt15->Fill(run,R_MPF,totEventWeight);
      hprof_chHEF_run_zpt15->Fill(run,Jet_chHEF[leadingJetIndex],totEventWeight);
      hprof_neHEF_run_zpt15->Fill(run,Jet_neHEF[leadingJetIndex],totEventWeight);
      hprof_chEmEF_run_zpt15->Fill(run,Jet_chEmEF[leadingJetIndex],totEventWeight);
      hprof_neEmEF_run_zpt15->Fill(run,Jet_neEmEF[leadingJetIndex],totEventWeight);
      hprof_muEF_run_zpt15->Fill(run,Jet_muEF[leadingJetIndex],totEventWeight);
      hprof_mz_run_zpt15->Fill(run,Zboson.M(),totEventWeight);
      hprof_nz_run_zpt15->Fill(run,totEventWeight);
    }
    if(leadingJet.Pt() > 15){
      hprof_db_run_JetPt15->Fill(run,R_pT,totEventWeight);
      hprof_mpf_run_JetPt15->Fill(run,R_MPF,totEventWeight);
      hprof_chHEF_run_JetPt15->Fill(run,Jet_chHEF[leadingJetIndex],totEventWeight);
      hprof_neHEF_run_JetPt15->Fill(run,Jet_neHEF[leadingJetIndex],totEventWeight);
      hprof_chEmEF_run_JetPt15->Fill(run,Jet_chEmEF[leadingJetIndex],totEventWeight);
      hprof_neEmEF_run_JetPt15->Fill(run,Jet_neEmEF[leadingJetIndex],totEventWeight);
      hprof_muEF_run_JetPt15->Fill(run,Jet_muEF[leadingJetIndex],totEventWeight);
      hprof_mz_run_JetPt15->Fill(run,Zboson.M(),totEventWeight);
      hprof_nz_run_JetPt15->Fill(run,totEventWeight);
    }
    if(ptave > 15){
      hprof_db_run_PtAve15->Fill(run,R_pT,totEventWeight);
      hprof_mpf_run_PtAve15->Fill(run,R_MPF,totEventWeight);
      hprof_chHEF_run_PtAve15->Fill(run,Jet_chHEF[leadingJetIndex],totEventWeight);
      hprof_neHEF_run_PtAve15->Fill(run,Jet_neHEF[leadingJetIndex],totEventWeight);
      hprof_chEmEF_run_PtAve15->Fill(run,Jet_chEmEF[leadingJetIndex],totEventWeight);
      hprof_neEmEF_run_PtAve15->Fill(run,Jet_neEmEF[leadingJetIndex],totEventWeight);
      hprof_muEF_run_PtAve15->Fill(run,Jet_muEF[leadingJetIndex],totEventWeight);
      hprof_mz_run_PtAve15->Fill(run,Zboson.M(),totEventWeight);
      hprof_nz_run_PtAve15->Fill(run,totEventWeight);
    }
    if(Zboson.Pt() > 30){
      hprof_db_run_zpt30->Fill(run,R_pT,totEventWeight);
      hprof_mpf_run_zpt30->Fill(run,R_MPF,totEventWeight);
      hprof_chHEF_run_zpt30->Fill(run,Jet_chHEF[leadingJetIndex],totEventWeight);
      hprof_neHEF_run_zpt30->Fill(run,Jet_neHEF[leadingJetIndex],totEventWeight);
      hprof_chEmEF_run_zpt30->Fill(run,Jet_chEmEF[leadingJetIndex],totEventWeight);
      hprof_neEmEF_run_zpt30->Fill(run,Jet_neEmEF[leadingJetIndex],totEventWeight);
      hprof_muEF_run_zpt30->Fill(run,Jet_muEF[leadingJetIndex],totEventWeight);
      hprof_mz_run_zpt30->Fill(run,Zboson.M(),totEventWeight);
      hprof_nz_run_zpt30->Fill(run,totEventWeight);
    }
    if(leadingJet.Pt() > 30){
      hprof_db_run_JetPt30->Fill(run,R_pT,totEventWeight);
      hprof_mpf_run_JetPt30->Fill(run,R_MPF,totEventWeight);
      hprof_chHEF_run_JetPt30->Fill(run,Jet_chHEF[leadingJetIndex],totEventWeight);
      hprof_neHEF_run_JetPt30->Fill(run,Jet_neHEF[leadingJetIndex],totEventWeight);
      hprof_chEmEF_run_JetPt30->Fill(run,Jet_chEmEF[leadingJetIndex],totEventWeight);
      hprof_neEmEF_run_JetPt30->Fill(run,Jet_neEmEF[leadingJetIndex],totEventWeight);
      hprof_muEF_run_JetPt30->Fill(run,Jet_muEF[leadingJetIndex],totEventWeight);
      hprof_mz_run_JetPt30->Fill(run,Zboson.M(),totEventWeight);
      hprof_nz_run_JetPt30->Fill(run,totEventWeight);
    }
    if(ptave > 30){
      hprof_db_run_PtAve30->Fill(run,R_pT,totEventWeight);
      hprof_mpf_run_PtAve30->Fill(run,R_MPF,totEventWeight);
      hprof_chHEF_run_PtAve30->Fill(run,Jet_chHEF[leadingJetIndex],totEventWeight);
      hprof_neHEF_run_PtAve30->Fill(run,Jet_neHEF[leadingJetIndex],totEventWeight);
      hprof_chEmEF_run_PtAve30->Fill(run,Jet_chEmEF[leadingJetIndex],totEventWeight);
      hprof_neEmEF_run_PtAve30->Fill(run,Jet_neEmEF[leadingJetIndex],totEventWeight);
      hprof_muEF_run_PtAve30->Fill(run,Jet_muEF[leadingJetIndex],totEventWeight);
      hprof_mz_run_PtAve30->Fill(run,Zboson.M(),totEventWeight);
      hprof_nz_run_PtAve30->Fill(run,totEventWeight);
    }
    if(Zboson.Pt() > 50){
      hprof_db_run_zpt50->Fill(run,R_pT,totEventWeight);
      hprof_mpf_run_zpt50->Fill(run,R_MPF,totEventWeight);
      hprof_chHEF_run_zpt50->Fill(run,Jet_chHEF[leadingJetIndex],totEventWeight);
      hprof_neHEF_run_zpt50->Fill(run,Jet_neHEF[leadingJetIndex],totEventWeight);
      hprof_chEmEF_run_zpt50->Fill(run,Jet_chEmEF[leadingJetIndex],totEventWeight);
      hprof_neEmEF_run_zpt50->Fill(run,Jet_neEmEF[leadingJetIndex],totEventWeight);
      hprof_muEF_run_zpt50->Fill(run,Jet_muEF[leadingJetIndex],totEventWeight);
      hprof_mz_run_zpt50->Fill(run,Zboson.M(),totEventWeight);
      hprof_nz_run_zpt50->Fill(run,totEventWeight);
    }
    if(leadingJet.Pt() > 50){
      hprof_db_run_JetPt50->Fill(run,R_pT,totEventWeight);
      hprof_mpf_run_JetPt50->Fill(run,R_MPF,totEventWeight);
      hprof_chHEF_run_JetPt50->Fill(run,Jet_chHEF[leadingJetIndex],totEventWeight);
      hprof_neHEF_run_JetPt50->Fill(run,Jet_neHEF[leadingJetIndex],totEventWeight);
      hprof_chEmEF_run_JetPt50->Fill(run,Jet_chEmEF[leadingJetIndex],totEventWeight);
      hprof_neEmEF_run_JetPt50->Fill(run,Jet_neEmEF[leadingJetIndex],totEventWeight);
      hprof_muEF_run_JetPt50->Fill(run,Jet_muEF[leadingJetIndex],totEventWeight);
      hprof_mz_run_JetPt50->Fill(run,Zboson.M(),totEventWeight);
      hprof_nz_run_JetPt50->Fill(run,totEventWeight);
    }
    if(ptave > 50){
      hprof_db_run_PtAve50->Fill(run,R_pT,totEventWeight);
      hprof_mpf_run_PtAve50->Fill(run,R_MPF,totEventWeight);
      hprof_chHEF_run_PtAve50->Fill(run,Jet_chHEF[leadingJetIndex],totEventWeight);
      hprof_neHEF_run_PtAve50->Fill(run,Jet_neHEF[leadingJetIndex],totEventWeight);
      hprof_chEmEF_run_PtAve50->Fill(run,Jet_chEmEF[leadingJetIndex],totEventWeight);
      hprof_neEmEF_run_PtAve50->Fill(run,Jet_neEmEF[leadingJetIndex],totEventWeight);
      hprof_muEF_run_PtAve50->Fill(run,Jet_muEF[leadingJetIndex],totEventWeight);
      hprof_mz_run_PtAve50->Fill(run,Zboson.M(),totEventWeight);
      hprof_nz_run_PtAve50->Fill(run,totEventWeight);
    }
    if(Zboson.Pt() > 110){
      hprof_db_run_zpt110->Fill(run,R_pT,totEventWeight);
      hprof_mpf_run_zpt110->Fill(run,R_MPF,totEventWeight);
      hprof_chHEF_run_zpt110->Fill(run,Jet_chHEF[leadingJetIndex],totEventWeight);
      hprof_neHEF_run_zpt110->Fill(run,Jet_neHEF[leadingJetIndex],totEventWeight);
      hprof_chEmEF_run_zpt110->Fill(run,Jet_chEmEF[leadingJetIndex],totEventWeight);
      hprof_neEmEF_run_zpt110->Fill(run,Jet_neEmEF[leadingJetIndex],totEventWeight);
      hprof_muEF_run_zpt110->Fill(run,Jet_muEF[leadingJetIndex],totEventWeight);
      hprof_mz_run_zpt110->Fill(run,Zboson.M(),totEventWeight);
      hprof_nz_run_zpt110->Fill(run,totEventWeight);
    }
    if(leadingJet.Pt() > 110){
      hprof_db_run_JetPt110->Fill(run,R_pT,totEventWeight);
      hprof_mpf_run_JetPt110->Fill(run,R_MPF,totEventWeight);
      hprof_chHEF_run_JetPt110->Fill(run,Jet_chHEF[leadingJetIndex],totEventWeight);
      hprof_neHEF_run_JetPt110->Fill(run,Jet_neHEF[leadingJetIndex],totEventWeight);
      hprof_chEmEF_run_JetPt110->Fill(run,Jet_chEmEF[leadingJetIndex],totEventWeight);
      hprof_neEmEF_run_JetPt110->Fill(run,Jet_neEmEF[leadingJetIndex],totEventWeight);
      hprof_muEF_run_JetPt110->Fill(run,Jet_muEF[leadingJetIndex],totEventWeight);
      hprof_mz_run_JetPt110->Fill(run,Zboson.M(),totEventWeight);
      hprof_nz_run_JetPt110->Fill(run,totEventWeight);
    }
    if(ptave > 110){
      hprof_db_run_PtAve110->Fill(run,R_pT,totEventWeight);
      hprof_mpf_run_PtAve110->Fill(run,R_MPF,totEventWeight);
      hprof_chHEF_run_PtAve110->Fill(run,Jet_chHEF[leadingJetIndex],totEventWeight);
      hprof_neHEF_run_PtAve110->Fill(run,Jet_neHEF[leadingJetIndex],totEventWeight);
      hprof_chEmEF_run_PtAve110->Fill(run,Jet_chEmEF[leadingJetIndex],totEventWeight);
      hprof_neEmEF_run_PtAve110->Fill(run,Jet_neEmEF[leadingJetIndex],totEventWeight);
      hprof_muEF_run_PtAve110->Fill(run,Jet_muEF[leadingJetIndex],totEventWeight);
      hprof_mz_run_PtAve110->Fill(run,Zboson.M(),totEventWeight);
      hprof_nz_run_PtAve110->Fill(run,totEventWeight);
    }
  }
  if(isData && alpha < 1.0 && fabs(leadingJet.Eta()) > 3 && fabs(leadingJet.Eta()) < 4){
    hprof_db_run_jeta34->Fill(run,R_pT,totEventWeight);
    hprof_mpf_run_jeta34->Fill(run,R_MPF,totEventWeight);
    hprof_mz_run_jeta34->Fill(run,Zboson.M(),totEventWeight);
    hprof_nz_run_jeta34->Fill(run,totEventWeight);
  }
  if(isData && alpha < 1.0 && fabs(leadingJet.Eta()) > 4){
    hprof_db_run_jeta45->Fill(run,R_pT,totEventWeight);
    hprof_mpf_run_jeta45->Fill(run,R_MPF,totEventWeight);
    hprof_mz_run_jeta45->Fill(run,Zboson.M(),totEventWeight);
    hprof_nz_run_jeta45->Fill(run,totEventWeight);
  }

  //TVector3 rawChsMET;
  //rawChsMET.SetPtEtaPhi(ChsMET_pt,0,ChsMET_phi);
  //std::cout << "check ChsMET_pt " << ChsMET_pt << std::endl;
  //printSync(Zboson,jetSelection,rawChsMET,MET);

  float JNPF = 0;
  for(size_t i = 0; i < jetIndex_all.size(); ++i){
    TLorentzVector j = getJet(jetIndex_all[i]);
    if(j.Pt() < 15) continue;
    if(fabs(j.Eta()) > 4.7) continue;
    JNPF+=cos(ROOT::Math::VectorUtil::DeltaPhi(j.Vect(),MET));
  }

  float R_pTGen  = -1;
  float R_MPFGen = -1;
  float R_GMPF   = -1;
  if(!isData){
    R_pTGen = leadingGenJet.Pt()/genZboson.Pt();
    R_MPFGen = 1 + GenMET_pt*(cos(GenMET_phi)*genZboson.Px() + sin(GenMET_phi)*genZboson.Py())/(genZboson.Pt()*genZboson.Pt());
    R_GMPF = 1 + GenMET_pt*(cos(GenMET_phi)*Zboson.Px() + sin(GenMET_phi)*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
  }

  TVector3 genMET;
  genMET.SetPtEtaPhi(GenMET_pt,0,GenMET_phi);

  float R_Genjet1 = -1;
  float R_Genjetn = -1;
  float R_Genuncl = -1;
  //float R_Jet1    = -1;
  float R_Z       = -1;
  float resp      = -1;
  float bal       = -1;
  if(!isData){

      resp = leadingJet.Pt()/leadingGenJet.Pt();
      bal  = leadingGenJet.Pt()/Zboson.Pt();

      TLorentzVector genjets_all(0,0,0,0);
      size_t n = nGenJet;
      for(size_t i = 0; i < n; ++i){
        TLorentzVector genjet = getGenJet(i);
        for(size_t j = 0; j < jetIndex_all.size(); ++j){
          TLorentzVector recojet = getJet(jetIndex_all[j]);
          double DR = ROOT::Math::VectorUtil::DeltaR(recojet,genjet);
          if(DR < 0.4) {
            genjets_all+=genjet;
            break;
          }
        }
      }
      //std::cout << "check genjets_all " << genjets_all.Pt() << std::endl;

      TLorentzVector genjets_notLeadingJet = genjets_all - leadingGenJet;

      TVector3 genMETuncl = -genMET - genjets_all.Vect() - Zboson.Vect();

      R_Genjet1 = -leadingGenJet.Pt()*(cos(leadingGenJet.Phi())*Zboson.Px() + sin(leadingGenJet.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
      R_Genjetn = -genjets_notLeadingJet.Pt()*(cos(genjets_notLeadingJet.Phi())*Zboson.Px() + sin(genjets_notLeadingJet.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
      R_Genuncl = -genMETuncl.Pt()*(cos(genMETuncl.Phi())*Zboson.Px() + sin(genMETuncl.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());

      //R_Jet1 = leadingJet.Pt()/leadingGenJet.Pt();
      R_Z    = genZboson.Pt()/Zboson.Pt();
      
  }

  h_Zpt_Rho_2->Fill(Zboson.Pt(),fixedGridRhoFastjetAll,totEventWeight);

  //std::cout << "check R_Genjet1 " << R_Genjet1 << " " << R_Genjetn << " " << R_Genuncl << std::endl;
  //std::cout << "check MET " << MET.Pt() << " " << (METuncl-leadingJet.Vect()-jets_notLeadingJet.Vect()).Pt() << std::endl;
  //std::cout << "check R_MPF " << R_MPF << " " << R_MPFjet1+R_MPFjetn+R_MPFuncl << " " << R_MPFjet1 << " " << R_MPFjetn << " " <<  R_MPFuncl <<std::endl;
  //h_Zpt_mZ->Fill(Zboson.Pt(),Zboson.M(),totEventWeight);
  //h_Zpt_mZgen->Fill(Zboson.Pt(),genZboson.M(),totEventWeight);

  //float mu = 0;
  //if(isData) mu = getAvgPU(run,luminosityBlock);
  //else mu = Pileup_nTrueInt;
  //std::cout << "check new fills " << mu << ":" << int(PV_npvs) << ":" << int(PV_npvsGood) << ":" << fixedGridRhoFastjetAll << ":" << fixedGridRhoFastjetCentral << ":" << fixedGridRhoFastjetCentralChargedPileUp << std::endl;
  h_mu_noPUrw->Fill(mu);
  h_npvall_noPUrw->Fill(int(PV_npvs));
  h_npvgood_noPUrw->Fill(int(PV_npvsGood));
  h_rho_noPUrw->Fill(fixedGridRhoFastjetAll);
  h_rho_central_noPUrw->Fill(fixedGridRhoFastjetCentral);
  h_rho_centralChargedPileUp_noPUrw->Fill(fixedGridRhoFastjetCentralChargedPileUp);
  h_mu_afterPUrw->Fill(mu,totEventWeight);
  h_npvall_afterPUrw->Fill(int(PV_npvs),totEventWeight);
  h_npvgood_afterPUrw->Fill(int(PV_npvsGood),totEventWeight);
  h_rho_afterPUrw->Fill(fixedGridRhoFastjetAll,totEventWeight);
  h_rho_central_afterPUrw->Fill(fixedGridRhoFastjetCentral,totEventWeight); 
  h_rho_centralChargedPileUp_afterPUrw->Fill(fixedGridRhoFastjetCentralChargedPileUp,totEventWeight);

  if(Zboson.Pt() > 30 and fabs(Zboson.Eta()) < 2.5 and alpha < 1.0 and leadingJet.Pt() > 30 and fabs(leadingJet.Eta()) < 1.3) {
    if(!isData){
      h_Mu0->Fill(Pileup_nTrueInt,totEventWeight);
      h_RhoVsMu0->Fill(Pileup_nTrueInt,fixedGridRhoFastjetAll,totEventWeight);
      h_NpvVsMu0->Fill(Pileup_nTrueInt,PV_npvsGood,totEventWeight);
    }else{
      //float mu = getAvgPU(run,luminosityBlock);
      h_Mu0->Fill(mu,totEventWeight);
      h_RhoVsMu0->Fill(mu,fixedGridRhoFastjetAll,totEventWeight);
      h_NpvVsMu0->Fill(mu,PV_npvsGood,totEventWeight);
    }
  }
  if(Zboson.Pt() > 100 and fabs(Zboson.Eta()) < 2.5 and leadingJet.Pt() > 30 and fabs(leadingJet.Eta()) < 1.3 and subLeadingJet.Pt() < 30) {
    if(!isData){
      h_Mu->Fill(Pileup_nTrueInt,totEventWeight);
      hprof_RhoVsMu->Fill(Pileup_nTrueInt,fixedGridRhoFastjetAll,totEventWeight);
      h_RhoVsMu->Fill(Pileup_nTrueInt,fixedGridRhoFastjetAll,totEventWeight);
      h_NpvVsMu->Fill(Pileup_nTrueInt,PV_npvsGood,totEventWeight);
    }else{
      //float mu = getAvgPU(run,luminosityBlock);
      h_Mu->Fill(mu,totEventWeight);
      hprof_RhoVsMu->Fill(mu,fixedGridRhoFastjetAll,totEventWeight);
      h_RhoVsMu->Fill(mu,fixedGridRhoFastjetAll,totEventWeight);
      h_NpvVsMu->Fill(mu,PV_npvsGood,totEventWeight);
    }
  }
  //std::cout << "check R_MPF " << R_MPF << " " << R_MPFjet1+R_MPFjetn+R_MPFuncl << std::endl;

  int jetGenFlavor = -1;
  if(!isData) jetGenFlavor = getJetFlavour(leadingJetIndex);
  //std::cout << "check jetGenFlavor " << jetGenFlavor << std::endl;

  if(!isData){
    double JESRpTGen = -1;
    if(R_pTGen > 0) JESRpTGen = R_pT/R_pTGen;
    if(genZboson.Pt() == 0) JESRpTGen = -10; 
    //if(JESRpTGen <= 0) cout << JESRpTGen << " = " << R_pT << "/" << R_pTGen << " (here, leadingJet.Pt()/Zbonson.Pt() = " << leadingJet.Pt() << "/" << Zboson.Pt() << " and leadingGenJet.Pt()/genZboson.Pt() = " << leadingGenJet.Pt() << "/" << genZboson.Pt() << endl;
    h_pTGenJet_JESRpTGen->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
    h_pTGenJet_JESRpTGen->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
    h_pTGenZ_JESRpTGen->Fill(genZboson.Pt(),JESRpTGen,totEventWeight);
    double JESRMPFGen = -10;
    if(R_MPFGen > 0) JESRMPFGen = R_MPF/R_MPFGen;
    h_pTGenJet_JESRMPFGen->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
    h_pTGenJet_JESRMPFGen->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
    h_pTGenZ_JESRMPFGen->Fill(genZboson.Pt(),JESRMPFGen,totEventWeight);
    
    switch(jetGenFlavor){
    case 5:
      h_pTGenJet_JESRpTGen_genb->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      h_pTGenJet_JESRMPFGen_genb->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      h_pTGenJet_JESRpTGen_genb->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      h_pTGenJet_JESRMPFGen_genb->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      h_pTGenZ_JESRpTGen_genb->Fill(genZboson.Pt(),JESRpTGen,totEventWeight);
      h_pTGenZ_JESRMPFGen_genb->Fill(genZboson.Pt(),JESRMPFGen,totEventWeight);
      break;
    case 4:
      h_pTGenJet_JESRpTGen_genc->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      h_pTGenJet_JESRMPFGen_genc->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      h_pTGenJet_JESRpTGen_genc->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      h_pTGenJet_JESRMPFGen_genc->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      h_pTGenZ_JESRpTGen_genc->Fill(genZboson.Pt(),JESRpTGen,totEventWeight);
      h_pTGenZ_JESRMPFGen_genc->Fill(genZboson.Pt(),JESRMPFGen,totEventWeight);
      break;
    case 21:
      h_pTGenJet_JESRpTGen_geng->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      h_pTGenJet_JESRMPFGen_geng->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      h_pTGenJet_JESRpTGen_geng->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      h_pTGenJet_JESRMPFGen_geng->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      h_pTGenZ_JESRpTGen_geng->Fill(genZboson.Pt(),JESRpTGen,totEventWeight);
      h_pTGenZ_JESRMPFGen_geng->Fill(genZboson.Pt(),JESRMPFGen,totEventWeight);
      break;
    case 3:
    case 2:
    case 1:
      h_pTGenJet_JESRpTGen_genuds->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      h_pTGenJet_JESRMPFGen_genuds->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      h_pTGenJet_JESRpTGen_genuds->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      h_pTGenJet_JESRMPFGen_genuds->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      h_pTGenZ_JESRpTGen_genuds->Fill(genZboson.Pt(),JESRpTGen,totEventWeight);
      h_pTGenZ_JESRMPFGen_genuds->Fill(genZboson.Pt(),JESRMPFGen,totEventWeight);
      break;
    default:
      break;
    }
  }

  /*
  for(size_t i = 0; i < nbins_alpha; ++i){
    //std::cout << "check filling " << i << " " << bins_alpha[i+1] << std::endl;
    for(size_t j = 0; j < bins_eta.size(); ++j){
      if(!passing(leadingJet.Eta(),bins_eta[j])) continue;
      if(alpha <= bins_alpha[i+1]) {
        h_Zpt_RpT[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_pT);
        h_Zpt_RMPF[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_MPF);
        switch(jetGenFlavor){
        case 5: cJetLabel->increment("b jets");
          h_Zpt_RpT_genb[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_pT);
          h_Zpt_RMPF_genb[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_MPF);
          break;
        case 4: cJetLabel->increment("c jets");
          h_Zpt_RpT_genc[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_pT);
          h_Zpt_RMPF_genc[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_MPF);
          break;
        case 21: cJetLabel->increment("gluon jets");
          h_Zpt_RpT_geng[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_pT);
          h_Zpt_RMPF_geng[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_MPF);
          break;
        default: cJetLabel->increment("uds jets");
          h_Zpt_RpT_genuds[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_pT);
          h_Zpt_RMPF_genuds[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_MPF);
        }
      }
    }
  }
  */
  ////  if(alpha > alphaCut(Zboson.Pt())) return false;
  cMain->increment("alpha",totEventWeight);

  //h_RpT->Fill(R_pT,totEventWeight);
  //h_RMPF->Fill(R_MPF,totEventWeight);

  //if(R_pT < 0.5) return false;                                                                                                             
  cMain->increment("RpT",totEventWeight);

  //pickEvents->fill(run,luminosityBlock,event);
  /*
  if(alpha < 0.1){
    std::cout << datasetName << " entry " << entry << std::endl;
    std::cout << "event,lumi,run " << run << " " << luminosityBlock << " " << event << std::endl;
    std::cout << "check alpha,R_pT,R_MPF,alphaCut " << alpha << " " << R_pT << " " << R_MPF << " " << alphaCut(Zboson.Pt()) << std::endl;
  }
  */
  //h_alpha_RpT->Fill(alpha,R_pT,totEventWeight);
  //h_alpha_RMPF->Fill(alpha,R_MPF,totEventWeight);
  /*
  if(!isData){
    //    int jetGenFlavor = getJetFlavour(leadingJetIndex);
    cJetLabel->increment("all jets");
    switch(jetGenFlavor){
    case 5: cJetLabel->increment("b jets");
      h_alpha_RpT_genb->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_genb->Fill(alpha,R_MPF,totEventWeight);
      break;
    case 4: cJetLabel->increment("c jets");
      break;
    case 21: cJetLabel->increment("gluon jets");
      break;
    default: cJetLabel->increment("uds jets");
      break;
    }
  }
  */
  /*
  if(alpha < 0.3 && Zboson.Pt() > 30){
    h_jet1cpt->Fill(leadingJet.Pt());
    h_jet1ceta->Fill(leadingJet.Eta());
    h_jet2cpt->Fill(subLeadingJet.Pt());
    h_jet2ceta->Fill(subLeadingJet.Eta());

    h_jet2etapt->Fill(subLeadingJet.Eta(),subLeadingJet.Pt());
  }
  */
  // btagging
  // Jet_btagCMVA
  // Jet_btagCSVV2
  // Jet_btagDeepB
  // Jet_btagDeepC
  // Jet_btagDeepFlavB
  //https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagRecommendation80X
  //CSVv2 pfCombinedInclusiveSecondaryVertexV2BJetTags
  // loose  0.460 CSVv2_ichep.csv
  // medium 0.800
  // tight  0.935
  //cMVAv2 pfCombinedMVAV2BJetTags "to be used only in ttbar jet pT regime" 
  // loose  -0.715 cMVAv2_ichep.csv
  // medium 0.185
  // tight  0.875

  //https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagRecommendation94X
  //CSVv2 pfCombinedInclusiveSecondaryVertexV2BJetTags
  // loose  0.5803 CSVv2_94XSF_V2_B_F.csv
  // medium 0.8838
  // tight  0.9693
  //DeepCSV pfDeepCSVJetTags:probb + pfDeepCSVJetTags:probbb
  // loose  0.1522 DeepCSV_94XSF_V3_B_F.csv
  // medium 0.4941
  // tight  0.8001
  //DeepFlavB
  // The b discriminator is the sum of probb, probbb, and problepb
  // loose 0.0521 DeepFlavB_94XSF_V1_B_F.csv
  // medium 0.3033
  // tight 0.7489

  //https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagRecommendation102X
  //DeepCSV
  // loose  0.1241
  // medium 0.4184
  // tight  0.7527
  //DeepFlavB
  // loose  0.0494
  // medium 0.2770
  // tight  0.7264


  // https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagRecommendation
  Btag *btagCSVV2 = new Btag();
  btagCSVV2->set("2016",0.460,0.800,0.935);
  btagCSVV2->set("2017",0.5803,0.8838,0.9693);
  btagCSVV2->set("2018",0.5803,0.8838,0.9693); // FIXME, same as 2017, 10112019/S.Lehti
  btagCSVV2->set("2022",0.5803,0.8838,0.9693);

  Btag *btagDeepB = new Btag();
  btagDeepB->set("2016",0.2217,0.6321,0.8953);
  btagDeepB->set("2017",0.1522,0.4941,0.8001);
  btagDeepB->set("2018",0.1241,0.4184,0.7527);
  btagDeepB->set("2022",0.1241,0.4184,0.7527);
  btagDeepB->set("2023",0.1241,0.4184,0.7527);
  btagDeepB->set("2024",0.1241,0.4184,0.7527);

  Btag *btagDeepC = new Btag();
  //btagDeepC->set("2016",-0.48,-0.1,0.69);
  //btagDeepC->set("2017",0.05,0.15,0.8);
  //btagDeepC->set("2018",0.04,0.137,0.66);
  //loosening the tight WP to get about same nember of c jets as b jets
  float frac = 0.5;
  btagDeepC->set("2016",-0.48,-0.1,-0.1+frac*(0.69+0.1));
  btagDeepC->set("2017",0.05,0.15,0.15+frac*(0.8-0.15));
  btagDeepC->set("2018",0.04,0.137,0.137+frac*(0.66-0.137));
  btagDeepC->set("2022",0.04,0.137,0.137+frac*(0.66-0.137));
  btagDeepC->set("2023",0.04,0.137,0.137+frac*(0.66-0.137));
  btagDeepC->set("2024",0.04,0.137,0.137+frac*(0.66-0.137));

  // https://btv-wiki.docs.cern.ch/ScaleFactors/#sf-campaigns
  Btag *btagDeepFlavB = new Btag();
  btagDeepFlavB->set("2016APV",0.0508,0.2598,0.6502);
  btagDeepFlavB->set("2016",0.0480,0.2489,0.6377);
  btagDeepFlavB->set("2017",0.0532,0.3040,0.7476);
  btagDeepFlavB->set("2018",0.0490,0.2783,0.7100);
  btagDeepFlavB->set("2022",0.0583,0.3086,0.7183);
  btagDeepFlavB->set("2022EE",0.0614,0.3196,0.73);
  btagDeepFlavB->set("2023",0.0614,0.3196,0.73); // same as 2022EE
  btagDeepFlavB->set("2024",0.0614,0.3196,0.73); // same as 2022EE

  Btag *btagDeepFlavC = new Btag();
  btagDeepFlavC->set("2016APV",0.039,0.098,0.270);
  btagDeepFlavC->set("2016",0.039,0.099,0.269);
  btagDeepFlavC->set("2017",0.030,0.085,0.520);
  btagDeepFlavC->set("2018",0.038,0.099,0.282);
  btagDeepFlavC->set("2022",0.042,0.108,0.303);
  btagDeepFlavC->set("2022EE",0.042,0.108,0.305);
  btagDeepFlavC->set("2023",0.042,0.108,0.305); // same as 2022EE
  btagDeepFlavC->set("2024",0.042,0.108,0.305); // same as 2022EE

  Btag *btagDeepFlavQG = new Btag();
  btagDeepFlavQG->set("2016APV",0.12,0.27,0.48);
  btagDeepFlavQG->set("2016",0.12,0.27,0.48);
  btagDeepFlavQG->set("2017",0.12,0.27,0.48); // Kimmo: https://indico.cern.ch/event/1304758/contributions/5487541/attachments/2680759/4653316/qg_tagging_UL17_jmar_11_07_2023.pdf
  btagDeepFlavQG->set("2018",0.12,0.27,0.48);
  btagDeepFlavQG->set("2022",0.12,0.27,0.48);
  btagDeepFlavQG->set("2022EE",0.12,0.27,0.48);
  btagDeepFlavQG->set("2023",0.12,0.27,0.48);
  btagDeepFlavQG->set("2024",0.12,0.27,0.48);

  Btag *gluontag = new Btag();
  gluontag->set("2016",0.32,0.32,0.32);
  gluontag->set("2017",0.32,0.32,0.32);
  gluontag->set("2018",0.32,0.32,0.32);
  gluontag->set("2022",0.32,0.32,0.32);
  gluontag->set("2023",0.32,0.32,0.32);
  gluontag->set("2024",0.32,0.32,0.32);

  Btag *particleNetAK4_Btag = new Btag();
  particleNetAK4_Btag->set("2016",0.5,0.5,0.5);
  particleNetAK4_Btag->set("2017",0.5,0.5,0.5);
  particleNetAK4_Btag->set("2018",0.5,0.5,0.5);
  particleNetAK4_Btag->set("2022",0.5,0.5,0.5);
  particleNetAK4_Btag->set("2023",0.5,0.5,0.5);
  particleNetAK4_Btag->set("2024",0.5,0.5,0.5);
  Btag *particleNetAK4_CvsBtag = new Btag();
  particleNetAK4_CvsBtag->set("2016",0.5,0.5,0.5);
  particleNetAK4_CvsBtag->set("2017",0.5,0.5,0.5);
  particleNetAK4_CvsBtag->set("2018",0.5,0.5,0.5);
  particleNetAK4_CvsBtag->set("2022",0.5,0.5,0.5);
  particleNetAK4_CvsBtag->set("2023",0.5,0.5,0.5);
  particleNetAK4_CvsBtag->set("2024",0.5,0.5,0.5);
  Btag *particleNetAK4_CvsLtag = new Btag();
  particleNetAK4_CvsLtag->set("2016",0.5,0.5,0.5);
  particleNetAK4_CvsLtag->set("2017",0.5,0.5,0.5);
  particleNetAK4_CvsLtag->set("2018",0.5,0.5,0.5);
  particleNetAK4_CvsLtag->set("2022",0.5,0.5,0.5);
  particleNetAK4_CvsLtag->set("2023",0.5,0.5,0.5);
  particleNetAK4_CvsLtag->set("2024",0.5,0.5,0.5);
  Btag *particleNetAK4_QvsGtag = new Btag();
  particleNetAK4_QvsGtag->set("2016",0.5,0.5,0.5);
  particleNetAK4_QvsGtag->set("2017",0.54,0.32,0.16);
  particleNetAK4_QvsGtag->set("2018",0.5,0.5,0.5);
  particleNetAK4_QvsGtag->set("2022",0.5,0.5,0.5);
  particleNetAK4_QvsGtag->set("2023",0.5,0.5,0.5);
  particleNetAK4_QvsGtag->set("2024",0.5,0.5,0.5);

  //std::cout << "check leading jet " << leadingJet.Pt() << " " << leadingJet.Eta() << std::endl;
  //std::cout << "check jet[0}      " << Jet_pt[0] << " " << Jet_eta[0] << " " << Jet_btagCSVV2[0] << std::endl;
  /*
  if(btagCSVV2->exists(year)){
    if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->loose(year)){ // leading jet
      cMain->increment("btagCSVV2loose",totEventWeight);

      h_alpha_RpT_btagCSVV2loose->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_btagCSVV2loose->Fill(alpha,R_MPF,totEventWeight);
      if(!isData){
       int jetGenFlavor = getJetFlavour(leadingJetIndex);
        switch(jetGenFlavor){
        case 5: 
          h_alpha_RpT_btagCSVV2loose_genb->Fill(alpha,R_pT,totEventWeight);
          h_alpha_RMPF_btagCSVV2loose_genb->Fill(alpha,R_MPF,totEventWeight);
          break;
        case 4: 
          break;
        case 21:
          break;
        default:
          break;
        }
      }
    }
    //std::cout << "check btag " << year << " " << btagCSVV2->medium(year) << std::endl;
    if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->medium(year)){ // leading jet
      cMain->increment("btagCSVV2medium",totEventWeight);

      h_alpha_RpT_btagCSVV2medium->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_btagCSVV2medium->Fill(alpha,R_MPF,totEventWeight);
      if(!isData){
        int jetGenFlavor = getJetFlavour(leadingJetIndex);
        switch(jetGenFlavor){
        case 5:
          h_alpha_RpT_btagCSVV2medium_genb->Fill(alpha,R_pT,totEventWeight);
          h_alpha_RMPF_btagCSVV2medium_genb->Fill(alpha,R_MPF,totEventWeight);
          break;
        case 4:
          break;
        case 21:
          break;
        default:
          break;
        }
      }
    }
    if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->tight(year)){ // leading jet
      cMain->increment("btagCSVV2tight",totEventWeight);

      h_alpha_RpT_btagCSVV2tight->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_btagCSVV2tight->Fill(alpha,R_MPF,totEventWeight);
      if(!isData){
        int jetGenFlavor = getJetFlavour(leadingJetIndex);
        switch(jetGenFlavor){
        case 5:
          h_alpha_RpT_btagCSVV2tight_genb->Fill(alpha,R_pT,totEventWeight);
          h_alpha_RMPF_btagCSVV2tight_genb->Fill(alpha,R_MPF,totEventWeight);
          break;
        case 4:
          break;
        case 21:
          break;
        default:
          break;
        }
      }
    }
  }
  */

  /*
  if(Jet_btagCMVA[0] > 0.5803){ // leading jet
    cMain->increment("btagCMVAloose",totEventWeight);

    h_alpha_RpT_btagCMVAloose->Fill(alpha,R_pT,totEventWeight);
    h_alpha_RMPF_btagCMVAloose->Fill(alpha,R_MPF,totEventWeight);
  }
  if(Jet_btagCMVA[0] > 0.8838){ // leading jet
    cMain->increment("btagCMVAmedium",totEventWeight);

    h_alpha_RpT_btagCMVAmedium->Fill(alpha,R_pT,totEventWeight);
    h_alpha_RMPF_btagCMVAmedium->Fill(alpha,R_MPF,totEventWeight);
  }
  if(Jet_btagCMVA[0] > 0.9693){ // leading jet
    cMain->increment("btagCMVAtight",totEventWeight);

    h_alpha_RpT_btagCMVAtight->Fill(alpha,R_pT,totEventWeight);
    h_alpha_RMPF_btagCMVAtight->Fill(alpha,R_MPF,totEventWeight);
  }
  */
  /*
  for(size_t i = 0; i < cuts.size(); ++i){
    if(!passing(Zboson.Pt(),cuts[i])) continue;

    h_alpha_RpT_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
    h_alpha_RMPF_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);

    int jetGenFlavor = getJetFlavour(leadingJetIndex);
    if(!isData && abs(jetGenFlavor) == 5){
      h_alpha_RpT_Zpt_genb[i]->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_Zpt_genb[i]->Fill(alpha,R_MPF,totEventWeight);
    }

    if(btagCSVV2->exists(year)){
      if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->loose(year)){ // leading jet
        h_alpha_RpT_btagCSVV2loose_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
        h_alpha_RMPF_btagCSVV2loose_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
        if(!isData && abs(jetGenFlavor) == 5){
          h_alpha_RpT_btagCSVV2loose_Zpt_genb[i]->Fill(alpha,R_pT,totEventWeight);
          h_alpha_RMPF_btagCSVV2loose_Zpt_genb[i]->Fill(alpha,R_MPF,totEventWeight);
        }
      }
      if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->medium(year)){ // leading jet
        h_alpha_RpT_btagCSVV2medium_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
        h_alpha_RMPF_btagCSVV2medium_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
        if(!isData && abs(jetGenFlavor) == 5){
          h_alpha_RpT_btagCSVV2medium_Zpt_genb[i]->Fill(alpha,R_pT,totEventWeight);
          h_alpha_RMPF_btagCSVV2medium_Zpt_genb[i]->Fill(alpha,R_MPF,totEventWeight);
        }
      }
      if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->tight(year)){ // leading jet
        h_alpha_RpT_btagCSVV2tight_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
        h_alpha_RMPF_btagCSVV2tight_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
        if(!isData && abs(jetGenFlavor) == 5){
          h_alpha_RpT_btagCSVV2tight_Zpt_genb[i]->Fill(alpha,R_pT,totEventWeight);
          h_alpha_RMPF_btagCSVV2tight_Zpt_genb[i]->Fill(alpha,R_MPF,totEventWeight);
        }
      }
    }
    // DeepB
    if(btagDeepB->exists(year)){
      if(Jet_btagDeepB[leadingJetIndex] > btagDeepB->loose(year)){ // leading jet                                                    
        h_alpha_RpT_btagDeepBloose_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
        h_alpha_RMPF_btagDeepBloose_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
      }
      if(Jet_btagDeepB[leadingJetIndex] > btagDeepB->medium(year)){ // leading jet                                                   
        h_alpha_RpT_btagDeepBmedium_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
        h_alpha_RMPF_btagDeepBmedium_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
      }
      if(Jet_btagDeepB[leadingJetIndex] > btagDeepB->tight(year)){ // leading jet                                                    
        h_alpha_RpT_btag_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
        h_alpha_RMPF_btag_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
      }
    }
    // DeepFlavB
    if(btagDeepFlavB->exists(year)){
      if(Jet_btagDeepFlavB[leadingJetIndex] > btagDeepFlavB->loose(year)){ // leading jet
        h_alpha_RpT_btagDeepFlavBloose_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
        h_alpha_RMPF_btagDeepFlavBloose_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
      }
      if(Jet_btagDeepFlavB[leadingJetIndex] > btagDeepFlavB->medium(year)){ // leading jet
        h_alpha_RpT_btagDeepFlavBmedium_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
        h_alpha_RMPF_btagDeepFlavBmedium_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
      }
      if(Jet_btagDeepFlavB[leadingJetIndex] > btagDeepFlavB->tight(year)){ // leading jet
        h_alpha_RpT_btagDeepFlavBtight_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
        h_alpha_RMPF_btagDeepFlavBtight_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
      }
    }

  }
  */
  //if(R_pT < 0 || R_pT > 2 || R_MPF < 0 || R_MPF > 2) return false;
  cMain->increment("RpT+MPF cuts",totEventWeight);

    h_jet1cpt->Fill(leadingJet.Pt());
    h_jet1ceta->Fill(leadingJet.Eta());
    h_jet2cpt->Fill(subLeadingJet.Pt());
    h_jet2ceta->Fill(subLeadingJet.Eta());

    h_jet2etapt->Fill(subLeadingJet.Eta(),subLeadingJet.Pt());

  size_t nbveto = 0;
  for(size_t i = 0; i < jetSelection.size(); i++){
    if(btagDeepFlavB->exists(year)){
      if(Jet_btagDeepFlavB[jetSelection[i]] > btagDeepFlavB->loose(year)){
        nbveto++;
      }
    }
  }
  //if(nbveto > 1) return false;
  cMain->increment("bveto",totEventWeight);

  //int jetGenFlavor = getJetFlavour(leadingJetIndex);
  //std::cout << "check jetGenFlavor " << jetGenFlavor << std::endl;
  //std::cout << "check npsWeights " << npsWeights << " " << isData << std::endl;
  for(size_t iPS = 0; iPS < npsWeights; ++iPS){
    float eweight = totEventWeight;//*psWeights[iPS];
    //std::cout << "check eweight " << iPS << " " << eweight << " " << psWeights[iPS] << std::endl;
    for(size_t i = 0; i < nbins_pt; ++i){
      //std::cout << "check nbins_pt " << subLeadingJet.Pt() << " " << bins_pt[i] << std::endl;
      if(subLeadingJet.Pt() < bins_pt[i]) {
        size_t index_pt = i+nbins_pt*iPS;
        h3D_Zpt_RMPFj2pt_mu[index_pt]->Fill(Zboson.Pt(),R_MPF,mu,eweight);
        h3D_Zpt_RMPFj2ptx_mu[index_pt]->Fill(Zboson.Pt(),R_MPFx,mu,eweight);
        h3D_Zpt_RMPFj2pt_eta[index_pt]->Fill(Zboson.Pt(),R_MPF,fabs(leadingJet.Eta()),eweight);
        h3D_Zpt_RMPFj2ptx_eta[index_pt]->Fill(Zboson.Pt(),R_MPFx,fabs(leadingJet.Eta()),eweight);
        for(size_t j = 0; j < bins_eta.size(); ++j){
          if(!passing(fabs(leadingJet.Eta()),bins_eta[j])) continue;

          size_t index = i+nbins_pt*j+nbins_pt*bins_eta.size()*iPS;

          h_Zpt_RMPFj2pt[index]->Fill(Zboson.Pt(),R_MPF,eweight);
          h_Zpt_RMPFjet1j2pt[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);

          h_Zpt_RMPFj2ptx[index]->Fill(Zboson.Pt(),R_MPFx,eweight);
          h_Zpt_RMPFjet1j2ptx[index]->Fill(Zboson.Pt(),R_MPFjet1x,eweight);

          switch(jetGenFlavor){
          case 5:
            h_Zpt_RMPFj2pt_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            h_Zpt_RMPFjet1j2pt_genb[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
            break;
          case 4:
            h_Zpt_RMPFj2pt_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            h_Zpt_RMPFjet1j2pt_genc[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
            break;
          case 21:
            h_Zpt_RMPFj2pt_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            h_Zpt_RMPFjet1j2pt_geng[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
            break;
          case 3:
          case 2:
          case 1:
            h_Zpt_RMPFj2pt_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            h_Zpt_RMPFjet1j2pt_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
            break;
          default:
            h_Zpt_RMPFj2pt_unclassified[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            h_Zpt_RMPFjet1j2pt_unclassified[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
          }
        }
      }
    }
    //cMain->increment("Plotting",totEventWeight);
    for(size_t i = 0; i < nbins_alpha; ++i){
      //std::cout << "check alpha " << i << " " << bins_alpha[i] << " alpha=" << alpha << " subl.jet pt=" << subLeadingJet.Pt() << std::endl;
      if(alpha < bins_alpha[i]) {
        size_t index_alpha = i+nbins_alpha*iPS;
        h3D_Zpt_RMPF_mu[index_alpha]->Fill(Zboson.Pt(),R_MPF,mu,eweight);
        h3D_Zpt_RMPFx_mu[index_alpha]->Fill(Zboson.Pt(),R_MPFx,mu,eweight);
        h3D_Zpt_RMPF_eta[index_alpha]->Fill(Zboson.Pt(),R_MPF,fabs(leadingJet.Eta()),eweight);
        h3D_Zpt_RMPFx_eta[index_alpha]->Fill(Zboson.Pt(),R_MPFx,fabs(leadingJet.Eta()),eweight);
	tprof2D_eta_Zpt_RMPF[index_alpha]->Fill(fabs(leadingJet.Eta()),Zboson.Pt(),R_MPF,eweight);
	tprof2D_eta_Zpt_RMPFx[index_alpha]->Fill(fabs(leadingJet.Eta()),Zboson.Pt(),R_MPFx,eweight);

        for(size_t j = 0; j < bins_eta.size(); ++j){
          //std::cout << "check jet eta " << j << " " << leadingJet.Pt() << " " << leadingJet.Eta() << " " << bins_eta[j] << " " << passing(leadingJet.Eta(),bins_eta[j]) << std::endl;
          if(!passing(fabs(leadingJet.Eta()),bins_eta[j])) continue;

          size_t index = i+nbins_alpha*j+nbins_alpha*bins_eta.size()*iPS;
          /*
              size_t ialpha = index%(nbins_alpha*bins_eta.size())%nbins_alpha;
              size_t ieta = index%(nbins_alpha*bins_eta.size())/nbins_alpha;
              size_t ipsw = index/(nbins_alpha*bins_eta.size());
              std::cout << "check index i " << ialpha << " " << i << std::endl;
              std::cout << "check index j " << ieta << " " << j << std::endl;
              std::cout << "check index iPS " << ipsw << " " << iPS << std::endl;
          std::cout << "check passing " << leadingJet.Pt() << " " << index << std::endl;
          */
          h_Zpt_mZ[index]->Fill(Zboson.Pt(),Zboson.M(),eweight);
          if(!isData) h_Zpt_mZgen[index]->Fill(Zboson.Pt(),genZboson.M(),eweight);
          h_Zpt_Rho[index]->Fill(Zboson.Pt(),fixedGridRhoFastjetAll,eweight);
	  h_JetPt_Rho[index]->Fill(leadingJet.Pt(),fixedGridRhoFastjetAll,eweight);
	  h_PtAve_Rho[index]->Fill(ptave,fixedGridRhoFastjetAll,eweight);
          if(leadingJetIndex >= 0){
            //std::cout << "check EF " << leadingJetIndex << " "
            //              << Jet_chHEF[leadingJetIndex] << " "
            //              << Jet_neHEF[leadingJetIndex] << " "
            //              << Jet_chEmEF[leadingJetIndex] << " "
            //              << Jet_neEmEF[leadingJetIndex] << " "
            //              << Jet_muEF[leadingJetIndex] << std::endl;
            h_Zpt_chHEF[index]->Fill(Zboson.Pt(),Jet_chHEF[leadingJetIndex],eweight);
            h_Zpt_neHEF[index]->Fill(Zboson.Pt(),Jet_neHEF[leadingJetIndex],eweight);
            h_Zpt_chEmEF[index]->Fill(Zboson.Pt(),Jet_chEmEF[leadingJetIndex],eweight);
            h_Zpt_neEmEF[index]->Fill(Zboson.Pt(),Jet_neEmEF[leadingJetIndex],eweight);
            h_Zpt_muEF[index]->Fill(Zboson.Pt(),Jet_muEF[leadingJetIndex],eweight);
          }

          h_Zpt_JNPF[index]->Fill(Zboson.Pt(),JNPF,eweight);

          h_Zpt_RpT[index]->Fill(Zboson.Pt(),R_pT,eweight);
	  /*
	  if(i==1 && j==0){
	    std::cout << "check run:lumi:event " << run << ":" << luminosityBlock << ":" << event << " check R_pT " << R_pT << " " << Zboson.Pt() << std::endl;
	    std::cout << "check run:lumi:event " << run << ":" << luminosityBlock << ":" << event << " check RMPF " << R_MPF << " " << Zboson.Pt() << " " << alpha << std::endl;
	    cMain->increment("Plotting",totEventWeight);
	  }
	  */
          h_Zpt_RMPF[index]->Fill(Zboson.Pt(),R_MPF,eweight);
          h_Zpt_RMPFx[index]->Fill(Zboson.Pt(),R_MPFx,eweight);
          //h_Zpt_RMPFfromType1[index]->Fill(Zboson.Pt(),R_MPFfromType1,eweight);
          //h_Zpt_RMPFPF[index]->Fill(Zboson.Pt(),R_MPFPF,eweight); 
          h_Zpt_RMPFjet1[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
          h_Zpt_RMPFjet1x[index]->Fill(Zboson.Pt(),R_MPFjet1x,eweight);
          h_Zpt_RMPFjetn[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
          h_Zpt_RMPFuncl[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
          h_Zpt_JNPF[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);

          h_ZpT_Mu_RpT[index]->Fill(Zboson.Pt(),mu,R_pT,eweight);
          h_ZpT_Mu_RMPF[index]->Fill(Zboson.Pt(),mu,R_MPF,eweight);
          h_ZpT_Mu_RMPFjet1[index]->Fill(Zboson.Pt(),mu,R_MPFjet1,eweight);
          h_ZpT_Mu_RMPFjetn[index]->Fill(Zboson.Pt(),mu,R_MPFjetn,eweight);
          h_ZpT_Mu_RMPFuncl[index]->Fill(Zboson.Pt(),mu,R_MPFuncl,eweight);

          h_ZpT_Mu_offset[index]->Fill(Zboson.Pt(),mu,offset,eweight);
          h_ZpT_Rho_offset[index]->Fill(Zboson.Pt(),fixedGridRhoFastjetAll,offset,eweight);
          h_ZpT_NPVgood_offset[index]->Fill(Zboson.Pt(),PV_npvsGood,offset,eweight);
          h_ZpT_NPVall_offset[index]->Fill(Zboson.Pt(),PV_npvs,offset,eweight);

          h_ZpT_NPVg_RpT[index]->Fill(Zboson.Pt(),PV_npvsGood,R_pT,eweight);
          h_ZpT_NPVg_RMPF[index]->Fill(Zboson.Pt(),PV_npvsGood,R_MPF,eweight);
          h_ZpT_NPVg_RMPFjet1[index]->Fill(Zboson.Pt(),PV_npvsGood,R_MPFjet1,eweight);
          h_ZpT_NPVg_RMPFjetn[index]->Fill(Zboson.Pt(),PV_npvsGood,R_MPFjetn,eweight);
          h_ZpT_NPVg_RMPFuncl[index]->Fill(Zboson.Pt(),PV_npvsGood,R_MPFuncl,eweight);

          h_JetPt_Mu_RpT[index]->Fill(leadingJet.Pt(),mu,R_pT,eweight);
          h_JetPt_Mu_RMPF[index]->Fill(leadingJet.Pt(),mu,R_MPF,eweight);
          h_JetPt_Mu_RMPFjet1[index]->Fill(leadingJet.Pt(),mu,R_MPFjet1,eweight);
          h_JetPt_Mu_RMPFjetn[index]->Fill(leadingJet.Pt(),mu,R_MPFjetn,eweight);
          h_JetPt_Mu_RMPFuncl[index]->Fill(leadingJet.Pt(),mu,R_MPFuncl,eweight);

          h_JetPt_NPVg_RpT[index]->Fill(leadingJet.Pt(),PV_npvsGood,R_pT,eweight);
          h_JetPt_NPVg_RMPF[index]->Fill(leadingJet.Pt(),PV_npvsGood,R_MPF,eweight);
          h_JetPt_NPVg_RMPFjet1[index]->Fill(leadingJet.Pt(),PV_npvsGood,R_MPFjet1,eweight);
          h_JetPt_NPVg_RMPFjetn[index]->Fill(leadingJet.Pt(),PV_npvsGood,R_MPFjetn,eweight);
          h_JetPt_NPVg_RMPFuncl[index]->Fill(leadingJet.Pt(),PV_npvsGood,R_MPFuncl,eweight);

          h_PtAve_Mu_RpT[index]->Fill(ptave,mu,R_pT,eweight);
          h_PtAve_Mu_RMPF[index]->Fill(ptave,mu,R_MPF,eweight);
          h_PtAve_Mu_RMPFjet1[index]->Fill(ptave,mu,R_MPFjet1,eweight);
          h_PtAve_Mu_RMPFjetn[index]->Fill(ptave,mu,R_MPFjetn,eweight);
          h_PtAve_Mu_RMPFuncl[index]->Fill(ptave,mu,R_MPFuncl,eweight);

          h_PtAve_NPVg_RpT[index]->Fill(ptave,PV_npvsGood,R_pT,eweight);
          h_PtAve_NPVg_RMPF[index]->Fill(ptave,PV_npvsGood,R_MPF,eweight);
          h_PtAve_NPVg_RMPFjet1[index]->Fill(ptave,PV_npvsGood,R_MPFjet1,eweight);
          h_PtAve_NPVg_RMPFjetn[index]->Fill(ptave,PV_npvsGood,R_MPFjetn,eweight);
          h_PtAve_NPVg_RMPFuncl[index]->Fill(ptave,PV_npvsGood,R_MPFuncl,eweight);

          if(Zboson.Pt() > 30){
            h_Zpt_Mu_Rho[index]->Fill(Zboson.Pt(),mu,fixedGridRhoFastjetAll,eweight);
            h_Zpt_Mu_NPVgood[index]->Fill(Zboson.Pt(),mu,PV_npvsGood,eweight);
            h_Zpt_Mu_NPVall[index]->Fill(Zboson.Pt(),mu,PV_npvs,eweight);
            h_Zpt_Mu_Mu[index]->Fill(Zboson.Pt(),mu,mu,eweight);
          }
          h_Mu_RpT[index]->Fill(mu,R_pT,eweight);
          h_Mu_RMPF[index]->Fill(mu,R_MPF,eweight);
          h_JetPt_RMPF[index]->Fill(leadingJet.Pt(),R_MPF,eweight);
          h_PtAve_RMPF[index]->Fill(ptave,R_MPF,eweight);
          h_Mu_RMPFjet1[index]->Fill(mu,R_MPFjet1,eweight);
          h_JetPt_RMPFjet1[index]->Fill(leadingJet.Pt(),R_MPFjet1,eweight);
          h_PtAve_RMPFjet1[index]->Fill(ptave,R_MPFjet1,eweight);
          h_Mu_RMPFjetn[index]->Fill(mu,R_MPFjetn,eweight);
          h_JetPt_RMPFjetn[index]->Fill(leadingJet.Pt(),R_MPFjetn,eweight);
          h_PtAve_RMPFjetn[index]->Fill(ptave,R_MPFjetn,eweight);
          h_Mu_RMPFuncl[index]->Fill(mu,R_MPFuncl,eweight);
          h_JetPt_RMPFuncl[index]->Fill(leadingJet.Pt(),R_MPFuncl,eweight);
          h_PtAve_RMPFuncl[index]->Fill(ptave,R_MPFuncl,eweight);

          h_Mu_ptdiff[index]->Fill(mu,ptdiff,eweight);

          h_JetPt_RpT[index]->Fill(leadingJet.Pt(),R_pT,eweight);
          h_PtAve_RpT[index]->Fill(ptave,R_pT,eweight);

          //h_JetPt_QGL[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
          //h_Zpt_QGL[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
          h_JetPt_muEF[index]->Fill(leadingJet.Pt(),Jet_muEF[leadingJetIndex],eweight);
          h_JetPt_chEmEF[index]->Fill(leadingJet.Pt(),Jet_chEmEF[leadingJetIndex],eweight);
          h_JetPt_chHEF[index]->Fill(leadingJet.Pt(),Jet_chHEF[leadingJetIndex],eweight);
          h_JetPt_neEmEF[index]->Fill(leadingJet.Pt(),Jet_neEmEF[leadingJetIndex],eweight);
          h_JetPt_neHEF[index]->Fill(leadingJet.Pt(),Jet_neHEF[leadingJetIndex],eweight);

          //h_PtAve_QGL[index]->Fill(ptave,Jet_qgl[leadingJetIndex],eweight);
          h_PtAve_muEF[index]->Fill(ptave,Jet_muEF[leadingJetIndex],eweight);
          h_PtAve_chEmEF[index]->Fill(ptave,Jet_chEmEF[leadingJetIndex],eweight);
          h_PtAve_chHEF[index]->Fill(ptave,Jet_chHEF[leadingJetIndex],eweight);
          h_PtAve_neEmEF[index]->Fill(ptave,Jet_neEmEF[leadingJetIndex],eweight);
          h_PtAve_neHEF[index]->Fill(ptave,Jet_neHEF[leadingJetIndex],eweight);

          //std::cout << "check h_Zpt_Mu_Rho " << mu << " " << fixedGridRhoFastjetAll << std::endl;
          cJetLabel->increment("all jets");
          if(!isData){
            h_Zpt_RGMPF[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
            h_Zpt_RGenjet1[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
            h_Zpt_RGenjetn[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
            h_Zpt_RGenuncl[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
            h_Zpt_RZ[index]->Fill(Zboson.Pt(),R_Z,eweight);
            h_Zpt_Resp[index]->Fill(Zboson.Pt(),resp,eweight);
            h_Zpt_RBal[index]->Fill(Zboson.Pt(),bal,eweight);
            h_pTgen_Resp[index]->Fill(leadingGenJet.Pt(),resp,eweight);

            h_JetPt_RGenjet1[index]->Fill(leadingJet.Pt(),R_Genjet1,eweight);
            h_JetPt_RGenjetn[index]->Fill(leadingJet.Pt(),R_Genjetn,eweight);
            h_JetPt_RGenuncl[index]->Fill(leadingJet.Pt(),R_Genuncl,eweight);
            h_JetPt_RBal[index]->Fill(leadingJet.Pt(),bal,eweight);

            h_PtAve_RGenjet1[index]->Fill(ptave,R_Genjet1,eweight);
            h_PtAve_RGenjetn[index]->Fill(ptave,R_Genjetn,eweight);
            h_PtAve_RGenuncl[index]->Fill(ptave,R_Genuncl,eweight);
            h_PtAve_RBal[index]->Fill(ptave,bal,eweight);

            switch(jetGenFlavor){
            case 5: cJetLabel->increment("b jets");
              h_Zpt_RpT_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
              h_Zpt_RMPF_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
              h_Zpt_RMPFjet1_genb[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
              h_Zpt_RMPFjetn_genb[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
              h_Zpt_RMPFuncl_genb[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
              //h_JetPt_QGL_genb[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
              //h_Zpt_QGL_genb[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
              h_Zpt_Resp_genb[index]->Fill(Zboson.Pt(),resp,eweight);
              h_Zpt_RGMPF_genb[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
              h_Zpt_RGenjet1_genb[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
              h_Zpt_RGenjetn_genb[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
              h_Zpt_RGenuncl_genb[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
              h_Zpt_RBal_genb[index]->Fill(Zboson.Pt(),bal,eweight);
              h_pTgen_Resp_genb[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              break;
            case 4: cJetLabel->increment("c jets");
              h_Zpt_RpT_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
              h_Zpt_RMPF_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
              h_Zpt_RMPFjet1_genc[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
              h_Zpt_RMPFjetn_genc[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
              h_Zpt_RMPFuncl_genc[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
              //h_JetPt_QGL_genc[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
              //h_Zpt_QGL_genc[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
              h_Zpt_Resp_genc[index]->Fill(Zboson.Pt(),resp,eweight);
              h_Zpt_RGMPF_genc[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
              h_Zpt_RGenjet1_genc[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
              h_Zpt_RGenjetn_genc[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
              h_Zpt_RGenuncl_genc[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
              h_Zpt_RBal_genc[index]->Fill(Zboson.Pt(),bal,eweight);
              h_pTgen_Resp_genc[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              break;
            case 21: cJetLabel->increment("gluon jets");
              h_Zpt_RpT_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
              h_Zpt_RMPF_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
              h_Zpt_RMPFjet1_geng[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
              h_Zpt_RMPFjetn_geng[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
              h_Zpt_RMPFuncl_geng[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
              //h_JetPt_QGL_geng[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
              //h_Zpt_QGL_geng[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
              h_Zpt_Resp_geng[index]->Fill(Zboson.Pt(),resp,eweight);
              h_Zpt_RGMPF_geng[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
              h_Zpt_RGenjet1_geng[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
              h_Zpt_RGenjetn_geng[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
              h_Zpt_RGenuncl_geng[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
              h_Zpt_RBal_geng[index]->Fill(Zboson.Pt(),bal,eweight);
              h_pTgen_Resp_geng[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              break;
            case 3:
              h_Zpt_RpT_gens[index]->Fill(Zboson.Pt(),R_pT,eweight);
              h_Zpt_RMPF_gens[index]->Fill(Zboson.Pt(),R_MPF,eweight);
              h_Zpt_RMPFjet1_gens[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
              h_Zpt_RMPFjetn_gens[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
              h_Zpt_RMPFuncl_gens[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);

              h_Zpt_RpT_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
              h_Zpt_RMPF_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
              h_Zpt_RMPFjet1_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
              h_Zpt_RMPFjetn_genuds[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
              h_Zpt_RMPFuncl_genuds[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
              //h_JetPt_QGL_genuds[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
              //h_Zpt_QGL_genuds[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
              h_Zpt_Resp_genuds[index]->Fill(Zboson.Pt(),resp,eweight);
              h_Zpt_RGMPF_genuds[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
              h_Zpt_RGenjet1_genuds[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
              h_Zpt_RGenjetn_genuds[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
              h_Zpt_RGenuncl_genuds[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
              h_Zpt_RBal_genuds[index]->Fill(Zboson.Pt(),bal,eweight);
              h_pTgen_Resp_genuds[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              break;
            case 2:
            case 1: cJetLabel->increment("uds jets");
              h_Zpt_RpT_genud[index]->Fill(Zboson.Pt(),R_pT,eweight);
              h_Zpt_RMPF_genud[index]->Fill(Zboson.Pt(),R_MPF,eweight);
              h_Zpt_RMPFjet1_genud[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
              h_Zpt_RMPFjetn_genud[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
              h_Zpt_RMPFuncl_genud[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);

              h_Zpt_RpT_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
              h_Zpt_RMPF_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
              h_Zpt_RMPFjet1_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
              h_Zpt_RMPFjetn_genuds[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
              h_Zpt_RMPFuncl_genuds[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
              //h_JetPt_QGL_genuds[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
              //h_Zpt_QGL_genuds[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
              h_Zpt_Resp_genuds[index]->Fill(Zboson.Pt(),resp,eweight);
              h_Zpt_RGMPF_genuds[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
              h_Zpt_RGenjet1_genuds[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
              h_Zpt_RGenjetn_genuds[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
              h_Zpt_RGenuncl_genuds[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
              h_Zpt_RBal_genuds[index]->Fill(Zboson.Pt(),bal,eweight);
              h_pTgen_Resp_genuds[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              break;
            default: cJetLabel->increment("unclassified jets");
              h_Zpt_RpT_unclassified[index]->Fill(Zboson.Pt(),R_pT,eweight);
              h_Zpt_RMPF_unclassified[index]->Fill(Zboson.Pt(),R_MPF,eweight);
              h_Zpt_RMPFjet1_unclassified[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
              h_Zpt_RMPFjetn_unclassified[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
              h_Zpt_RMPFuncl_unclassified[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
              //h_JetPt_QGL_unclassified[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
              //h_Zpt_QGL_unclassified[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
              h_Zpt_Resp_unclassified[index]->Fill(Zboson.Pt(),resp,eweight);
              h_Zpt_RGMPF_unclassified[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
              h_Zpt_RGenjet1_unclassified[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
              h_Zpt_RGenjetn_unclassified[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
              h_Zpt_RGenuncl_unclassified[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
              h_Zpt_RBal_unclassified[index]->Fill(Zboson.Pt(),bal,eweight);
              h_pTgen_Resp_unclassified[index]->Fill(leadingGenJet.Pt(),resp,eweight);
            }
          }

          /*
          if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->loose(year)){ // leading jet
            h_Zpt_RpT_btagCSVV2loose[index]->Fill(Zboson.Pt(),R_pT,eweight);
            h_Zpt_RMPF_btagCSVV2loose[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            if(!isData){
              //std::cout << "check jetGenFlavor " << jetGenFlavor << std::endl;
              switch(jetGenFlavor){
              case 5:
                //std::cout << "check filling 5" << std::endl;
                h_Zpt_RpT_btagCSVV2loose_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagCSVV2loose_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 4:
                //std::cout << "check filling 4" << std::endl;
                h_Zpt_RpT_btagCSVV2loose_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagCSVV2loose_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 21:
                h_Zpt_RpT_btagCSVV2loose_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagCSVV2loose_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 3:
              case 2:
              case 1:
                h_Zpt_RpT_btagCSVV2loose_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagCSVV2loose_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
                //default:
              }
            }
          }
          if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->medium(year)){ // leading jet
            h_Zpt_RpT_btagCSVV2medium[index]->Fill(Zboson.Pt(),R_pT,eweight);
            h_Zpt_RMPF_btagCSVV2medium[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            if(!isData){
              switch(jetGenFlavor){ 
              case 5:
                h_Zpt_RpT_btagCSVV2medium_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagCSVV2medium_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 4:
                h_Zpt_RpT_btagCSVV2medium_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagCSVV2medium_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 21:
                h_Zpt_RpT_btagCSVV2medium_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagCSVV2medium_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 3:
              case 2:
              case 1:
                h_Zpt_RpT_btagCSVV2medium_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagCSVV2medium_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
                //default:
              }
            }
          }
          if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->tight(year)){ // leading jet
            h_Zpt_RpT_btagCSVV2tight[index]->Fill(Zboson.Pt(),R_pT,eweight);
            h_Zpt_RMPF_btagCSVV2tight[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            if(!isData){
              switch(jetGenFlavor){
              case 5:
                h_Zpt_RpT_btagCSVV2tight_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagCSVV2tight_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 4:
                h_Zpt_RpT_btagCSVV2tight_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagCSVV2tight_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 21:
                h_Zpt_RpT_btagCSVV2tight_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagCSVV2tight_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 3:
              case 2:
              case 1:
                h_Zpt_RpT_btagCSVV2tight_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagCSVV2tight_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
                //default:
              }
            }
          }
          */

          bool tagged = false;
          if(Jet_btagDeepFlavB[leadingJetIndex] > btagDeepFlavB->tight(year)){
	    //if(Jet_btagDeepB[leadingJetIndex] > btagDeepB->tight(year)){
	    //if(Jet_particleNetAK4_B[leadingJetIndex] > particleNetAK4_Btag->tight(year)){ // leading jet
            tagged = true;
            h_Zpt_RpT_btag[index]->Fill(Zboson.Pt(),R_pT,eweight);
            h_Zpt_RMPF_btag[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            h_Zpt_RMPFjet1_btag[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
            h_Zpt_RMPFjetn_btag[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
            h_Zpt_RMPFuncl_btag[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
            //h_JetPt_QGL_btag[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
            //h_Zpt_QGL_btag[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
            h_JetPt_muEF_btag[index]->Fill(leadingJet.Pt(),Jet_muEF[leadingJetIndex],eweight);
            h_JetPt_chEmEF_btag[index]->Fill(leadingJet.Pt(),Jet_chEmEF[leadingJetIndex],eweight);
            h_JetPt_chHEF_btag[index]->Fill(leadingJet.Pt(),Jet_chHEF[leadingJetIndex],eweight);
            h_JetPt_neEmEF_btag[index]->Fill(leadingJet.Pt(),Jet_neEmEF[leadingJetIndex],eweight);
            h_JetPt_neHEF_btag[index]->Fill(leadingJet.Pt(),Jet_neHEF[leadingJetIndex],eweight);

            //h_PtAve_QGL_btag[index]->Fill(ptave,Jet_qgl[leadingJetIndex],eweight);
            h_PtAve_muEF_btag[index]->Fill(ptave,Jet_muEF[leadingJetIndex],eweight);
            h_PtAve_chEmEF_btag[index]->Fill(ptave,Jet_chEmEF[leadingJetIndex],eweight);
            h_PtAve_chHEF_btag[index]->Fill(ptave,Jet_chHEF[leadingJetIndex],eweight);
            h_PtAve_neEmEF_btag[index]->Fill(ptave,Jet_neEmEF[leadingJetIndex],eweight);
            h_PtAve_neHEF_btag[index]->Fill(ptave,Jet_neHEF[leadingJetIndex],eweight);

            if(Zboson.Pt() > 30){
              h_Zpt_Mu_Rho_btag[index]->Fill(Zboson.Pt(),mu,fixedGridRhoFastjetAll,eweight);
              h_Zpt_Mu_NPVgood_btag[index]->Fill(Zboson.Pt(),mu,PV_npvsGood,eweight);
              h_Zpt_Mu_NPVall_btag[index]->Fill(Zboson.Pt(),mu,PV_npvs,eweight);
              h_Zpt_Mu_Mu_btag[index]->Fill(Zboson.Pt(),mu,mu,eweight);
            }
            h_Mu_RpT_btag[index]->Fill(mu,R_pT,eweight);
            h_Mu_RMPF_btag[index]->Fill(mu,R_MPF,eweight);
            h_JetPt_RMPF_btag[index]->Fill(leadingJet.Pt(),R_MPF,eweight);
            h_PtAve_RMPF_btag[index]->Fill(ptave,R_MPF,eweight);
            h_Mu_RMPFjet1_btag[index]->Fill(mu,R_MPFjet1,eweight);
            h_JetPt_RMPFjet1_btag[index]->Fill(leadingJet.Pt(),R_MPFjet1,eweight);
            h_PtAve_RMPFjet1_btag[index]->Fill(ptave,R_MPFjet1,eweight);
            h_Mu_RMPFjetn_btag[index]->Fill(mu,R_MPFjetn,eweight);
            h_JetPt_RMPFjetn_btag[index]->Fill(leadingJet.Pt(),R_MPFjetn,eweight);
            h_PtAve_RMPFjetn_btag[index]->Fill(ptave,R_MPFjetn,eweight);
            h_Mu_RMPFuncl_btag[index]->Fill(mu,R_MPFuncl,eweight);
            h_JetPt_RMPFuncl_btag[index]->Fill(leadingJet.Pt(),R_MPFuncl,eweight);
            h_PtAve_RMPFuncl_btag[index]->Fill(ptave,R_MPFuncl,eweight);
            h_JetPt_RpT_btag[index]->Fill(leadingJet.Pt(),R_pT,eweight);
            h_PtAve_RpT_btag[index]->Fill(ptave,R_pT,eweight);
            cJetLabel_btag->increment("all jets");
            if(!isData){
              h_Zpt_RGMPF_btag[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
              h_Zpt_RGenjet1_btag[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
              h_Zpt_RGenjetn_btag[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
              h_Zpt_RGenuncl_btag[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
              h_Zpt_Resp_btag[index]->Fill(Zboson.Pt(),resp,eweight);
              h_Zpt_RBal_btag[index]->Fill(Zboson.Pt(),bal,eweight);
              h_pTgen_Resp_btag[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              switch(jetGenFlavor){
              case 5:cJetLabel_btag->increment("b jets");
                h_Zpt_RpT_btag_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btag_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_btag_genb[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_btag_genb[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_btag_genb[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_btag_genb[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_btag_genb[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_btag_genb[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_btag_genb[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_btag_genb[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_btag_genb[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_btag_genb[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_btag_genb[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_btag_genb[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 4:cJetLabel_btag->increment("c jets");
                h_Zpt_RpT_btag_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btag_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_btag_genc[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_btag_genc[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_btag_genc[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_btag_genc[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_btag_genc[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_btag_genc[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_btag_genc[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_btag_genc[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_btag_genc[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_btag_genc[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_btag_genc[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_btag_genc[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 21:cJetLabel_btag->increment("gluon jets");
                h_Zpt_RpT_btag_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btag_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_btag_geng[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_btag_geng[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_btag_geng[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_btag_geng[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_btag_geng[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_btag_geng[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_btag_geng[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_btag_geng[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_btag_geng[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_btag_geng[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_btag_geng[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_btag_geng[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 3:
                h_Zpt_RpT_btag_gens[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btag_gens[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_btag_gens[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_btag_gens[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_btag_gens[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);

                h_Zpt_RpT_btag_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btag_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_btag_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_btag_genuds[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_btag_genuds[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_btag_genuds[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_btag_genuds[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_btag_genuds[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_btag_genuds[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_btag_genuds[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_btag_genuds[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_btag_genuds[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_btag_genuds[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_btag_genuds[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 2:
              case 1:cJetLabel_btag->increment("uds jets");
                h_Zpt_RpT_btag_genud[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btag_genud[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_btag_genud[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_btag_genud[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_btag_genud[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);

                h_Zpt_RpT_btag_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btag_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_btag_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_btag_genuds[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_btag_genuds[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_btag_genuds[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_btag_genuds[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_btag_genuds[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_btag_genuds[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_btag_genuds[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_btag_genuds[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_btag_genuds[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_btag_genuds[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_btag_genuds[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              default:cJetLabel_btag->increment("unclassified jets");
                h_Zpt_RpT_btag_unclassified[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btag_unclassified[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_btag_unclassified[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_btag_unclassified[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_btag_unclassified[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_btag_unclassified[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_btag_unclassified[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_btag_unclassified[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_btag_unclassified[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_btag_unclassified[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_btag_unclassified[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_btag_unclassified[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_btag_unclassified[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_btag_unclassified[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              }
            }
          }

          /*
          if(Jet_btagDeepFlavB[leadingJetIndex] > btagDeepFlavB->tight(year)){ // leading jet
            h_Zpt_RpT_btagDeepFlavBtight[index]->Fill(Zboson.Pt(),R_pT,eweight);
            h_Zpt_RMPF_btagDeepFlavBtight[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            if(!isData){
              h_Zpt_RGMPF_btagDeepFlavBtight[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
              h_Zpt_RGenjet1_btagDeepFlavBtight[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
              h_Zpt_RGenjetn_btagDeepFlavBtight[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
              h_Zpt_RGenuncl_btagDeepFlavBtight[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
              switch(jetGenFlavor){
              case 5:
                h_Zpt_RpT_btagDeepFlavBtight_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagDeepFlavBtight_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RGMPF_btagDeepFlavBtight_genb[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_btagDeepFlavBtight_genb[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_btagDeepFlavBtight_genb[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_btagDeepFlavBtight_genb[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                break;
              case 4:
                h_Zpt_RpT_btagDeepFlavBtight_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagDeepFlavBtight_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RGMPF_btagDeepFlavBtight_genc[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_btagDeepFlavBtight_genc[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_btagDeepFlavBtight_genc[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_btagDeepFlavBtight_genc[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                break;
              case 21:
                h_Zpt_RpT_btagDeepFlavBtight_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagDeepFlavBtight_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RGMPF_btagDeepFlavBtight_geng[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_btagDeepFlavBtight_geng[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_btagDeepFlavBtight_geng[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_btagDeepFlavBtight_geng[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                break;
              case 3:
              case 2:
              case 1:
                h_Zpt_RpT_btagDeepFlavBtight_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagDeepFlavBtight_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RGMPF_btagDeepFlavBtight_genuds[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_btagDeepFlavBtight_genuds[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_btagDeepFlavBtight_genuds[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_btagDeepFlavBtight_genuds[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                break;
                //default:
              }
            }
          }
          */
	  if(Jet_btagDeepFlavB[leadingJetIndex] <= btagDeepFlavB->tight(year) &&
             Jet_btagDeepFlavCvL[leadingJetIndex] > btagDeepFlavC->tight(year)){
	    //Jet_btagDeepB[leadingJetIndex] <= btagDeepB->tight(year) &&
	    //Jet_btagDeepCvL[leadingJetIndex] > btagDeepC->tight(year)){
	    //if(Jet_particleNetAK4_B[leadingJetIndex] <= particleNetAK4_Btag->tight(year) &&
	    //Jet_particleNetAK4_CvsL[leadingJetIndex] > particleNetAK4_CvsLtag->tight(year)){ // leading jet
            tagged = true;
            h_Zpt_RpT_ctag[index]->Fill(Zboson.Pt(),R_pT,eweight);
            h_Zpt_RMPF_ctag[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            h_Zpt_RMPFjet1_ctag[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
            h_Zpt_RMPFjetn_ctag[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
            h_Zpt_RMPFuncl_ctag[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
            //h_JetPt_QGL_ctag[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
            //h_Zpt_QGL_ctag[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
            h_JetPt_muEF_ctag[index]->Fill(leadingJet.Pt(),Jet_muEF[leadingJetIndex],eweight);
            h_JetPt_chEmEF_ctag[index]->Fill(leadingJet.Pt(),Jet_chEmEF[leadingJetIndex],eweight);
            h_JetPt_chHEF_ctag[index]->Fill(leadingJet.Pt(),Jet_chHEF[leadingJetIndex],eweight);
            h_JetPt_neEmEF_ctag[index]->Fill(leadingJet.Pt(),Jet_neEmEF[leadingJetIndex],eweight);
            h_JetPt_neHEF_ctag[index]->Fill(leadingJet.Pt(),Jet_neHEF[leadingJetIndex],eweight);

            //h_PtAve_QGL_ctag[index]->Fill(ptave,Jet_qgl[leadingJetIndex],eweight);
            h_PtAve_muEF_ctag[index]->Fill(ptave,Jet_muEF[leadingJetIndex],eweight);
            h_PtAve_chEmEF_ctag[index]->Fill(ptave,Jet_chEmEF[leadingJetIndex],eweight);
            h_PtAve_chHEF_ctag[index]->Fill(ptave,Jet_chHEF[leadingJetIndex],eweight);
            h_PtAve_neEmEF_ctag[index]->Fill(ptave,Jet_neEmEF[leadingJetIndex],eweight);
            h_PtAve_neHEF_ctag[index]->Fill(ptave,Jet_neHEF[leadingJetIndex],eweight);

            if(Zboson.Pt() > 30){
              h_Zpt_Mu_Rho_ctag[index]->Fill(Zboson.Pt(),mu,fixedGridRhoFastjetAll,eweight);
              h_Zpt_Mu_NPVgood_ctag[index]->Fill(Zboson.Pt(),mu,PV_npvsGood,eweight);
              h_Zpt_Mu_NPVall_ctag[index]->Fill(Zboson.Pt(),mu,PV_npvs,eweight);
              h_Zpt_Mu_Mu_ctag[index]->Fill(Zboson.Pt(),mu,mu,eweight);
            }
            h_Mu_RpT_ctag[index]->Fill(mu,R_pT,eweight);
            h_Mu_RMPF_ctag[index]->Fill(mu,R_MPF,eweight);
            h_JetPt_RMPF_ctag[index]->Fill(leadingJet.Pt(),R_MPF,eweight);
            h_PtAve_RMPF_ctag[index]->Fill(ptave,R_MPF,eweight);
            h_Mu_RMPFjet1_ctag[index]->Fill(mu,R_MPFjet1,eweight);
            h_JetPt_RMPFjet1_ctag[index]->Fill(leadingJet.Pt(),R_MPFjet1,eweight);
            h_PtAve_RMPFjet1_ctag[index]->Fill(ptave,R_MPFjet1,eweight);
            h_Mu_RMPFjetn_ctag[index]->Fill(mu,R_MPFjetn,eweight);
            h_JetPt_RMPFjetn_ctag[index]->Fill(leadingJet.Pt(),R_MPFjetn,eweight);
            h_PtAve_RMPFjetn_ctag[index]->Fill(ptave,R_MPFjetn,eweight);
            h_Mu_RMPFuncl_ctag[index]->Fill(mu,R_MPFuncl,eweight);
            h_JetPt_RMPFuncl_ctag[index]->Fill(leadingJet.Pt(),R_MPFuncl,eweight);
            h_PtAve_RMPFuncl_ctag[index]->Fill(ptave,R_MPFuncl,eweight);
            h_JetPt_RpT_ctag[index]->Fill(leadingJet.Pt(),R_pT,eweight);
            h_PtAve_RpT_ctag[index]->Fill(ptave,R_pT,eweight);
            cJetLabel_ctag->increment("all jets");
            if(!isData){
              h_Zpt_RGMPF_ctag[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
              h_Zpt_RGenjet1_ctag[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
              h_Zpt_RGenjetn_ctag[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
              h_Zpt_RGenuncl_ctag[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
              h_Zpt_Resp_ctag[index]->Fill(Zboson.Pt(),resp,eweight); 
              h_Zpt_RBal_ctag[index]->Fill(Zboson.Pt(),bal,eweight);
              h_pTgen_Resp_ctag[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              switch(jetGenFlavor){
              case 5:cJetLabel_ctag->increment("b jets");
                h_Zpt_RpT_ctag_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_ctag_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_ctag_genb[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_ctag_genb[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_ctag_genb[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_ctag_genb[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_ctag_genb[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_ctag_genb[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_ctag_genb[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_ctag_genb[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_ctag_genb[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_ctag_genb[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_ctag_genb[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_ctag_genb[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 4:cJetLabel_ctag->increment("c jets");
                h_Zpt_RpT_ctag_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_ctag_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_ctag_genc[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_ctag_genc[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_ctag_genc[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_ctag_genc[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_ctag_genc[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_ctag_genc[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_ctag_genc[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_ctag_genc[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_ctag_genc[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_ctag_genc[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_ctag_genc[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_ctag_genc[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 21:cJetLabel_ctag->increment("gluon jets");
                h_Zpt_RpT_ctag_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_ctag_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_ctag_geng[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_ctag_geng[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_ctag_geng[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_ctag_geng[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_ctag_geng[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_ctag_geng[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_ctag_geng[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_ctag_geng[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_ctag_geng[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_ctag_geng[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_ctag_geng[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_ctag_geng[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 3:
                h_Zpt_RpT_ctag_gens[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_ctag_gens[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_ctag_gens[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_ctag_gens[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_ctag_gens[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);

                h_Zpt_RpT_ctag_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_ctag_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_ctag_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_ctag_genuds[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_ctag_genuds[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_ctag_genuds[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_ctag_genuds[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_ctag_genuds[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_ctag_genuds[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_ctag_genuds[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_ctag_genuds[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_ctag_genuds[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_ctag_genuds[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_ctag_genuds[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 2:
              case 1:cJetLabel_ctag->increment("uds jets");
                h_Zpt_RpT_ctag_genud[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_ctag_genud[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_ctag_genud[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_ctag_genud[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_ctag_genud[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);

                h_Zpt_RpT_ctag_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_ctag_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_ctag_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_ctag_genuds[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_ctag_genuds[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_ctag_genuds[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_ctag_genuds[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_ctag_genuds[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_ctag_genuds[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_ctag_genuds[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_ctag_genuds[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_ctag_genuds[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_ctag_genuds[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_ctag_genuds[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              default: cJetLabel_ctag->increment("unclassified jets");
                h_Zpt_RpT_ctag_unclassified[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_ctag_unclassified[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_ctag_unclassified[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_ctag_unclassified[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_ctag_unclassified[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_ctag_unclassified[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_ctag_unclassified[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_ctag_unclassified[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_ctag_unclassified[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_ctag_unclassified[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_ctag_unclassified[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_ctag_unclassified[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_ctag_unclassified[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_ctag_unclassified[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              }
            }
          }
	  if(Jet_btagDeepFlavB[leadingJetIndex] <= btagDeepFlavB->tight(year) &&
             Jet_btagDeepFlavCvL[leadingJetIndex] <= btagDeepFlavC->tight(year) &&
             Jet_btagDeepFlavQG[leadingJetIndex] >= 0 &&
             Jet_btagDeepFlavQG[leadingJetIndex] < btagDeepFlavQG->tight(year)){
	    //Jet_btagDeepB[leadingJetIndex] <= btagDeepB->tight(year) &&
	    //Jet_btagDeepCvL[leadingJetIndex] <= btagDeepC->tight(year) &&
	    //Jet_btagDeepFlavQG[leadingJetIndex] >= 0 &&
	    //Jet_btagDeepFlavQG[leadingJetIndex] < gluontag->tight(year)){
	    //if(Jet_particleNetAK4_B[leadingJetIndex] <= particleNetAK4_Btag->tight(year) &&
	    //Jet_particleNetAK4_CvsL[leadingJetIndex] <= particleNetAK4_CvsLtag->tight(year) &&
	    //Jet_particleNetAK4_QvsG[leadingJetIndex] >= 0 &&
	    //Jet_particleNetAK4_QvsG[leadingJetIndex] < particleNetAK4_QvsGtag->tight(year)){
            tagged = true;
            h_Zpt_RpT_gluontag[index]->Fill(Zboson.Pt(),R_pT,eweight);
            h_Zpt_RMPF_gluontag[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            h_Zpt_RMPFjet1_gluontag[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
            h_Zpt_RMPFjetn_gluontag[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
            h_Zpt_RMPFuncl_gluontag[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
            //h_JetPt_QGL_gluontag[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
            //h_Zpt_QGL_gluontag[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
            h_JetPt_muEF_gluontag[index]->Fill(leadingJet.Pt(),Jet_muEF[leadingJetIndex],eweight);
            h_JetPt_chEmEF_gluontag[index]->Fill(leadingJet.Pt(),Jet_chEmEF[leadingJetIndex],eweight);
            h_JetPt_chHEF_gluontag[index]->Fill(leadingJet.Pt(),Jet_chHEF[leadingJetIndex],eweight);
            h_JetPt_neEmEF_gluontag[index]->Fill(leadingJet.Pt(),Jet_neEmEF[leadingJetIndex],eweight);
            h_JetPt_neHEF_gluontag[index]->Fill(leadingJet.Pt(),Jet_neHEF[leadingJetIndex],eweight);

            //h_PtAve_QGL_gluontag[index]->Fill(ptave,Jet_qgl[leadingJetIndex],eweight);
            h_PtAve_muEF_gluontag[index]->Fill(ptave,Jet_muEF[leadingJetIndex],eweight);
            h_PtAve_chEmEF_gluontag[index]->Fill(ptave,Jet_chEmEF[leadingJetIndex],eweight);
            h_PtAve_chHEF_gluontag[index]->Fill(ptave,Jet_chHEF[leadingJetIndex],eweight);
            h_PtAve_neEmEF_gluontag[index]->Fill(ptave,Jet_neEmEF[leadingJetIndex],eweight);
            h_PtAve_neHEF_gluontag[index]->Fill(ptave,Jet_neHEF[leadingJetIndex],eweight);

            if(Zboson.Pt() > 30){
              h_Zpt_Mu_Rho_gluontag[index]->Fill(Zboson.Pt(),mu,fixedGridRhoFastjetAll,eweight);
              h_Zpt_Mu_NPVgood_gluontag[index]->Fill(Zboson.Pt(),mu,PV_npvsGood,eweight);
              h_Zpt_Mu_NPVall_gluontag[index]->Fill(Zboson.Pt(),mu,PV_npvs,eweight);
              h_Zpt_Mu_Mu_gluontag[index]->Fill(Zboson.Pt(),mu,mu,eweight);
            }
            h_Mu_RpT_gluontag[index]->Fill(mu,R_pT,eweight);
            h_Mu_RMPF_gluontag[index]->Fill(mu,R_MPF,eweight);
            h_JetPt_RMPF_gluontag[index]->Fill(leadingJet.Pt(),R_MPF,eweight);
            h_PtAve_RMPF_gluontag[index]->Fill(ptave,R_MPF,eweight);
            h_Mu_RMPFjet1_gluontag[index]->Fill(mu,R_MPFjet1,eweight);
            h_JetPt_RMPFjet1_gluontag[index]->Fill(leadingJet.Pt(),R_MPFjet1,eweight);
            h_PtAve_RMPFjet1_gluontag[index]->Fill(ptave,R_MPFjet1,eweight);
            h_Mu_RMPFjetn_gluontag[index]->Fill(mu,R_MPFjetn,eweight);
            h_JetPt_RMPFjetn_gluontag[index]->Fill(leadingJet.Pt(),R_MPFjetn,eweight);
            h_PtAve_RMPFjetn_gluontag[index]->Fill(ptave,R_MPFjetn,eweight);
            h_Mu_RMPFuncl_gluontag[index]->Fill(mu,R_MPFuncl,eweight);
            h_JetPt_RMPFuncl_gluontag[index]->Fill(leadingJet.Pt(),R_MPFuncl,eweight);
            h_PtAve_RMPFuncl_gluontag[index]->Fill(ptave,R_MPFuncl,eweight);
            h_JetPt_RpT_gluontag[index]->Fill(leadingJet.Pt(),R_pT,eweight);
            h_PtAve_RpT_gluontag[index]->Fill(ptave,R_pT,eweight);
            cJetLabel_gluontag->increment("all jets");
            if(!isData){
              h_Zpt_RGMPF_gluontag[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
              h_Zpt_RGenjet1_gluontag[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
              h_Zpt_RGenjetn_gluontag[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
              h_Zpt_RGenuncl_gluontag[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
              h_Zpt_RBal_gluontag[index]->Fill(Zboson.Pt(),bal,eweight);
              h_Zpt_Resp_gluontag[index]->Fill(Zboson.Pt(),resp,eweight);
              h_pTgen_Resp_gluontag[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              switch(jetGenFlavor){
              case 5:cJetLabel_gluontag->increment("b jets");
                h_Zpt_RpT_gluontag_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_gluontag_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_gluontag_genb[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_gluontag_genb[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_gluontag_genb[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_gluontag_genb[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_gluontag_genb[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_gluontag_genb[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_gluontag_genb[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_gluontag_genb[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_gluontag_genb[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_RBal_gluontag_genb[index]->Fill(Zboson.Pt(),bal,eweight);
                h_Zpt_Resp_gluontag_genb[index]->Fill(Zboson.Pt(),resp,eweight);
                h_pTgen_Resp_gluontag_genb[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 4:cJetLabel_gluontag->increment("c jets");
                h_Zpt_RpT_gluontag_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_gluontag_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_gluontag_genc[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_gluontag_genc[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_gluontag_genc[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_gluontag_genc[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_gluontag_genc[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_gluontag_genc[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_gluontag_genc[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_gluontag_genc[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_gluontag_genc[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_gluontag_genc[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_gluontag_genc[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_gluontag_genc[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 21:cJetLabel_gluontag->increment("gluon jets");
                h_Zpt_RpT_gluontag_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_gluontag_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_gluontag_geng[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_gluontag_geng[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_gluontag_geng[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_gluontag_genc[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_gluontag_genc[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_gluontag_geng[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_gluontag_geng[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_gluontag_geng[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_gluontag_geng[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_RBal_gluontag_geng[index]->Fill(Zboson.Pt(),bal,eweight);
                h_Zpt_Resp_gluontag_geng[index]->Fill(Zboson.Pt(),resp,eweight);
                h_pTgen_Resp_gluontag_geng[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 3:
                h_Zpt_RpT_gluontag_gens[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_gluontag_gens[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_gluontag_gens[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_gluontag_gens[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_gluontag_gens[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                
                h_Zpt_RpT_gluontag_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_gluontag_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_gluontag_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_gluontag_genuds[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_gluontag_genuds[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_gluontag_genuds[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_gluontag_genuds[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_gluontag_genuds[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_gluontag_genuds[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_gluontag_genuds[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_gluontag_genuds[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_RBal_gluontag_genuds[index]->Fill(Zboson.Pt(),bal,eweight);
                h_Zpt_Resp_gluontag_genuds[index]->Fill(Zboson.Pt(),resp,eweight);
                h_pTgen_Resp_gluontag_genuds[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 2:
              case 1: cJetLabel_gluontag->increment("uds jets");
                h_Zpt_RpT_gluontag_genud[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_gluontag_genud[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_gluontag_genud[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_gluontag_genud[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_gluontag_genud[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                
                h_Zpt_RpT_gluontag_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_gluontag_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_gluontag_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_gluontag_genuds[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_gluontag_genuds[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_gluontag_genuds[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_gluontag_genuds[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_gluontag_genuds[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_gluontag_genuds[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_gluontag_genuds[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_gluontag_genuds[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_RBal_gluontag_genuds[index]->Fill(Zboson.Pt(),bal,eweight);
                h_Zpt_Resp_gluontag_genuds[index]->Fill(Zboson.Pt(),resp,eweight);
                h_pTgen_Resp_gluontag_genuds[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              default: cJetLabel_gluontag->increment("unclassified jets");
                h_Zpt_RpT_gluontag_unclassified[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_gluontag_unclassified[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_gluontag_unclassified[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_gluontag_unclassified[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_gluontag_unclassified[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_gluontag_unclassified[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_gluontag_unclassified[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_gluontag_unclassified[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_gluontag_unclassified[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_gluontag_unclassified[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_gluontag_unclassified[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_RBal_gluontag_unclassified[index]->Fill(Zboson.Pt(),bal,eweight);
                h_Zpt_Resp_gluontag_unclassified[index]->Fill(Zboson.Pt(),resp,eweight);
                h_pTgen_Resp_gluontag_unclassified[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              }
            }
          }
	  if(Jet_btagDeepFlavB[leadingJetIndex] <= btagDeepFlavB->tight(year) &&
             Jet_btagDeepFlavCvL[leadingJetIndex] <= btagDeepFlavC->tight(year) &&
             Jet_btagDeepFlavQG[leadingJetIndex] >= btagDeepFlavQG->tight(year)){
	    //Jet_btagDeepB[leadingJetIndex] <= btagDeepB->tight(year) &&
	    //Jet_btagDeepCvL[leadingJetIndex] <= btagDeepC->tight(year) &&
	    //Jet_btagDeepFlavQG[leadingJetIndex] >= gluontag->tight(year)){
	    //if(Jet_particleNetAK4_B[leadingJetIndex] <= particleNetAK4_Btag->tight(year) &&
	    //Jet_particleNetAK4_CvsL[leadingJetIndex] <= particleNetAK4_CvsLtag->tight(year) &&
	    //Jet_particleNetAK4_QvsG[leadingJetIndex] >= particleNetAK4_QvsGtag->tight(year)){
            tagged = true;
            h_Zpt_RpT_quarktag[index]->Fill(Zboson.Pt(),R_pT,eweight);
            h_Zpt_RMPF_quarktag[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            h_Zpt_RMPFjet1_quarktag[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
            h_Zpt_RMPFjetn_quarktag[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
            h_Zpt_RMPFuncl_quarktag[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
            //h_JetPt_QGL_quarktag[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
            //h_Zpt_QGL_quarktag[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
            h_JetPt_muEF_quarktag[index]->Fill(leadingJet.Pt(),Jet_muEF[leadingJetIndex],eweight);
            h_JetPt_chEmEF_quarktag[index]->Fill(leadingJet.Pt(),Jet_chEmEF[leadingJetIndex],eweight);
            h_JetPt_chHEF_quarktag[index]->Fill(leadingJet.Pt(),Jet_chHEF[leadingJetIndex],eweight);
            h_JetPt_neEmEF_quarktag[index]->Fill(leadingJet.Pt(),Jet_neEmEF[leadingJetIndex],eweight);
            h_JetPt_neHEF_quarktag[index]->Fill(leadingJet.Pt(),Jet_neHEF[leadingJetIndex],eweight);

            //h_PtAve_QGL_quarktag[index]->Fill(ptave,Jet_qgl[leadingJetIndex],eweight);
            h_PtAve_muEF_quarktag[index]->Fill(ptave,Jet_muEF[leadingJetIndex],eweight);
            h_PtAve_chEmEF_quarktag[index]->Fill(ptave,Jet_chEmEF[leadingJetIndex],eweight);
            h_PtAve_chHEF_quarktag[index]->Fill(ptave,Jet_chHEF[leadingJetIndex],eweight);
            h_PtAve_neEmEF_quarktag[index]->Fill(ptave,Jet_neEmEF[leadingJetIndex],eweight);
            h_PtAve_neHEF_quarktag[index]->Fill(ptave,Jet_neHEF[leadingJetIndex],eweight);

            if(Zboson.Pt() > 30){
              h_Zpt_Mu_Rho_quarktag[index]->Fill(Zboson.Pt(),mu,fixedGridRhoFastjetAll,eweight);
              h_Zpt_Mu_NPVgood_quarktag[index]->Fill(Zboson.Pt(),mu,PV_npvsGood,eweight);
              h_Zpt_Mu_NPVall_quarktag[index]->Fill(Zboson.Pt(),mu,PV_npvs,eweight);
              h_Zpt_Mu_Mu_quarktag[index]->Fill(Zboson.Pt(),mu,mu,eweight);
            }
            h_Mu_RpT_quarktag[index]->Fill(mu,R_pT,eweight);
            h_Mu_RMPF_quarktag[index]->Fill(mu,R_MPF,eweight);
            h_JetPt_RMPF_quarktag[index]->Fill(leadingJet.Pt(),R_MPF,eweight);
            h_PtAve_RMPF_quarktag[index]->Fill(ptave,R_MPF,eweight);
            h_Mu_RMPFjet1_quarktag[index]->Fill(mu,R_MPFjet1,eweight);
            h_JetPt_RMPFjet1_quarktag[index]->Fill(leadingJet.Pt(),R_MPFjet1,eweight);
            h_PtAve_RMPFjet1_quarktag[index]->Fill(ptave,R_MPFjet1,eweight);
            h_Mu_RMPFjetn_quarktag[index]->Fill(mu,R_MPFjetn,eweight);
            h_JetPt_RMPFjetn_quarktag[index]->Fill(leadingJet.Pt(),R_MPFjetn,eweight);
            h_PtAve_RMPFjetn_quarktag[index]->Fill(ptave,R_MPFjetn,eweight);
            h_Mu_RMPFuncl_quarktag[index]->Fill(mu,R_MPFuncl,eweight);
            h_JetPt_RMPFuncl_quarktag[index]->Fill(leadingJet.Pt(),R_MPFuncl,eweight);
            h_PtAve_RMPFuncl_quarktag[index]->Fill(ptave,R_MPFuncl,eweight);
            h_JetPt_RpT_quarktag[index]->Fill(leadingJet.Pt(),R_pT,eweight);
            h_PtAve_RpT_quarktag[index]->Fill(ptave,R_pT,eweight);
            cJetLabel_quarktag->increment("all jets");
            if(!isData){
              h_Zpt_RGMPF_quarktag[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
              h_Zpt_RGenjet1_quarktag[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
              h_Zpt_RGenjetn_quarktag[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
              h_Zpt_RGenuncl_quarktag[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
              h_Zpt_Resp_quarktag[index]->Fill(Zboson.Pt(),resp,eweight);
              h_Zpt_RBal_quarktag[index]->Fill(Zboson.Pt(),bal,eweight);
              h_pTgen_Resp_quarktag[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              switch(jetGenFlavor){
              case 5:cJetLabel_quarktag->increment("b jets");
                h_Zpt_RpT_quarktag_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_quarktag_genb[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_quarktag_genb[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_quarktag_genb[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_quarktag_genb[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_quarktag_genb[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_quarktag_genb[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_quarktag_genb[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_quarktag_genb[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_quarktag_genb[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_quarktag_genb[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_quarktag_genb[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_quarktag_genb[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 4:cJetLabel_quarktag->increment("c jets");
                h_Zpt_RpT_quarktag_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_quarktag_genc[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_quarktag_genc[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_quarktag_genc[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_quarktag_genc[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_quarktag_genc[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_quarktag_genc[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_quarktag_genc[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_quarktag_genc[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_quarktag_genc[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_quarktag_genc[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_quarktag_genc[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_quarktag_genc[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 21:cJetLabel_quarktag->increment("gluon jets");
                h_Zpt_RpT_quarktag_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_quarktag_geng[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_quarktag_geng[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_quarktag_geng[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_quarktag_geng[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_quarktag_geng[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_quarktag_geng[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_quarktag_geng[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_quarktag_geng[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_quarktag_geng[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_quarktag_geng[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_quarktag_geng[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_quarktag_geng[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 3:cJetLabel_quarktag->increment("uds jets");
                h_Zpt_RpT_quarktag_gens[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_gens[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_quarktag_gens[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_quarktag_gens[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_quarktag_gens[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                
                h_Zpt_RpT_quarktag_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_quarktag_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_quarktag_genuds[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_quarktag_genuds[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_quarktag_genuds[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_quarktag_genuds[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_quarktag_genuds[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_quarktag_genuds[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_quarktag_genuds[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_quarktag_genuds[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_quarktag_genuds[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_quarktag_genuds[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_quarktag_genuds[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 2:
              case 1:cJetLabel_quarktag->increment("uds jets");
                h_Zpt_RpT_quarktag_genud[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_genud[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_quarktag_genud[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_quarktag_genud[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_quarktag_genud[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                
                h_Zpt_RpT_quarktag_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_quarktag_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_quarktag_genuds[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_quarktag_genuds[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_quarktag_genuds[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_quarktag_genuds[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGenjet1_quarktag_genuds[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_quarktag_genuds[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_quarktag_genuds[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_quarktag_genuds[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_quarktag_genuds[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_quarktag_genuds[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              default: cJetLabel_quarktag->increment("unclassified jets");
                h_Zpt_RpT_quarktag_unclassified[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_unclassified[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_quarktag_unclassified[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_quarktag_unclassified[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_quarktag_unclassified[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_quarktag_unclassified[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_quarktag_unclassified[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_RpT_quarktag_unclassified[index]->Fill(Zboson.Pt(),R_pT,eweight);
                //h_Zpt_RMPF_quarktag_unclassified[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                //h_Zpt_RMPFjet1_quarktag_unclassified[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                //h_Zpt_RMPFjetn_quarktag_unclassified[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                //h_Zpt_RMPFuncl_quarktag_unclassified[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                h_Zpt_RGenjet1_quarktag_unclassified[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_quarktag_unclassified[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_quarktag_unclassified[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_quarktag_unclassified[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_quarktag_unclassified[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_quarktag_unclassified[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              }
            }
          }

          if(!tagged){
            h_Zpt_RpT_notag[index]->Fill(Zboson.Pt(),R_pT,eweight);
            h_Zpt_RMPF_notag[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            h_Zpt_RMPFjet1_notag[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
            h_Zpt_RMPFjetn_notag[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
            h_Zpt_RMPFuncl_notag[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
            //h_JetPt_QGL_notag[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
            //h_Zpt_QGL_notag[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
            h_JetPt_muEF_notag[index]->Fill(leadingJet.Pt(),Jet_muEF[leadingJetIndex],eweight);
            h_JetPt_chEmEF_notag[index]->Fill(leadingJet.Pt(),Jet_chEmEF[leadingJetIndex],eweight);
            h_JetPt_chHEF_notag[index]->Fill(leadingJet.Pt(),Jet_chHEF[leadingJetIndex],eweight);
            h_JetPt_neEmEF_notag[index]->Fill(leadingJet.Pt(),Jet_neEmEF[leadingJetIndex],eweight);
            h_JetPt_neHEF_notag[index]->Fill(leadingJet.Pt(),Jet_neHEF[leadingJetIndex],eweight);

            //h_PtAve_QGL_notag[index]->Fill(ptave,Jet_qgl[leadingJetIndex],eweight);
            h_PtAve_muEF_notag[index]->Fill(ptave,Jet_muEF[leadingJetIndex],eweight);
            h_PtAve_chEmEF_notag[index]->Fill(ptave,Jet_chEmEF[leadingJetIndex],eweight);
            h_PtAve_chHEF_notag[index]->Fill(ptave,Jet_chHEF[leadingJetIndex],eweight);
            h_PtAve_neEmEF_notag[index]->Fill(ptave,Jet_neEmEF[leadingJetIndex],eweight);
            h_PtAve_neHEF_notag[index]->Fill(ptave,Jet_neHEF[leadingJetIndex],eweight);

            if(Zboson.Pt() > 30){
              h_Zpt_Mu_Rho_notag[index]->Fill(Zboson.Pt(),mu,fixedGridRhoFastjetAll,eweight);
              h_Zpt_Mu_NPVgood_notag[index]->Fill(Zboson.Pt(),mu,PV_npvsGood,eweight);
              h_Zpt_Mu_NPVall_notag[index]->Fill(Zboson.Pt(),mu,PV_npvs,eweight);
              h_Zpt_Mu_Mu_notag[index]->Fill(Zboson.Pt(),mu,mu,eweight);
            }
            h_Mu_RpT_notag[index]->Fill(mu,R_pT,eweight);
            h_Mu_RMPF_notag[index]->Fill(mu,R_MPF,eweight);
            h_JetPt_RMPF_notag[index]->Fill(leadingJet.Pt(),R_MPF,eweight);
            h_PtAve_RMPF_notag[index]->Fill(ptave,R_MPF,eweight);
            h_Mu_RMPFjet1_notag[index]->Fill(mu,R_MPFjet1,eweight);
            h_JetPt_RMPFjet1_notag[index]->Fill(leadingJet.Pt(),R_MPFjet1,eweight);
            h_PtAve_RMPFjet1_notag[index]->Fill(ptave,R_MPFjet1,eweight);
            h_Mu_RMPFjetn_notag[index]->Fill(mu,R_MPFjetn,eweight);
            h_JetPt_RMPFjetn_notag[index]->Fill(leadingJet.Pt(),R_MPFjetn,eweight);
            h_PtAve_RMPFjetn_notag[index]->Fill(ptave,R_MPFjetn,eweight);
            h_Mu_RMPFuncl_notag[index]->Fill(mu,R_MPFuncl,eweight);
            h_JetPt_RMPFuncl_notag[index]->Fill(leadingJet.Pt(),R_MPFuncl,eweight);
            h_PtAve_RMPFuncl_notag[index]->Fill(ptave,R_MPFuncl,eweight);
            h_JetPt_RpT_notag[index]->Fill(leadingJet.Pt(),R_pT,eweight);
            h_PtAve_RpT_notag[index]->Fill(ptave,R_pT,eweight);
            cJetLabel_notag->increment("all jets");
            if(!isData){
              h_Zpt_RGMPF_notag[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
              h_Zpt_RGenjet1_notag[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
              h_Zpt_RGenjetn_notag[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
              h_Zpt_RGenuncl_notag[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
              h_Zpt_RBal_notag[index]->Fill(Zboson.Pt(),bal,eweight);
              h_Zpt_Resp_notag[index]->Fill(Zboson.Pt(),resp,eweight);
              h_pTgen_Resp_notag[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              switch(jetGenFlavor){
              case 5:cJetLabel_notag->increment("b jets");
                h_Zpt_RpT_notag_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_notag_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_notag_genb[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_notag_genb[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_notag_genb[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_notag_genb[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_notag_genb[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_notag_genb[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_notag_genb[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_notag_genb[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_notag_genb[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_RBal_notag_genb[index]->Fill(Zboson.Pt(),bal,eweight);
                h_Zpt_Resp_notag_genb[index]->Fill(Zboson.Pt(),resp,eweight);
                h_pTgen_Resp_notag_genb[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 4:cJetLabel_notag->increment("c jets");
                h_Zpt_RpT_notag_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_notag_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_notag_genc[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_notag_genc[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_notag_genc[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_notag_genc[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_notag_genc[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_notag_genc[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_notag_genc[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_notag_genc[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_notag_genc[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_Resp_notag_genc[index]->Fill(Zboson.Pt(),resp,eweight);
                h_Zpt_RBal_notag_genc[index]->Fill(Zboson.Pt(),bal,eweight);
                h_pTgen_Resp_notag_genc[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 21:cJetLabel_notag->increment("gluon jets");
                h_Zpt_RpT_notag_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_notag_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_notag_geng[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_notag_geng[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_notag_geng[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_notag_geng[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_notag_geng[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_notag_geng[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_notag_geng[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_notag_geng[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_notag_geng[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_RBal_notag_geng[index]->Fill(Zboson.Pt(),bal,eweight);
                h_Zpt_Resp_notag_geng[index]->Fill(Zboson.Pt(),resp,eweight);
                h_pTgen_Resp_notag_geng[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 3:
                h_Zpt_RpT_notag_gens[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_notag_gens[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_notag_gens[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_notag_gens[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_notag_gens[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                
                h_Zpt_RpT_notag_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_notag_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_notag_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_notag_genuds[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_notag_genuds[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_notag_genuds[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_notag_genuds[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_notag_genuds[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_notag_genuds[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_notag_genuds[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_notag_genuds[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_RBal_notag_genuds[index]->Fill(Zboson.Pt(),bal,eweight);
                h_Zpt_Resp_notag_genuds[index]->Fill(Zboson.Pt(),resp,eweight);
                h_pTgen_Resp_notag_genuds[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              case 2:
              case 1: cJetLabel_notag->increment("uds jets");
                h_Zpt_RpT_notag_genud[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_notag_genud[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_notag_genud[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_notag_genud[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_notag_genud[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                
                h_Zpt_RpT_notag_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_notag_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_notag_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_notag_genuds[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_notag_genuds[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_notag_genuds[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_notag_genuds[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_notag_genuds[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_notag_genuds[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_notag_genuds[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_notag_genuds[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_RBal_notag_genuds[index]->Fill(Zboson.Pt(),bal,eweight);
                h_Zpt_Resp_notag_genuds[index]->Fill(Zboson.Pt(),resp,eweight);
                h_pTgen_Resp_notag_genuds[index]->Fill(leadingGenJet.Pt(),resp,eweight);
                break;
              default: cJetLabel_notag->increment("unclassified jets");
                h_Zpt_RpT_notag_unclassified[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_notag_unclassified[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                h_Zpt_RMPFjet1_notag_unclassified[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
                h_Zpt_RMPFjetn_notag_unclassified[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
                h_Zpt_RMPFuncl_notag_unclassified[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
                //h_JetPt_QGL_notag_unclassified[index]->Fill(leadingJet.Pt(),Jet_qgl[leadingJetIndex],eweight);
                //h_Zpt_QGL_notag_unclassified[index]->Fill(Zboson.Pt(),Jet_qgl[leadingJetIndex],eweight);
                h_Zpt_RGMPF_notag_unclassified[index]->Fill(Zboson.Pt(),R_GMPF,eweight);
                h_Zpt_RGenjet1_notag_unclassified[index]->Fill(Zboson.Pt(),R_Genjet1,eweight);
                h_Zpt_RGenjetn_notag_unclassified[index]->Fill(Zboson.Pt(),R_Genjetn,eweight);
                h_Zpt_RGenuncl_notag_unclassified[index]->Fill(Zboson.Pt(),R_Genuncl,eweight);
                h_Zpt_RBal_notag_unclassified[index]->Fill(Zboson.Pt(),bal,eweight);
                h_Zpt_Resp_notag_unclassified[index]->Fill(Zboson.Pt(),resp,eweight);
                h_pTgen_Resp_notag_unclassified[index]->Fill(leadingGenJet.Pt(),resp,eweight);
              }
            }
          }
          
	  if(Jet_btagDeepFlavB[leadingJetIndex] <= btagDeepFlavB->tight(year) &&
             Jet_btagDeepFlavCvL[leadingJetIndex] <= btagDeepFlavC->tight(year) &&
             Jet_btagDeepFlavQG[leadingJetIndex] < 0){
	    //if(Jet_btagDeepB[leadingJetIndex] <= btagDeepB->tight(year) &&
	    //   Jet_btagDeepCvL[leadingJetIndex] <= btagDeepC->tight(year) &&
	    //   Jet_btagDeepFlavQG[leadingJetIndex] < 0){
	    cJetLabel_qgtLT0tag->increment("all jets");
            if(!isData){
              switch(jetGenFlavor){
              case 5:cJetLabel_qgtLT0tag->increment("b jets");
                break;
              case 4:cJetLabel_qgtLT0tag->increment("c jets");
                break;
              case 21:cJetLabel_qgtLT0tag->increment("gluon jets");
                break;
              case 3:
              case 2:
              case 1: cJetLabel_qgtLT0tag->increment("uds jets");
                break;
              default: cJetLabel_qgtLT0tag->increment("unclassified jets");
              }
            }
          }

        }
      }
    }
  }
  return true;
}

void Analysis::printSync(TLorentzVector& Zboson, std::vector<int> jetSelection,TVector3& rmet,TVector3& met){
  std::cout << run << ":" << luminosityBlock << ":" << event << ", zpt=" << Zboson.Pt();
  /*
  for(size_t i = 0; i < jetSelection.size(); ++i){
    TLorentzVector jet = getJet(jetSelection[i]);
    std::cout << " j"<<i+1<<"pt=" << jet.Pt();
  }
  */
  std::cout << " rawMET=" << rmet.Pt() << " MET=" << met.Pt() << std::endl;
  for(size_t i = 0; i < jetSelection.size(); ++i){
    TLorentzVector jet = getJet(jetSelection[i]);
    std::cout << "    j"<<i+1<<"pt=" << Jet_ptraw[jetSelection[i]] << ", eta=" << jet.Eta() << ", phi=" << jet.Phi() << std::endl;
  }
}

/*
bool Analysis::GetTriggerDecision(const char* trg){
  std::vector<std::string> triggerNames;
  std::string s_trg = std::string(trg);
  size_t pos = s_trg.find("||");
  if (pos == s_trg.size()){
    triggerNames.push_back(s_trg);
  }else{
    while (pos < s_trg.size()){
      pos = s_trg.find("||");
      triggerNames.push_back(s_trg.substr(0,pos));
      s_trg = s_trg.substr(pos+2,s_trg.size());
    }
  }
  bool trgDecision = false;
  for(size_t i = 0; i < triggerNames.size(); ++i){
    //    std::cout << "check trgMap " << trgMap[triggerNames[i].c_str()] << std::endl;
    //    TObject* to = fReader.FindObject(triggerNames[i].c_str());
    //    TTreeReaderValue<Bool_t> trgBit = {fReader, triggerNames[i].c_str()};
    //    trgDecision = trgDecision || *trgBit;
    //    TBranch* b = fChain->FindBranch(triggerNames[i].c_str());
    //    bool value = b->
    //        std::cout << "check branch " << triggerBit->GetName() << std::endl;
    //    bool tmp = *HLT_IsoMu27;
    //bool tmp2 = *HLT_Mu45_eta2p1;
    std::cout << "check triggerBit " << triggerNames[i] << " " << triggerBit[i] << std::endl; 
  }
  //    std::cout << "check " << *HLT_test << std::endl;
    
    
  
  return trgDecision;
}
*/
