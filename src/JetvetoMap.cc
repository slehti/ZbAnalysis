#include "JetvetoMap.h"
#include <iostream>

JetvetoMap::JetvetoMap(std::string year, bool isData){
  std::string filename = "";
  std::string histo = "";
  if(std::string(year).find("2016") != std::string::npos) {
    filename = std::string("hotjets-UL16.root");
    if(isData) histo    = std::string("h2hot_ul16_plus_hbm2_hbp12_qie11");
    else histo    = std::string("h2hot_mc");
  }
  if(std::string(year).find("2017") != std::string::npos) {
    filename = std::string("hotjets-UL17_v2.root");
    if(isData) histo = std::string("h2hot_ul17_plus_hep17_plus_hbpw89");
  }
  if(std::string(year).find("2018") != std::string::npos) {
    filename = std::string("hotjets-UL18.root");
    if(isData) histo = std::string("h2hot_ul18_plus_hem1516_and_hbp2m1");
  }

  if(std::string(year).find("2022") != std::string::npos) {
    filename = std::string("jetveto2022EFG.root");
    if(year == "2022C" || year == "2022D") filename = std::string("jetveto2022CD.root");
    if(isData) histo = std::string("jetvetomap");    
  }
  if(std::string(year).find("2023") != std::string::npos) {
    filename = std::string("jetveto2023BC.root");
    if(year == "2023D") filename = std::string("jetveto2023D.root");
    if(isData) histo = std::string("jetvetomap");
  }
  if(std::string(year).find("2024") != std::string::npos) {
    filename = std::string("jetveto2024BCDEFGHI.root");
    //if(year == "2024F" || year == "2024G" || year == "2024H" || year == "2024I") filename = std::string("jetveto2024FG_FPix_V6M.root");
    if(isData) histo = std::string("jetvetomap");
  }

  useMap = false;
  if(histo.length() > 0){
    useMap = true;
  }

  if(useMap){
    std::cout << "Using JetvetoMap file " << filename << ", histo " << histo << std::endl;
    TFile* fMAP = TFile::Open(filename.c_str());
    map = (TH2D*)fMAP->Get(histo.c_str());
    map->SetDirectory(0);
    fMAP->Close();
  }else{
    std::cout << "Not using JetvetoMap" << std::endl;
  }
}
JetvetoMap::~JetvetoMap(){}

bool JetvetoMap::pass(TLorentzVector& jet){
  if(!useMap) return true;

  int i = map->GetXaxis()->FindBin(jet.Eta());
  int j = map->GetYaxis()->FindBin(jet.Phi());

  return !(map->GetBinContent(i,j) > 0);
}
