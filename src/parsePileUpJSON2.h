#ifndef __parsePileUpJSON2_C__
#define __parsePileUpJSON2_C__

// Use script JSONtoASCII.py to transfer pileup jsons to txt format
// https://github.com/cihar29/OffsetTreeMaker
// lxplus.cern.ch:/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/PileUp/UltraLegacy/pileup_latest.txt
// JSONtoASCII.py pileup_latest.txt >& pileup_2016.txt

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <utility>

#include "TFile.h"
#include "TTree.h"
#include "TKey.h"

using namespace std;

map<int, map<int, float> > m_PU;
map<int, map<int, map<int, float> > > m_PUbx;
//float MINBIAS_XS_Run2 = 69200;
//float MINBIAS_XS_Run3 = 80000;
//float MINBIAS_XS_Run3 = 75300;

bool filefound = true;

float getAvgPU(int run, int ls) {
  if(!filefound) return 0;
  return m_PU[run][ls];
}

//float getAvgPU(int run, int ls, int bx) {
//  return m_PUbx[run][ls][bx];
//}
/*
int parsePileUpJSON2(string filename="lumi-per-bx.root") {

  //### Lumi per BX ###//

  TFile* file = TFile::Open( filename.data() );
  int nBuckets = 3564;

  TIter nextkey(file->GetListOfKeys());
  TKey* key;
  while ( (key = (TKey*)nextkey()) ) {
    TString keyname = key->GetName();
    TTree* tree = (TTree*) file->Get(keyname);
    int run = stoi( keyname.Data() );

    UInt_t ls;
    float lumi[nBuckets];

    tree->SetBranchAddress("LumiSection", &ls);
    tree->SetBranchAddress("Luminosity",  lumi);

    Long64_t nEntries = tree->GetEntries();
    for (Long64_t n=0; n<nEntries; n++) {
      tree->GetEntry(n);

      for (int i=0; i<nBuckets; i++) m_PUbx[run][ls][i] = lumi[i] * MINBIAS_XS / pow(2,18);
    }
  }

*/
int parsePileUpJSON2(string filename="pileup.txt",double MINBIAS_XS = 69200) {
  //### Using Brilcalc ###//
/*  cout << "Opening " << filename << "...";

  string line;
  ifstream file(filename);

  if (file.is_open()){
    cout << "ok" << endl;

    //loop over lines in file
    while ( getline(file,line) ){

      string run_str, ls_str;
      string str;
      int delim_pos;
      float PU = -1;

      if ( line.at(0) != '#' ){

        //loop over strings in line
        for (int string_num=0; (delim_pos = line.find(",")) != -1; string_num++){

          str = line.substr(0, delim_pos);
          line.erase(0, delim_pos + 1);

          if (string_num == 0)  //first string holds run number
            run_str = str.substr(0, str.find(":"));

          else if (string_num == 1) //second string has ls
            ls_str = str.substr(0, str.find(":"));

          else if (string_num == 7) //eighth string has pu
            PU = stof( str );
        }
        int run = stoi( run_str );
        int ls = stoi( ls_str );

        m_PU[run][ls] = PU;
      }
    }
    file.close();
  }
  else
    cout << "Unable to open file" << endl;

*/
  //### Using Pileup JSON file ###//
  //float MINBIAS_XS = MINBIAS_XS_Run2;
  //if(atoi(year.c_str()) > 2020) MINBIAS_XS = MINBIAS_XS_Run3;
  cout << "Minimum Bias Cross Section: " << MINBIAS_XS << endl;
  cout << "Opening " << filename << "...";

  string line;
  ifstream file(filename);

  if (file.is_open()){
    cout << "ok" << endl;

    //loop over lines in file
    while ( getline(file,line) ){

      string str;
      int delim_pos, run, ls;
      float PU = -1;

      if ( line.at(0) != '#' ){

        //loop over strings in line
        for (int string_num=0; (delim_pos = line.find(" ")) != -1; string_num++){

          str = line.substr(0, delim_pos);
          line.erase(0, delim_pos + 1);

          if (string_num == 0)  //first string holds run number
            run = stoi( str );

          else if (string_num == 1) //second string has ls
            ls = stoi( str );
        }
        PU = stod( line ) * MINBIAS_XS;
        m_PU[run][ls] = PU;
      }
    }
    file.close();
  }
  else{
    cout << "Unable to open file" << endl;
    filefound = false;
  }
  return 0;
}

#endif //__parsePileUpJSON2_C__

