#ifndef Analysis_h
#define Analysis_h

#include <iostream>

#include "TDirectory.h"
#include "TLorentzVector.h"
#include "TVector2.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TRandom3.h"
#include "BaseSelector.h"
#include "Counters.h"
#include "PileupWeight.h"
#include "PickEvents.h"
#include "Lumimask.h"
#include "JetvetoMap.h"

#include "RoccoR.h"

#include "CondFormats/JetMETObjects/interface/JetCorrectorParameters.h"
#include "CondFormats/JetMETObjects/interface/FactorizedJetCorrector.h"
#include "JetMETCorrections/Modules/interface/JetResolution.h"

#include "L2ResHistograms.h"

#include <vector>
#include <memory>
#include <random>
#include <time.h>

class Btag{
 public:
  Btag(){};
  ~Btag(){};

  void set(std::string year,double l, double m, double t){
    ll[year] = l;
    mm[year] = m;
    tt[year] = t;
  }
  bool  exists(std::string year){
    if(ll.find(year) != ll.end()) return true;
    return false;
  }
  double loose(std::string year){
    if(ll.find(year) != ll.end()) return ll[year];
    else printWarning(year);
    return -1;
  }
  double medium(std::string year){
    if(mm.find(year) != mm.end()) return mm[year];
    else printWarning(year);
    return -1;
  }
  double tight(std::string year){
    if(tt.find(year) != tt.end()) return tt[year];
    else printWarning(year);
    return -1;
  }

 private:
  void printWarning(std::string year){
    std::cout << "No btagging WP found for " << year << std::endl;
    exit(1);
  }
  std::map<std::string,double> ll;
  std::map<std::string,double> mm;
  std::map<std::string,double> tt;
};


class Analysis: public BaseSelector {
public:
  explicit Analysis();
  virtual ~Analysis();

  virtual void Begin(TTree*) override;
  virtual void SlaveBegin(TTree*) override;
  //  virtual void setupBranches(BranchManager& branchManager);
  virtual bool Process(Long64_t entry) override;
  virtual void Terminate() override;
  
  virtual void Init(TTree*) override;
  
private:

  Counters* cMain;
  Counters* cJetLabel;
  Counters* cJetLabel_btag;
  Counters* cJetLabel_ctag;
  Counters* cJetLabel_gluontag;
  Counters* cJetLabel_quarktag;
  Counters* cJetLabel_notag;
  Counters* cJetLabel_qgtLT0tag;

  float eventWeight;
  PileupWeight* pileupWeight;
  //  std::vector<float> psWeights;
  size_t npsWeights;
  float* psWeights;
  Lumimask* lumimask;

  void printStatus();
  long maxEvents;
  time_t timer;

  bool rcFound;
  RoccoR rc;

  JetvetoMap* jetvetoMap;

  L2ResHistograms* l2resHistograms;

  template <class T>
    T GetParameter (const char* parameterName);
  std::vector<std::string> GetParameters (const char*);
    
  void setupTriggerBranches(TTree*);
  void setupMETCleaningBranches(TTree*);
  void setupGenLevelBranches(TTree*);
  void setupBranches(TTree*);

  bool getTriggerDecision();
  bool triggeredBy(std::string);
  bool METCleaning();

  std::vector<std::string> cuts;

  TLorentzVector triggerMatching(TLorentzVector lepton);

  std::vector<TLorentzVector> getMuons(int charge);
  std::vector<TLorentzVector> getElectrons(int charge);
  std::vector<TLorentzVector> getGenParticles(int pid);

  void collectionSizeCheck(const char* name, uint dim, uint maxdim);

  bool passLeptonPtCut(std::vector<TLorentzVector> particles, int iLepton);

  bool passElectronID(int iElectron);
  //std::vector<TLorentzVector> getJets();
  std::vector<int> getJetIndexList();
  void applyJEC(std::vector<int> jetIndices);
  TLorentzVector getJet(int);
  TLorentzVector getGenJet(int);
  std::vector<int> drClean(std::vector<int> jetIndices,std::vector<TLorentzVector> compare, double cone=0.4);
  std::vector<int> jetPt(std::vector<int> jetIndices, double ptcut = 12);
  std::vector<int> jetId(std::vector<int> jetIndices);
  bool jetId(int jetIndex);
  bool matchGenJetFound(int jetIndex);
  std::vector<int> ptOrder(std::vector<int> jetIndices);
  int getLeadingJetIndex(std::vector<int> jets, size_t nth=0);
  //  std::vector<TLorentzVector> drClean(std::vector<TLorentzVector> orig, std::vector<TLorentzVector> compare, double cone=0.4);
  TLorentzVector getZboson(std::vector<TLorentzVector> leptonsPlus,std::vector<TLorentzVector> leptonsMinus);
  //  TLorentzVector selectLeadingJet(std::vector<TLorentzVector> jets, size_t nth=0);
  double alphaCut(double zpt);
  TVector3 recalculateMET(std::vector<int> jets, int label=1);
  bool passing(double, std::string);
  bool exists(const std::string&);
  int getJetFlavour(int jetIndex);
  //size_t getEtaBin(double);

  double smearing(int index, float rho);
  double smearingKIT(float pt, float eta, float rho);
  JME::JetResolution* m_jetResolution;
  JME::JetResolutionScaleFactor* m_jetResolutionScaleFactor;
  FactorizedJetCorrector *jersf;
  mutable std::mt19937 m_randomNumberGenerator;
  TRandom* m_randomNumberGenerator2;

  void printSync(TLorentzVector&,std::vector<int>,TVector3&,TVector3&);

  const char* datasetName;
  const char* year;
  const char* gitCommit;

  bool isData;
  bool smearingON;
  bool pileupreweightingON;

  double lumi;

  int leptonflavor;

  std::vector<std::string> triggerNames;
  Bool_t* triggerBit;

  std::vector<std::string> JECs;

  std::vector<std::string> filterFlags;
  Bool_t* filter;


  std::vector<TLorentzVector> leptonsPlus;
  std::vector<TLorentzVector> leptonsMinus;

  JetCorrectorParameters *l1rc;
  JetCorrectorParameters *l1;
  JetCorrectorParameters *l2;
  JetCorrectorParameters *l3;
  JetCorrectorParameters *l2l3;
  FactorizedJetCorrector* jec_L1rc;
  FactorizedJetCorrector* jec_L1fj;
  FactorizedJetCorrector* jec_L1fjL2RelL2Abs;
  FactorizedJetCorrector* jec_L1fjL2RelL2AbsL2L3Res;
  FactorizedJetCorrector* jersfvspt;

  TFile* fOUT;

  TH1D* h_psweight0;
  TH1D* h_psweight1;
  TH1D* h_psweight2;
  TH1D* h_psweight3;

  TH1D* pu_mc_reweighted;

  // Time stability of JEC
  TProfile* hprof_db_run_zpt15;
  TProfile* hprof_db_run_zpt30;
  TProfile* hprof_db_run_jeta34;
  TProfile* hprof_db_run_jeta45;
  TProfile* hprof_db_run_zpt50;
  TProfile* hprof_db_run_zpt110;
  TProfile* hprof_mpf_run_zpt15;
  TProfile* hprof_mpf_run_zpt30;
  TProfile* hprof_mpf_run_jeta34;
  TProfile* hprof_mpf_run_jeta45;
  TProfile* hprof_mpf_run_zpt50; 
  TProfile* hprof_mpf_run_zpt110;
  TProfile* hprof_chHEF_run_zpt15;
  TProfile* hprof_neHEF_run_zpt15;
  TProfile* hprof_chEmEF_run_zpt15;
  TProfile* hprof_neEmEF_run_zpt15;
  TProfile* hprof_muEF_run_zpt15;
  TProfile* hprof_chHEF_run_zpt30;
  TProfile* hprof_neHEF_run_zpt30;
  TProfile* hprof_chEmEF_run_zpt30;
  TProfile* hprof_neEmEF_run_zpt30;
  TProfile* hprof_muEF_run_zpt30;
  TProfile* hprof_chHEF_run_zpt50;
  TProfile* hprof_neHEF_run_zpt50;
  TProfile* hprof_chEmEF_run_zpt50;
  TProfile* hprof_neEmEF_run_zpt50;
  TProfile* hprof_muEF_run_zpt50;
  TProfile* hprof_chHEF_run_zpt110;
  TProfile* hprof_neHEF_run_zpt110;
  TProfile* hprof_chEmEF_run_zpt110;
  TProfile* hprof_neEmEF_run_zpt110;
  TProfile* hprof_muEF_run_zpt110;
  TProfile* hprof_mz_run_zpt15;
  TProfile* hprof_mz_run_zpt30;
  TProfile* hprof_mz_run_jeta34;
  TProfile* hprof_mz_run_jeta45;
  TProfile* hprof_mz_run_zpt50;
  TProfile* hprof_mz_run_zpt110;
  TProfile* hprof_mz_run_zpt0;

  TProfile* hprof_db_run_JetPt15;
  TProfile* hprof_db_run_JetPt30;
  TProfile* hprof_db_run_JetPt50;
  TProfile* hprof_db_run_JetPt110;
  TProfile* hprof_mpf_run_JetPt15;
  TProfile* hprof_mpf_run_JetPt30;
  TProfile* hprof_mpf_run_JetPt50;
  TProfile* hprof_mpf_run_JetPt110;
  TProfile* hprof_chHEF_run_JetPt15;
  TProfile* hprof_neHEF_run_JetPt15;
  TProfile* hprof_chEmEF_run_JetPt15;
  TProfile* hprof_neEmEF_run_JetPt15;
  TProfile* hprof_muEF_run_JetPt15;
  TProfile* hprof_chHEF_run_JetPt30;
  TProfile* hprof_neHEF_run_JetPt30;
  TProfile* hprof_chEmEF_run_JetPt30;
  TProfile* hprof_neEmEF_run_JetPt30;
  TProfile* hprof_muEF_run_JetPt30;
  TProfile* hprof_chHEF_run_JetPt50;
  TProfile* hprof_neHEF_run_JetPt50;
  TProfile* hprof_chEmEF_run_JetPt50;
  TProfile* hprof_neEmEF_run_JetPt50;
  TProfile* hprof_muEF_run_JetPt50;
  TProfile* hprof_chHEF_run_JetPt110;
  TProfile* hprof_neHEF_run_JetPt110;
  TProfile* hprof_chEmEF_run_JetPt110;
  TProfile* hprof_neEmEF_run_JetPt110;
  TProfile* hprof_muEF_run_JetPt110;
  TProfile* hprof_mz_run_JetPt15;
  TProfile* hprof_mz_run_JetPt30;
  TProfile* hprof_mz_run_JetPt50;
  TProfile* hprof_mz_run_JetPt110;

  TProfile* hprof_db_run_PtAve15;
  TProfile* hprof_db_run_PtAve30;
  TProfile* hprof_db_run_PtAve50;
  TProfile* hprof_db_run_PtAve110;
  TProfile* hprof_mpf_run_PtAve15;
  TProfile* hprof_mpf_run_PtAve30;
  TProfile* hprof_mpf_run_PtAve50;
  TProfile* hprof_mpf_run_PtAve110;
  TProfile* hprof_chHEF_run_PtAve15;
  TProfile* hprof_neHEF_run_PtAve15;
  TProfile* hprof_chEmEF_run_PtAve15;
  TProfile* hprof_neEmEF_run_PtAve15;
  TProfile* hprof_muEF_run_PtAve15;
  TProfile* hprof_chHEF_run_PtAve30;
  TProfile* hprof_neHEF_run_PtAve30;
  TProfile* hprof_chEmEF_run_PtAve30;
  TProfile* hprof_neEmEF_run_PtAve30;
  TProfile* hprof_muEF_run_PtAve30;
  TProfile* hprof_chHEF_run_PtAve50;
  TProfile* hprof_neHEF_run_PtAve50;
  TProfile* hprof_chEmEF_run_PtAve50;
  TProfile* hprof_neEmEF_run_PtAve50;
  TProfile* hprof_muEF_run_PtAve50;
  TProfile* hprof_chHEF_run_PtAve110;
  TProfile* hprof_neHEF_run_PtAve110;
  TProfile* hprof_chEmEF_run_PtAve110;
  TProfile* hprof_neEmEF_run_PtAve110;
  TProfile* hprof_muEF_run_PtAve110;
  TProfile* hprof_mz_run_PtAve15;
  TProfile* hprof_mz_run_PtAve30;
  TProfile* hprof_mz_run_PtAve50;
  TProfile* hprof_mz_run_PtAve110;

  TH1D* hprof_nz_run_zpt15;
  TH1D* hprof_nz_run_zpt30;
  TH1D* hprof_nz_run_jeta34;
  TH1D* hprof_nz_run_jeta45;
  TH1D* hprof_nz_run_zpt50;
  TH1D* hprof_nz_run_zpt110;
  TH1D* hprof_nz_run_zpt0;
  TH1D* hprof_nz_run_incl;

  TH1D* hprof_nz_run_JetPt15;
  TH1D* hprof_nz_run_JetPt30;
  TH1D* hprof_nz_run_JetPt50;
  TH1D* hprof_nz_run_JetPt110;
  
  TH1D* hprof_nz_run_PtAve15;
  TH1D* hprof_nz_run_PtAve30;
  TH1D* hprof_nz_run_PtAve50;
  TH1D* hprof_nz_run_PtAve110;

  TH1D* h_njets;
  TH1D* h_njets_sele;
  TH1D* h_njets_aas; // after all selections
  TH1D* h_njets_id;
  TH1D* h_njets_pt;

  TH1D* h_METorig;
  TH1D* h_MET;

  TH1D* pu_data;
  TH1D* h_pu_weight;

  TH1D* h_mu_noPUrw;
  TH1D* h_npvall_noPUrw;
  TH1D* h_npvgood_noPUrw;
  TH1D* h_rho_noPUrw;
  TH1D* h_rho_central_noPUrw;
  TH1D* h_rho_centralChargedPileUp_noPUrw;
  TH1D* h_mu_afterPUrw;
  TH1D* h_npvall_afterPUrw;
  TH1D* h_npvgood_afterPUrw;
  TH1D* h_rho_afterPUrw;
  TH1D* h_rho_central_afterPUrw;
  TH1D* h_rho_centralChargedPileUp_afterPUrw;

  TH1D* h_Mu;
  TProfile* hprof_RhoVsMu;
  TH2D* h_RhoVsMu;
  TH2D* h_NpvVsMu;
  TH1D* h_Mu0;
  TH2D* h_RhoVsMu0;
  TH2D* h_NpvVsMu0;

  TH1D* h_Jet_jetId;
  TH1D* h_Jet_electronIdx1;
  TH1D* h_Jet_electronIdx2;
  TH1D* h_Jet_muonIdx1;
  TH1D* h_Jet_muonIdx2;
  TH1D* h_Jet_nElectrons;
  TH1D* h_Jet_nMuons;
  TH1D* h_Jet_puId;

  TH1D* h_jet1pt;
  TH1D* h_jet1eta;
  TH1D* h_jet2pt;
  TH1D* h_jet2eta;

  TH1D* h_jetseta_pt20;
  TH1D* h_jetseta_pt30;
  TH1D* h_jetseta_pt40;

  TH1D* h_jet_smearOff;
  TH1D* h_jet_smearOn;
  TH1D* h_jet_smearFactor;

  TH1D* h_lep1pt;
  TH1D* h_lep1eta;
  TH1D* h_lep2pt;
  TH1D* h_lep2eta;

  TH1D* h_lep1pt_hltoveroffline;
  TH1D* h_lep2pt_hltoveroffline;

  TH1D* h_phibb;
  /*
  TProfile* h_JetPt_chHEF;
  TProfile* h_JetPt_neHEF;
  TProfile* h_JetPt_neEmEF;
  TProfile* h_JetPt_chEmEF;
  TProfile* h_JetPt_muEF;
  */
  TH1D* h_jet1cpt;
  TH1D* h_jet1ceta;
  TH1D* h_jet2cpt;
  TH1D* h_jet2ceta;

  TH2D*  h_JetPt_QGL_template;
  TH2D** h_JetPt_QGL;
  TH2D** h_JetPt_QGL_genb;
  TH2D** h_JetPt_QGL_genc;
  TH2D** h_JetPt_QGL_geng;
  TH2D** h_JetPt_QGL_genuds;
  TH2D** h_JetPt_QGL_unclassified;
  TH2D** h_JetPt_QGL_btag;
  TH2D** h_JetPt_QGL_btag_genb;
  TH2D** h_JetPt_QGL_btag_genc;
  TH2D** h_JetPt_QGL_btag_geng;
  TH2D** h_JetPt_QGL_btag_genuds;
  TH2D** h_JetPt_QGL_btag_unclassified;
  TH2D** h_JetPt_QGL_ctag;
  TH2D** h_JetPt_QGL_ctag_genb;
  TH2D** h_JetPt_QGL_ctag_genc;
  TH2D** h_JetPt_QGL_ctag_geng;
  TH2D** h_JetPt_QGL_ctag_genuds;
  TH2D** h_JetPt_QGL_ctag_unclassified;
  TH2D** h_JetPt_QGL_gluontag;
  TH2D** h_JetPt_QGL_gluontag_genb;
  TH2D** h_JetPt_QGL_gluontag_genc;
  TH2D** h_JetPt_QGL_gluontag_geng;
  TH2D** h_JetPt_QGL_gluontag_genuds;
  TH2D** h_JetPt_QGL_gluontag_unclassified;
  TH2D** h_JetPt_QGL_quarktag;
  TH2D** h_JetPt_QGL_quarktag_genb;
  TH2D** h_JetPt_QGL_quarktag_genc;
  TH2D** h_JetPt_QGL_quarktag_geng;
  TH2D** h_JetPt_QGL_quarktag_genuds;
  TH2D** h_JetPt_QGL_quarktag_unclassified;
  TH2D** h_JetPt_QGL_notag;
  TH2D** h_JetPt_QGL_notag_genb;
  TH2D** h_JetPt_QGL_notag_genc;
  TH2D** h_JetPt_QGL_notag_geng;
  TH2D** h_JetPt_QGL_notag_genuds;
  TH2D** h_JetPt_QGL_notag_unclassified;

  TH2D*  h_Zpt_leptonpt_template;
  TH2D*  h_Zpt_leptonm_pt;
  TH2D*  h_Zpt_leptonp_pt;

  TH2D*  h_Zpt_QGL_template;
  TH2D** h_Zpt_QGL;
  TH2D** h_Zpt_QGL_genb;
  TH2D** h_Zpt_QGL_genc;
  TH2D** h_Zpt_QGL_geng;
  TH2D** h_Zpt_QGL_genuds;
  TH2D** h_Zpt_QGL_unclassified;
  TH2D** h_Zpt_QGL_btag;
  TH2D** h_Zpt_QGL_btag_genb;
  TH2D** h_Zpt_QGL_btag_genc;
  TH2D** h_Zpt_QGL_btag_geng;
  TH2D** h_Zpt_QGL_btag_genuds;
  TH2D** h_Zpt_QGL_btag_unclassified;
  TH2D** h_Zpt_QGL_ctag;
  TH2D** h_Zpt_QGL_ctag_genb;
  TH2D** h_Zpt_QGL_ctag_genc;
  TH2D** h_Zpt_QGL_ctag_geng;
  TH2D** h_Zpt_QGL_ctag_genuds;
  TH2D** h_Zpt_QGL_ctag_unclassified;
  TH2D** h_Zpt_QGL_gluontag;
  TH2D** h_Zpt_QGL_gluontag_genb;
  TH2D** h_Zpt_QGL_gluontag_genc;
  TH2D** h_Zpt_QGL_gluontag_geng;
  TH2D** h_Zpt_QGL_gluontag_genuds;
  TH2D** h_Zpt_QGL_gluontag_unclassified;
  TH2D** h_Zpt_QGL_quarktag;
  TH2D** h_Zpt_QGL_quarktag_genb;
  TH2D** h_Zpt_QGL_quarktag_genc;
  TH2D** h_Zpt_QGL_quarktag_geng;
  TH2D** h_Zpt_QGL_quarktag_genuds;
  TH2D** h_Zpt_QGL_quarktag_unclassified;
  TH2D** h_Zpt_QGL_notag;
  TH2D** h_Zpt_QGL_notag_genb;
  TH2D** h_Zpt_QGL_notag_genc;
  TH2D** h_Zpt_QGL_notag_geng;
  TH2D** h_Zpt_QGL_notag_genuds;
  TH2D** h_Zpt_QGL_notag_unclassified;


  TH2D*  h_JetPt_muEF_template;
  TH2D** h_JetPt_muEF;
  TH2D** h_JetPt_muEF_btag;
  TH2D** h_JetPt_muEF_ctag;
  TH2D** h_JetPt_muEF_gluontag;
  TH2D** h_JetPt_muEF_quarktag;
  TH2D** h_JetPt_muEF_notag;

  TH2D*  h_JetPt_chEmEF_template;
  TH2D** h_JetPt_chEmEF;
  TH2D** h_JetPt_chEmEF_btag;
  TH2D** h_JetPt_chEmEF_ctag;
  TH2D** h_JetPt_chEmEF_gluontag;
  TH2D** h_JetPt_chEmEF_quarktag;
  TH2D** h_JetPt_chEmEF_notag;

  TH2D*  h_JetPt_chHEF_template;
  TH2D** h_JetPt_chHEF;
  TH2D** h_JetPt_chHEF_btag;
  TH2D** h_JetPt_chHEF_ctag;
  TH2D** h_JetPt_chHEF_gluontag;
  TH2D** h_JetPt_chHEF_quarktag;
  TH2D** h_JetPt_chHEF_notag;

  TH2D*  h_JetPt_neEmEF_template;
  TH2D** h_JetPt_neEmEF;
  TH2D** h_JetPt_neEmEF_btag;
  TH2D** h_JetPt_neEmEF_ctag;
  TH2D** h_JetPt_neEmEF_gluontag;
  TH2D** h_JetPt_neEmEF_quarktag;
  TH2D** h_JetPt_neEmEF_notag;

  TH2D*  h_JetPt_neHEF_template;
  TH2D** h_JetPt_neHEF;
  TH2D** h_JetPt_neHEF_btag;
  TH2D** h_JetPt_neHEF_ctag;
  TH2D** h_JetPt_neHEF_gluontag;
  TH2D** h_JetPt_neHEF_quarktag;
  TH2D** h_JetPt_neHEF_notag;

  TH2D*  h_PtAve_QGL_template;
  TH2D** h_PtAve_QGL;
  TH2D** h_PtAve_QGL_btag;
  TH2D** h_PtAve_QGL_ctag;
  TH2D** h_PtAve_QGL_gluontag;
  TH2D** h_PtAve_QGL_quarktag;
  TH2D** h_PtAve_QGL_notag;

  TH2D*  h_PtAve_muEF_template;
  TH2D** h_PtAve_muEF;
  TH2D** h_PtAve_muEF_btag;
  TH2D** h_PtAve_muEF_ctag;
  TH2D** h_PtAve_muEF_gluontag;
  TH2D** h_PtAve_muEF_quarktag;
  TH2D** h_PtAve_muEF_notag;

  TH2D*  h_PtAve_chEmEF_template;
  TH2D** h_PtAve_chEmEF;
  TH2D** h_PtAve_chEmEF_btag;
  TH2D** h_PtAve_chEmEF_ctag;
  TH2D** h_PtAve_chEmEF_gluontag;
  TH2D** h_PtAve_chEmEF_quarktag;
  TH2D** h_PtAve_chEmEF_notag;

  TH2D*  h_PtAve_chHEF_template;
  TH2D** h_PtAve_chHEF;
  TH2D** h_PtAve_chHEF_btag;
  TH2D** h_PtAve_chHEF_ctag;
  TH2D** h_PtAve_chHEF_gluontag;
  TH2D** h_PtAve_chHEF_quarktag;
  TH2D** h_PtAve_chHEF_notag;

  TH2D*  h_PtAve_neEmEF_template;
  TH2D** h_PtAve_neEmEF;
  TH2D** h_PtAve_neEmEF_btag;
  TH2D** h_PtAve_neEmEF_ctag;
  TH2D** h_PtAve_neEmEF_gluontag;
  TH2D** h_PtAve_neEmEF_quarktag;
  TH2D** h_PtAve_neEmEF_notag;

  TH2D*  h_PtAve_neHEF_template;
  TH2D** h_PtAve_neHEF;
  TH2D** h_PtAve_neHEF_btag;
  TH2D** h_PtAve_neHEF_ctag;
  TH2D** h_PtAve_neHEF_gluontag;
  TH2D** h_PtAve_neHEF_quarktag;
  TH2D** h_PtAve_neHEF_notag;

  TH2D* h_jet2etapt;

  TH1D* h_Zpt;
  TH1D* h_Zeta;
  TH1D* h_leptonpt;
  TH1D* h_leptoneta;
  TH1D* h_Zpt_a10;
  TH1D* h_Zpt_a15;
  TH1D* h_Zpt_a20;
  TH1D* h_Zpt_a30;
  TH1D* h_Zpt_a40;
  TH1D* h_Zpt_a50;
  TH1D* h_Zpt_a60;
  TH1D* h_Zpt_a80;
  TH1D* h_Zpt_a100;

  TH1D* h_Zmass;
  TH1D* h_Mmumu;

  TH1D* h_alpha;
  TH1D** h_alphaZpt;

  TH1D* h_RpT;
  TH1D* h_RMPF;

  TH2D* h_pTGenJet_JESRpTGen;
  TH2D* h_pTGenJet_JESRpTGen_genb;
  TH2D* h_pTGenJet_JESRpTGen_genc;
  TH2D* h_pTGenJet_JESRpTGen_geng;
  TH2D* h_pTGenJet_JESRpTGen_genuds;
  TH2D* h_pTGenJet_JESRpTGen_unclassified;

  TH2D* h_pTGenJet_JESRMPFGen;
  TH2D* h_pTGenJet_JESRMPFGen_genb;
  TH2D* h_pTGenJet_JESRMPFGen_genc;
  TH2D* h_pTGenJet_JESRMPFGen_geng;
  TH2D* h_pTGenJet_JESRMPFGen_genuds;
  TH2D* h_pTGenJet_JESRMPFGen_unclassified;
  /*
  TH2D* h_pTGenJet_JESRpTGen;
  TH2D* h_pTGenJet_JESRpTGen_genb;
  TH2D* h_pTGenJet_JESRpTGen_genc;
  TH2D* h_pTGenJet_JESRpTGen_geng;
  TH2D* h_pTGenJet_JESRpTGen_genuds;
  TH2D* h_pTGenJet_JESRpTGen_unclassified;

  TH2D* h_pTGenJet_JESRMPFGen;
  TH2D* h_pTGenJet_JESRMPFGen_genb;
  TH2D* h_pTGenJet_JESRMPFGen_genc;
  TH2D* h_pTGenJet_JESRMPFGen_geng;
  TH2D* h_pTGenJet_JESRMPFGen_genuds;
  TH2D* h_pTGenJet_JESRMPFGen_unclassified;
  */
  TH2D* h_pTGenZ_JESRpTGen;
  TH2D* h_pTGenZ_JESRpTGen_genb;
  TH2D* h_pTGenZ_JESRpTGen_genc;
  TH2D* h_pTGenZ_JESRpTGen_geng;
  TH2D* h_pTGenZ_JESRpTGen_genuds;
  TH2D* h_pTGenZ_JESRpTGen_unclassified;

  TH2D* h_pTGenZ_JESRMPFGen;
  TH2D* h_pTGenZ_JESRMPFGen_genb;
  TH2D* h_pTGenZ_JESRMPFGen_genc;
  TH2D* h_pTGenZ_JESRMPFGen_geng;
  TH2D* h_pTGenZ_JESRMPFGen_genuds;
  TH2D* h_pTGenZ_JESRMPFGen_unclassified;
  /*
  TH2D* h_alpha_RpT;
  TH2D* h_alpha_RMPF;
  TH2D* h_alpha_RpT_genb;
  TH2D* h_alpha_RMPF_genb;
  
  TH2D* h_alpha_RpT_btagCSVV2loose;
  TH2D* h_alpha_RMPF_btagCSVV2loose;
  TH2D* h_alpha_RpT_btagCSVV2medium;
  TH2D* h_alpha_RMPF_btagCSVV2medium;
  TH2D* h_alpha_RpT_btagCSVV2tight;
  TH2D* h_alpha_RMPF_btagCSVV2tight;
  TH2D* h_alpha_RpT_btagCSVV2loose_genb;
  TH2D* h_alpha_RMPF_btagCSVV2loose_genb;
  TH2D* h_alpha_RpT_btagCSVV2medium_genb;
  TH2D* h_alpha_RMPF_btagCSVV2medium_genb;
  TH2D* h_alpha_RpT_btagCSVV2tight_genb;
  TH2D* h_alpha_RMPF_btagCSVV2tight_genb;
  
  TH2D* h_alpha_RpT_btagCMVAloose;
  TH2D* h_alpha_RMPF_btagCMVAloose;
  TH2D* h_alpha_RpT_btagCMVAmedium;
  TH2D* h_alpha_RMPF_btagCMVAmedium;
  TH2D* h_alpha_RpT_btagCMVAtight;
  TH2D* h_alpha_RMPF_btagCMVAtight;
  TH2D* h_alpha_RpT_btagCMVAloose_genb;
  TH2D* h_alpha_RMPF_btagCMVAloose_genb;
  TH2D* h_alpha_RpT_btagCMVAmedium_genb;
  TH2D* h_alpha_RMPF_btagCMVAmedium_genb;
  TH2D* h_alpha_RpT_btagCMVAtight_genb;
  TH2D* h_alpha_RMPF_btagCMVAtight_genb;

  TH2D* h_alpha_RpT_btagDeepBloose;
  TH2D* h_alpha_RMPF_btagDeepBloose;
  TH2D* h_alpha_RpT_btagDeepBmedium;
  TH2D* h_alpha_RMPF_btagDeepBmedium;
  TH2D* h_alpha_RpT_btag;
  TH2D* h_alpha_RMPF_btag;
  TH2D* h_alpha_RpT_btagDeepBloose_genb;
  TH2D* h_alpha_RMPF_btagDeepBloose_genb;
  TH2D* h_alpha_RpT_btagDeepBmedium_genb;
  TH2D* h_alpha_RMPF_btagDeepBmedium_genb;
  TH2D* h_alpha_RpT_btag_genb;
  TH2D* h_alpha_RMPF_btag_genb;

  TH2D* h_alpha_RpT_btagDeepFlavBloose;
  TH2D* h_alpha_RMPF_btagDeepFlavBloose;
  TH2D* h_alpha_RpT_btagDeepFlavBmedium;
  TH2D* h_alpha_RMPF_btagDeepFlavBmedium;
  TH2D* h_alpha_RpT_btagDeepFlavBtight;
  TH2D* h_alpha_RMPF_btagDeepFlavBtight;
  TH2D* h_alpha_RpT_btagDeepFlavBloose_genb;
  TH2D* h_alpha_RMPF_btagDeepFlavBloose_genb;
  TH2D* h_alpha_RpT_btagDeepFlavBmedium_genb;
  TH2D* h_alpha_RMPF_btagDeepFlavBmedium_genb;
  TH2D* h_alpha_RpT_btagDeepFlavBtight_genb;
  TH2D* h_alpha_RMPF_btagDeepFlavBtight_genb;


  TH2D** h_alpha_RpT_Zpt;
  TH2D** h_alpha_RMPF_Zpt;
  TH2D** h_alpha_RpT_Zpt_genb;
  TH2D** h_alpha_RMPF_Zpt_genb;

  TH2D** h_alpha_RpT_btagCSVV2loose_Zpt;
  TH2D** h_alpha_RMPF_btagCSVV2loose_Zpt;
  TH2D** h_alpha_RpT_btagCSVV2medium_Zpt;
  TH2D** h_alpha_RMPF_btagCSVV2medium_Zpt;
  TH2D** h_alpha_RpT_btagCSVV2tight_Zpt;
  TH2D** h_alpha_RMPF_btagCSVV2tight_Zpt;
  TH2D** h_alpha_RpT_btagCSVV2loose_Zpt_genb;
  TH2D** h_alpha_RMPF_btagCSVV2loose_Zpt_genb;
  TH2D** h_alpha_RpT_btagCSVV2medium_Zpt_genb;
  TH2D** h_alpha_RMPF_btagCSVV2medium_Zpt_genb;
  TH2D** h_alpha_RpT_btagCSVV2tight_Zpt_genb;
  TH2D** h_alpha_RMPF_btagCSVV2tight_Zpt_genb;

  TH2D** h_alpha_RpT_btagDeepBloose_Zpt;
  TH2D** h_alpha_RMPF_btagDeepBloose_Zpt;
  TH2D** h_alpha_RpT_btagDeepBmedium_Zpt;
  TH2D** h_alpha_RMPF_btagDeepBmedium_Zpt;
  TH2D** h_alpha_RpT_btag_Zpt;
  TH2D** h_alpha_RMPF_btag_Zpt;
  TH2D** h_alpha_RpT_btagDeepBloose_Zpt_genb;
  TH2D** h_alpha_RMPF_btagDeepBloose_Zpt_genb;
  TH2D** h_alpha_RpT_btagDeepBmedium_Zpt_genb;
  TH2D** h_alpha_RMPF_btagDeepBmedium_Zpt_genb;
  TH2D** h_alpha_RpT_btag_Zpt_genb;
  TH2D** h_alpha_RMPF_btag_Zpt_genb;

  TH2D** h_alpha_RpT_btagDeepFlavBloose_Zpt;
  TH2D** h_alpha_RMPF_btagDeepFlavBloose_Zpt;
  TH2D** h_alpha_RpT_btagDeepFlavBmedium_Zpt;
  TH2D** h_alpha_RMPF_btagDeepFlavBmedium_Zpt;
  TH2D** h_alpha_RpT_btagDeepFlavBtight_Zpt;
  TH2D** h_alpha_RMPF_btagDeepFlavBtight_Zpt;
  TH2D** h_alpha_RpT_btagDeepFlavBloose_Zpt_genb;
  TH2D** h_alpha_RMPF_btagDeepFlavBloose_Zpt_genb;
  TH2D** h_alpha_RpT_btagDeepFlavBmedium_Zpt_genb;
  TH2D** h_alpha_RMPF_btagDeepFlavBmedium_Zpt_genb;
  TH2D** h_alpha_RpT_btagDeepFlavBtight_Zpt_genb;
  TH2D** h_alpha_RMPF_btagDeepFlavBtight_Zpt_genb;
  */

  TH2D*  h_Zpt_R_template;
  TH2D*  h_Zpt_mZ_template;
  TProfile2D*  h3D_Zpt_R_mu_template;
  TH3D*  h3D_Zpt_R_eta_template;

  TH2D** h_Zpt_mZ;
  TH2D** h_Zpt_mZgen;

  TH2D*  h_Zpt_Rho_template;
  TH2D** h_Zpt_Rho;
  TH2D* h_Zpt_Rho_1;
  TH2D* h_Zpt_Rho_2;
  TH2D** h_JetPt_Rho;
  TH2D** h_PtAve_Rho;

  TH2D*  h_Zpt_ef_template;
  TH2D** h_Zpt_chHEF;
  TH2D** h_Zpt_neHEF;
  TH2D** h_Zpt_chEmEF;
  TH2D** h_Zpt_neEmEF;
  TH2D** h_Zpt_muEF;

  TH2D** h_Zpt_RpT;
  TH2D** h_Zpt_RMPF;
  TH2D** h_Zpt_RMPFx;
  TH2D** h_Zpt_RMPFjet1;
  TH2D** h_Zpt_RMPFjet1x;
  TH2D** h_Zpt_RMPFjetn;
  TH2D** h_Zpt_RMPFuncl;
  TH2D** h_Zpt_RMPFfromType1;
  TH2D** h_Zpt_RMPFPF;

  TProfile2D** h3D_Zpt_RMPF_mu;
  TProfile2D** h3D_Zpt_RMPFx_mu;
  TProfile2D** h3D_Zpt_RMPFj2pt_mu;
  TProfile2D** h3D_Zpt_RMPFj2ptx_mu;

  TProfile2D* tprof2D_eta_Zpt_RMPF_template;
  TProfile2D** tprof2D_eta_Zpt_RMPF;
  TProfile2D** tprof2D_eta_Zpt_RMPFx;
  
  TH3D** h3D_Zpt_RMPF_eta;
  TH3D** h3D_Zpt_RMPFx_eta;
  TH3D** h3D_Zpt_RMPFj2pt_eta;   
  TH3D** h3D_Zpt_RMPFj2ptx_eta;

  TH2D** h_Zpt_RMPFj2pt;
  TH2D** h_Zpt_RMPFj2pt_genb;
  TH2D** h_Zpt_RMPFj2pt_genc;
  TH2D** h_Zpt_RMPFj2pt_geng;
  TH2D** h_Zpt_RMPFj2pt_genuds;
  TH2D** h_Zpt_RMPFj2pt_unclassified;

  TH2D** h_Zpt_RMPFjet1j2pt;
  TH2D** h_Zpt_RMPFjet1j2pt_genb;
  TH2D** h_Zpt_RMPFjet1j2pt_genc;
  TH2D** h_Zpt_RMPFjet1j2pt_geng;
  TH2D** h_Zpt_RMPFjet1j2pt_genuds;
  TH2D** h_Zpt_RMPFjet1j2pt_unclassified;

  TH2D** h_Zpt_RMPFj2ptx;
  TH2D** h_Zpt_RMPFjet1j2ptx;

  TProfile2D*  h_Zpt_Mu_Rho_template;
  TProfile2D** h_Zpt_Mu_Rho;

  TProfile2D*  h_Zpt_Mu_NPVgood_template;
  TProfile2D** h_Zpt_Mu_NPVgood;

  TProfile2D*  h_Zpt_Mu_NPVall_template;
  TProfile2D** h_Zpt_Mu_NPVall;

  TProfile2D*  h_Zpt_Mu_Mu_template;
  TProfile2D** h_Zpt_Mu_Mu;

  
  TProfile2D*  h_Pt_Mu_RpT_template;
  TProfile2D** h_ZpT_Mu_offset;
  TProfile2D** h_ZpT_Rho_offset;
  TProfile2D** h_ZpT_NPVgood_offset;
  TProfile2D** h_ZpT_NPVall_offset;
  TProfile2D** h_ZpT_Mu_RpT;
  TProfile2D** h_ZpT_Mu_RMPF;
  TProfile2D** h_ZpT_Mu_RMPFjet1;
  TProfile2D** h_ZpT_Mu_RMPFjetn;
  TProfile2D** h_ZpT_Mu_RMPFuncl;
  TProfile2D** h_JetPt_Mu_RpT;
  TProfile2D** h_JetPt_Mu_RMPF;
  TProfile2D** h_JetPt_Mu_RMPFjet1;
  TProfile2D** h_JetPt_Mu_RMPFjetn;
  TProfile2D** h_JetPt_Mu_RMPFuncl;
  TProfile2D** h_PtAve_Mu_RpT;
  TProfile2D** h_PtAve_Mu_RMPF;
  TProfile2D** h_PtAve_Mu_RMPFjet1;
  TProfile2D** h_PtAve_Mu_RMPFjetn;
  TProfile2D** h_PtAve_Mu_RMPFuncl;

  TProfile2D** h_ZpT_NPVg_RpT;
  TProfile2D** h_ZpT_NPVg_RMPF;
  TProfile2D** h_ZpT_NPVg_RMPFjet1;
  TProfile2D** h_ZpT_NPVg_RMPFjetn;
  TProfile2D** h_ZpT_NPVg_RMPFuncl;
  TProfile2D** h_JetPt_NPVg_RpT;
  TProfile2D** h_JetPt_NPVg_RMPF;
  TProfile2D** h_JetPt_NPVg_RMPFjet1;
  TProfile2D** h_JetPt_NPVg_RMPFjetn;
  TProfile2D** h_JetPt_NPVg_RMPFuncl;
  TProfile2D** h_PtAve_NPVg_RpT;
  TProfile2D** h_PtAve_NPVg_RMPF;
  TProfile2D** h_PtAve_NPVg_RMPFjet1;
  TProfile2D** h_PtAve_NPVg_RMPFjetn;
  TProfile2D** h_PtAve_NPVg_RMPFuncl;

  TH2D*  h_Mu_RpT_template;
  TH2D** h_Mu_RpT;

  TH2D*  h_Mu_ptdiff_template;
  TH2D** h_Mu_ptdiff;

  TH2D*  h_Mu_RMPF_template;
  TH2D*  h_JetPt_RMPF_template;
  TH2D*  h_PtAve_RMPF_template;
  TH2D** h_Mu_RMPF;
  TH2D** h_JetPt_RMPF;
  TH2D** h_PtAve_RMPF;

  TH2D*  h_Mu_RMPFjet1_template;
  TH2D*  h_JetPt_RMPFjet1_template;
  TH2D*  h_PtAve_RMPFjet1_template;
  TH2D** h_Mu_RMPFjet1;
  TH2D** h_JetPt_RMPFjet1;
  TH2D** h_PtAve_RMPFjet1;

  TH2D*  h_Mu_RMPFjetn_template;
  TH2D*  h_JetPt_RMPFjetn_template;
  TH2D*  h_PtAve_RMPFjetn_template;
  TH2D** h_Mu_RMPFjetn;
  TH2D** h_JetPt_RMPFjetn;
  TH2D** h_PtAve_RMPFjetn;

  TH2D*  h_Mu_RMPFuncl_template;
  TH2D*  h_JetPt_RMPFuncl_template;
  TH2D*  h_PtAve_RMPFuncl_template;
  TH2D** h_Mu_RMPFuncl;
  TH2D** h_JetPt_RMPFuncl;
  TH2D** h_PtAve_RMPFuncl;

  TH2D*  h_JetPt_RpT_template;
  TH2D*  h_PtAve_RpT_template;
  TH2D** h_JetPt_RpT;
  TH2D** h_PtAve_RpT;

  TH2D*  h_Zpt_jnpf_template;
  TH2D** h_Zpt_JNPF;

  //  TH2D** h_Zpt_RpT_btagCSVV2loose;
  //  TH2D** h_Zpt_RMPF_btagCSVV2loose;
  //  TH2D** h_Zpt_RpT_btagCSVV2medium;
  //  TH2D** h_Zpt_RMPF_btagCSVV2medium;
  //  TH2D** h_Zpt_RpT_btagCSVV2tight;
  //  TH2D** h_Zpt_RMPF_btagCSVV2tight;

  TH2D** h_Zpt_RpT_genb;
  TH2D** h_Zpt_RMPF_genb;
  TH2D** h_Zpt_RMPFjet1_genb;
  TH2D** h_Zpt_RMPFjetn_genb;
  TH2D** h_Zpt_RMPFuncl_genb;
  //  TH2D** h_Zpt_RpT_btagCSVV2loose_genb;
  //  TH2D** h_Zpt_RMPF_btagCSVV2loose_genb;
  //  TH2D** h_Zpt_RpT_btagCSVV2medium_genb;
  //  TH2D** h_Zpt_RMPF_btagCSVV2medium_genb;
  //  TH2D** h_Zpt_RpT_btagCSVV2tight_genb;
  //  TH2D** h_Zpt_RMPF_btagCSVV2tight_genb;

  TH2D** h_Zpt_RpT_genc;
  TH2D** h_Zpt_RMPF_genc;
  TH2D** h_Zpt_RMPFjet1_genc;
  TH2D** h_Zpt_RMPFjetn_genc;
  TH2D** h_Zpt_RMPFuncl_genc;
  //  TH2D** h_Zpt_RpT_btagCSVV2loose_genc;
  //  TH2D** h_Zpt_RMPF_btagCSVV2loose_genc;
  //  TH2D** h_Zpt_RpT_btagCSVV2medium_genc;
  //  TH2D** h_Zpt_RMPF_btagCSVV2medium_genc;
  //  TH2D** h_Zpt_RpT_btagCSVV2tight_genc;
  //  TH2D** h_Zpt_RMPF_btagCSVV2tight_genc;

  TH2D** h_Zpt_RpT_genuds;
  TH2D** h_Zpt_RMPF_genuds;
  TH2D** h_Zpt_RMPFjet1_genuds;
  TH2D** h_Zpt_RMPFjetn_genuds;
  TH2D** h_Zpt_RMPFuncl_genuds;

  TH2D** h_Zpt_RpT_gens;
  TH2D** h_Zpt_RMPF_gens;
  TH2D** h_Zpt_RMPFjet1_gens;
  TH2D** h_Zpt_RMPFjetn_gens;
  TH2D** h_Zpt_RMPFuncl_gens;

  TH2D** h_Zpt_RpT_genud;
  TH2D** h_Zpt_RMPF_genud;
  TH2D** h_Zpt_RMPFjet1_genud;
  TH2D** h_Zpt_RMPFjetn_genud;
  TH2D** h_Zpt_RMPFuncl_genud;

  //  TH2D** h_Zpt_RpT_btagCSVV2loose_genuds;
  //  TH2D** h_Zpt_RMPF_btagCSVV2loose_genuds;
  //  TH2D** h_Zpt_RpT_btagCSVV2medium_genuds;
  //  TH2D** h_Zpt_RMPF_btagCSVV2medium_genuds;
  //  TH2D** h_Zpt_RpT_btagCSVV2tight_genuds;
  //  TH2D** h_Zpt_RMPF_btagCSVV2tight_genuds;

  TH2D** h_Zpt_RpT_geng;
  TH2D** h_Zpt_RMPF_geng;
  TH2D** h_Zpt_RMPFjet1_geng;
  TH2D** h_Zpt_RMPFjetn_geng;
  TH2D** h_Zpt_RMPFuncl_geng;
  //  TH2D** h_Zpt_RpT_btagCSVV2loose_geng;
  //  TH2D** h_Zpt_RMPF_btagCSVV2loose_geng;
  //  TH2D** h_Zpt_RpT_btagCSVV2medium_geng;
  //  TH2D** h_Zpt_RMPF_btagCSVV2medium_geng;
  //  TH2D** h_Zpt_RpT_btagCSVV2tight_geng;
  //  TH2D** h_Zpt_RMPF_btagCSVV2tight_geng;
  TH2D** h_Zpt_RpT_unclassified;
  TH2D** h_Zpt_RMPF_unclassified;
  TH2D** h_Zpt_RMPFjet1_unclassified;
  TH2D** h_Zpt_RMPFjetn_unclassified;
  TH2D** h_Zpt_RMPFuncl_unclassified;

  //  TH2D** h_Zpt_RpT_btagDeepBloose;
  //  TH2D** h_Zpt_RMPF_btagDeepBloose;
  //  TH2D** h_Zpt_RpT_btagDeepBmedium;
  //  TH2D** h_Zpt_RMPF_btagDeepBmedium;
  TH2D** h_Zpt_RpT_btag;
  TH2D** h_Zpt_RMPF_btag;
  TH2D** h_Zpt_RMPFjet1_btag;
  TH2D** h_Zpt_RMPFjetn_btag;
  TH2D** h_Zpt_RMPFuncl_btag;
  TProfile2D** h_Zpt_Mu_Rho_btag;
  TProfile2D** h_Zpt_Mu_NPVgood_btag;
  TProfile2D** h_Zpt_Mu_NPVall_btag;
  TProfile2D** h_Zpt_Mu_Mu_btag;
  TH2D** h_Mu_RpT_btag;
  TH2D** h_JetPt_RpT_btag;
  TH2D** h_PtAve_RpT_btag;
  TH2D** h_Mu_RMPF_btag;
  TH2D** h_JetPt_RMPF_btag;
  TH2D** h_PtAve_RMPF_btag;
  TH2D** h_Mu_RMPFjet1_btag;
  TH2D** h_JetPt_RMPFjet1_btag;
  TH2D** h_PtAve_RMPFjet1_btag;
  TH2D** h_Mu_RMPFjetn_btag;
  TH2D** h_JetPt_RMPFjetn_btag;
  TH2D** h_PtAve_RMPFjetn_btag;
  TH2D** h_Mu_RMPFuncl_btag;
  TH2D** h_JetPt_RMPFuncl_btag;
  TH2D** h_PtAve_RMPFuncl_btag;

  //  TH2D** h_Zpt_RpT_btagDeepBloose_genb;
  //  TH2D** h_Zpt_RMPF_btagDeepBloose_genb;
  //  TH2D** h_Zpt_RpT_btagDeepBmedium_genb;
  //  TH2D** h_Zpt_RMPF_btagDeepBmedium_genb;
  TH2D** h_Zpt_RpT_btag_genb;
  TH2D** h_Zpt_RMPF_btag_genb;
  TH2D** h_Zpt_RMPFjet1_btag_genb;
  TH2D** h_Zpt_RMPFjetn_btag_genb;
  TH2D** h_Zpt_RMPFuncl_btag_genb;

  //  TH2D** h_Zpt_RpT_btagDeepBloose_genc;
  //  TH2D** h_Zpt_RMPF_btagDeepBloose_genc;
  //  TH2D** h_Zpt_RpT_btagDeepBmedium_genc;
  //  TH2D** h_Zpt_RMPF_btagDeepBmedium_genc;
  TH2D** h_Zpt_RpT_btag_genc;
  TH2D** h_Zpt_RMPF_btag_genc;
  TH2D** h_Zpt_RMPFjet1_btag_genc;
  TH2D** h_Zpt_RMPFjetn_btag_genc;
  TH2D** h_Zpt_RMPFuncl_btag_genc;


  //  TH2D** h_Zpt_RpT_btagDeepBloose_genuds;
  //  TH2D** h_Zpt_RMPF_btagDeepBloose_genuds;
  //  TH2D** h_Zpt_RpT_btagDeepBmedium_genuds;
  //  TH2D** h_Zpt_RMPF_btagDeepBmedium_genuds;
  TH2D** h_Zpt_RpT_btag_genuds;
  TH2D** h_Zpt_RMPF_btag_genuds;
  TH2D** h_Zpt_RMPFjet1_btag_genuds;
  TH2D** h_Zpt_RMPFjetn_btag_genuds;
  TH2D** h_Zpt_RMPFuncl_btag_genuds;
  
  TH2D** h_Zpt_RpT_btag_gens;
  TH2D** h_Zpt_RMPF_btag_gens;
  TH2D** h_Zpt_RMPFjet1_btag_gens;
  TH2D** h_Zpt_RMPFjetn_btag_gens;
  TH2D** h_Zpt_RMPFuncl_btag_gens;

  TH2D** h_Zpt_RpT_btag_genud;
  TH2D** h_Zpt_RMPF_btag_genud;
  TH2D** h_Zpt_RMPFjet1_btag_genud;
  TH2D** h_Zpt_RMPFjetn_btag_genud;
  TH2D** h_Zpt_RMPFuncl_btag_genud;

  //  TH2D** h_Zpt_RpT_btagDeepBloose_geng;
  //  TH2D** h_Zpt_RMPF_btagDeepBloose_geng;
  //  TH2D** h_Zpt_RpT_btagDeepBmedium_geng;
  //  TH2D** h_Zpt_RMPF_btagDeepBmedium_geng;
  TH2D** h_Zpt_RpT_btag_geng;
  TH2D** h_Zpt_RMPF_btag_geng;
  TH2D** h_Zpt_RMPFjet1_btag_geng;
  TH2D** h_Zpt_RMPFjetn_btag_geng;
  TH2D** h_Zpt_RMPFuncl_btag_geng;

  TH2D** h_Zpt_RpT_btag_unclassified;
  TH2D** h_Zpt_RMPF_btag_unclassified;
  TH2D** h_Zpt_RMPFjet1_btag_unclassified;
  TH2D** h_Zpt_RMPFjetn_btag_unclassified;
  TH2D** h_Zpt_RMPFuncl_btag_unclassified;


  //  TH2D** h_Zpt_RpT_btagDeepCloose;
  //  TH2D** h_Zpt_RMPF_btagDeepCloose;
  //  TH2D** h_Zpt_RpT_btagDeepCmedium;
  //  TH2D** h_Zpt_RMPF_btagDeepCmedium;
  TH2D** h_Zpt_RpT_ctag;
  TH2D** h_Zpt_RMPF_ctag;
  TH2D** h_Zpt_RMPFjet1_ctag;
  TH2D** h_Zpt_RMPFjetn_ctag;
  TH2D** h_Zpt_RMPFuncl_ctag;

  TProfile2D** h_Zpt_Mu_Rho_ctag;
  TProfile2D** h_Zpt_Mu_NPVgood_ctag;
  TProfile2D** h_Zpt_Mu_NPVall_ctag;
  TProfile2D** h_Zpt_Mu_Mu_ctag;
  TH2D** h_Mu_RpT_ctag;
  TH2D** h_JetPt_RpT_ctag;
  TH2D** h_PtAve_RpT_ctag;
  TH2D** h_Mu_RMPF_ctag;
  TH2D** h_JetPt_RMPF_ctag;
  TH2D** h_PtAve_RMPF_ctag;
  TH2D** h_Mu_RMPFjet1_ctag;
  TH2D** h_JetPt_RMPFjet1_ctag;
  TH2D** h_PtAve_RMPFjet1_ctag;
  TH2D** h_Mu_RMPFjetn_ctag;
  TH2D** h_JetPt_RMPFjetn_ctag;
  TH2D** h_PtAve_RMPFjetn_ctag;
  TH2D** h_Mu_RMPFuncl_ctag;
  TH2D** h_JetPt_RMPFuncl_ctag;
  TH2D** h_PtAve_RMPFuncl_ctag;

  //  TH2D** h_Zpt_RpT_btagDeepCloose_genb;
  //  TH2D** h_Zpt_RMPF_btagDeepCloose_genb;
  //  TH2D** h_Zpt_RpT_btagDeepCmedium_genb;
  //  TH2D** h_Zpt_RMPF_btagDeepCmedium_genb;
  TH2D** h_Zpt_RpT_ctag_genb;
  TH2D** h_Zpt_RMPF_ctag_genb;
  TH2D** h_Zpt_RMPFjet1_ctag_genb;
  TH2D** h_Zpt_RMPFjetn_ctag_genb;
  TH2D** h_Zpt_RMPFuncl_ctag_genb;

  //  TH2D** h_Zpt_RpT_btagDeepCloose_genc;
  //  TH2D** h_Zpt_RMPF_btagDeepCloose_genc;
  //  TH2D** h_Zpt_RpT_btagDeepCmedium_genc;
  //  TH2D** h_Zpt_RMPF_btagDeepCmedium_genc;
  TH2D** h_Zpt_RpT_ctag_genc;
  TH2D** h_Zpt_RMPF_ctag_genc;
  TH2D** h_Zpt_RMPFjet1_ctag_genc;
  TH2D** h_Zpt_RMPFjetn_ctag_genc;
  TH2D** h_Zpt_RMPFuncl_ctag_genc;

  //  TH2D** h_Zpt_RpT_btagDeepCloose_genuds;
  //  TH2D** h_Zpt_RMPF_btagDeepCloose_genuds;
  //  TH2D** h_Zpt_RpT_btagDeepCmedium_genuds;
  //  TH2D** h_Zpt_RMPF_btagDeepCmedium_genuds;
  TH2D** h_Zpt_RpT_ctag_genuds;
  TH2D** h_Zpt_RMPF_ctag_genuds;
  TH2D** h_Zpt_RMPFjet1_ctag_genuds;
  TH2D** h_Zpt_RMPFjetn_ctag_genuds;
  TH2D** h_Zpt_RMPFuncl_ctag_genuds;

  TH2D** h_Zpt_RpT_ctag_gens;
  TH2D** h_Zpt_RMPF_ctag_gens;
  TH2D** h_Zpt_RMPFjet1_ctag_gens;
  TH2D** h_Zpt_RMPFjetn_ctag_gens;
  TH2D** h_Zpt_RMPFuncl_ctag_gens;

  TH2D** h_Zpt_RpT_ctag_genud;
  TH2D** h_Zpt_RMPF_ctag_genud;
  TH2D** h_Zpt_RMPFjet1_ctag_genud;
  TH2D** h_Zpt_RMPFjetn_ctag_genud;
  TH2D** h_Zpt_RMPFuncl_ctag_genud;

  //  TH2D** h_Zpt_RpT_btagDeepCloose_geng;
  //  TH2D** h_Zpt_RMPF_btagDeepCloose_geng;
  //  TH2D** h_Zpt_RpT_btagDeepCmedium_geng;
  //  TH2D** h_Zpt_RMPF_btagDeepCmedium_geng;
  TH2D** h_Zpt_RpT_ctag_geng;
  TH2D** h_Zpt_RMPF_ctag_geng;
  TH2D** h_Zpt_RMPFjet1_ctag_geng;
  TH2D** h_Zpt_RMPFjetn_ctag_geng;
  TH2D** h_Zpt_RMPFuncl_ctag_geng;

  TH2D** h_Zpt_RpT_ctag_unclassified;
  TH2D** h_Zpt_RMPF_ctag_unclassified;
  TH2D** h_Zpt_RMPFjet1_ctag_unclassified;
  TH2D** h_Zpt_RMPFjetn_ctag_unclassified;
  TH2D** h_Zpt_RMPFuncl_ctag_unclassified;

  //  TH2D** h_Zpt_RpT_btagDeepFlavBloose;
  //  TH2D** h_Zpt_RMPF_btagDeepFlavBloose;
  //  TH2D** h_Zpt_RpT_btagDeepFlavBmedium;
  //  TH2D** h_Zpt_RMPF_btagDeepFlavBmedium;
  TH2D** h_Zpt_RpT_btagDeepFlavBtight;
  TH2D** h_Zpt_RMPF_btagDeepFlavBtight;

  //  TH2D** h_Zpt_RpT_btagDeepFlavBloose_genb;
  //  TH2D** h_Zpt_RMPF_btagDeepFlavBloose_genb;
  //  TH2D** h_Zpt_RpT_btagDeepFlavBmedium_genb;
  //  TH2D** h_Zpt_RMPF_btagDeepFlavBmedium_genb;
  TH2D** h_Zpt_RpT_btagDeepFlavBtight_genb;
  TH2D** h_Zpt_RMPF_btagDeepFlavBtight_genb;

  //  TH2D** h_Zpt_RpT_btagDeepFlavBloose_genc;
  //  TH2D** h_Zpt_RMPF_btagDeepFlavBloose_genc;
  //  TH2D** h_Zpt_RpT_btagDeepFlavBmedium_genc;
  //  TH2D** h_Zpt_RMPF_btagDeepFlavBmedium_genc;
  TH2D** h_Zpt_RpT_btagDeepFlavBtight_genc;
  TH2D** h_Zpt_RMPF_btagDeepFlavBtight_genc;

  //  TH2D** h_Zpt_RpT_btagDeepFlavBloose_genuds;
  //  TH2D** h_Zpt_RMPF_btagDeepFlavBloose_genuds;
  //  TH2D** h_Zpt_RpT_btagDeepFlavBmedium_genuds;
  //  TH2D** h_Zpt_RMPF_btagDeepFlavBmedium_genuds;
  TH2D** h_Zpt_RpT_btagDeepFlavBtight_genuds;
  TH2D** h_Zpt_RMPF_btagDeepFlavBtight_genuds;

  //  TH2D** h_Zpt_RpT_btagDeepFlavBloose_geng;
  //  TH2D** h_Zpt_RMPF_btagDeepFlavBloose_geng;
  //  TH2D** h_Zpt_RpT_btagDeepFlavBmedium_geng;
  //  TH2D** h_Zpt_RMPF_btagDeepFlavBmedium_geng;
  TH2D** h_Zpt_RpT_btagDeepFlavBtight_geng;
  TH2D** h_Zpt_RMPF_btagDeepFlavBtight_geng;

  TH2D** h_Zpt_RpT_btagDeepFlavBtight_unclassified;
  TH2D** h_Zpt_RMPF_btagDeepFlavBtight_unclassified;

  TH2D** h_Zpt_RpT_gluontag;
  TH2D** h_Zpt_RMPF_gluontag;
  TH2D** h_Zpt_RMPFjet1_gluontag;
  TH2D** h_Zpt_RMPFjetn_gluontag;   
  TH2D** h_Zpt_RMPFuncl_gluontag;

  TProfile2D** h_Zpt_Mu_Rho_gluontag;
  TProfile2D** h_Zpt_Mu_NPVgood_gluontag;
  TProfile2D** h_Zpt_Mu_NPVall_gluontag;
  TProfile2D** h_Zpt_Mu_Mu_gluontag;
  TH2D** h_Mu_RpT_gluontag;
  TH2D** h_JetPt_RpT_gluontag;
  TH2D** h_PtAve_RpT_gluontag;
  TH2D** h_Mu_RMPF_gluontag;
  TH2D** h_JetPt_RMPF_gluontag;
  TH2D** h_PtAve_RMPF_gluontag; 
  TH2D** h_Mu_RMPFjet1_gluontag;
  TH2D** h_JetPt_RMPFjet1_gluontag;
  TH2D** h_PtAve_RMPFjet1_gluontag;
  TH2D** h_Mu_RMPFjetn_gluontag;
  TH2D** h_JetPt_RMPFjetn_gluontag;
  TH2D** h_PtAve_RMPFjetn_gluontag;
  TH2D** h_Mu_RMPFuncl_gluontag;
  TH2D** h_JetPt_RMPFuncl_gluontag;
  TH2D** h_PtAve_RMPFuncl_gluontag;

  TH2D** h_Zpt_RpT_gluontag_genb;
  TH2D** h_Zpt_RMPF_gluontag_genb;
  TH2D** h_Zpt_RMPFjet1_gluontag_genb;
  TH2D** h_Zpt_RMPFjetn_gluontag_genb;
  TH2D** h_Zpt_RMPFuncl_gluontag_genb;

  TH2D** h_Zpt_RpT_gluontag_genc;
  TH2D** h_Zpt_RMPF_gluontag_genc;
  TH2D** h_Zpt_RMPFjet1_gluontag_genc;
  TH2D** h_Zpt_RMPFjetn_gluontag_genc;
  TH2D** h_Zpt_RMPFuncl_gluontag_genc;

  TH2D** h_Zpt_RpT_gluontag_genuds;
  TH2D** h_Zpt_RMPF_gluontag_genuds;
  TH2D** h_Zpt_RMPFjet1_gluontag_genuds;
  TH2D** h_Zpt_RMPFjetn_gluontag_genuds;
  TH2D** h_Zpt_RMPFuncl_gluontag_genuds;

  TH2D** h_Zpt_RpT_gluontag_gens;
  TH2D** h_Zpt_RMPF_gluontag_gens;
  TH2D** h_Zpt_RMPFjet1_gluontag_gens;
  TH2D** h_Zpt_RMPFjetn_gluontag_gens;
  TH2D** h_Zpt_RMPFuncl_gluontag_gens;

  TH2D** h_Zpt_RpT_gluontag_genud;
  TH2D** h_Zpt_RMPF_gluontag_genud;
  TH2D** h_Zpt_RMPFjet1_gluontag_genud;
  TH2D** h_Zpt_RMPFjetn_gluontag_genud;
  TH2D** h_Zpt_RMPFuncl_gluontag_genud;

  TH2D** h_Zpt_RpT_gluontag_geng;
  TH2D** h_Zpt_RMPF_gluontag_geng;
  TH2D** h_Zpt_RMPFjet1_gluontag_geng;
  TH2D** h_Zpt_RMPFjetn_gluontag_geng;
  TH2D** h_Zpt_RMPFuncl_gluontag_geng;

  TH2D** h_Zpt_RpT_gluontag_unclassified;
  TH2D** h_Zpt_RMPF_gluontag_unclassified;
  TH2D** h_Zpt_RMPFjet1_gluontag_unclassified;
  TH2D** h_Zpt_RMPFjetn_gluontag_unclassified;
  TH2D** h_Zpt_RMPFuncl_gluontag_unclassified;
  

  TH2D** h_Zpt_RpT_quarktag;
  TH2D** h_Zpt_RMPF_quarktag;
  TH2D** h_Zpt_RMPFjet1_quarktag;
  TH2D** h_Zpt_RMPFjetn_quarktag;
  TH2D** h_Zpt_RMPFuncl_quarktag;

  TProfile2D** h_Zpt_Mu_Rho_quarktag;
  TProfile2D** h_Zpt_Mu_NPVgood_quarktag;
  TProfile2D** h_Zpt_Mu_NPVall_quarktag;
  TProfile2D** h_Zpt_Mu_Mu_quarktag;
  TH2D** h_Mu_RpT_quarktag;
  TH2D** h_JetPt_RpT_quarktag;
  TH2D** h_PtAve_RpT_quarktag;
  TH2D** h_Mu_RMPF_quarktag;
  TH2D** h_JetPt_RMPF_quarktag;
  TH2D** h_PtAve_RMPF_quarktag;
  TH2D** h_Mu_RMPFjet1_quarktag;
  TH2D** h_JetPt_RMPFjet1_quarktag;
  TH2D** h_PtAve_RMPFjet1_quarktag;
  TH2D** h_Mu_RMPFjetn_quarktag;
  TH2D** h_JetPt_RMPFjetn_quarktag;
  TH2D** h_PtAve_RMPFjetn_quarktag;
  TH2D** h_Mu_RMPFuncl_quarktag;
  TH2D** h_JetPt_RMPFuncl_quarktag;
  TH2D** h_PtAve_RMPFuncl_quarktag;

  TH2D** h_Zpt_RpT_quarktag_genb;
  TH2D** h_Zpt_RMPF_quarktag_genb;
  TH2D** h_Zpt_RMPFjet1_quarktag_genb;
  TH2D** h_Zpt_RMPFjetn_quarktag_genb;
  TH2D** h_Zpt_RMPFuncl_quarktag_genb;

  TH2D** h_Zpt_RpT_quarktag_genc;
  TH2D** h_Zpt_RMPF_quarktag_genc;
  TH2D** h_Zpt_RMPFjet1_quarktag_genc;
  TH2D** h_Zpt_RMPFjetn_quarktag_genc;
  TH2D** h_Zpt_RMPFuncl_quarktag_genc;

  TH2D** h_Zpt_RpT_quarktag_genuds;
  TH2D** h_Zpt_RMPF_quarktag_genuds;
  TH2D** h_Zpt_RMPFjet1_quarktag_genuds;
  TH2D** h_Zpt_RMPFjetn_quarktag_genuds;
  TH2D** h_Zpt_RMPFuncl_quarktag_genuds;

  TH2D** h_Zpt_RpT_quarktag_geng;
  TH2D** h_Zpt_RMPF_quarktag_geng;
  TH2D** h_Zpt_RMPFjet1_quarktag_geng;
  TH2D** h_Zpt_RMPFjetn_quarktag_geng;
  TH2D** h_Zpt_RMPFuncl_quarktag_geng;

  TH2D** h_Zpt_RpT_quarktag_gens;
  TH2D** h_Zpt_RMPF_quarktag_gens;
  TH2D** h_Zpt_RMPFjet1_quarktag_gens;
  TH2D** h_Zpt_RMPFjetn_quarktag_gens;
  TH2D** h_Zpt_RMPFuncl_quarktag_gens;

  TH2D** h_Zpt_RpT_quarktag_genud;
  TH2D** h_Zpt_RMPF_quarktag_genud;
  TH2D** h_Zpt_RMPFjet1_quarktag_genud;
  TH2D** h_Zpt_RMPFjetn_quarktag_genud;
  TH2D** h_Zpt_RMPFuncl_quarktag_genud;

  TH2D** h_Zpt_RpT_quarktag_unclassified;
  TH2D** h_Zpt_RMPF_quarktag_unclassified;
  TH2D** h_Zpt_RMPFjet1_quarktag_unclassified;
  TH2D** h_Zpt_RMPFjetn_quarktag_unclassified;
  TH2D** h_Zpt_RMPFuncl_quarktag_unclassified;


  TH2D** h_Zpt_RpT_notag;
  TH2D** h_Zpt_RMPF_notag;
  TH2D** h_Zpt_RMPFjet1_notag;
  TH2D** h_Zpt_RMPFjetn_notag;
  TH2D** h_Zpt_RMPFuncl_notag;

  TProfile2D** h_Zpt_Mu_Rho_notag;
  TProfile2D** h_Zpt_Mu_NPVgood_notag;
  TProfile2D** h_Zpt_Mu_NPVall_notag;
  TProfile2D** h_Zpt_Mu_Mu_notag;
  TH2D** h_Mu_RpT_notag;
  TH2D** h_JetPt_RpT_notag;
  TH2D** h_PtAve_RpT_notag;
  TH2D** h_Mu_RMPF_notag;
  TH2D** h_JetPt_RMPF_notag;
  TH2D** h_PtAve_RMPF_notag;
  TH2D** h_Mu_RMPFjet1_notag;
  TH2D** h_JetPt_RMPFjet1_notag;
  TH2D** h_PtAve_RMPFjet1_notag;
  TH2D** h_Mu_RMPFjetn_notag;
  TH2D** h_JetPt_RMPFjetn_notag;
  TH2D** h_PtAve_RMPFjetn_notag;
  TH2D** h_Mu_RMPFuncl_notag;
  TH2D** h_JetPt_RMPFuncl_notag;
  TH2D** h_PtAve_RMPFuncl_notag;

  TH2D** h_Zpt_RpT_notag_genb;
  TH2D** h_Zpt_RMPF_notag_genb;
  TH2D** h_Zpt_RMPFjet1_notag_genb;
  TH2D** h_Zpt_RMPFjetn_notag_genb;
  TH2D** h_Zpt_RMPFuncl_notag_genb;

  TH2D** h_Zpt_RpT_notag_genc;
  TH2D** h_Zpt_RMPF_notag_genc;
  TH2D** h_Zpt_RMPFjet1_notag_genc;
  TH2D** h_Zpt_RMPFjetn_notag_genc;
  TH2D** h_Zpt_RMPFuncl_notag_genc;

  TH2D** h_Zpt_RpT_notag_genuds;
  TH2D** h_Zpt_RMPF_notag_genuds;
  TH2D** h_Zpt_RMPFjet1_notag_genuds;
  TH2D** h_Zpt_RMPFjetn_notag_genuds;
  TH2D** h_Zpt_RMPFuncl_notag_genuds;

  TH2D** h_Zpt_RpT_notag_gens;
  TH2D** h_Zpt_RMPF_notag_gens;
  TH2D** h_Zpt_RMPFjet1_notag_gens;
  TH2D** h_Zpt_RMPFjetn_notag_gens;
  TH2D** h_Zpt_RMPFuncl_notag_gens;

  TH2D** h_Zpt_RpT_notag_genud;
  TH2D** h_Zpt_RMPF_notag_genud;
  TH2D** h_Zpt_RMPFjet1_notag_genud;
  TH2D** h_Zpt_RMPFjetn_notag_genud;
  TH2D** h_Zpt_RMPFuncl_notag_genud;

  TH2D** h_Zpt_RpT_notag_geng;
  TH2D** h_Zpt_RMPF_notag_geng;
  TH2D** h_Zpt_RMPFjet1_notag_geng;
  TH2D** h_Zpt_RMPFjetn_notag_geng;
  TH2D** h_Zpt_RMPFuncl_notag_geng;

  TH2D** h_Zpt_RpT_notag_unclassified;
  TH2D** h_Zpt_RMPF_notag_unclassified;
  TH2D** h_Zpt_RMPFjet1_notag_unclassified;
  TH2D** h_Zpt_RMPFjetn_notag_unclassified;
  TH2D** h_Zpt_RMPFuncl_notag_unclassified;


  TH2D** h_Zpt_RZ;
  
  TH2D** h_Zpt_RBal;
  TH2D** h_Zpt_Resp;
  TH2D** h_Zpt_RGMPF;
  TH2D** h_Zpt_RGenjet1;
  TH2D** h_Zpt_RGenjetn;
  TH2D** h_Zpt_RGenuncl;

  TH2D** h_JetPt_RBal;
  TH2D** h_JetPt_RGenjet1;
  TH2D** h_JetPt_RGenjetn;
  TH2D** h_JetPt_RGenuncl;

  TH2D** h_PtAve_RBal;
  TH2D** h_PtAve_RGenjet1;
  TH2D** h_PtAve_RGenjetn;
  TH2D** h_PtAve_RGenuncl;

  TH2D** h_Zpt_RBal_btag;
  TH2D** h_Zpt_Resp_btag;
  TH2D** h_Zpt_RGMPF_btag;
  TH2D** h_Zpt_RGenjet1_btag;
  TH2D** h_Zpt_RGenjetn_btag;
  TH2D** h_Zpt_RGenuncl_btag;

  TH2D** h_Zpt_RBal_ctag;
  TH2D** h_Zpt_Resp_ctag;
  TH2D** h_Zpt_RGMPF_ctag;
  TH2D** h_Zpt_RGenjet1_ctag;
  TH2D** h_Zpt_RGenjetn_ctag;
  TH2D** h_Zpt_RGenuncl_ctag;

  TH2D** h_Zpt_RBal_gluontag;
  TH2D** h_Zpt_Resp_gluontag;
  TH2D** h_Zpt_RGMPF_gluontag;
  TH2D** h_Zpt_RGenjet1_gluontag;
  TH2D** h_Zpt_RGenjetn_gluontag;
  TH2D** h_Zpt_RGenuncl_gluontag;

  TH2D** h_Zpt_RBal_quarktag;
  TH2D** h_Zpt_Resp_quarktag;
  TH2D** h_Zpt_RGMPF_quarktag;
  TH2D** h_Zpt_RGenjet1_quarktag;
  TH2D** h_Zpt_RGenjetn_quarktag;
  TH2D** h_Zpt_RGenuncl_quarktag;

  TH2D** h_Zpt_RBal_notag;
  TH2D** h_Zpt_Resp_notag;
  TH2D** h_Zpt_RGMPF_notag;
  TH2D** h_Zpt_RGenjet1_notag;
  TH2D** h_Zpt_RGenjetn_notag;
  TH2D** h_Zpt_RGenuncl_notag;


  TH2D** h_Zpt_RBal_genb;
  TH2D** h_Zpt_Resp_genb;
  TH2D** h_Zpt_RGMPF_genb;
  TH2D** h_Zpt_RGenjet1_genb;
  TH2D** h_Zpt_RGenjetn_genb;
  TH2D** h_Zpt_RGenuncl_genb;

  TH2D** h_Zpt_RBal_btag_genb;
  TH2D** h_Zpt_Resp_btag_genb;
  TH2D** h_Zpt_RGMPF_btag_genb;
  TH2D** h_Zpt_RGenjet1_btag_genb;
  TH2D** h_Zpt_RGenjetn_btag_genb;
  TH2D** h_Zpt_RGenuncl_btag_genb;

  TH2D** h_Zpt_RBal_ctag_genb;
  TH2D** h_Zpt_Resp_ctag_genb;
  TH2D** h_Zpt_RGMPF_ctag_genb;
  TH2D** h_Zpt_RGenjet1_ctag_genb;
  TH2D** h_Zpt_RGenjetn_ctag_genb;
  TH2D** h_Zpt_RGenuncl_ctag_genb;

  TH2D** h_Zpt_RBal_gluontag_genb;
  TH2D** h_Zpt_Resp_gluontag_genb;
  TH2D** h_Zpt_RGMPF_gluontag_genb;
  TH2D** h_Zpt_RGenjet1_gluontag_genb;
  TH2D** h_Zpt_RGenjetn_gluontag_genb;
  TH2D** h_Zpt_RGenuncl_gluontag_genb;

  TH2D** h_Zpt_RBal_quarktag_genb;
  TH2D** h_Zpt_Resp_quarktag_genb;
  TH2D** h_Zpt_RGMPF_quarktag_genb;
  TH2D** h_Zpt_RGenjet1_quarktag_genb;
  TH2D** h_Zpt_RGenjetn_quarktag_genb;
  TH2D** h_Zpt_RGenuncl_quarktag_genb;

  TH2D** h_Zpt_RBal_notag_genb;
  TH2D** h_Zpt_Resp_notag_genb;
  TH2D** h_Zpt_RGMPF_notag_genb;
  TH2D** h_Zpt_RGenjet1_notag_genb;
  TH2D** h_Zpt_RGenjetn_notag_genb;
  TH2D** h_Zpt_RGenuncl_notag_genb;


  TH2D** h_Zpt_RBal_genc;
  TH2D** h_Zpt_Resp_genc;
  TH2D** h_Zpt_RGMPF_genc;
  TH2D** h_Zpt_RGenjet1_genc;
  TH2D** h_Zpt_RGenjetn_genc;
  TH2D** h_Zpt_RGenuncl_genc;

  TH2D** h_Zpt_RBal_btag_genc;
  TH2D** h_Zpt_Resp_btag_genc;
  TH2D** h_Zpt_RGMPF_btag_genc;
  TH2D** h_Zpt_RGenjet1_btag_genc;
  TH2D** h_Zpt_RGenjetn_btag_genc;
  TH2D** h_Zpt_RGenuncl_btag_genc;

  TH2D** h_Zpt_RBal_ctag_genc;
  TH2D** h_Zpt_Resp_ctag_genc;
  TH2D** h_Zpt_RGMPF_ctag_genc;
  TH2D** h_Zpt_RGenjet1_ctag_genc;
  TH2D** h_Zpt_RGenjetn_ctag_genc;
  TH2D** h_Zpt_RGenuncl_ctag_genc;

  TH2D** h_Zpt_RBal_gluontag_genc;
  TH2D** h_Zpt_Resp_gluontag_genc;
  TH2D** h_Zpt_RGMPF_gluontag_genc;
  TH2D** h_Zpt_RGenjet1_gluontag_genc;
  TH2D** h_Zpt_RGenjetn_gluontag_genc;
  TH2D** h_Zpt_RGenuncl_gluontag_genc;

  TH2D** h_Zpt_RBal_quarktag_genc;
  TH2D** h_Zpt_Resp_quarktag_genc;
  TH2D** h_Zpt_RGMPF_quarktag_genc;
  TH2D** h_Zpt_RGenjet1_quarktag_genc;
  TH2D** h_Zpt_RGenjetn_quarktag_genc;
  TH2D** h_Zpt_RGenuncl_quarktag_genc;

  TH2D** h_Zpt_RBal_notag_genc;
  TH2D** h_Zpt_Resp_notag_genc;
  TH2D** h_Zpt_RGMPF_notag_genc;
  TH2D** h_Zpt_RGenjet1_notag_genc;
  TH2D** h_Zpt_RGenjetn_notag_genc;
  TH2D** h_Zpt_RGenuncl_notag_genc;


  TH2D** h_Zpt_RBal_genuds;
  TH2D** h_Zpt_Resp_genuds;
  TH2D** h_Zpt_RGMPF_genuds;
  TH2D** h_Zpt_RGenjet1_genuds;
  TH2D** h_Zpt_RGenjetn_genuds;
  TH2D** h_Zpt_RGenuncl_genuds;

  TH2D** h_Zpt_RBal_btag_genuds;
  TH2D** h_Zpt_Resp_btag_genuds;
  TH2D** h_Zpt_RGMPF_btag_genuds;
  TH2D** h_Zpt_RGenjet1_btag_genuds;
  TH2D** h_Zpt_RGenjetn_btag_genuds;
  TH2D** h_Zpt_RGenuncl_btag_genuds;

  TH2D** h_Zpt_RBal_ctag_genuds;
  TH2D** h_Zpt_Resp_ctag_genuds;
  TH2D** h_Zpt_RGMPF_ctag_genuds;
  TH2D** h_Zpt_RGenjet1_ctag_genuds;
  TH2D** h_Zpt_RGenjetn_ctag_genuds;
  TH2D** h_Zpt_RGenuncl_ctag_genuds;

  TH2D** h_Zpt_RBal_gluontag_genuds;
  TH2D** h_Zpt_Resp_gluontag_genuds;
  TH2D** h_Zpt_RGMPF_gluontag_genuds;
  TH2D** h_Zpt_RGenjet1_gluontag_genuds;
  TH2D** h_Zpt_RGenjetn_gluontag_genuds;
  TH2D** h_Zpt_RGenuncl_gluontag_genuds;

  TH2D** h_Zpt_RBal_quarktag_genuds;
  TH2D** h_Zpt_Resp_quarktag_genuds;
  TH2D** h_Zpt_RGMPF_quarktag_genuds;
  TH2D** h_Zpt_RGenjet1_quarktag_genuds;
  TH2D** h_Zpt_RGenjetn_quarktag_genuds;
  TH2D** h_Zpt_RGenuncl_quarktag_genuds;

  TH2D** h_Zpt_RBal_notag_genuds;
  TH2D** h_Zpt_Resp_notag_genuds;
  TH2D** h_Zpt_RGMPF_notag_genuds;
  TH2D** h_Zpt_RGenjet1_notag_genuds;
  TH2D** h_Zpt_RGenjetn_notag_genuds;
  TH2D** h_Zpt_RGenuncl_notag_genuds;


  TH2D** h_Zpt_RBal_geng;
  TH2D** h_Zpt_Resp_geng;
  TH2D** h_Zpt_RGMPF_geng;
  TH2D** h_Zpt_RGenjet1_geng;
  TH2D** h_Zpt_RGenjetn_geng;
  TH2D** h_Zpt_RGenuncl_geng;

  TH2D** h_Zpt_RBal_btag_geng;
  TH2D** h_Zpt_Resp_btag_geng;
  TH2D** h_Zpt_RGMPF_btag_geng;
  TH2D** h_Zpt_RGenjet1_btag_geng;
  TH2D** h_Zpt_RGenjetn_btag_geng;
  TH2D** h_Zpt_RGenuncl_btag_geng;

  TH2D** h_Zpt_RBal_ctag_geng;
  TH2D** h_Zpt_Resp_ctag_geng;
  TH2D** h_Zpt_RGMPF_ctag_geng;
  TH2D** h_Zpt_RGenjet1_ctag_geng;
  TH2D** h_Zpt_RGenjetn_ctag_geng;
  TH2D** h_Zpt_RGenuncl_ctag_geng;

  TH2D** h_Zpt_RBal_gluontag_geng;
  TH2D** h_Zpt_Resp_gluontag_geng;
  TH2D** h_Zpt_RGMPF_gluontag_geng;
  TH2D** h_Zpt_RGenjet1_gluontag_geng;
  TH2D** h_Zpt_RGenjetn_gluontag_geng;
  TH2D** h_Zpt_RGenuncl_gluontag_geng;

  TH2D** h_Zpt_RBal_quarktag_geng;
  TH2D** h_Zpt_Resp_quarktag_geng;
  TH2D** h_Zpt_RGMPF_quarktag_geng;
  TH2D** h_Zpt_RGenjet1_quarktag_geng;
  TH2D** h_Zpt_RGenjetn_quarktag_geng;
  TH2D** h_Zpt_RGenuncl_quarktag_geng;

  TH2D** h_Zpt_RBal_notag_geng;
  TH2D** h_Zpt_Resp_notag_geng;
  TH2D** h_Zpt_RGMPF_notag_geng;
  TH2D** h_Zpt_RGenjet1_notag_geng;
  TH2D** h_Zpt_RGenjetn_notag_geng;
  TH2D** h_Zpt_RGenuncl_notag_geng;


  TH2D** h_Zpt_RBal_unclassified;
  TH2D** h_Zpt_Resp_unclassified;
  TH2D** h_Zpt_RGMPF_unclassified;
  TH2D** h_Zpt_RGenjet1_unclassified;
  TH2D** h_Zpt_RGenjetn_unclassified;
  TH2D** h_Zpt_RGenuncl_unclassified;

  TH2D** h_Zpt_RBal_btag_unclassified;
  TH2D** h_Zpt_Resp_btag_unclassified;
  TH2D** h_Zpt_RGMPF_btag_unclassified;
  TH2D** h_Zpt_RGenjet1_btag_unclassified;
  TH2D** h_Zpt_RGenjetn_btag_unclassified;
  TH2D** h_Zpt_RGenuncl_btag_unclassified;

  TH2D** h_Zpt_RBal_ctag_unclassified;
  TH2D** h_Zpt_Resp_ctag_unclassified;
  TH2D** h_Zpt_RGMPF_ctag_unclassified;
  TH2D** h_Zpt_RGenjet1_ctag_unclassified;
  TH2D** h_Zpt_RGenjetn_ctag_unclassified;
  TH2D** h_Zpt_RGenuncl_ctag_unclassified;

  TH2D** h_Zpt_RBal_gluontag_unclassified;
  TH2D** h_Zpt_Resp_gluontag_unclassified;
  TH2D** h_Zpt_RGMPF_gluontag_unclassified;
  TH2D** h_Zpt_RGenjet1_gluontag_unclassified;
  TH2D** h_Zpt_RGenjetn_gluontag_unclassified;
  TH2D** h_Zpt_RGenuncl_gluontag_unclassified;

  TH2D** h_Zpt_RBal_quarktag_unclassified;
  TH2D** h_Zpt_Resp_quarktag_unclassified;
  TH2D** h_Zpt_RGMPF_quarktag_unclassified;
  TH2D** h_Zpt_RGenjet1_quarktag_unclassified;
  TH2D** h_Zpt_RGenjetn_quarktag_unclassified;
  TH2D** h_Zpt_RGenuncl_quarktag_unclassified;

  TH2D** h_Zpt_RBal_notag_unclassified;
  TH2D** h_Zpt_Resp_notag_unclassified;
  TH2D** h_Zpt_RGMPF_notag_unclassified;
  TH2D** h_Zpt_RGenjet1_notag_unclassified;
  TH2D** h_Zpt_RGenjetn_notag_unclassified;
  TH2D** h_Zpt_RGenuncl_notag_unclassified;


  TH2D** h_Zpt_RZ_genb;
  TH2D** h_Zpt_RZ_genc;
  TH2D** h_Zpt_RZ_genuds;
  TH2D** h_Zpt_RZ_geng;
  TH2D** h_Zpt_RZ_unclassified;


  TH2D*  h_pTgen_R_template;

  TH2D** h_pTgen_Resp;
  TH2D** h_pTgen_Resp_btag;
  TH2D** h_pTgen_Resp_ctag;
  TH2D** h_pTgen_Resp_gluontag;
  TH2D** h_pTgen_Resp_quarktag;
  TH2D** h_pTgen_Resp_notag;
  
  TH2D** h_pTgen_Resp_genb;
  TH2D** h_pTgen_Resp_btag_genb;
  TH2D** h_pTgen_Resp_ctag_genb;
  TH2D** h_pTgen_Resp_gluontag_genb;
  TH2D** h_pTgen_Resp_quarktag_genb;
  TH2D** h_pTgen_Resp_notag_genb;

  TH2D** h_pTgen_Resp_genc;
  TH2D** h_pTgen_Resp_btag_genc;
  TH2D** h_pTgen_Resp_ctag_genc;
  TH2D** h_pTgen_Resp_gluontag_genc;
  TH2D** h_pTgen_Resp_quarktag_genc;
  TH2D** h_pTgen_Resp_notag_genc;

  TH2D** h_pTgen_Resp_genuds;
  TH2D** h_pTgen_Resp_btag_genuds;
  TH2D** h_pTgen_Resp_ctag_genuds;
  TH2D** h_pTgen_Resp_gluontag_genuds;
  TH2D** h_pTgen_Resp_quarktag_genuds;
  TH2D** h_pTgen_Resp_notag_genuds;

  TH2D** h_pTgen_Resp_geng;
  TH2D** h_pTgen_Resp_btag_geng;
  TH2D** h_pTgen_Resp_ctag_geng;
  TH2D** h_pTgen_Resp_gluontag_geng;
  TH2D** h_pTgen_Resp_quarktag_geng;
  TH2D** h_pTgen_Resp_notag_geng;

  TH2D** h_pTgen_Resp_unclassified;
  TH2D** h_pTgen_Resp_btag_unclassified;
  TH2D** h_pTgen_Resp_ctag_unclassified;
  TH2D** h_pTgen_Resp_gluontag_unclassified;
  TH2D** h_pTgen_Resp_quarktag_unclassified;
  TH2D** h_pTgen_Resp_notag_unclassified;


  Double_t* bins_alpha;
  size_t nbins_alpha;
  Double_t* bins_pt;
  size_t nbins_pt;

  std::vector<std::string> bins_eta;

  PickEvents* pickEvents;
};

#endif
