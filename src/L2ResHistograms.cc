#include "L2ResHistograms.h"
#include <iostream>
L2ResHistograms::L2ResHistograms(){}
L2ResHistograms::~L2ResHistograms(){}

void L2ResHistograms::book(){
  //double vxd[] = {0., 0.261, 0.522, 0.783, 1.044, 1.305, 1.479, 1.653, 1.93, 2.172, 2.322, 2.5, 2.65, 2.853, 2.964, 3.139, 3.314, 3.489, 3.839, 4.013, 4.583, 5.191};
  double vxd[] = {0, 0.087, 0.174, 0.261, 0.348, 0.435, 0.522, 0.609, 0.696, 0.783, 0.879, 0.957, 1.044, 1.131, 1.218, 1.305, 1.392, 1.479, 1.566, 1.653, 1.74, 1.83, 1.93, 2.043, 2.172, 2.322, 2.5, 2.65, 2.853, 2.964, 3.139, 3.314, 3.489, 3.664, 3.839, 4.013, 4.191, 4.363, 4.538, 4.716, 4.889, 5.191};
  const int nxd = sizeof(vxd)/sizeof(vxd[0])-1;

  double vptd[] = 
  //{59.,85.,104.,170.,236., 302., 370., 460., 575.}; // central
  //{86., 110., 132., 204., 279., 373.} // forward
    {15, 21, 28, 37, 49,
     59, 86, 110, 132, 170, 204, 236, 279, 302, 373, 460, 575,
     638, 737, 846, 967, 1101, 1248,
     1410, 1588, 1784, 2000, 2238, 2500, 2787, 3103};
  double nptd = sizeof(vptd)/sizeof(vptd[0])-1;

  // Counting of events, and JEC L2L3Res+JERSF for undoing
  h2pteta = new TH2D("h2pteta",";#eta;p_{T,avp} (GeV);"
		     "N_{events}",nxd,vxd, nptd, vptd);
  p2jes = new TProfile2D("p2jes",";#eta;p_{T,avp} (GeV);"
                         "JES(probe)/JES(tag)",
                         nxd,vxd, nptd, vptd);
  p2res = new TProfile2D("p2res",";#eta;p_{T,avp} (GeV);"
			 "JES(probe)/JES(tag)",
			 nxd,vxd, nptd, vptd);
  p2jsf = new TProfile2D("p2jsf",";#eta;p_{T,avp} (GeV);"
			 "JERSF(probe)/JERSF(tag)",
			 nxd,vxd, nptd, vptd);
  p2r = new TProfile2D("p2r",";#eta;p_{T,avp} (GeV);"
                         "JERSF(probe)/JERSF(tag)",
                         nxd,vxd, nptd, vptd);
  // MPF decomposition for HDM method
  p2m0 = new TProfile2D("p2m0",";#eta;p_{T,avp} (GeV);MPF0",
			nxd,vxd, nptd, vptd);
  p2m2 = new TProfile2D("p2m2",";#eta;p_{T,avp} (GeV);MPF2",
			nxd,vxd, nptd, vptd);
  p2mn = new TProfile2D("p2mn",";#eta;p_{T,avp} (GeV);MPFn",
			nxd,vxd, nptd, vptd);
  p2mu = new TProfile2D("p2mu",";#eta;p_{T,avp} (GeV);MPFu",
			nxd,vxd, nptd, vptd);
  p2m0x = new TProfile2D("p2m0x",";#eta;p_{T,avp} (GeV);"
			 "MPF0X (MPFX)",nxd,vxd, nptd, vptd, "S");
  p2m2x = new TProfile2D("p2m2x",";#eta;p_{T,avp} (GeV);"
			 "MPF2X (DBX)",nxd,vxd, nptd, vptd, "S");

  // Extra for FRS studies
  p2mnu = new TProfile2D("p2mnu",";#eta;p_{T,avp} (GeV);MPFnu",
			 nxd,vxd, nptd, vptd);
  p2mnx = new TProfile2D("p2mnx",";#eta;p_{T,avp} (GeV);"
			 "MPFNX",nxd,vxd, nptd, vptd, "S");
  p2mux = new TProfile2D("p2mux",";#eta;p_{T,avp} (GeV);"
			 "MPFUX",nxd,vxd, nptd, vptd, "S");
  p2mnux = new TProfile2D("p2mnux",";#eta;p_{T,avp} (GeV);"
			  "MPFNUX",nxd,vxd, nptd, vptd, "S");

  h2ptetatc = new TH2D("h2ptetatc",";#eta;p_{T,tag} (GeV);"
		       "N_{events}",nxd,vxd, nptd, vptd);
  p2restc = new TProfile2D("p2restc",";#eta;p_{T,tag} (GeV);"
			   "JES(probe)/JES(tag)",
			   nxd,vxd, nptd, vptd);
  p2jsftc = new TProfile2D("p2jsftc",";#eta;p_{T,tag} (GeV);"
			   "JERSF(probe)/JERSF(tag)",
			   nxd,vxd, nptd, vptd);
  p2m0tc = new TProfile2D("p2m0tc",";#eta;p_{T,tag} (GeV);MPF0",
			  nxd,vxd, nptd, vptd);
  p2m2tc = new TProfile2D("p2m2tc",";#eta;p_{T,tag} (GeV);MPF2",
			  nxd,vxd, nptd, vptd);
  p2mntc = new TProfile2D("p2mntc",";#eta;p_{T,tag} (GeV);MPFn",
			  nxd,vxd, nptd, vptd);
  p2mutc = new TProfile2D("p2mutc",";#eta;p_{T,tag} (GeV);MPFu",
			  nxd,vxd, nptd, vptd);

  h2ptetapf = new TH2D("h2ptetapf",";#eta;p_{T,probe} (GeV);"
		       "N_{events}",nxd,vxd, nptd, vptd);
  p2respf = new TProfile2D("p2respf",";#eta;p_{T,probe} (GeV);"
			   "JES(probe)/JES(tag)",
			   nxd,vxd, nptd, vptd);
  p2jsfpf = new TProfile2D("p2jsfpf",";#eta;p_{T,probe} (GeV);"
			   "JERSF(probe)/JERSF(tag)",
			   nxd,vxd, nptd, vptd);
  p2m0pf = new TProfile2D("p2m0pf",";#eta;p_{T,probe} (GeV);MPF0",
			  nxd,vxd, nptd, vptd);
  p2m2pf = new TProfile2D("p2m2pf",";#eta;p_{T,probe} (GeV);MPF2",
			  nxd,vxd, nptd, vptd);
  p2mnpf = new TProfile2D("p2mnpf",";#eta;p_{T,probe} (GeV);MPFn",
			  nxd,vxd, nptd, vptd);
  p2mupf = new TProfile2D("p2mupf",";#eta;p_{T,probe} (GeV);MPFu",
			  nxd,vxd, nptd, vptd);
}

void L2ResHistograms::fill(TLorentzVector& p4t, TLorentzVector& p4p, double& res, double& jes, double& jetSF, TLorentzVector& p4m0, TLorentzVector& p4m2, TLorentzVector& p4mn, TLorentzVector& p4mu, double& w){
  /*
  TLorentzVector p4m0, p4m2, p4mn, p4mu;
  p4m0.SetPtEtaPhiM(MET.Pt(),0,MET.Phi(),0); // type-I MET
  p4m2.SetPtEtaPhiM(MET1.Pt(),0,MET1.Phi(),0);
  p4mn.SetPtEtaPhiM(METn.Pt(),0,METn.Phi(),0);
  p4mu.SetPtEtaPhiM(METuncl.Pt(),0,METuncl.Phi(),0); // p4mu = p4m0 -p4m2 -p4mn; unclustered MET
  */
  double eta = p4p.Eta();
  //double res = Jet_RES[index];
  double jsf = jetSF; //Jet_pt[index]/Jet_ptorig[index];
  double abseta = fabs(eta);

  double pttag = p4t.Pt();
  double ptprobe = p4p.Pt();
  ////double ptave = 0.5*(pttag+ptprobe);

  TLorentzVector p4b,p4bt,p4bp;
  p4b.SetPtEtaPhiM(0,0,0,0);
  p4bt.SetPtEtaPhiM(1,0,p4t.Phi(),0);
  p4bp.SetPtEtaPhiM(1,0,p4p.Phi(),0);
  p4b += p4bt;
  p4b -= p4bp;
  p4b.SetPtEtaPhiM(p4b.Pt(),0.,p4b.Phi(),0.);
  p4b *= 1./p4b.Pt();
  TLorentzVector p4bx;
  p4bx.SetPtEtaPhiM(p4b.Pt(),0.,p4b.Phi()+0.5*TMath::Pi(),0.);
	  
  // Average projection pT to bisector axis, pT,avp
  // as explained in JME-21-001 (HDM method: bisector extension)
  double ptavp2 = 0.5*(p4t.Vect().Dot(p4b.Vect()) -
		       p4p.Vect().Dot(p4b.Vect()));
  double m0b = 1 + (p4m0.Vect().Dot(p4b.Vect()))/ptavp2;
  double m2b = 1 + (p4m2.Vect().Dot(p4b.Vect()))/ptavp2;
  double mnb = 0 + (p4mn.Vect().Dot(p4b.Vect()))/ptavp2;
  double mub = 0 + (p4mu.Vect().Dot(p4b.Vect()))/ptavp2;
  //double mob = 0 + (p4mo.Vect().Dot(p4b.Vect()))/ptavp;

  double m0bx = 1 + (p4m0.Vect().Dot(p4bx.Vect()))/ptavp2;
  double m2bx = 1 + (p4m2.Vect().Dot(p4bx.Vect()))/ptavp2;

  // Extras
  double cu = 1./0.92;
  double mnub = 0 + ((p4mn+cu*p4mu).Vect().Dot(p4b.Vect()))/ptavp2;
  double mnbx = 0 + (p4mn.Vect().Dot(p4bx.Vect()))/ptavp2;
  double mubx = 0 + (p4mu.Vect().Dot(p4bx.Vect()))/ptavp2;
  double mnubx = 0 + ((p4mn+cu*p4mu).Vect().Dot(p4bx.Vect()))/ptavp2;

  TLorentzVector p4d;
  p4d.SetPtEtaPhiM(0,0,0,0);
  p4d += p4t;
  p4d -= p4p;
  p4d.SetPtEtaPhiM(p4d.Pt(),0.,p4d.Phi(),0.);
  p4d *= 1./p4d.Pt();
  TLorentzVector p4dx;
  p4dx.SetPtEtaPhiM(p4d.Pt(),0.,p4d.Phi()+0.5*TMath::Pi(),0.);

  ////double m0d = 1 + (p4m0.Vect().Dot(p4d.Vect()))/ptave;
  ////double m2d = 1 + (p4m2.Vect().Dot(p4d.Vect()))/ptave;
  ////double mnd = 0 + (p4mn.Vect().Dot(p4d.Vect()))/ptave;
  ////double mud = 0 + (p4mu.Vect().Dot(p4d.Vect()))/ptave;
  //double mod = 0 + (p4mo.Vect().Dot(p4d.Vect()))/ptave;

  ////double m0dx = 1 + (p4m0.Vect().Dot(p4dx.Vect()))/ptave;
  ////double m2dx = 1 + (p4m2.Vect().Dot(p4dx.Vect()))/ptave;

  // central axis + pttag norm&binning
  TLorentzVector p4c;
  p4c.SetPtEtaPhiM(0,0,0,0);
  p4c += p4t;
  p4c.SetPtEtaPhiM(p4c.Pt(),0.,p4c.Phi(),0.);
  p4c *= 1./p4c.Pt();
  TLorentzVector p4cx;
  p4cx.SetPtEtaPhiM(p4c.Pt(),0.,p4c.Phi()+0.5*TMath::Pi(),0.);

  double m0c = 1 + (p4m0.Vect().Dot(p4c.Vect()))/pttag;
  double m2c = 1 + (p4m2.Vect().Dot(p4c.Vect()))/pttag;
  double mnc = 0 + (p4mn.Vect().Dot(p4c.Vect()))/pttag;
  double muc = 0 + (p4mu.Vect().Dot(p4c.Vect()))/pttag;
  //double moc = 0 + (p4mo.Vect().Dot(p4c.Vect()))/pttag;

  ////double m0cx = 1 + (p4m0.Vect().Dot(p4cx.Vect()))/pttag;
  ////double m2cx = 1 + (p4m2.Vect().Dot(p4cx.Vect()))/pttag;

  // forward axis ("backwards" to tag hemisphere) + ptprobe norm&binning
  TLorentzVector p4f;
  p4f.SetPtEtaPhiM(0,0,0,0);
  p4f -= p4p;
  p4f.SetPtEtaPhiM(p4f.Pt(),0.,p4f.Phi(),0.);
  p4f *= 1./p4f.Pt();
  TLorentzVector p4fx;
  p4fx.SetPtEtaPhiM(p4f.Pt(),0.,p4f.Phi()+0.5*TMath::Pi(),0.);

  double m0f = 1 + (p4m0.Vect().Dot(p4f.Vect()))/ptprobe;
  double m2f = 1 + (p4m2.Vect().Dot(p4f.Vect()))/ptprobe;
  double mnf = 0 + (p4mn.Vect().Dot(p4f.Vect()))/ptprobe;
  double muf = 0 + (p4mu.Vect().Dot(p4f.Vect()))/ptprobe;
  //double mof = 0 + (p4mo.Vect().Dot(p4f.Vect()))/ptprobe;

  ////double m0fx = 1 + (p4m0.Vect().Dot(p4fx.Vect()))/ptprobe;
  ////double m2fx = 1 + (p4m2.Vect().Dot(p4fx.Vect()))/ptprobe;


  // Filling
    
  h2pteta->Fill(abseta, pttag, w);
  p2jes->Fill(abseta, pttag, jes, w);
  p2res->Fill(abseta, pttag, res, w);
  p2jsf->Fill(abseta, pttag, jsf, w);
  p2r->Fill(abseta, pttag, p4p.Pt() / pttag, w);
  p2m0->Fill(abseta, pttag, m0b, w);
  p2m2->Fill(abseta, pttag, m2b, w);
  p2mn->Fill(abseta, pttag, mnb, w);
  p2mu->Fill(abseta, pttag, mub, w);
	      
  p2m0x->Fill(abseta, pttag, m0bx, w);
  p2m2x->Fill(abseta, pttag, m2bx, w);

  // Extras for FSR studies
  p2mnu->Fill(abseta, pttag, mnub, w);
  p2mnx->Fill(abseta, pttag, mnbx, w);
  p2mux->Fill(abseta, pttag, mubx, w);
  p2mnux->Fill(abseta, pttag, mnubx, w);

  h2ptetatc->Fill(abseta, pttag, w);
  p2restc->Fill(abseta, pttag, res, w);
  p2jsftc->Fill(abseta, pttag, jsf, w);
  p2m0tc->Fill(abseta, pttag, m0c, w);
  p2m2tc->Fill(abseta, pttag, m2c, w);
  p2mntc->Fill(abseta, pttag, mnc, w);
  p2mutc->Fill(abseta, pttag, muc, w);

  h2ptetapf->Fill(abseta, ptprobe, w);
  p2respf->Fill(abseta, ptprobe, res, w);
  p2jsfpf->Fill(abseta, ptprobe, jsf, w);
  p2m0pf->Fill(abseta, ptprobe, m0f, w);
  p2m2pf->Fill(abseta, ptprobe, m2f, w);
  p2mnpf->Fill(abseta, ptprobe, mnf, w);
  p2mupf->Fill(abseta, ptprobe, muf, w);

}

void L2ResHistograms::write(std::string filename){
  TFile* fOUT = TFile::Open(filename.c_str(),"RECREATE");
  fOUT->cd();
  write();
  fOUT->Close();
}
void L2ResHistograms::write(){
 
  h2pteta->Write();
  p2res->Write();
  p2jes->Write();
  p2r->Write();
  p2m0->Write();
  p2m2->Write();
  p2mn->Write();
  p2mu->Write();
  p2m0x->Write();
  p2m2x->Write();
  p2mnu->Write();
  p2mnx->Write();
  p2mux->Write();
  p2mnux->Write();
  h2ptetatc->Write();
  h2ptetapf->Write();
  p2restc->Write();
  p2m0tc->Write();
  p2m2tc->Write();
  p2mntc->Write();
  p2mutc->Write();
  p2respf->Write();
  p2m0pf->Write();
  p2m2pf->Write();
  p2mnpf->Write();
  p2mupf->Write();
  p2jsf->Write();
  p2jsftc->Write();
  p2jsfpf->Write();

}
