#ifndef PileupWeight_h
#define PileupWeight_h

#include "TH1D.h"

class PileupWeight {
 public:
  PileupWeight(TH1D*, TH1D*);
  ~PileupWeight();
  
  double getWeight(int pileup);
  void calculateWeights(TH1D*, TH1D*);

  TH1D* getPUData(){return h_data;}
  TH1D* getPUMC(){return h_mc;}
  TH1D* getPUWeightHistogram(){return h_weight;}

 private:
  TH1D *h_data;
  TH1D *h_mc;
  TH1D *h_weight;

  bool noData;
};
#endif
