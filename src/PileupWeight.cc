#include "PileupWeight.h"
#include <iostream>

PileupWeight::PileupWeight(TH1D* hData, TH1D* hMC){
  calculateWeights(hData,hMC);
}

PileupWeight::~PileupWeight() {}

double PileupWeight::getWeight(int pileup){
  if(h_weight == nullptr)
    std::cout << "PileupWeight enabled, but no PileupWeights in multicrab!" << std::endl;

  int bin = h_weight->GetXaxis()->FindBin( pileup );
  return h_weight->GetBinContent( bin );
}

void PileupWeight::calculateWeights(TH1D* hData, TH1D* hMC){
  h_data = hData;
  h_mc   = hMC;
  //std::cout << "check PileupWeight::calculateWeights " << hData << " " << hMC << std::endl;
  noData = false;
  if(h_data == nullptr)
    std::cout << "Did not find data pileup distributions" << std::endl;

  if(h_mc == nullptr)
    std::cout << "Did not find mc pileup distributions" << std::endl;
  /*
  std::cout << "original data pu" << std::endl;
  for (int i = 1; i < h_data->GetNbinsX()+2; ++i)
    std::cout << i << ":" << h_data->GetBinCenter(i) << " " << h_data->GetBinContent(i) << std::endl;
  */
  h_data->Scale(1.0/h_data->Integral());
  /*
  std::cout << "scaled data pu" << std::endl;
  for (int i = 1; i < h_data->GetNbinsX()+2; ++i)
    std::cout << i << ":" << h_data->GetBinCenter(i) << " " << h_data->GetBinContent(i) << std::endl;
  */
  h_mc->Scale(1.0/h_mc->Integral());

  if(h_data->Integral() < 0.99 || h_data->Integral() > 1.01) {
    std::cout << "Problem with data pileup " << h_data->Integral() << std::endl;
    exit(1);
  }
  if(h_mc->Integral() < 0.99 || h_mc->Integral() > 1.01) {
    std::cout << "Problem with mc pileup " << h_mc->Integral() << std::endl;
    exit(1);
  }
  //std::cout << "check data mc names " << h_data->GetName() << ", " << h_mc->GetName() << std::endl;
  //std::cout << "check data mc integral " << h_data->Integral() << ", " << h_mc->Integral() << std::endl;
  //std::cout << "check data mc nbins " << h_data->GetNbinsX() << ", " << h_mc->GetNbinsX() << std::endl;

  h_weight = (TH1D*)h_data->Clone("pileup_weights");
  h_weight->Divide(h_mc);
  //for (int i = 1; i < h_weight->GetNbinsX()+2; ++i)
  //  std::cout << i << ":" << h_weight->GetBinCenter(i) << " " << h_data->GetBinContent(i) << " " << h_mc->GetBinContent(i) << " " << h_weight->GetBinContent(i) << std::endl;
}
