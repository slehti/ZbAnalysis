// copied from https://github.com/miquork/dijet/blob/master/DijetHistosFill.C
#ifndef L2ResHistograms_h
#define L2ResHistograms_h

#include "TFile.h"
#include "TH2D.h"
#include "TProfile2D.h"
#include "TLorentzVector.h"
#include "TVector3.h"

#include <string>

class L2ResHistograms {
 public:
  L2ResHistograms();
  ~L2ResHistograms();

  void book();
  void fill(TLorentzVector&, TLorentzVector&, double&, double&, double&, TLorentzVector&, TLorentzVector&, TLorentzVector&, TLorentzVector&, double& w);
  void write(std::string filename);
  void write();

 private:
  TH2D *h2pteta;
  TProfile2D *p2jes, *p2r;
  TProfile2D *p2res, *p2m0, *p2m2, *p2mn, *p2mu;
  TProfile2D *p2m0x, *p2m2x;

  // Extra for FSR studies
  TProfile2D *p2mnu, *p2mnx, *p2mux, *p2mnux;
  TH2D *h2ptetatc, *h2ptetapf;
  TProfile2D *p2restc, *p2m0tc, *p2m2tc, *p2mntc, *p2mutc; // pT,tag (central)
  TProfile2D *p2respf, *p2m0pf, *p2m2pf, *p2mnpf, *p2mupf; // pT,probe (forward)

  // Smearing controls
  TProfile2D *p2jsf, *p2jsftc, *p2jsfpf;
};
#endif
