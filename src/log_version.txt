A brief summary of version changes to the ROOT output files for Mikko
-----------------------------------------------------------------------------------
Ver1: used whitelist = ["DoubleMuon_Run2018A","DY1JetsToLL_M_50"]
Ver2: run all datasets with whitelist = []
Ver3: updated binsy with a step of 0.02 for the range [-1.51,2.51] (old: a step of 0.05 for [-0.5,2.5])
Ver4: removed [0,2] windows for R_pT & R_MPF, extended upper range to 3.51 for binsy, and more bins for mZ histograms with 0.5 GeV bins in [70,120]
Ver5: updated muIso cone 0.3 -> 0.4, muEta max 2.3 -> 2.4, JESRMPFGen = -1 -> -10, and added hprof_pTGenJet_JESRpTGen with ylow = 0.03 and yup = 3.5
Ver6: added "if(genZboson.Pt() == 0) JESRpTGen = -10;" as JESRMPFGen = -10 already updated in Ver5 (hprof_pTGenJet_JESRpTGen with ylow = -1.5)
      added the selection criteria for UE study: Z_pT > 30, |Z_Eta| < 2.5, alpha < 0.3, phiBB cut, leadingJet.Pt() > 30 and |leadingJet.Eta()| < 1.3
      (note: subLeadingJet.Pt() > 15 and |subLeadingJet.Eta()| < 2.5)

[Done, Error] Ver7: updated bins_alpha by adding two additional bins of 0.001 and 0.5, and applied the latest JECs for 2016 (V11 -> V20) and 2018 (V13h -> V19)
 multicrab_ZbNanoAODAnalysis_v10214_Run2016BCDEFGH_20191125T1505_processed20200206T1956_Electron/DY1JetsToLL_M_50_Run2016/results/histograms.root probably not closed
 multicrab_ZbNanoAODAnalysis_v10214_Run2016BCDEFGH_20191125T1505_processed20200206T2007_Muon/DY1JetsToLL_M_50_Run2016/results/histograms.root probably not closed
 Note: The latest JECs for 2016 is V11. The code has been updated properly on Feb 26, 2020 (https://gitlab.cern.ch/minsuk/ZbAnalysis/)
 By the way, re-applying JEC is only for recalculating MET-typeI and it wasn't used in the current code.

[Preparing] Ver8: adding the JER smearning for MC
                  (note: use a method in jetsys at the moment, but recommend to use NanoAODTools, jmeCorrections with applyingSmearing)

The files are located in my cernbox (lxplus.cern.ch:/eos/home-m/minsuk/ZbAnalysis)
-----------------------------------------------------------------------------------
-rw-------. 1 minsuk zh  9158914 Jan 21 13:34 jme_bplusZ_Run2_Ver1.root
-rw-------. 1 minsuk zh  9456862 Jan 21 20:02 jme_bplusZ_Run2_Ver2.root
-rw-------. 1 minsuk zh  9414277 Jan 23 14:04 jme_bplusZ_Run2_Ver3.root
-rw-------. 1 minsuk zh  9467922 Jan 27 16:26 jme_bplusZ_Run2_Ver4.root
-rw-r--r--. 1 minsuk zh  8782715 Jan 28 13:43 jme_bplusZ_Run2_Ver4_Run2017.root
-rw-r--r--. 1 minsuk zh  8784033 Jan 29 17:08 jme_bplusZ_Run2_Ver5_Run2017.root
-rw-r--r--. 1 minsuk zh  9467019 Jan 29 21:07 jme_bplusZ_Run2_Ver5.root
-rw-r--r--. 1 minsuk zh 12324585 Jan 31 23:37 jme_bplusZ_Run2_Ver5_update.root
-rw-r--r--. 1 minsuk zh 12324361 Feb  2 01:36 jme_bplusZ_Run2_Ver6.root

Note: jme_bplusZ_Run2_Ver*_Run2017.root from running plot.py with Run2017 only.
      This is just for test purpose because cross section weights were mixed up for 2016 and 2017 (and applied for 2018) in plot.py.

Note2: jme_bplusZ_RUn2_Ver6.root (jme_bplusZ_Run2_Ver5_update.root) has the directory of profile_Run2017.
       It contains the 2017 MC profiles of RMPFx per true flavor (per flavor region), with cross section weights.

Update in Ver6: removed the line zero in the y-axis in h_pTGenJet_JESRpTGen by JESRpTGen = R_pT/R_pTGen = 0 when R_pT = 0 (here, R_pT = leadingJet.Pt()/Zboson.Pt() and R_pTGen = leadingGenJet.Pt()/genZboson.Pt()) => if genZboson.Pt()==0, then R_pTGen = inf, resulting in JESRpTGen = 0
