#!/usr/bin/env python

import sys
import os
import re
import subprocess

def sort(filenames,n):
    subgroups = {}
    fn_re = re.compile("\S+\.root") #jet_response_gluontag_alpha25_eta_13_19_PSWeight2.root
    i = 0
    nGroup = 1
    for f in filenames:
        match = fn_re.search(f)
        if match:
            if i == 0:
                subgroups["group%s"%nGroup] = []
            subgroups["group%s"%nGroup].append(f)

            if i < n:
                i += 1
            else:                
                nGroup += 1
                i = 0
        else:
            print("Not accepting file",f)
    return subgroups

def execute(cmd):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
    (s_in, s_out) = (p.stdin, p.stdout)

    f = s_out
    ret=[]
    for line in f:
        line = str(line,'utf-8')
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

def main():

    path = os.getcwd()
    if len(sys.argv) > 1:
        path = sys.argv[1]
    
    filenames = execute("ls %s"%path)
    filepath = []
    for f in filenames:
        filepath.append(os.path.join(path,f))
    

    fngroups = sort(filepath,500)
    for k in fngroups.keys():
        fp = os.path.join(path,"tmp_%s.root"%k)
        cmd = "hadd %s"%fp
        for f in fngroups[k]:
            cmd += " "+f
        print(cmd)
        os.system(cmd)

    final_cmd = "hadd %s"%os.path.join(path,"jme_bplusZ_merged_vX.root")
    for k in fngroups.keys():
        fp = os.path.join(path,"tmp_%s.root"%k)
        final_cmd += " %s"%fp
    os.system(final_cmd)

    print("Merged jme_bplusZ_merged_vX.root")
    
if __name__=="__main__":
    main()
